
/*! \file P2PAVCore.h


\copyright Copyright (c) 2010 by Throughtek Co., Ltd. All Rights Reserved.

Revision Table

Version     | Name             |Date           |Description
------------|------------------|---------------|-------------------
0.1.0.0     |Joshua            |2013-02-18     |Trial Version
0.1.0.1     |Joshua
		Fix: DoRecvVideo,nRecvSize=avRecvFrameData 将收到的fps,kbps信息nRecvSize>0回馈
0.1.0.4		|Joshua			   |
		Fix: In IOTYPE_USER_IPCAM_GET_FLOWINFO_RESP unit of collect_interval is s
0.1.0.5		|Joshua			   |2013-04-26
		+  #define GOI_FrameTotal			 6 //*out_pObj is tkINT32 *
		+  #define GIO_FrameLoseIncomplete 7 //*out_pObj is tkINT32 *


*/
#ifndef _P2PAVCore_H_
#define _P2PAVCore_H_

#include "P2PAVCoreType.h"

#ifdef P2PAVCORE_EXPORTS
#define P2PAVCORE_API __declspec(dllexport)
#else
#define P2PAVCORE_API __declspec(dllimport)
#endif

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef struct __st_SInfo
{
	unsigned char Mode; //!< 0: P2P mode, 1: Relay mode, 2: LAN mode
	char CorD; //!< 0: As a Client, 1: As a Device
	char UID[21]; //!< The UID of the device
	char RemoteIP[17]; //!< The IP address of remote site used during this IOTC session
	unsigned short RemotePort; //!< The port number of remote site used during this IOTC session
	unsigned long TX_Packetcount; //!< The total packets sent from the device and the client during this IOTC session
	unsigned long RX_Packetcount; //!< The total packets received in the device and the client during this IOTC session
	unsigned long IOTCVersion; //!< The IOTC version 
	unsigned short VID; //!< The Vendor ID, part of VPG mechanism
	unsigned short PID; //!< The Product ID, part of VPG mechanism
	unsigned short GID; //!< The Group ID, part of VPG mechanism
	unsigned char NatType; //!< The remote NAT type
	unsigned char isSecure; //!< 0: The IOTC session is in non-secure mode, 1: The IOTC session is in secure mode
}CORE_STSInfo;

/**
* \details Device serch info, containing all the information
* when client searches devices in LAN. 
*/
typedef struct __st_LanSearchInfo
{
	char UID[21]; //!< The UID of discoveried device
	char IP[16]; //!< The IP address of discoveried device
	unsigned short port; //!< The port number of discoveried device used for IOTC session connection
	char Reserved; //!< Reserved, no use
}CORE_STLanSearchInfo;


#define GOI_OnlineNum		1	//*out_pObj is tkUCHAR *
#define GOI_ConnectMode		2	//*out_pObj is tkUCHAR *
#define GOI_FrameRate		3	//*out_pObj is tkFLOAT *
#define GOI_BytesRate		4	//*out_pObj is tkFLOAT *
#define GOI_SessionInfo		5	//*out_pObj is CORE_STSInfo *
#define GOI_FrameTotal		6	//*out_pObj is tkINT32 *
#define GIO_FrameLoseIncomplete 7 //*out_pObj is tkINT32 *

//----------
#define TYPE_STATUS_INFO			0
#define TYPE_STATUS_ERRCODE			1

#define SINFO_FAIL_CREATE_THREAD	-1000

#define SINFO_CONNECTING			1
#define SINFO_CONNECTED				2
#define SINFO_DISCONNECTED			3
#define SINFO_FPS_ETC_READY			4


typedef tkVOID (WINAPI *cbOnAVFrame)(tkCHAR *pData, tkINT32 nSize, tkVOID *pFrmInfo, tkINT32 nFrmInfoSize, tkUINT32 nTag, tkVOID *param);
typedef tkVOID (WINAPI *cbOnStatus)(tkINT32 nType, tkINT32 nValue, tkHandle pObj,  tkUINT32 nTag, tkVOID *param);
typedef tkVOID (WINAPI *cbOnRecvIOCtrl)(tkINT32 nIOType, tkCHAR *pIOData, tkINT32 nIODataSize, tkHandle pObj, tkUINT32 nTag, tkVOID *param);

P2PAVCORE_API tkVOID	apiGetIOTCVer(tkUINT32 *pArrayVer, tkUINT32 nArrMaxSize); //arrIndex 0=IOTCAPI ver; arrIndex 1=AVAPI ver;
P2PAVCORE_API tkUINT32  apiGetVersion();
P2PAVCORE_API tkHandle  apiCreateObj(tkUINT32 nTag, tkCHAR *szUID, tkCHAR *szUser, tkCHAR *szPwd);
P2PAVCORE_API tkVOID	apiReleaseObj(tkHandle *ppObj);
P2PAVCORE_API tkVOID    apiSetConnInfo(tkHandle pObj, tkUINT32 nTag, tkCHAR *szUID, tkCHAR *szUser, tkCHAR *szPwd);
P2PAVCORE_API tkINT32	apiConnect(tkHandle pObj);
P2PAVCORE_API tkINT32	apiDisconnect(tkHandle pObj, tkCHAR bForceStop);

P2PAVCORE_API tkVOID	apiRegCB_OnAVFrame(tkHandle pObj,cbOnAVFrame cbFun,	tkPVOID lParam);
P2PAVCORE_API tkVOID	apiRegCB_OnStatus(tkHandle pObj,cbOnStatus cbFun,	tkPVOID lParam);
P2PAVCORE_API tkVOID	apiRegCB_OnRecvIOCtrl(tkHandle pObj,	cbOnRecvIOCtrl cbFun, tkPVOID lParam);

P2PAVCORE_API tkINT32	apiStartVideo(tkHandle pObj);
P2PAVCORE_API tkVOID	apiStopVideo(tkHandle pObj);
P2PAVCORE_API tkINT32	apiStartAudio(tkHandle pObj);
P2PAVCORE_API tkVOID	apiStopAudio(tkHandle pObj);
P2PAVCORE_API tkINT32	apiStartSpeak(tkHandle pObj, tkINT32 nCodeID_audio);
P2PAVCORE_API tkVOID	apiStopSpeak(tkHandle pObj);
P2PAVCORE_API tkINT32	apiSendIOCtrl(tkHandle pObj, tkINT32 nIOType, tkUCHAR *pIOCtrlData, tkUINT32 nDataSize);

P2PAVCORE_API tkCHAR*	apiGetUID(tkHandle pObj);
P2PAVCORE_API tkVOID	apiGetObjInfo(tkHandle pObj, tkUINT32 nGOIType, tkVOID *out_pObj);
/**
* \brief Used by search devices in LAN.
*
* \details When clients and devices are stay in a LAN environment, client can call this function
*			to discovery devices and connect it directly.
*
* \param psLanSearchInfo [in] The array of struct st_LanSearchInfo to store search result
*
* \param nArrayLen [in] The size of the psLanSearchInfo array
*
* \param nWaitTimeMs [in] The timeout in milliseconds before discovery process end.
*
* \return IOTC_ER_NoERROR if search devices in LAN successfully
* \return Error code if return value < 0
*			- #IOTC_ER_INVALID_ARG The arguments passed in to this function is invalid. 
*			- #IOTC_ER_FAIL_CREATE_SOCKET Fails to create sockets
*			- #IOTC_ER_FAIL_SOCKET_OPT Fails to set up socket options
*			- #IOTC_ER_FAIL_SOCKET_BIND Fails to bind sockets 
*/
P2PAVCORE_API int apiGetLanObj(CORE_STLanSearchInfo *psLanSearchInfo, int nArrayLen, int nWaitTimeMs);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif