
#ifndef _err_IOTC_H_
#define _err_IOTC_H_

/** IOTC servers have no response, probably caused by many types of Internet connection issues. */
// See [Troubleshooting](..\Troubleshooting\index.htm#IOTC_ER_SERVER_NOT_RESPONSE)
#define	IOTC_ER_SERVER_NOT_RESPONSE					-1

/** IOTC masters cannot be resolved their domain name, probably caused
 * by network connection or DNS setting issues. */
#define	IOTC_ER_FAIL_RESOLVE_HOSTNAME				-2

/** IOTC module is already initialized. It is not necessary to re-initialize. */
#define IOTC_ER_ALREADY_INITIALIZED                 -3

/** IOTC module fails to create Mutexs when doing initialization. Please
 * check if OS has sufficient Mutexs for IOTC platform. */
#define IOTC_ER_FAIL_CREATE_MUTEX                   -4

/** IOTC module fails to create threads. Please check if OS has ability
 * to create threads for IOTC module. */
#define IOTC_ER_FAIL_CREATE_THREAD                  -5

/** IOTC module fails to create sockets. Please check if OS supports socket service */
#define IOTC_ER_FAIL_CREATE_SOCKET                  -6

/** IOTC module fails to set up socket options. */
#define IOTC_ER_FAIL_SOCKET_OPT                     -7

/** IOTC module fails to bind sockets */
#define IOTC_ER_FAIL_SOCKET_BIND                    -8

/** The specified UID is not licensed or expired. */
#define IOTC_ER_UNLICENSE                           -10

/** The device is already under login process currently
 * so it is prohibited to invoke login again at this moment. */
#define IOTC_ER_LOGIN_ALREADY_CALLED                -11

/** IOTC module is not initialized yet. Please use IOTC_Initialize() or
 * IOTC_Initialize2() for initialization. */
#define IOTC_ER_NOT_INITIALIZED                     -12

/** The specified timeout has expired during the execution of some IOTC
 * module service. For most cases, it is caused by slow response of remote
 * site or network connection issues */
#define IOTC_ER_TIMEOUT                             -13

/** The specified IOTC session ID is not valid */
#define IOTC_ER_INVALID_SID                         -14

/** The specified device's name is unknown to the IOTC servers */
#define IOTC_ER_UNKNOWN_DEVICE                      -15

/** IOTC module fails to get the local IP address */
#define IOTC_ER_FAIL_GET_LOCAL_IP                   -16

/** The device already start to listen for connections from clients. It is
 * not necessary to listen again. */
#define IOTC_ER_LISTEN_ALREADY_CALLED               -17

/** The number of IOTC sessions has reached maximum.
 * To increase the max number of IOTC sessions, please use IOTC_Set_Max_Session_Number()
 * before initializing IOTC module. */
#define IOTC_ER_EXCEED_MAX_SESSION                  -18

/** IOTC servers cannot locate the specified device, probably caused by
 * disconnection from the device or that device does not login yet. */
#define IOTC_ER_CAN_NOT_FIND_DEVICE                 -19

/** The client is already connecting to a device currently
 * so it is prohibited to invoke connection again at this moment. */
#define IOTC_ER_CONNECT_IS_CALLING                  -20

/** The remote site already closes this IOTC session.
 * Please call IOTC_Session_Close() to release IOTC session resource in locate site. */
#define IOTC_ER_SESSION_CLOSE_BY_REMOTE             -22

/** This IOTC session is disconnected because remote site has no any response
 * after a specified timeout expires, i.e. #IOTC_SESSION_ALIVE_TIMEOUT */
#define IOTC_ER_REMOTE_TIMEOUT_DISCONNECT           -23

/** The client fails to connect to a device because the device is not listening for connections. */
#define IOTC_ER_DEVICE_NOT_LISTENING                -24

/** The IOTC channel of specified channel ID is not turned on before transferring data. */
#define IOTC_ER_CH_NOT_ON                           -26

/** A client stops connecting to a device by calling IOTC_Connect_Stop() */
#define IOTC_ER_FAIL_CONNECT_SEARCH                 -27

/** Too few masters are specified when initializing IOTC module.
 * Two masters are required for initialization at minimum. */
#define IOTC_ER_MASTER_TOO_FEW                      -28

/** A client fails to pass certification of a device due to incorrect key. */
#define IOTC_ER_AES_CERTIFY_FAIL                    -29

/** The number of IOTC channels for a IOTC session has reached maximum, say, #MAX_CHANNEL_NUMBER */
#define IOTC_ER_SESSION_NO_FREE_CHANNEL             -31

/** Cannot connect to masters neither UDP mode nor TCP mode by IP or host name ways */
#define IOTC_ER_TCP_TRAVEL_FAILED					-32

/** Cannot connect to IOTC servers in TCP */
#define IOTC_ER_TCP_CONNECT_TO_SERVER_FAILED        -33

/** A client wants to connect to a device in non-secure mode while that device
 * supports secure mode only. */
#define IOTC_ER_CLIENT_NOT_SECURE_MODE              -34

/** A client wants to connect to a device in secure mode while that device does
 * not support secure mode. */
#define IOTC_ER_CLIENT_SECURE_MODE					-35

/** A device does not support connection in secure mode */
#define IOTC_ER_DEVICE_NOT_SECURE_MODE              -36

/** A device does not support connection in non-secure mode */
#define IOTC_ER_DEVICE_SECURE_MODE					-37

/** The IOTC session mode specified in IOTC_Listen2(), IOTC_Connect_ByUID2()
 * or IOTC_Connect_ByName2() is not valid.
 * Please see #IOTCSessionMode for possible modes. */
#define IOTC_ER_INVALID_MODE                        -38

/** A device stops listening for connections from clients. */
#define IOTC_ER_EXIT_LISTEN                         -39

/** The specified device does not support TCP relay */
#define IOTC_ER_NO_TCP_SUPPORT                      -40

/** Network is unreachable, please check the network settings */
#define	IOTC_ER_NETWORK_UNREACHABLE     			-41

/** A client fails to connect to a device via relay mode */
#define IOTC_ER_FAIL_SETUP_RELAY					-42

/** A client fails to use UDP relay mode to connect to a device
 * because UDP relay mode is not supported for that device by IOTC servers */
#define IOTC_ER_NOT_SUPPORT_RELAY					-43



/** The passed-in arguments for the function are incorrect */
#define		AV_ER_INVALID_ARG					-20000

/** The buffer to receive is too small to store */
#define		AV_ER_BUFPARA_MAXSIZE_INSUFF		-20001

/** The number of AV channels has reached maximum.
 * The maximum number of AV channels is determined by the passed-in
 * argument of avInitialize() */
#define		AV_ER_EXCEED_MAX_CHANNEL			-20002

/** Insufficient memory for allocation */
#define		AV_ER_MEM_INSUFF					-20003

/** AV fails to create threads. Please check if OS has ability to create threads for AV. */
#define		AV_ER_FAIL_CREATE_THREAD			-20004

/** A warning error code to indicate that the sending queue of video frame of an AV server
 * is almost full, probably caused by slow handling of an AV client or network
 * issue. Please note that this is just a warning, the video frame is actually
 * put in the queue. */
#define		AV_ER_EXCEED_MAX_ALARM				-20005

/** The frame to be sent exceeds the currently remaining video frame buffer.
 * The maximum of video frame buffer is controlled by avServSetMaxBufSize() */
#define		AV_ER_EXCEED_MAX_SIZE				-20006

/** The specified AV server has no response */
#define		AV_ER_SERV_NO_RESPONSE				-20007

/** An AV client does not call avClientStart() yet */
#define		AV_ER_CLIENT_NO_AVLOGIN				-20008

/** The client fails in authentication due to incorrect view account or password */
#define		AV_ER_WRONG_VIEWACCorPWD			-20009

/** The IOTC session of specified AV channel is not valid */
#define		AV_ER_INVALID_SID					-20010

/** The specified timeout has expired during some operation */
#define		AV_ER_TIMEOUT						-20011

/** The data is not ready for receiving yet. */
#define		AV_ER_DATA_NOREADY					-20012

/** Some parts of a frame are lost during receiving */
#define		AV_ER_INCOMPLETE_FRAME				-20013

/** The whole frame is lost during receiving */
#define		AV_ER_LOSED_THIS_FRAME				-20014

/** The remote site already closes the IOTC session.
 * Please call IOTC_Session_Close() to release local IOTC session resource */
#define		AV_ER_SESSION_CLOSE_BY_REMOTE		-20015

/** This IOTC session is disconnected because remote site has no any response
 * after a specified timeout expires. */
#define		AV_ER_REMOTE_TIMEOUT_DISCONNECT		-20016

/** The AV server start process is terminated by avServExit() */
#define		AV_ER_SERVER_EXIT		    		-20017

/** The AV client start process is terminated by avClientExit() */
#define		AV_ER_CLIENT_EXIT		    		-20018

/** AV module has not been initialized */
#define		AV_ER_NOT_INITIALIZED	    		-20019

/** By design, an AV client cannot send frame and audio data to an AV server */
#define		AV_ER_CLIENT_NOT_SUPPORT	   		-20020

/** The AV channel of specified AV channel ID is already in sending IO control process */
#define		AV_ER_SENDIOCTRL_ALREADY_CALLED	   	-20021

/** The sending IO control process is terminated by avSendIOCtrlExit() */
#define		AV_ER_SENDIOCTRL_EXIT		    	-20022

#endif
