#pragma once


// CDlgSysLoginPW 对话框

class CDlgSysLoginPW : public CDialog
{
	DECLARE_DYNAMIC(CDlgSysLoginPW)

public:
	CDlgSysLoginPW(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CDlgSysLoginPW();

// 对话框数据
	enum { IDD = DLG_SYS_LOGINPW };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	virtual BOOL OnInitDialog();
};
