// DlgDevInfo.cpp : 实现文件
//

#include "stdafx.h"
#include "P2PCamLiveForPC.h"
#include "P2PCamLiveForPCView.h"
#include "DlgDevInfo.h"
#include "TimeTableUI.h"
#include "DlgDevSchRec.h"
#include "DlgRemoteSettings.h"
#include "DlgRemoteInfo.h"
#include "P2PAVCore.h"
#include "DevInfo.h"
#include "DlgFlash.h"


// CDlgDevInfo 对话框
IMPLEMENT_DYNAMIC(CDlgDevInfo, CDialog)

CDlgDevInfo::CDlgDevInfo(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgDevInfo::IDD, pParent)
{
	m_bAddDev=TRUE;
	m_pCurDevInfo=NULL;
}

CDlgDevInfo::~CDlgDevInfo()
{
}

void CDlgDevInfo::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST2, m_ctrlList);
}


BEGIN_MESSAGE_MAP(CDlgDevInfo, CDialog)
	ON_BN_CLICKED(IDC_BTN3, &CDlgDevInfo::OnBnClickedBtn3)
	ON_BN_CLICKED(IDC_BTN4, &CDlgDevInfo::OnBnClickedBtn4)
	ON_BN_CLICKED(IDC_BTN1, &CDlgDevInfo::OnBnClickedBtn1)
	ON_BN_CLICKED(IDC_BTN2, &CDlgDevInfo::OnBnClickedBtn2)
	ON_BN_CLICKED(IDC_BTN5, &CDlgDevInfo::OnBnClickedBtn5)
	ON_BN_CLICKED(IDC_SEARCH, &CDlgDevInfo::OnBnClickedSearch)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST2, &CDlgDevInfo::OnNMDblclkList2)
END_MESSAGE_MAP()


// CDlgDevInfo 消息处理程序
void CDlgDevInfo::OnBnClickedBtn3()
{
	CDlgDevSchRec dlg;
	if(m_pCurDevInfo) {
		dlg.m_csWeekPlan[0]=m_pCurDevInfo->GetRecPlan(0);
		dlg.m_csWeekPlan[1]=m_pCurDevInfo->GetRecPlan(1);
		dlg.m_csWeekPlan[2]=m_pCurDevInfo->GetRecPlan(2);
		dlg.m_csWeekPlan[3]=m_pCurDevInfo->GetRecPlan(3);
		dlg.m_csWeekPlan[4]=m_pCurDevInfo->GetRecPlan(4);
		dlg.m_csWeekPlan[5]=m_pCurDevInfo->GetRecPlan(5);
		dlg.m_csWeekPlan[6]=m_pCurDevInfo->GetRecPlan(6);
	}
	if(IDOK==dlg.DoModal()){
		if(m_pCurDevInfo){
			m_pCurDevInfo->SetRecPlan(
				dlg.m_csWeekPlan[0],
				dlg.m_csWeekPlan[1],
				dlg.m_csWeekPlan[2],
				dlg.m_csWeekPlan[3],
				dlg.m_csWeekPlan[4],
				dlg.m_csWeekPlan[5],
				dlg.m_csWeekPlan[6]);

			bool bErr=false;
			CString csDBFile, csSQL;
			csDBFile.Format(_T("%scamera.db"), g_csAppPath);
			try{
				g_SQLite3DB.open(csDBFile);
				csSQL.Format(_T("update device_info set record_plan_w0='%s',record_plan_w1='%s',record_plan_w2='%s', \
								record_plan_w3='%s',record_plan_w4='%s',record_plan_w5='%s',record_plan_w6='%s' \
								where dev_uid='%s'"), dlg.m_csWeekPlan[0], dlg.m_csWeekPlan[1], dlg.m_csWeekPlan[2], 
								dlg.m_csWeekPlan[3], dlg.m_csWeekPlan[4], dlg.m_csWeekPlan[5], dlg.m_csWeekPlan[6],
								m_pCurDevInfo->GetUID());
				g_SQLite3DB.execDML(csSQL);
			}catch(...){ bErr=true; }
			g_SQLite3DB.close();
		}
	}
}

void CDlgDevInfo::OnBnClickedBtn4()
{
	if(!m_pCurDevInfo->IsConnected()){
		m_pCurDevInfo->ConnectDev(CDevInfo::DEV_TYPE_LOCAL, FALSE);
		CDlgFlash dlgFlash(_T("Connecting this device..."), 5);
		dlgFlash.DoModal();
		int nRet=ReqRemoteSettings();
		Sleep(3000);

		if(nRet<-1){
			CDlgFlash dlgFlash(_T("Failed to connect this device. Please try again."));
			dlgFlash.DoModal();
			return;
		}
	}

	CDlgRemoteSettings dlg;
	dlg.m_pCurDevInfo=m_pCurDevInfo;
	dlg.DoModal();
}

int CDlgDevInfo::ReqRemoteSettings()
{
	if(m_pCurDevInfo==NULL) return -1;
	m_pCurDevInfo->ResetRemoteValue();
	
	int nRet=0;
	tkUCHAR req[8]={0};
	memset(req,0,sizeof(req));
	nRet=m_pCurDevInfo->SendIOCtrl(IOTYPE_USER_IPCAM_GETSTREAMCTRL_REQ,	req, sizeof(req));
	if(nRet>=0) nRet=m_pCurDevInfo->SendIOCtrl(IOTYPE_USER_IPCAM_GET_VIDEOMODE_REQ,	req, sizeof(req));
	if(nRet>=0) nRet=m_pCurDevInfo->SendIOCtrl(IOTYPE_USER_IPCAM_GET_ENVIRONMENT_REQ,req, sizeof(req));
	if(nRet>=0) nRet=m_pCurDevInfo->SendIOCtrl(IOTYPE_USER_IPCAM_GETMOTIONDETECT_REQ,req, sizeof(req));
	if(nRet>=0) nRet=m_pCurDevInfo->SendIOCtrl(IOTYPE_USER_IPCAM_GETRECORD_REQ, req, sizeof(req));
	if(nRet>=0) nRet=m_pCurDevInfo->SendIOCtrl(IOTYPE_USER_IPCAM_LISTWIFIAP_REQ,req, 4);
	if(nRet>=0) nRet=m_pCurDevInfo->SendIOCtrl(IOTYPE_USER_IPCAM_DEVINFO_REQ, req, 4);

	return nRet;
}

BOOL CDlgDevInfo::CheckBeforeSave()
{
	CString csText, csNote;
	csNote.LoadString(MB_TITLE_INFO);
	GetDlgItemText(IDC_EDIT1, csText);
	if(csText.IsEmpty()){
		csText.LoadString(DEVINFO_NAME_EMPTY);
		MessageBox(csText, csNote, MB_ICONINFORMATION | MB_OK);
		return FALSE;
	}
	GetDlgItemText(IDC_EDIT2, csText);
	if(csText.IsEmpty()){
		csText.LoadString(DEVINFO_UID_EMPTY);
		MessageBox(csText, csNote, MB_ICONINFORMATION | MB_OK);
		return FALSE;
	}
// 	GetDlgItemText(IDC_EDIT3, csText);
// 	if(csText.IsEmpty()){
// 		csText.LoadString(DEVINFO_PWD_EMPTY);
// 		MessageBox(csText, csNote, MB_ICONINFORMATION | MB_OK);
// 		return FALSE;
// 	}
	return TRUE;
}

void CDlgDevInfo::OnBnClickedBtn1()//save
{
	CString csDBFile, csSQL;
	CString csDevName, csUID, csPasswd;
	
	if(!CheckBeforeSave()) return;	

	csDBFile.Format(_T("%scamera.db"), g_csAppPath);
	GetDlgItemText(IDC_EDIT1, csDevName);
	GetDlgItemText(IDC_EDIT2, csUID);
	GetDlgItemText(IDC_EDIT3, csPasswd);

	bool bErr=false;
	if(m_bAddDev){
		try{
			bool bExist=false;
			g_SQLite3DB.open(csDBFile);

			csSQL.Format(_T("select dev_uid from device_info where dev_uid = '%s'"), csUID);
			CppSQLite3Query SQLiteQuery = g_SQLite3DB.execQuery(csSQL);
			if(!SQLiteQuery.eof()) bExist=true;
			SQLiteQuery.finalize();			
			if(bExist){
				CString csIDSstr, csIDSstrTitle;
				csIDSstrTitle.LoadString(MB_TITLE_INFO);
				csIDSstr.LoadString(DEVINFO_EXISTED);
				MessageBox(csIDSstr, csIDSstrTitle, MB_ICONINFORMATION | MB_OK);
				g_SQLite3DB.close();
				return;
			}
			csSQL.Format(_T("insert into device_info(dev_name,dev_uid,view_acc,view_pwd) values('%s','%s','admin','%s')"), csDevName, csUID, csPasswd);
			g_SQLite3DB.execDML(csSQL);
		}catch(...){ bErr=true; }
		g_SQLite3DB.close();

		if(!bErr){
			CP2PCamLiveForPCView *pView=CP2PCamLiveForPCApp::GetActiveView();
			CDevInfo *pDevInfo=pView->m_objVWndMgr.GetEmptyDevInfo();

			pDevInfo->SetDevInfo(csDevName, csUID, _T("admin"), csPasswd);
			m_pCurDevInfo=pDevInfo;
		}
	}else{
		try{
			g_SQLite3DB.open(csDBFile);
			csSQL.Format(_T("update device_info set dev_name='%s',view_pwd='%s' where dev_uid='%s'"), csDevName, csPasswd, csUID);
			g_SQLite3DB.execDML(csSQL);
		}catch(...){ bErr=true; }
		g_SQLite3DB.close();
		
		if(!bErr && m_pCurDevInfo!=NULL) m_pCurDevInfo->SetDevInfo(csDevName, csUID, _T("admin"), csPasswd);
	}

	CDialog::OnOK();
}

void CDlgDevInfo::OnBnClickedBtn2()
{
	CDialog::OnCancel();
}

void CDlgDevInfo::OnBnClickedBtn5()
{
	if(!m_pCurDevInfo->IsConnected()){
		m_pCurDevInfo->ConnectDev(CDevInfo::DEV_TYPE_LOCAL, FALSE);
		
		CDlgFlash dlgFlash(_T("Connecting this device..."));
		dlgFlash.DoModal();
		int nRet=ReqRemoteSettings();
		
		if(nRet<-1){
			CDlgFlash dlgFlash(_T("Failed to connect this device. Please try again."));
			dlgFlash.DoModal();
			return;
		}
	}

	CDlgRemoteInfo dlg;
	dlg.m_pCurDevInfo=m_pCurDevInfo;
	dlg.DoModal();
}

void CDlgDevInfo::OnBnClickedSearch()
{
	#define MAX_NUM_SEARCH	32
	CORE_STLanSearchInfo lanObj[MAX_NUM_SEARCH];
	CString csText, csTmp, csFilenameToSearch,csSearch(_T(""));
	int i=0;
	int nNum=apiGetLanObj(lanObj, MAX_NUM_SEARCH, 2000);
	CStdioFile objFile;
	BOOL bOpen=FALSE;

	m_ctrlList.DeleteAllItems();
	csFilenameToSearch.Format(_T("%sDevsSearched.txt"), g_csAppPath);
	bOpen=objFile.Open(csFilenameToSearch, CFile::modeCreate|CFile::modeReadWrite|CFile::typeText);
	for(i=0; i<nNum; i++){
		//TRACE("  %d: UID=%s, IP=%s, udpPort=%d\n", i, lanObj[i].UID, lanObj[i].IP, lanObj[i].port);
		csText.Format(_T("%02d"), i+1);
		m_ctrlList.InsertItem(i, csText);
		csSearch.Append(csText);
		csSearch.Append(_T(";"));

		BSTRFromCStrU(CP_ACP, lanObj[i].UID, csTmp);
		csText.Format(_T("%s"), csTmp);
		m_ctrlList.SetItemText(i, 1, csText);
		csSearch.Append(csText);
		csSearch.Append(_T(";"));

		BSTRFromCStrU(CP_ACP, lanObj[i].IP, csTmp);
		csText.Format(_T("%s"), csTmp);
		m_ctrlList.SetItemText(i, 2, csText);
		csSearch.Append(csText);
		csSearch.Append(_T(";"));

		csText.Format(_T("%d"), lanObj[i].port);
		m_ctrlList.SetItemText(i, 3, csText);
		csSearch.Append(csText);
		csSearch.Append(_T(";\n"));

		objFile.WriteString(csSearch);
		csSearch=_T("");
	}
	if(bOpen) objFile.Close();
}

void CDlgDevInfo::FillDevSearchedToListCtrl()
{
	//fill data to listctrl
	BOOL bOpen=FALSE;
	CString csFilenameToSearch;
	CStdioFile objFile;
	csFilenameToSearch.Format(_T("%sDevsSearched.txt"), g_csAppPath);
	bOpen=objFile.Open(csFilenameToSearch, CFile::modeRead|CFile::typeText);
	if(bOpen){
		CString csRow;
		CStringArray arrCols;
		int i=0;
		while(objFile.ReadString(csRow))
		{
			SplitString2(csRow, arrCols, ';');
			if(arrCols.GetCount()<4) continue;
			i=0;
			m_ctrlList.InsertItem(i, arrCols[0]);
			m_ctrlList.SetItemText(i, 1, arrCols[1]);
			m_ctrlList.SetItemText(i, 2, arrCols[2]);
			m_ctrlList.SetItemText(i, 3, arrCols[3]);
		}
	}
	if(bOpen) objFile.Close();
}

BOOL CDlgDevInfo::OnInitDialog()
{
	CDialog::OnInitDialog();
	CString csText;
	if(m_bAddDev) csText.LoadString(WND_TITLE_ADD_DEV);
	else csText.LoadString(WND_TITLE_MODIFY_DEV);
	SetWindowText(csText);

	//initialize listctrl head
	int iInitCol=0;
	CString csCols;
	CStringArray arrCols;
	csCols.LoadString(IDS_SEARCH_COLS);
	SplitString2(csCols, arrCols, ';');
	if(arrCols.GetCount()<4) {
		m_ctrlList.InsertColumn(iInitCol++, _T("No"),  LVCFMT_LEFT, 40);		//0=No
		m_ctrlList.InsertColumn(iInitCol++, _T("UID"), LVCFMT_LEFT, 160);		//1=UID
		m_ctrlList.InsertColumn(iInitCol++, _T("IP"),  LVCFMT_LEFT, 100);		//2=IP
		m_ctrlList.InsertColumn(iInitCol++, _T("UDP Port"), LVCFMT_LEFT, 80);	//3=UDP port
	}else{
		m_ctrlList.InsertColumn(iInitCol++, arrCols[0],  LVCFMT_LEFT, 40);	//0=No
		m_ctrlList.InsertColumn(iInitCol++, arrCols[1], LVCFMT_LEFT, 160);	//1=UID
		m_ctrlList.InsertColumn(iInitCol++, arrCols[2],  LVCFMT_LEFT, 100);	//2=IP
		m_ctrlList.InsertColumn(iInitCol++, arrCols[3], LVCFMT_LEFT, 80);	//3=UDP port
	}
	m_ctrlList.SetExtendedStyle(LVS_EX_FULLROWSELECT);

	FillDevSearchedToListCtrl();
	if(m_bAddDev){
		CP2PCamLiveForPCApp::CtrlEnable(m_hWnd, IDC_BTN3, FALSE);
		CP2PCamLiveForPCApp::CtrlEnable(m_hWnd, IDC_BTN4, FALSE);
		CP2PCamLiveForPCApp::CtrlEnable(m_hWnd, IDC_BTN5, FALSE);
	}else {
		CP2PCamLiveForPCApp::CtrlEnable(m_hWnd, IDC_EDIT2, FALSE);
		if(m_pCurDevInfo){
			SetDlgItemText(IDC_EDIT1, m_pCurDevInfo->GetDevName());
			SetDlgItemText(IDC_EDIT2, m_pCurDevInfo->GetUID());
			SetDlgItemText(IDC_EDIT3, m_pCurDevInfo->GetViewPasswd());
		}		
	}

	ReqRemoteSettings();
	return TRUE;
}

void CDlgDevInfo::OnNMDblclkList2(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	int nRow=pNMItemActivate->iItem;
	if(nRow>=0 && m_bAddDev){
		SetDlgItemText(IDC_EDIT2, m_ctrlList.GetItemText(nRow,1));
	}
	*pResult = 0;
}
