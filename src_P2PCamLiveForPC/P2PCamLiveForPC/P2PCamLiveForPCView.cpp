// P2PCamLiveForPCView.cpp : implementation of the CP2PCamLiveForPCView class
//

#include "stdafx.h"
#include "P2PCamLiveForPC.h"

#include "P2PCamLiveForPCDoc.h"
#include "P2PCamLiveForPCView.h"
#include "MainFrm.h"
#include "FullScreenHandler.h"
#include "DevInfo.h"
#include "DlgPTZ.h"
#include "DlgFlash.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CP2PCamLiveForPCView
IMPLEMENT_DYNCREATE(CP2PCamLiveForPCView, CView)

BEGIN_MESSAGE_MAP(CP2PCamLiveForPCView, CView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
END_MESSAGE_MAP()

// CP2PCamLiveForPCView construction/destruction

CP2PCamLiveForPCView::CP2PCamLiveForPCView():m_objVWndMgr(this)
{
	
}

CP2PCamLiveForPCView::~CP2PCamLiveForPCView()
{
}

BOOL CP2PCamLiveForPCView::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style|=WS_CLIPSIBLINGS|WS_CLIPCHILDREN;
	return CView::PreCreateWindow(cs);
}


BOOL CP2PCamLiveForPCView::OnEraseBkgnd(CDC* pDC)
{
	return TRUE;
}

void CP2PCamLiveForPCView::OnDraw(CDC* pDC)
{
	m_objVWndMgr.DrawVWnd(pDC);
}


// CP2PCamLiveForPCView diagnostics

#ifdef _DEBUG
void CP2PCamLiveForPCView::AssertValid() const
{
	CView::AssertValid();
}

void CP2PCamLiveForPCView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CP2PCamLiveForPCDoc* CP2PCamLiveForPCView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CP2PCamLiveForPCDoc)));
	return (CP2PCamLiveForPCDoc*)m_pDocument;
}
#endif //_DEBUG


// CP2PCamLiveForPCView message handlers
int CP2PCamLiveForPCView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if(CView::OnCreate(lpCreateStruct) == -1) return -1;

	CVideoWnd *pVideoWnd=NULL;
	CPtrArray *pPtrArray=m_objVWndMgr.GetVWnds();
	for(int i=0; i<MAX_CHN_NUM; i++) {
		pVideoWnd=(CVideoWnd *)(*pPtrArray)[i];
		pVideoWnd->OnCreate();
	}

	return 0;
}

void CP2PCamLiveForPCView::OnInitialUpdate()
{
	CView::OnInitialUpdate();
	
	CMainFrame *pMainWnd=(CMainFrame *)AfxGetMainWnd();
	pMainWnd->m_dlgCtrl1.PostMessage(OM_FILL_DEV_INFO, 0, 0);
}

void CP2PCamLiveForPCView::OnSize(UINT nType, int cx, int cy)
{
	CView::OnSize(nType, cx, cy);
	if(cx>0 && cy>0){
		CRect rect;	GetClientRect(&rect);
		m_objVWndMgr.SetWholeWndSize(&rect);
	}
}


void CP2PCamLiveForPCView::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	//1. get video window hited
	short nWndIndex=m_objVWndMgr.GetHitWndIndexBy(point);
	CVideoWnd *pVideoWnd=m_objVWndMgr.GetVideoWndBy(nWndIndex);
	if(!pVideoWnd) return;

	//2. frame maximize
	CMainFrame *pMainWnd=(CMainFrame *)AfxGetMainWnd();
	if(g_objFullScrnHandle.InFullScreenMode()) {
		pMainWnd->m_DockPageBar.ShowWindow(SW_SHOW);
		g_objFullScrnHandle.Restore(pMainWnd);
	}else {
		pMainWnd->m_DockPageBar.ShowWindow(SW_HIDE);
		g_objFullScrnHandle.Maximize(pMainWnd, this);
	}

	//3. view maximize
	short nWndIndexFullScrn=m_objVWndMgr.GetWndIndexFullScrn();
	if(nWndIndexFullScrn==-1){ //fullscreen
		m_objVWndMgr.SetWndIndexFullScrn(nWndIndex);
		m_objVWndMgr.ChgOtherWndSize(1, nWndIndex);
		pVideoWnd->CreateRgn();
		TRACE("Maximize. nWndIndexFullScrn==-1\n");
	}else{
		if(nWndIndex==nWndIndexFullScrn){ //restore
			m_objVWndMgr.ChgOtherWndSize(0, nWndIndex);
			m_objVWndMgr.SetWndIndexFullScrn(-1);
			//TRACE("Restore. nWndIndex==nWndIndexFullScrn\n");
		}else{				//fullscreen
			m_objVWndMgr.SetWndIndexFullScrn(nWndIndex);
			m_objVWndMgr.ChgOtherWndSize(1, nWndIndex);
			pVideoWnd->CreateRgn();
			//TRACE("Maximize. nWndIndex!=nWndIndexFullScrn\n");
		}
	}

	//CView::OnLButtonDblClk(nFlags, point);
}

void CP2PCamLiveForPCView::StopAllAudio()
{
	CDevInfo  *pDevInfo=NULL;
	CPtrArray *ptrDevInfo=m_objVWndMgr.GetDevInfos();
	for(int i=0; i<MAX_CHN_NUM; i++)
	{
		pDevInfo=(CDevInfo  *)(*ptrDevInfo)[i];
		if(pDevInfo->IsStartAudio()) pDevInfo->StopAudio();
	}
}

void CP2PCamLiveForPCView::StopAllSpeak()
{
	CDevInfo  *pDevInfo=NULL;
	CPtrArray *ptrDevInfo=m_objVWndMgr.GetDevInfos();
	for(int i=0; i<MAX_CHN_NUM; i++)
	{
		pDevInfo=(CDevInfo  *)(*ptrDevInfo)[i];
		if(pDevInfo->IsStartSpeak()) pDevInfo->StopSpeak();
	}
}

void CP2PCamLiveForPCView::OnLButtonDown(UINT nFlags, CPoint point)
{
	short nWndIndex=m_objVWndMgr.GetHitWndIndexBy(point);
	if(nWndIndex<0) return;

	CVideoWnd *pVideo=m_objVWndMgr.GetVideoWndBy(nWndIndex);
	BOOL bFocus=pVideo->IsFocus();

	short nWndIndexMax=m_objVWndMgr.GetWndIndexFullScrn();
	if(nWndIndexMax!=-1 && nWndIndexMax==nWndIndex);
	else{ //if nWndIndex isn't maximize
		if(m_objVWndMgr.LButtonDown(nFlags, point, nWndIndex)!=nWndIndex){
// 			if(g_nCtrlPanelIndex==1){
// 				CMainFrame *pMainWnd=(CMainFrame *)AfxGetMainWnd();
// 				pMainWnd->m_dlgCtrl2.SendMessage(OM_DEVSELED_INVIEW, (WPARAM)nWndIndex, (LPARAM)pVideo);
// 			}
		}

		CDevInfo *pDevInfo=NULL;
		pDevInfo=m_objVWndMgr.GetDevInfoBy(nWndIndex);
		if(pDevInfo && pDevInfo->IsStartConnect()){
			short nClickBtn=-1;
			nClickBtn=pVideo->CheckClickVideBtn(nFlags, point);
			if(nClickBtn==2){ //disconnect dev
				TRACE(_T("OnLButtonDown 2\n"));
				pDevInfo->StopVideo();
				pDevInfo->StopAudio();
				pDevInfo->StopSpeak();
				pDevInfo->DisconnectDev();

			}else if(nClickBtn==3){		//capture
				TRACE(_T("OnLButtonDown 3\n"));
				CString csText;
				if(pDevInfo->m_pVideoWnd->Snapshot()==0){
					csText.LoadString(IDS_SNAPSHOT_OK);
					CDlgFlash dlg(csText);
					dlg.DoModal();
				}else{
					csText.LoadString(IDS_SNAPSHOT_FAIL);
					CDlgFlash dlg(csText);
					dlg.DoModal();
				}

			}else if (nClickBtn==4){	//sound
				TRACE(_T("OnLButtonDown 4\n"));
				BOOL bStarted=pDevInfo->IsStartAudio();
				StopAllAudio();
				if(!bStarted){
					pDevInfo->StartAudio();
					pVideo->IconSetAudio(TRUE);
				}else pVideo->IconSetAudio(FALSE);

			}else if (nClickBtn==5){	//talk
				TRACE(_T("OnLButtonDown 5\n"));
				BOOL bStarted=pDevInfo->IsStartSpeak();
				StopAllSpeak();
				if(!bStarted) {
					pDevInfo->StartSpeak();
					pVideo->IconSetSpeak(TRUE);
				}else pVideo->IconSetSpeak(FALSE);
			}

			if(nClickBtn<0 && bFocus){
				CDlgPTZ dlg;
				dlg.m_pDevInfo=pDevInfo;
				dlg.DoModal();
			}
		}
	}	
	CView::OnLButtonDown(nFlags, point);
}

void CP2PCamLiveForPCView::OnRButtonDown(UINT nFlags, CPoint point)
{
	TRACE(_T("OnRButtonDown\n"));
	short nWndIndex=m_objVWndMgr.GetHitWndIndexBy(point);
	if(nWndIndex<0) return;
	TRACE(_T("OnRButtonDown, nWndIndex=%d\n"), nWndIndex);

	CView::OnRButtonDown(nFlags, point);
}

BOOL CP2PCamLiveForPCView::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message==WM_KEYDOWN){
		if(pMsg->wParam==VK_ESCAPE){
			if(g_objFullScrnHandle.InFullScreenMode()){
				CPoint point(pMsg->pt);
				OnLButtonDblClk(MK_LBUTTON, point);
			}			
		}
	}
	return CView::PreTranslateMessage(pMsg);
}

