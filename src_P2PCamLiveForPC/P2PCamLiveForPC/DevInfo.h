#pragma once
#include "afx.h"
#include "P2PAVCore.h"
#include "avioctrldefs.h"


class CVideoWnd;
class CDevInfo : public CObject
{
public:
	typedef enum _DEV_TYPE { DEV_TYPE_LOCAL, DEV_TYPE_INTERNET } DEV_TYPE_E;

	CDevInfo(UINT32 nTag, CVideoWnd *pVideoWnd, LPCTSTR lpDevName=NULL, LPCTSTR lpUID=NULL, LPCTSTR lpViewAcc=NULL, LPCTSTR lpViewPasswd=NULL);
	virtual ~CDevInfo(void);

	void SetDevInfo(LPCTSTR lpDevName, LPCTSTR lpUID, LPCTSTR lpViewAcc, LPCTSTR lpViewPasswd);
	void SetFunCallBack();

	BOOL IsEmpty();

	CString GetDevName()   { return m_csDevName;	}
	CString GetUID()	   { return m_csUID;		}
	CString GetViewAcc()   { return m_csViewAcc;	}
	CString GetViewPasswd(){ return m_csViewPasswd;	}
	void	SetRecPlan(LPCTSTR w0, LPCTSTR w1,LPCTSTR w2,LPCTSTR w3,LPCTSTR w4,LPCTSTR w5,LPCTSTR w6);
	CString GetRecPlan(int nWeekIndex);
	tkHandle GetP2PHandle(){ return m_handleP2P;	}
	
	void SetFlagConnected(BOOL bConnected) { m_bConnected=bConnected;}
	
	BOOL IsStartConnect()	{ return m_bStartedConnect;	}
	BOOL IsStartAudio()		{ return m_bStartedAudio;	}
	BOOL IsStartSpeak()		{ return m_bStartedSpeak;	}

	BOOL IsConnected()		{ return m_bConnected;		}
	BOOL IsAndStartVideo()	{ return m_bAndStartVideo;	}
	DEV_TYPE_E GetDevType() { return m_eDevType;		}
	void ResetRemoteValue();

	int  ConnectDev(DEV_TYPE_E eDevType, BOOL bAndStartVideo);
	void DisconnectDev();
	int  StartVideo();
	void StopVideo();
	int  StartAudio();
	void StopAudio();
	int  StartSpeak();
	void StopSpeak();

	int SendIOCtrl(tkINT32 nIOType, tkUCHAR *pIOCtrlData, tkUINT32 nDataSize);

public:
	CVideoWnd *m_pVideoWnd;
	volatile SWifiAp m_wifiAP[32];
	SMsgAVIoctrlDeviceInfoResp m_devInfo;
	volatile int    m_nWifiApNum;
	volatile int	m_nVideoQuality;
	volatile int    m_nVideoFlip;
	volatile int    m_nEnvMode;
	volatile int    m_nMotionDet;
	volatile int    m_nRecMode;

protected:
	UINT32 m_nTag;
	void   *m_handleP2P;
	DEV_TYPE_E m_eDevType;

	volatile BOOL   m_bStartedConnect;
	volatile BOOL   m_bStartedAudio;
	volatile BOOL   m_bStartedSpeak;

	volatile BOOL   m_bConnected;
	volatile BOOL	m_bAndStartVideo;

	CString m_csDevName, m_csUID, m_csViewAcc, m_csViewPasswd;
	CString m_rec_plan_w0, m_rec_plan_w1, m_rec_plan_w2,
			m_rec_plan_w3, m_rec_plan_w4, m_rec_plan_w5, m_rec_plan_w6;
};
