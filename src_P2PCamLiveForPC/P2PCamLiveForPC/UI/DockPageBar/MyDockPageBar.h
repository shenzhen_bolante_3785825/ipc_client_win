// MyDockPageBar.h: interface for the CMyDockPageBar class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MYDOCKPAGEBAR_H__D70839DC_F1F9_4F75_AFAC_1FEC50196830__INCLUDED_)
#define AFX_MYDOCKPAGEBAR_H__D70839DC_F1F9_4F75_AFAC_1FEC50196830__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DockPageBar.h"

class CMyDockPageBar : public CDockPageBar  
{
public:
	CMyDockPageBar();
	virtual ~CMyDockPageBar();

	using CDockPageBar::AddPage;
	CObject * AddPage(CRuntimeClass* pClass,UINT nIDTemplate, LPCTSTR sText, UINT IconID);
	

protected:
	//{{AFX_MSG(CMyDockPageBar)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnWindowPosChanged(WINDOWPOS* lpwndpos);	
	//}}AFX_MSG
	
protected:
	BOOL m_bDisaBtnClose;

	DECLARE_MESSAGE_MAP()
};

#endif // !defined(AFX_MYDOCKPAGEBAR_H__D70839DC_F1F9_4F75_AFAC_1FEC50196830__INCLUDED_)
