// MyDockPageBar.cpp: implementation of the CMyDockPageBar class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MyDockPageBar.h"
#include "Resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMyDockPageBar::CMyDockPageBar()
{
	m_bDisaBtnClose=FALSE;
}

CMyDockPageBar::~CMyDockPageBar()
{

}

BEGIN_MESSAGE_MAP(CMyDockPageBar, CDockPageBar)
	//{{AFX_MSG_MAP(CMyDockPageBar)
	ON_WM_CREATE()
	ON_WM_WINDOWPOSCHANGED()	
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

int CMyDockPageBar::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDockPageBar::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	return 0;
}

CObject* CMyDockPageBar::AddPage(CRuntimeClass *pClass, UINT nIDTemplate, LPCTSTR sText, UINT IconID)
{
	CDialog *pDlg = (CDialog*)pClass->CreateObject();
	if(pDlg != NULL)
	{
		if(pDlg->Create(nIDTemplate,this))
		{
			if(AddPage(pDlg, sText, IconID)) return pDlg;
			else{
				delete pDlg;
				return NULL;
			}
		}
	}
	
	return NULL;
}

void CMyDockPageBar::OnWindowPosChanged(WINDOWPOS* lpwndpos)
{
	CDockPageBar::OnWindowPosChanged(lpwndpos);

	//should only be called once, when floated.
	if(IsFloating()){
		if(m_pDockBar && !m_bDisaBtnClose)
		{
			CWnd* pParent =m_pDockBar->GetParent();
			if(pParent->IsKindOf(RUNTIME_CLASS(CMiniFrameWnd)))
			{
						//remove close button of toolbar
				//pParent->ModifyStyle(WS_SYSMENU,0,0);

						//disable close button of toolbar
				CMenu* pMenu = pParent->GetSystemMenu(FALSE);
				pMenu->ModifyMenu(SC_CLOSE, MF_BYCOMMAND | MF_GRAYED);

				m_bDisaBtnClose=TRUE;
			}
		}
	}else if(m_bDisaBtnClose) {
		m_bDisaBtnClose= FALSE;
	}
}


