
//Fun:	分离用division分隔的字符串
//Para: source:	用division分隔的字符串
//		dest:	分离后的字符串数组
//		division:分隔符
//author: zrju
//
void SplitString(CString source, CStringArray &dest, char division)
{
	if(source.IsEmpty()) return;
	int iLenSource=source.GetLength();
	TCHAR c=source.GetAt(iLenSource-1);
	if(c!=division) source+=CString(division);
	int iSize=0, i=0, j=0;

	dest.RemoveAll();
	for(i=0; i<source.GetLength(); i++){
		if(source.GetAt(i)==division){
			dest.Add(source.Left(i));
			iSize=dest.GetSize()-1;			
			for(j=0; j<iSize; j++){
				dest[iSize]=dest[iSize].Right(dest[iSize].GetLength()-
											  dest[j].GetLength()-1);
			}
		}//if(source.GetAt(i)==division)
	}
}