// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__76F4DC14_A4F9_487A_8B89_174DC3481B7A__INCLUDED_)
#define AFX_STDAFX_H__76F4DC14_A4F9_487A_8B89_174DC3481B7A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afx.h>
#include <afxwin.h>
#include <afxcmn.h>			// MFC support for Windows Common Controls

#include "stdafx.hpp"

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__76F4DC14_A4F9_487A_8B89_174DC3481B7A__INCLUDED_)
