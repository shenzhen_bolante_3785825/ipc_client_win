// DevModifyPasswd.cpp : 实现文件
//

#include "stdafx.h"
#include "P2PCamLiveForPC.h"
#include "DevModifyPasswd.h"

// CDevModifyPasswd 对话框
IMPLEMENT_DYNAMIC(CDevModifyPasswd, CDialog)

CDevModifyPasswd::CDevModifyPasswd(CWnd* pParent /*=NULL*/)
	: CDialog(CDevModifyPasswd::IDD, pParent)
{
	m_csOldPwd	=_T("");
	m_csNewPwd	=_T("");
	m_csConfirmPwd=_T("");
}

CDevModifyPasswd::~CDevModifyPasswd()
{
}

void CDevModifyPasswd::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDevModifyPasswd, CDialog)
	ON_BN_CLICKED(IDC_BTN1, &CDevModifyPasswd::OnBnClickedBtn1)
	ON_BN_CLICKED(IDC_BTN2, &CDevModifyPasswd::OnBnClickedBtn2)
END_MESSAGE_MAP()


// CDevModifyPasswd 消息处理程序
void CDevModifyPasswd::OnBnClickedBtn1()
{
	
	CDialog::OnOK();
}

void CDevModifyPasswd::OnBnClickedBtn2()
{
	CDialog::OnCancel();
}
