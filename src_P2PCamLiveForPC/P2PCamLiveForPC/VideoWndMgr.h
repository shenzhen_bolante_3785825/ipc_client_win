// VideoWndMgr.h: interface for the CVideoWndMgr class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_VIDEOWNDMGR_H__7AA62C00_297D_473B_B8D3_60E2C5C77675__INCLUDED_)
#define AFX_VIDEOWNDMGR_H__7AA62C00_297D_473B_B8D3_60E2C5C77675__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "VideoWnd.h"
#include "P2PAVCore.h"


#define WNDSPACING	1


typedef struct {	
	int nTag;
	int nInfoCode;
	tkPVOID pObj;
	CString *pStatus;
}ST_STATUS;

typedef struct {
	tkINT32 nTag;
	tkINT32 nIOType;
	tkPVOID pObj;
	tkINT32 nIODataSize;
	tkCHAR  ioData[1024];
}ST_QUEUE_SENDIO;


class CP2PCamLiveForPCView;
class CDevInfo;

class CVideoWndMgr : public CObject
{
public:
	typedef enum{ MODE1=4, MODE2=6, MODE3=9, MODE4=16,} ENUM_MODE;

	CVideoWndMgr(CP2PCamLiveForPCView *pParent, ENUM_MODE nVWndMode=MODE2);
	virtual ~CVideoWndMgr();	

protected:
	CP2PCamLiveForPCView *m_pParent;

	COLORREF m_crBackground;
	CBrush	 m_wndbkBrush;			// background brush
	CBrush	 m_wndbkBrushUnusedChan;	//to flip page

	CRect		m_rectWholeWnd;
	ENUM_MODE	m_nVWndMode;
	int			m_nCurPageNo;			//base on 0; to flip page
	short		m_nIndexFullScrn;		//-1: without fullscreen window
	CRgn		m_rgnExcessArea;
	CRgn		m_rgnDontUseChannelArea;//to flip page

	CPtrArray	m_ptrVWnds;				//array of CVideoWnd *
	CPtrArray	m_ptrDevInfo;			//array of CDevInfo *
	CPtrList m_ptrRecvedIOCtrl;
	BOOL     m_bThreadExit;

	CRITICAL_SECTION m_critical;
	UINT		m_nTimerID_SendIOCtrl;
	PVOID		m_hHandleIOCtrl;

	void Code2Str(int nCode, CString &csInfo);
	tkVOID AppendIOCtrl(tkINT32 nIOType, tkCHAR *pIOData, tkINT32 nIODataSize, tkPVOID pObj, tkUINT32 nTag);

	static tkVOID WINAPI OnAVFrame(tkCHAR *pData, tkINT32 nSize, tkVOID *pFrmInfo, tkINT32 nFrmInfoSize, tkUINT32 nTag, tkVOID *param);
	static tkVOID WINAPI OnStatus(tkINT32 nType, tkINT32 nValue, tkPVOID pObj, tkUINT32 nTag, tkVOID *param);
	static tkVOID WINAPI OnRecvIOCtrl(tkINT32 nIOType, tkCHAR *pIOData, tkINT32 nIODataSize, tkPVOID pObj, tkUINT32 nTag, tkVOID *param);
	static tkULONG WINAPI ThreadHandleRecvIOCtrl(tkPVOID lpPara);
	tkINT32 DoHandleRecvIOCtrl();

public:
	void SetMode(ENUM_MODE nVWndMode);
	void SetWholeWndSize(LPRECT rectWholeWnd);
	void GetExcessRgn(CRgn &rgnExcessArea);
	void CalcuMode1();
	void CalcuMode2();
	void CalcuMode3();
	void CalcuMode4();

	void  KillFocusAllWnd();
	void  SetFocusBy(CPoint point);
	short SetFocusBy(short nCurWndIndex, bool bDoTalkSound);
	short GetFocusWndIndex(); //this index is subscript of m_ptrVWnds	
	short GetHitWndIndexBy(CPoint point);
	CVideoWnd *GetVideoWndBy(CPoint point);
	CVideoWnd *GetVideoWndBy(short nWndIndex); //nWndIndex is subscript of array
	CVideoWnd *GetVideoWndByFixChn(short nRealChnNo);

	void  SetWndIndexFullScrn(short nWndNoFullScrn) { m_nIndexFullScrn=nWndNoFullScrn;	}
	short GetWndIndexFullScrn()						{ return m_nIndexFullScrn;			}
	void  ChgOtherWndSize(short nOpType, short nWndIndexExcluded);

	void  StretchVWnd(short nWndIndex); //nWndIndex=-1: all video window
	short LButtonDown(UINT nFlags, CPoint point, short nCurWndIndex);
	void  DrawVWnd(CDC *pDC);	
	CPtrArray *GetVWnds()							{ return &m_ptrVWnds;				}

	CVideoWnd *GetIdleWndIndex(short &nWndIndex);
	CVideoWnd *GetVideoWndFocused();
	CVideoWnd *AllocVideoWndBy(IN short nWndIndex);
	CVideoWnd *AutoAllocVideoWnd(OUT short &out_nWndIndex);
	void      SetIsDrawMDData(int nRealChn, BOOL bValue);
	BOOL	  GetIsDrawMDData(int nRealChn, BOOL *bValue); //return: whether success;
	void VideoWndRectNull(ENUM_MODE nVWndMode, int nCurPageNo);

	void testLoadJpgFile();
	int  RandInThreshold();
	
	//===================================================================================
	void ReadDevInfosFromDB();
	CDevInfo  *GetEmptyDevInfo();
	CDevInfo  *GetDevInfoBy(UINT index){
		if(index<MAX_CHN_NUM) return (CDevInfo  *)m_ptrDevInfo[index];
		else return NULL;
	}
	CPtrArray *GetDevInfos()						{ return &m_ptrDevInfo;				}

};

#endif // !defined(AFX_VIDEOWNDMGR_H__7AA62C00_297D_473B_B8D3_60E2C5C77675__INCLUDED_)
