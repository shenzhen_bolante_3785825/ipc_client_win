#pragma once
#include "afx.h"

#define OPT_MIN_TIME_RECONN				2
#define OPT_MAX_TIME_RECONN				65536

#define OPT_MIN_TIMES_RECONN			3
#define OPT_MAX_TIMES_RECONN			60

#define OPT_MIN_ALARM_SOUNDTIMELEN		5
#define OPT_MAX_ALARM_SOUNDTIMELEN		60

#define OPT_MIN_ALACFREQ				1
#define OPT_MAX_ALACFREQ				3
#define OPT_MIN_RECORD_TIMELEN			10
#define OPT_MAX_RECORD_TIMELEN			30

#define CTRLBIT_AUTO_CONNECT			0x01
#define CTRLBIT_ENABLE_SEND_EMAIL		0x02
#define CTRLBIT_CONN_CAMS_WHEN_START	0x04
#define CTRLBIT_VIDEO_STRETCH			0x08
#define CTRLBIT_CLOSE_STATUS			0x10
#define CTRLBIT_CONFIRM_WHEN_APP_CLOSE	0x20


class CSysSettings : public CObject
{
public:
	CSysSettings(void);
	virtual ~CSysSettings(void);

	void Reset();
public:
	SYSTEMTIME timeLastOP;		//16
	SYSTEMTIME timeLastStart;	//Login date; 16

	int m_ctrl_bit;
	int m_reconn_time;
	int m_reconn_times;
	CString m_sound_file_path;
	int m_sound_duration;
	CString m_snap_rec_file_path;
	CString m_web_site;
	int m_web_port;
	CString m_mail_subject;
	CString m_mail_body;
	CString m_mail_host;
	CString m_mail_to;
	CString m_mail_account;
	CString m_mail_passwd;

public:
	BOOL IsAutoConnect()		 { return ((m_ctrl_bit & CTRLBIT_AUTO_CONNECT) ? TRUE : FALSE);			}
	BOOL IsEnableSendEmail()	 { return ((m_ctrl_bit & CTRLBIT_ENABLE_SEND_EMAIL) ? TRUE : FALSE);	}
	BOOL IsConnAllCamsWhenStart(){ return ((m_ctrl_bit & CTRLBIT_CONN_CAMS_WHEN_START) ? TRUE : FALSE); }
	BOOL IsVideoStretch()		 { return ((m_ctrl_bit & CTRLBIT_VIDEO_STRETCH) ? TRUE : FALSE);		}
	BOOL IsCloseStatus()		 { return ((m_ctrl_bit & CTRLBIT_CLOSE_STATUS) ? TRUE : FALSE);			}
	BOOL IsConfirmWhenAppClose() { return ((m_ctrl_bit & CTRLBIT_CONFIRM_WHEN_APP_CLOSE) ? TRUE : FALSE);}
	
	void WriteSysSettingsToDB();
	void ReadSysSettingsFromDB();

};
