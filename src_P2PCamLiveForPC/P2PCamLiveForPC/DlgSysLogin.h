#pragma once
#include "afxwin.h"


// CDlgSysLogin 对话框
#define LOGIN_OK		8
#define LOGIN_RESTART	9

class CDlgSysLogin : public CDialog
{
	DECLARE_DYNAMIC(CDlgSysLogin)

public:
	CDlgSysLogin(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CDlgSysLogin();

// 对话框数据
	enum { IDD = DLG_SYS_LOGIN };
	enum { COMBOBOX_USER_COUNT=1, };

	int m_nPass;	//=LOGIN_OK, ok
	int m_iOPType;	//=0,normal; =1, timer arrive lock system

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CButton m_ctlBtnCancel;
	CButton m_ctlCheck1;
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
