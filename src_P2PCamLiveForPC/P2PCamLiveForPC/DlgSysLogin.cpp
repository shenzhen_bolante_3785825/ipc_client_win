// DlgSysLogin.cpp : 实现文件
//

#include "stdafx.h"
#include "P2PCamLiveForPC.h"
#include "MD5.h"
#include "DlgSysLogin.h"
#include "DlgSysLoginPW.h"

// CDlgSysLogin 对话框
IMPLEMENT_DYNAMIC(CDlgSysLogin, CDialog)

CDlgSysLogin::CDlgSysLogin(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgSysLogin::IDD, pParent)
{
	m_iOPType=0;
	m_nPass=0;
}

CDlgSysLogin::~CDlgSysLogin()
{
}

void CDlgSysLogin::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDCANCEL, m_ctlBtnCancel);
	DDX_Control(pDX, IDC_CHECK1, m_ctlCheck1);
}


BEGIN_MESSAGE_MAP(CDlgSysLogin, CDialog)
	ON_BN_CLICKED(IDOK, &CDlgSysLogin::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CDlgSysLogin::OnBnClickedCancel)
END_MESSAGE_MAP()

BOOL CDlgSysLogin::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_nPass=0;
	if(m_iOPType==0) {
		m_ctlCheck1.EnableWindow(TRUE);
		m_ctlBtnCancel.EnableWindow(TRUE);

	}else if(m_iOPType==1) {
		m_ctlCheck1.EnableWindow(FALSE);
		m_ctlBtnCancel.EnableWindow(FALSE);
	}

	GetDlgItem(IDC_PASSW)->SetWindowTextW(_T("admin"));

	CWinApp *ptheApp=&theApp;
	CString csU1;
	csU1=ptheApp->GetProfileString(_T("Settings"), _T("user"), _T(""));
	CComboBox *pCombo=(CComboBox *)GetDlgItem(IDC_USER);
	if(pCombo){
		pCombo->ResetContent();
		if(!csU1.IsEmpty()) pCombo->AddString(csU1);
		if(pCombo->GetCount()==0) pCombo->AddString(_T("admin"));

		pCombo->SetItemHeight(0,16);
		pCombo->SetCurSel(0);
	}
	return TRUE;
}

// CDlgSysLogin 消息处理程序
void CDlgSysLogin::OnBnClickedOk()
{
	::GetLocalTime(&g_objSysSettings.timeLastOP);	

	CString csIDSstr, csIDSstrTitle;
	csIDSstrTitle.LoadString(MB_TITLE_INFO);

	CString csUser, csPassW, csTmp;
	GetDlgItemText(IDC_USER, csUser);
	GetDlgItemText(IDC_PASSW, csPassW);
	csPassW.TrimLeft();	csPassW.TrimRight();
	if(csPassW.IsEmpty()){
		csIDSstr.LoadString(LOGIN_INFO1);
		MessageBox(csIDSstr, csIDSstrTitle, MB_ICONINFORMATION | MB_OK);
		return ;

	}else { //验证密码
		CString csDBFile, csSQL, csText;
		bool bOk=false;
		CMD5 myMD5; char *pPassW=NULL;
		char chPassw[128];
		CStrFromWSTRU(CP_ACP, csPassW, csPassW.GetLength(), chPassw);
		pPassW=myMD5.MD5_Algorithm(chPassw);
		
		csDBFile.Format(_T("%scamera.db"), g_csAppPath);
		try{
			g_SQLite3DB.open(csDBFile);
			BSTRFromCStrU(CP_ACP, pPassW, csTmp);
			csSQL.Format(_T("select user_name from user_info where user_name='%s' and user_pwd='%s'"), csUser, csTmp);
			CppSQLite3Query SQLiteQuery = g_SQLite3DB.execQuery(csSQL);
			if(!SQLiteQuery.eof())  bOk=true;
			SQLiteQuery.finalize();
		}catch(...){}
		g_SQLite3DB.close();

		if(!bOk) {
			csIDSstr.LoadString(LOGIN_INFO2);
			MessageBox(csIDSstr, csIDSstrTitle, MB_ICONINFORMATION | MB_OK);
			return;
		}

		m_nPass=LOGIN_OK;
		g_csLoginUser=csUser;
		g_csLoginPwd =csPassW;
		theApp.WriteProfileString(_T("Settings"), _T("user"), csUser);
		if(m_ctlCheck1.GetCheck()){ //登陆的时候更改密码
			CDlgSysLoginPW dlg;
			g_csLoginUser=csUser;
			if(dlg.DoModal()==IDOK) {
				m_nPass=LOGIN_RESTART;
				OnOK();
				return;
			}
		}
	}

	m_nPass=LOGIN_OK;
	OnOK();
}

void CDlgSysLogin::OnBnClickedCancel()
{
	OnCancel();
}


BOOL CDlgSysLogin::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message==WM_KEYDOWN){
		switch(pMsg->wParam)
		{
			case VK_RETURN:
				OnOK();

			case VK_ESCAPE:
				return TRUE;

			default:;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}
