// P2PCamLiveForPCDoc.cpp : implementation of the CP2PCamLiveForPCDoc class
//

#include "stdafx.h"
#include "P2PCamLiveForPC.h"

#include "P2PCamLiveForPCDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CP2PCamLiveForPCDoc

IMPLEMENT_DYNCREATE(CP2PCamLiveForPCDoc, CDocument)

BEGIN_MESSAGE_MAP(CP2PCamLiveForPCDoc, CDocument)
END_MESSAGE_MAP()


// CP2PCamLiveForPCDoc construction/destruction

CP2PCamLiveForPCDoc::CP2PCamLiveForPCDoc()
{
	// TODO: add one-time construction code here

}

CP2PCamLiveForPCDoc::~CP2PCamLiveForPCDoc()
{
}

BOOL CP2PCamLiveForPCDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}




// CP2PCamLiveForPCDoc serialization

void CP2PCamLiveForPCDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}


// CP2PCamLiveForPCDoc diagnostics

#ifdef _DEBUG
void CP2PCamLiveForPCDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CP2PCamLiveForPCDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CP2PCamLiveForPCDoc commands
