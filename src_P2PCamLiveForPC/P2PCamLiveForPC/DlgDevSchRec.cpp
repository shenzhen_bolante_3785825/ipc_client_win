// DlgDevSchRec.cpp : 实现文件
//

#include "stdafx.h"
#include "P2PCamLiveForPC.h"
#include "DlgDevSchRec.h"


// CDlgDevSchRec 对话框
IMPLEMENT_DYNAMIC(CDlgDevSchRec, CDialog)

CDlgDevSchRec::CDlgDevSchRec(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgDevSchRec::IDD, pParent)
{
	m_pDC=NULL;
	m_nModelID=0;
	for(int i=0; i<MINIDOWNROWLINE; i++) m_csWeekPlan[i]=_T("");
}

CDlgDevSchRec::~CDlgDevSchRec()
{
}

void CDlgDevSchRec::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDlgDevSchRec, CDialog)
//	ON_WM_MOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_BTN2, &CDlgDevSchRec::OnBnClickedBtn2)
	ON_BN_CLICKED(IDC_BTN1, &CDlgDevSchRec::OnBnClickedBtn1)
END_MESSAGE_MAP()

BOOL CDlgDevSchRec::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_pDC=GetDC();

	GetDlgItem(IDC_SCH_REC)->GetWindowRect(&m_grouprec);
	ScreenToClient(&m_grouprec);
	m_grouprec.bottom=m_grouprec.top+240;
	m_grouprec.DeflateRect(4,4);
	memset(&m_modeList.recModelInfo[m_nModelID], 0, sizeof(ModelInfo));
	
	CString csWeek;
	int i=0, k=0;
	for(i=0; i<MINIDOWNROWLINE; i++)
	{
		csWeek=m_csWeekPlan[i];
		if(csWeek.GetLength()<MINIDOWNCOLLINE) continue;
		for(k=0; k<MINIDOWNCOLLINE; k++)
			m_modeList.recModelInfo[m_nModelID].recPlan[i][k]=(csWeek.GetAt(k)=='1') ? 1 : 0;		
	}
	DrawModle(m_pDC, m_grouprec, &m_modeList.recModelInfo[m_nModelID]);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CDlgDevSchRec::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	CDC *pDC = GetDC();
	DrawModle(pDC, m_grouprec, &m_modeList.recModelInfo[m_nModelID]);
}


void CDlgDevSchRec::OnMouseMove(UINT nFlags, CPoint point)
{
	CRect rc(m_grouprec.right-DTNECOLREC*MINIDOWNCOLLINE, m_grouprec.bottom-DTNEROWREC*MINIDOWNROWLINE, m_grouprec.right, m_grouprec.bottom);
	if(rc.PtInRect(point))
	{
		ModelInfo *pModelInfo = &m_modeList.recModelInfo[m_nModelID];
		if(nFlags == MK_RBUTTON) DrawTableField(m_pDC, point, m_grouprec,  pModelInfo, FALSE);   
		else if(nFlags == MK_LBUTTON) DrawTableField(m_pDC, point, m_grouprec,  pModelInfo);
	}

	CDialog::OnMouseMove(nFlags, point);
}

void CDlgDevSchRec::OnLButtonDown(UINT nFlags, CPoint point)
{
	CRect rc(m_grouprec.right-DTNECOLREC*MINIDOWNCOLLINE, m_grouprec.bottom-DTNEROWREC*MINIDOWNROWLINE, m_grouprec.right, m_grouprec.bottom);
	if(rc.PtInRect(point))
	{
		ModelInfo *pModelInfo = &m_modeList.recModelInfo[m_nModelID];
		DrawTableField(m_pDC, point, m_grouprec, pModelInfo);
	}

	CDialog::OnLButtonDown(nFlags, point);
}

void CDlgDevSchRec::OnRButtonDown(UINT nFlags, CPoint point)
{
	CRect rc(m_grouprec.right-DTNECOLREC*MINIDOWNCOLLINE, m_grouprec.bottom-DTNEROWREC*MINIDOWNROWLINE, m_grouprec.right, m_grouprec.bottom);
	if(rc.PtInRect(point))
	{
		ModelInfo *pModelInfo = &m_modeList.recModelInfo[m_nModelID];
		DrawTableField(m_pDC, point, m_grouprec, pModelInfo, FALSE);  
	}

	CDialog::OnRButtonDown(nFlags, point);
}

void CDlgDevSchRec::OnBnClickedBtn1()
{
	CString csWeek;
	int i=0, k=0;
	for(i=0; i<MINIDOWNROWLINE; i++)
	{
		csWeek=_T("");
		for(k=0; k<MINIDOWNCOLLINE; k++){
			if(m_modeList.recModelInfo[m_nModelID].recPlan[i][k]) csWeek.Append(_T("1"));
			else csWeek.Append(_T("0"));			
		}
		m_csWeekPlan[i]=csWeek;
	}
	CDialog::OnOK();
}


void CDlgDevSchRec::OnBnClickedBtn2()
{
	CDialog::OnCancel();
}
