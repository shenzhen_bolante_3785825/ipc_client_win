#pragma once


// CPanelInternetDev 对话框
class CPanelInternetDev : public CDialog
{
	DECLARE_DYNAMIC(CPanelInternetDev)

public:
	CPanelInternetDev(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CPanelInternetDev();

// 对话框数据
	enum { IDD = PANEL_INTERNET_DEV };

	typedef enum { DEVSTA_OFFLINE, DEVSTA_START, DEVSTA_ONLINE, DEVSTA_IPCONFLICT, } DEVSTA_E;
	enum { GRID_COLUMN_SIZE=4, GRID_COL_DEVID=1, GRID_COL_IMG=2, GRID_COL_ALARM=3,};
	enum { ALARM_COL_IMG_0=0, ALARM_COL_IMG_1=5, };

protected:
	CGridCtrl	m_Grid;
	CImageList  m_ImageList;
	CToolBar	m_ToolBar;

	void InitGridRowNo();
	BOOL InitGridCtrl();
	void InitToolbar();

	int ReadJson(char *pdata);

	afx_msg void OnTbtnWeb();
	afx_msg void OnUpdateTbtnWeb(CCmdUI* pCmdUI);
	afx_msg void OnTbtnRefresh();
	afx_msg void OnUpdateTbtnRefresh(CCmdUI* pCmdUI);
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL OnInitDialog();
	void FillDevInfo();
};
