
class CVideoWnd;
class CFullScreenHandler {
public:
	CFullScreenHandler();
	~CFullScreenHandler();

	void Maximize(CFrameWnd* pFrame, CWnd* pView);
	void Restore(CFrameWnd* pFrame);

	void Maximize(CFrameWnd* pFrame, CWnd* pView, CVideoWnd *pVideoWnd); //bad

	BOOL InFullScreenMode() { return !m_rcRestore.IsRectEmpty(); }
	CSize GetMaxSize();

protected:
	CRect m_rcRestore;
};

// Global instance
extern CFullScreenHandler g_objFullScrnHandle;
