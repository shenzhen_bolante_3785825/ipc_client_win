#include "StdAfx.h"
#include "DevInfo.h"
#include "AVFRAMEINFO.h"
#include "VideoWnd.h"

CDevInfo::CDevInfo(UINT32 nTag, CVideoWnd *pVideoWnd, LPCTSTR lpDevName, LPCTSTR lpUID, LPCTSTR lpViewAcc, LPCTSTR lpDevPasswd)
{
	m_bStartedConnect=FALSE;
	m_bStartedAudio  =FALSE;
	m_bStartedSpeak  =FALSE;

	m_bConnected	 =FALSE;
	m_bAndStartVideo =FALSE;
	m_eDevType		 =DEV_TYPE_LOCAL;

	ResetRemoteValue();

	m_handleP2P=NULL;
	m_nTag	   =nTag;
	m_pVideoWnd=pVideoWnd;
	m_handleP2P=apiCreateObj(m_nTag, NULL, NULL, NULL);
	SetDevInfo(lpDevName, lpUID, lpViewAcc, lpDevPasswd);
}

CDevInfo::~CDevInfo(void)
{
	if(m_handleP2P){
		apiStopAudio(m_handleP2P);
		apiStopVideo(m_handleP2P);
		apiStopSpeak(m_handleP2P);
		apiDisconnect(m_handleP2P, 1);
		apiReleaseObj((tkHandle *)&m_handleP2P);
	}
}

void CDevInfo::SetDevInfo(LPCTSTR lpDevName, LPCTSTR lpUID, LPCTSTR lpViewAcc, LPCTSTR lpViewPasswd)
{
	if(m_handleP2P) {
		char chUID[128]={0},chAcc[128]={0},chPasswd[128]={0};
		CString csUID(lpUID), csAcc(lpViewAcc), csPasswd(lpViewPasswd);
		CStrFromWSTRU(CP_ACP, csUID, csUID.GetLength(), chUID);
		CStrFromWSTRU(CP_ACP, csAcc, csAcc.GetLength(), chAcc);
		CStrFromWSTRU(CP_ACP, csPasswd, csPasswd.GetLength(), chPasswd);

		apiSetConnInfo(m_handleP2P, m_nTag, chUID, chAcc, chPasswd);
	}

	m_csDevName=lpDevName;
	m_csUID=lpUID;
	m_csViewAcc=lpViewAcc;
	m_csViewPasswd=lpViewPasswd;
}

BOOL CDevInfo::IsEmpty()
{
	if(m_csUID.IsEmpty()) return TRUE;
	else return FALSE;
}

void CDevInfo::SetRecPlan(LPCTSTR w0, LPCTSTR w1,LPCTSTR w2,LPCTSTR w3,LPCTSTR w4,LPCTSTR w5,LPCTSTR w6)
{
	m_rec_plan_w0=w0;
	m_rec_plan_w1=w1;
	m_rec_plan_w2=w2;
	m_rec_plan_w3=w3;
	m_rec_plan_w4=w4;
	m_rec_plan_w5=w5;
	m_rec_plan_w6=w6;
}

CString CDevInfo::GetRecPlan(int nWeekIndex)
{
	switch(nWeekIndex)
	{
		case 0:
			return m_rec_plan_w0;
		case 1:
			return m_rec_plan_w1;
		case 2:
			return m_rec_plan_w2;
		case 3:
			return m_rec_plan_w3;
		case 4:
			return m_rec_plan_w4;
		case 5:
			return m_rec_plan_w5;
		case 6:
			return m_rec_plan_w6;
		default:;
	}
}

void CDevInfo::ResetRemoteValue()
{
	m_nVideoQuality	=-1;
	m_nVideoFlip=-1;
	m_nEnvMode	=-1;
	m_nMotionDet=-1;
	m_nRecMode	=-1;
	m_nWifiApNum=-1;
	memset((void*)m_wifiAP, 0, sizeof(m_wifiAP));
	memset(&m_devInfo, 0, sizeof(m_devInfo));
}

int  CDevInfo::ConnectDev(DEV_TYPE_E eDevType, BOOL bAndStartVideo)
{
	if(m_handleP2P==NULL || IsEmpty()) return -1;
	if(m_pVideoWnd){
		m_pVideoWnd->IconSetAudio(FALSE);
		m_pVideoWnd->IconSetSpeak(FALSE);
	}
	if(m_bConnected && bAndStartVideo){
		StartVideo();
		m_bAndStartVideo=bAndStartVideo;
	}

	if(m_bStartedConnect) return 1;
	m_bStartedConnect=1;
	m_bAndStartVideo=bAndStartVideo;

	m_eDevType=eDevType;
	return apiConnect(m_handleP2P);
}

void CDevInfo::DisconnectDev()
{
	if(m_handleP2P==NULL || IsEmpty()) return;
	if(!m_bStartedConnect) return;

	m_bStartedConnect=0;
	m_bAndStartVideo=FALSE;
	apiDisconnect(m_handleP2P, 1);
	m_bConnected=FALSE;
}

int  CDevInfo::StartVideo()
{
	if(m_handleP2P==NULL || IsEmpty()) return -1;
	return apiStartVideo(m_handleP2P);
}

void CDevInfo::StopVideo()
{
	if(m_handleP2P==NULL || IsEmpty()) return;
	apiStopVideo(m_handleP2P);
}

int  CDevInfo::StartAudio()
{
	if(m_handleP2P==NULL || IsEmpty()) return -1;
	if(m_bStartedAudio) return 1;
	m_bStartedAudio=1;
	return apiStartAudio(m_handleP2P);
}

void CDevInfo::StopAudio()
{
	if(m_handleP2P==NULL || IsEmpty()) return;
	if(!m_bStartedAudio) return;
	m_bStartedAudio=0;
	apiStopAudio(m_handleP2P);
}

int  CDevInfo::StartSpeak()
{
	if(m_handleP2P==NULL || IsEmpty()) return -1;
	if(m_bStartedSpeak) return 1;
	m_bStartedSpeak=1;
	return apiStartSpeak(m_handleP2P, MEDIA_CODEC_AUDIO_ADPCM);
}

void CDevInfo::StopSpeak()
{
	if(m_handleP2P==NULL || IsEmpty()) return;
	if(!m_bStartedSpeak) return;
	m_bStartedSpeak=0;
	apiStopSpeak(m_handleP2P);
}

int CDevInfo::SendIOCtrl(tkINT32 nIOType, tkUCHAR *pIOCtrlData, tkUINT32 nDataSize)
{
	if(m_handleP2P==NULL || IsEmpty()) return -1;
	return apiSendIOCtrl(m_handleP2P, nIOType, pIOCtrlData, nDataSize);
}

