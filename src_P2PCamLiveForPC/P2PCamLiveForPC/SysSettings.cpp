#include "StdAfx.h"
#include "SysSettings.h"

CSysSettings::CSysSettings(void)
{
	Reset();
}

CSysSettings::~CSysSettings(void)
{

}

void CSysSettings::Reset()
{
	::GetLocalTime(&timeLastOP);
	memcpy(&timeLastStart, &timeLastOP, sizeof(SYSTEMTIME));

	//low-->hight: 重连设置=0x01; 允许发送Email=0x02; 系统启动时连线所有摄像机=0x04; 
	//			   所有通道视频都拉伸=0x08;
	//			   关闭状态条=0x10;
	//			   系统关闭时，弹出确认对话框=0x20
	//
	CString csText;
	m_ctrl_bit		=0x01;
	m_reconn_time	=2;	//2s
	m_reconn_times	=5;
	m_sound_file_path=_T("");
	m_sound_duration=10;
	m_snap_rec_file_path=_T("");
	m_web_site		=_T("sztutk.3322.org");
	m_web_port		=81;
	m_mail_subject	=_T("Alarm:[DevName]");
	m_mail_body		=_T("Alarm from [DevName]([UID])");
	m_mail_host		=_T("");
	m_mail_to		=_T("");
	m_mail_account	=_T("");
	m_mail_passwd	=_T("");
}

void CSysSettings::WriteSysSettingsToDB()
{
	bool bErr=false;
	CString csDBFile, csSQL;
	csDBFile.Format(_T("%scamera.db"), g_csAppPath);
	try{
		g_SQLite3DB.open(csDBFile);
		csSQL.Format(_T("update sys_info set ctrl_bit=%d,reconn_time=%d,reconn_times=%d,sound_file_path='%s', \
sound_duration=%d,snap_rec_file_path='%s',web_site='%s',web_port=%d, mail_subject='%s',mail_body='%s',mail_host='%s', \
mail_to='%s',mail_account='%s',mail_passwd='%s' where db_id=1"), m_ctrl_bit,
		m_reconn_time, m_reconn_times, m_sound_file_path, m_sound_duration, 
		m_snap_rec_file_path, m_web_site, m_web_port, m_mail_subject, m_mail_body, 
		m_mail_host, m_mail_to, m_mail_account, m_mail_passwd);
		g_SQLite3DB.execDML(csSQL);
	}catch(...){ bErr=true; }
	g_SQLite3DB.close();
}

void CSysSettings::ReadSysSettingsFromDB()
{
	CString csDBFile, csSQL=_T("select * from sys_info where db_id=1");
	csDBFile.Format(_T("%scamera.db"), g_csAppPath);
	try{
		g_SQLite3DB.open(csDBFile);
		CppSQLite3Query SQLiteQuery = g_SQLite3DB.execQuery(csSQL);
		if(!SQLiteQuery.eof()){
			m_ctrl_bit		=::_ttoi(SQLiteQuery.fieldValue(_T("ctrl_bit")));
			m_reconn_time	=::_ttoi(SQLiteQuery.fieldValue(_T("reconn_time")));
			m_reconn_times	=::_ttoi(SQLiteQuery.fieldValue(_T("reconn_times")));
			m_sound_file_path=SQLiteQuery.fieldValue(_T("sound_file_path"));
			m_sound_duration=::_ttoi(SQLiteQuery.fieldValue(_T("sound_duration")));
			m_snap_rec_file_path=SQLiteQuery.fieldValue(_T("snap_rec_file_path"));
			m_web_site		=SQLiteQuery.fieldValue(_T("web_site"));
			m_web_port		=::_ttoi(SQLiteQuery.fieldValue(_T("web_port")));
			m_mail_subject	=SQLiteQuery.fieldValue(_T("mail_subject"));
			m_mail_body		=SQLiteQuery.fieldValue(_T("mail_body"));
			m_mail_host		=SQLiteQuery.fieldValue(_T("mail_host"));
			m_mail_to		=SQLiteQuery.fieldValue(_T("mail_to"));
			m_mail_account	=SQLiteQuery.fieldValue(_T("mail_account"));
			m_mail_passwd	=SQLiteQuery.fieldValue(_T("mail_passwd"));
		}
		SQLiteQuery.finalize();
	}catch(...){}
	g_SQLite3DB.close();
}