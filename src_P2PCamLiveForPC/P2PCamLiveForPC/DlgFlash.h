#if !defined(AFX_DLGFLASH_H__979DAD62_D7CD_43BF_B873_B42FE020FC83__INCLUDED_)
#define AFX_DLGFLASH_H__979DAD62_D7CD_43BF_B873_B42FE020FC83__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgFlash.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgFlash dialog

class CDlgFlash : public CDialog
{
// Construction
public:
	CDlgFlash(LPCTSTR lpTipText, UINT nTimeWait_sec=2, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgFlash)
	enum { IDD = IDD_FLASH };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgFlash)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	UINT	m_timerID;
	UINT    m_nTimeWait_sec;
	CString m_csTipInfo;
	CFont	m_font;
	CRect   m_rectClient;
	COLORREF m_crTipText, m_crBk;

	// Generated message map functions
	//{{AFX_MSG(CDlgFlash)
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDestroy();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnPaint();
	//}}AFX_MSG
public:
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGFLASH_H__979DAD62_D7CD_43BF_B873_B42FE020FC83__INCLUDED_)
