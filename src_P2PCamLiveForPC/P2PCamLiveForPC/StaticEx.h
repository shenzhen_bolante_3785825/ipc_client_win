#if !defined(AFX_STATICEX_H__1C65383A_9805_4E55_87FA_9CB0207D5FB5__INCLUDED_)
#define AFX_STATICEX_H__1C65383A_9805_4E55_87FA_9CB0207D5FB5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// StaticEx.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CStaticEx window

class CStaticEx : public CStatic
{
// Construction
public:
	CStaticEx(COLORREF crBkgnd);

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStaticEx)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CStaticEx();

	// Generated message map functions
protected:
	//{{AFX_MSG(CStaticEx)
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	//}}AFX_MSG

	COLORREF	m_crBkgnd;
	CBrush		m_brBkgnd;
	BOOL		m_bWndSizeEmpty;

public:
	void SetBkgndColor(COLORREF crBkgnd)	{ m_crBkgnd=crBkgnd;		}
	void SetWndEmpty(BOOL bEmpty=TRUE)		{ m_bWndSizeEmpty=bEmpty;	}
	BOOL IsWndEmpty()						{ return m_bWndSizeEmpty;	}

	DECLARE_MESSAGE_MAP()
	virtual void DrawItem(LPDRAWITEMSTRUCT /*lpDrawItemStruct*/);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STATICEX_H__1C65383A_9805_4E55_87FA_9CB0207D5FB5__INCLUDED_)
