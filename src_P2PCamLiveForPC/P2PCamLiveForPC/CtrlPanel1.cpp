// CtrlPanel1.cpp : 实现文件
//

#include "stdafx.h"
#include "P2PCamLiveForPC.h"
#include "CtrlPanel1.h"
#include "DlgSysSettings.h"
#include "P2PCamLiveForPCView.h"
#include "MainFrm.h"
#include "PanelLocalDev.h"
#include "PanelInternetDev.h"


// CCtrlPanel1 对话框
IMPLEMENT_DYNAMIC(CCtrlPanel1, CDialog)

CCtrlPanel1::CCtrlPanel1(CWnd* pParent /*=NULL*/)
	: CDialog(CCtrlPanel1::IDD, pParent)
{
	m_pTab1	=NULL;
	m_pTab2=NULL;
	m_iCurPage=0;
}

CCtrlPanel1::~CCtrlPanel1()
{

}

void CCtrlPanel1::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	//DDX_Control(pDX, IDC_GRID, m_Grid);
	DDX_Control(pDX, ID_MI_LANSEARCH, m_ctlLanSearch);
	DDX_Control(pDX, ID_MI_PARA, m_ctlSysSetting);
	DDX_Control(pDX, ID_MI_HIST, m_ctlHist);
	DDX_Control(pDX, IDC_ABOUT, m_ctlAbout);
	DDX_Control(pDX, IDC_CHECK1, m_ctlCheck1);
	DDX_Control(pDX, IDC_CHECK2, m_ctlCheck2);
	DDX_Control(pDX, IDC_CHECK3, m_ctlCheck3);
	DDX_Control(pDX, IDC_CHECK4, m_ctlCheck4);
	DDX_Control(pDX, IDC_TAB1, m_ctlTab);
}


BEGIN_MESSAGE_MAP(CCtrlPanel1, CDialog)
	ON_WM_SIZE()
	ON_BN_CLICKED(ID_MI_LANSEARCH, &CCtrlPanel1::OnBnClickedMiLansearch)
	ON_BN_CLICKED(ID_MI_PARA, &CCtrlPanel1::OnBnClickedMiPara)
	ON_BN_CLICKED(IDC_ABOUT,  &CCtrlPanel1::OnBnClickedAbout)
	ON_COMMAND(ID_MI_HIS_RECORD, OnMiHistRecord)
	ON_COMMAND(ID_MI_HIS_SNAP, OnMiHistSnap)
	ON_BN_CLICKED(IDC_CHECK1, &CCtrlPanel1::OnBnClickedCheck1)
	ON_BN_CLICKED(IDC_CHECK2, &CCtrlPanel1::OnBnClickedCheck2)
	ON_BN_CLICKED(IDC_CHECK3, &CCtrlPanel1::OnBnClickedCheck3)
	ON_BN_CLICKED(IDC_CHECK4, &CCtrlPanel1::OnBnClickedCheck4)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB1, &CCtrlPanel1::OnTcnSelchangeTab1)
	ON_MESSAGE(OM_FILL_DEV_INFO, &CCtrlPanel1::OnFillDevInfo)

	ON_WM_DESTROY()
END_MESSAGE_MAP()

BOOL CCtrlPanel1::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	InitUI();
	InitVar();
	InitTabPages();

	return TRUE;
}


void CCtrlPanel1::OnDestroy()
{
	CDialog::OnDestroy();

	if(m_pTab1)	{
		m_pTab1->DestroyWindow();
		delete m_pTab1;
		m_pTab1=NULL;
	}

	if(m_pTab2)	{
		m_pTab2->DestroyWindow();
		delete m_pTab2;
		m_pTab2=NULL;
	}
}


BOOL CCtrlPanel1::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
{	
	BOOL b1=FALSE, b2=FALSE;
	if(m_pTab1) b1=m_pTab1->OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
	if(m_pTab2) b2=m_pTab2->OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
	if(nID==IDC_TAB1) CDialog::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
	if(b1 || b2) return TRUE;

	return CDialog::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}

void CCtrlPanel1::InitUI()
{
	CString csIDSstr;
	m_ctlLanSearch.SetBitmaps(IDB_SEARCH_1, RGB(255,255,255), IDB_SEARCH, RGB(255,255,255));
	//csIDSstr.LoadString(CP_BTN_TIP13); m_ctlLanSearch.SetTooltipText(csIDSstr);//"搜索工具"
	m_ctlSysSetting.SetBitmaps(IDB_SYS_SETTINGS_1, RGB(255,255,255), IDB_SYS_SETTINGS, RGB(255,255,255));
	//csIDSstr.LoadString(CP_BTN_TIP14); m_ctlPara.SetTooltipText(csIDSstr);//"系統設置"

	m_ctlHist.SetBitmaps(IDB_HIST_1, RGB(255,255,255), IDB_HIST, RGB(255,255,255));
	m_ctlHist.SetMenu(IDR_POPUPHIS, m_hWnd);
	//csIDSstr.LoadString(CP_BTN_TIP15); m_ctlHist.SetTooltipText(csIDSstr);//"歷史記錄"

	m_ctlAbout.SetBitmaps(IDB_INFO_1, RGB(255,255,255), IDB_INFO, RGB(255,255,255));
	//csIDSstr.LoadString(CP_BTN_TIP16); m_ctlAbout.SetTooltipText(csIDSstr);//"关于IPCamSystem"

	m_ctlCheck1.SetBitmaps(IDB_SCRN1_1, RGB(255,255,255), IDB_SCRN1, RGB(255,255,255));
	//csIDSstr.LoadString(CP_BTN_TIP7); m_ctlCheck4.SetTooltipText(csIDSstr);//"4 split-screen"

	m_ctlCheck2.SetBitmaps(IDB_SCRN2_1, RGB(255,255,255), IDB_SCRN2, RGB(255,255,255));	
	//csIDSstr.LoadString(CP_BTN_TIP8); m_ctlCheck5.SetTooltipText(csIDSstr);//"6 split-screen"

	m_ctlCheck3.SetBitmaps(IDB_SCRN3_1, RGB(255,255,255), IDB_SCRN3, RGB(255,255,255));	
	//csIDSstr.LoadString(CP_BTN_TIP9); m_ctlCheck7.SetTooltipText(csIDSstr);//"9 split-screen"

	m_ctlCheck4.SetBitmaps(IDB_SCRN4_1, RGB(255,255,255), IDB_SCRN4, RGB(255,255,255));	
	//csIDSstr.LoadString(CP_BTN_TIP10); m_ctlCheck8.SetTooltipText(csIDSstr);//"16 split-screen"
}

void CCtrlPanel1::InitVar()
{
	m_bEnableAdd =TRUE;
	m_bEnableEdit=TRUE;
	m_bEnableDel =TRUE;
}

void CCtrlPanel1::InitTabPages()
{
	CString csTitle1, csTitle2;

	m_ctlTab.InsertItem(0, _T("Local device list"));
	m_ctlTab.InsertItem(1, _T("Internet device list"));

	CRect rect;
	m_ctlTab.GetClientRect(&rect);
	if(!m_pTab1){
		m_pTab1=new CPanelLocalDev(&m_ctlTab);
		m_pTab1->Create(CPanelLocalDev::IDD, &m_ctlTab);
		m_pTab1->MoveWindow(0,22,rect.Width(),rect.Height());
	}	
	if(!m_pTab2){
		m_pTab2=new CPanelInternetDev(&m_ctlTab);
		m_pTab2->Create(CPanelInternetDev::IDD, &m_ctlTab);
		m_pTab2->MoveWindow(0,22,rect.Width(),rect.Height());
	}

	m_ctlTab.SetCurSel(m_iCurPage);
	TabSwitch(m_iCurPage);
}

void CCtrlPanel1::TabSwitch(int iKind)
{
	switch(iKind)
	{
		case 0:	//local device
			if(m_pTab1) m_pTab1->ShowWindow(SW_SHOW);
			if(m_pTab2)	m_pTab2->ShowWindow(SW_HIDE);		
			break;

		case 1:	//internet device
			if(m_pTab1) m_pTab1->ShowWindow(SW_HIDE);
			if(m_pTab2)	m_pTab2->ShowWindow(SW_SHOW);		
			break;
		default:;
	}
}

// CCtrlPanel1 消息处理程序
void CCtrlPanel1::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	CRect rcStatic;
	CWnd *pWnd=GetDlgItem(IDC_STATIC_DEV);
	if(pWnd && pWnd->GetSafeHwnd()){		
		pWnd->GetWindowRect(rcStatic);
		ScreenToClient(rcStatic);
		rcStatic.bottom=cy;
		pWnd->MoveWindow(rcStatic);
	}

	if(m_ctlTab.GetSafeHwnd()){
		rcStatic.DeflateRect(2,20,2,2);
		m_ctlTab.SetWindowPos(&wndTop, rcStatic.left, rcStatic.top, rcStatic.Width(), rcStatic.Height(), SWP_DEFERERASE);

		rcStatic.bottom-=10;
		if(m_pTab1) m_pTab1->MoveWindow(0,22,rcStatic.Width(),rcStatic.Height());
		if(m_pTab2) m_pTab2->MoveWindow(0,22,rcStatic.Width(),rcStatic.Height());
	}
}

void CCtrlPanel1::OnBnClickedMiLansearch()
{
	char strSearch[MAX_PATH]={0};
	CString csText;
	csText.Format(_T("%sLANSearch.exe"), g_csAppPath);
	CStrFromWSTRU(CP_ACP, csText, csText.GetLength(), strSearch);
	UINT nRet=WinExec(strSearch, SW_SHOW);
	if(nRet==ERROR_FILE_NOT_FOUND || nRet==ERROR_PATH_NOT_FOUND){
		CString csIDSstr, csIDSstrTitle, csStrKeyTmp;
		csIDSstr.LoadString(IDS_NOT_LANSEARCH);
		csIDSstrTitle.LoadString(MB_TITLE_INFO);
		MessageBox(csIDSstr, csIDSstrTitle, MB_ICONINFORMATION | MB_OK);
	}

	m_ctlLanSearch.ActivateTooltip();
}

void CCtrlPanel1::OnBnClickedMiPara()
{
	CDlgSysSettings dlg;
	dlg.DoModal();
}

void CCtrlPanel1::OnMiHistRecord() 
{
	// 	CString csPath;
	// 	if(g_paraSet.file_path[0]=='\0'){
	// 		csPath.Format("%s", g_csAppPath);
	// 	}else csPath=g_paraSet.file_path;
	// 
	// 	if(!csPath.IsEmpty()) {
	// 		if(PathFileExists(csPath))	ShellExecute(NULL,NULL,csPath, NULL,NULL,SW_SHOW);	
	// 		else {
	// 			CString csIDSstr, csIDSstrTitle;
	// 			csIDSstr.LoadString(CP_MB_INFO1);
	// 			csIDSstrTitle.LoadString(MB_TITLE_INFO);
	// 			MessageBox(csIDSstr, csIDSstrTitle, MB_ICONINFORMATION | MB_OK);
	// 		}
	// 	}
// 	CDlgHistroy dlg;
// 	dlg.DoModal();
	TRACE(_T("OnMiHistRecord\n"));
}

void CCtrlPanel1::OnMiHistSnap() 
{
// 	CDlgHistroySnap dlg;
// 	dlg.DoModal();
	TRACE(_T("OnMiHistSnap\n"));
}

void CCtrlPanel1::OnBnClickedAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

void CCtrlPanel1::OnBnClickedCheck1()	//4 split
{
	if(m_ctlCheck1.GetCheck()) {
		m_ctlCheck2.SetCheck(FALSE);
		m_ctlCheck3.SetCheck(FALSE);
		m_ctlCheck4.SetCheck(FALSE);
		CP2PCamLiveForPCView *pView=CP2PCamLiveForPCApp::GetActiveView();
		if(pView) pView->m_objVWndMgr.SetMode(CVideoWndMgr::MODE1);

	}else {
		m_ctlCheck1.SetCheck(TRUE);
	}
}

void CCtrlPanel1::OnBnClickedCheck2()	//6 split
{
	if(m_ctlCheck2.GetCheck()){
		m_ctlCheck1.SetCheck(FALSE);
		m_ctlCheck3.SetCheck(FALSE);
		m_ctlCheck4.SetCheck(FALSE);
		CP2PCamLiveForPCView *pView=CP2PCamLiveForPCApp::GetActiveView();
		if(pView) pView->m_objVWndMgr.SetMode(CVideoWndMgr::MODE2);

	}else {
		m_ctlCheck2.SetCheck(TRUE);
	}
}

void CCtrlPanel1::OnBnClickedCheck3()	//9 split
{
	if(m_ctlCheck3.GetCheck()){
		m_ctlCheck1.SetCheck(FALSE);
		m_ctlCheck2.SetCheck(FALSE);
		m_ctlCheck4.SetCheck(FALSE);
		CP2PCamLiveForPCView *pView=CP2PCamLiveForPCApp::GetActiveView();
		if(pView) pView->m_objVWndMgr.SetMode(CVideoWndMgr::MODE3);

	}else {
		m_ctlCheck3.SetCheck(TRUE);
	}
}

void CCtrlPanel1::OnBnClickedCheck4()	//16 split
{
	if(m_ctlCheck4.GetCheck()){
		m_ctlCheck1.SetCheck(FALSE);
		m_ctlCheck2.SetCheck(FALSE);
		m_ctlCheck3.SetCheck(FALSE);
		CP2PCamLiveForPCView *pView=CP2PCamLiveForPCApp::GetActiveView();
		if(pView) pView->m_objVWndMgr.SetMode(CVideoWndMgr::MODE4);

	}else {
		m_ctlCheck4.SetCheck(TRUE);
	}
}

void CCtrlPanel1::OnTcnSelchangeTab1(NMHDR *pNMHDR, LRESULT *pResult)
{
	m_iCurPage=m_ctlTab.GetCurSel();
	TabSwitch(m_iCurPage);

	*pResult = 0;
}

LRESULT CCtrlPanel1::OnFillDevInfo(WPARAM wParam, LPARAM lParam)
{
	if(m_pTab1) m_pTab1->FillDevInfo();
	if(m_pTab2) m_pTab2->FillDevInfo();
	
	return 0L;
}