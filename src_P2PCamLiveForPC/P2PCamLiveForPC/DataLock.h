
#ifndef _DATA_LOCK_H_
#define _DATA_LOCK_H_

class CDataLock
{
public:
	CDataLock(CRITICAL_SECTION& cs, const CString& strFunc)
	{
		m_strFunc = strFunc;
		m_pcs = &cs;
		Lock();
	}
	virtual ~CDataLock() { Unlock(); }
	void Lock()		{ EnterCriticalSection(m_pcs); }
	void Unlock()	{ LeaveCriticalSection(m_pcs); }

protected:
	CRITICAL_SECTION*	m_pcs;
	CString				m_strFunc;
};

#endif
