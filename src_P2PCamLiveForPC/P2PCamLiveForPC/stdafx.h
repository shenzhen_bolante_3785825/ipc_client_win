// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // Exclude rarely-used stuff from Windows headers
#endif

#define WINVER	0x0501
#define _WIN32_WINNT 0x0501

#include "targetver.h"

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // some CString constructors will be explicit

// turns off MFC's hiding of some common and often safely ignored warning messages
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions


#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>           // MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>                     // MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT


#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif

#include <map>
#include <vector>
#include <set>

#include <SHLWAPI.H>
#pragma comment(lib, "shlwapi.lib")
#include "SysSettings.h"

#include "sizecbar.h"
#include "scbarg.h"
#include "scbarcf.h"
#include "AutoHideBar.h"

#include "GridCtrl.h"
#include "TreeColumn.h"
#include "BtnDataBase.h"
#include "GridBtnCell.h"
#include "GridBtnCellCombo.h"
#include "CppSQLite3.h"
#include "app_struct.h"

BOOL CStrFromWSTRU(IN UINT codePage, IN LPCWSTR wstr, IN UINT len, OUT char *pStr);
BOOL BSTRFromCStrU(IN UINT codePage, IN LPCSTR s, OUT CString &csWStr);
void SplitString2(CString source, CStringArray &dest, char division);


#define THIS_APP_VER	    0x00010000
#define MAX_CHN_NUM			16

extern MWND_POSSIZE g_stWndPosSize;
extern CString		g_csAppPath;
extern CString		g_csLoginUser, g_csLoginPwd;
extern CSysSettings	g_objSysSettings;
extern CppSQLite3DB	g_SQLite3DB;

