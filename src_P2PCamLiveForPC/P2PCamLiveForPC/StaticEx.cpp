// StaticEx.cpp : implementation file
//

#include "stdafx.h"
#include "P2PCamLiveForPC.h"
#include "StaticEx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CStaticEx

CStaticEx::CStaticEx(COLORREF crBkgnd)
{
	m_brBkgnd.CreateSolidBrush(crBkgnd);
}

CStaticEx::~CStaticEx()
{
	m_brBkgnd.DeleteObject();
}


BEGIN_MESSAGE_MAP(CStaticEx, CStatic)
	//{{AFX_MSG_MAP(CStaticEx)
	ON_WM_ERASEBKGND()
	ON_WM_CTLCOLOR_REFLECT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CStaticEx message handlers

BOOL CStaticEx::OnEraseBkgnd(CDC* pDC) 
{	
// 	CRect rect;
// 	GetClientRect(&rect);
// 	dc.FillRect(&rect, &m_brBkgnd);

	return TRUE;
}

HBRUSH CStaticEx::CtlColor(CDC* pDC, UINT nCtlColor) 
{
	//return (HBRUSH)m_brBkgnd.GetSafeHandle();
	return NULL;
}



void CStaticEx::DrawItem(LPDRAWITEMSTRUCT /*lpDrawItemStruct*/)
{

	
}
