// DlgEmailSetting.cpp : 实现文件
//

#include "stdafx.h"
#include "P2PCamLiveForPC.h"
#include "DlgEmailSetting.h"


// CDlgEmailSetting 对话框
IMPLEMENT_DYNAMIC(CDlgEmailSetting, CDialog)

CDlgEmailSetting::CDlgEmailSetting(CWnd* pParent /*=NULL*/) : CDialog(CDlgEmailSetting::IDD, pParent)
{
	
}

CDlgEmailSetting::~CDlgEmailSetting()
{
}

void CDlgEmailSetting::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDlgEmailSetting, CDialog)
	ON_BN_CLICKED(IDC_BTN1, &CDlgEmailSetting::OnBnClickedBtn1)
	ON_BN_CLICKED(IDC_TESTSMTP, &CDlgEmailSetting::OnBnClickedTestsmtp)
	ON_BN_CLICKED(IDC_BTN2, &CDlgEmailSetting::OnBnClickedBtn2)
END_MESSAGE_MAP()

BOOL CDlgEmailSetting::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetDlgItemText(IDC_EDIT1, g_objSysSettings.m_mail_subject);
	SetDlgItemText(IDC_EDIT2, g_objSysSettings.m_mail_body);
	SetDlgItemText(IDC_EDIT3, g_objSysSettings.m_mail_host);
	SetDlgItemText(IDC_EDIT4, g_objSysSettings.m_mail_to);
	SetDlgItemText(IDC_EDIT5, g_objSysSettings.m_mail_account);
	SetDlgItemText(IDC_EDIT6, g_objSysSettings.m_mail_passwd);
	
	return TRUE;
}


// CDlgEmailSetting 消息处理程序
void CDlgEmailSetting::OnBnClickedTestsmtp()
{
	
}

void CDlgEmailSetting::OnBnClickedBtn1()
{
	CString csText;
	GetDlgItemText(IDC_EDIT1, csText); g_objSysSettings.m_mail_subject=csText;
	GetDlgItemText(IDC_EDIT2, csText); g_objSysSettings.m_mail_body=csText;
	GetDlgItemText(IDC_EDIT3, csText); g_objSysSettings.m_mail_host=csText;
	GetDlgItemText(IDC_EDIT4, csText); g_objSysSettings.m_mail_to=csText;
	GetDlgItemText(IDC_EDIT5, csText); g_objSysSettings.m_mail_account=csText;
	GetDlgItemText(IDC_EDIT6, csText); g_objSysSettings.m_mail_passwd=csText;
	g_objSysSettings.WriteSysSettingsToDB();

	CDialog::OnOK();
}

void CDlgEmailSetting::OnBnClickedBtn2()
{
	CDialog::OnCancel();
}
