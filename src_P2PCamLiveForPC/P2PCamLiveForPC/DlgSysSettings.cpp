// DlgSysSettings.cpp : 实现文件
//

#include "stdafx.h"
#include "P2PCamLiveForPC.h"
#include "DlgSysSettings.h"
#include "DlgEmailSetting.h"
#include "FolderDialog.h"
#include "MainFrm.h"

// CDlgSysSettings 对话框
IMPLEMENT_DYNAMIC(CDlgSysSettings, CDialog)

CDlgSysSettings::CDlgSysSettings(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgSysSettings::IDD, pParent)
{

}

CDlgSysSettings::~CDlgSysSettings()
{
}

void CDlgSysSettings::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SPIN1, m_ctlSpin1);
	DDX_Control(pDX, IDC_SPIN2, m_ctlSpin2);
	DDX_Control(pDX, IDC_SPIN3, m_ctlSpin3);
	DDX_Control(pDX, IDC_CHECK1, m_ctlCheck1);
	DDX_Control(pDX, IDC_CHECK2, m_ctlCheck2);
	DDX_Control(pDX, IDC_CHECK3, m_ctlCheck3);
	DDX_Control(pDX, IDC_CHECK4, m_ctlCheck4);
	DDX_Control(pDX, IDC_CHECK5, m_ctlCheck5);
	DDX_Control(pDX, IDC_CHECK6, m_ctlCheck6);
}


BEGIN_MESSAGE_MAP(CDlgSysSettings, CDialog)
	ON_BN_CLICKED(IDC_BUTTON1, &CDlgSysSettings::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CDlgSysSettings::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_REMOTE_MODI_PWD, &CDlgSysSettings::OnBnClickedRemoteModiPwd)
	ON_BN_CLICKED(IDC_BFOLDER2, &CDlgSysSettings::OnBnClickedBfolder2)
	ON_BN_CLICKED(IDC_BFOLDER, &CDlgSysSettings::OnBnClickedBfolder)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN1, &CDlgSysSettings::OnDeltaposSpin1)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN2, &CDlgSysSettings::OnDeltaposSpin2)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN3, &CDlgSysSettings::OnDeltaposSpin3)
END_MESSAGE_MAP()


BOOL CDlgSysSettings::OnInitDialog()
{
	CDialog::OnInitDialog();
	InitSpin();
	
	//check item
	if(g_objSysSettings.IsAutoConnect()) m_ctlCheck1.SetCheck(TRUE);
	else m_ctlCheck1.SetCheck(FALSE);

	if(g_objSysSettings.IsEnableSendEmail()) m_ctlCheck2.SetCheck(TRUE);
	else m_ctlCheck2.SetCheck(FALSE);

	if(g_objSysSettings.IsConnAllCamsWhenStart()) m_ctlCheck3.SetCheck(TRUE);
	else m_ctlCheck3.SetCheck(FALSE);

	if(g_objSysSettings.IsVideoStretch()) m_ctlCheck4.SetCheck(TRUE);
	else m_ctlCheck4.SetCheck(FALSE);

	if(g_objSysSettings.IsCloseStatus()) m_ctlCheck5.SetCheck(TRUE);
	else m_ctlCheck5.SetCheck(FALSE);

	if(g_objSysSettings.IsConfirmWhenAppClose()) m_ctlCheck6.SetCheck(TRUE);
	else m_ctlCheck6.SetCheck(FALSE);

	//
	SetDlgItemText(IDC_EDIT3, g_objSysSettings.m_sound_file_path);
	SetDlgItemText(IDC_EDIT5, g_objSysSettings.m_snap_rec_file_path);
	SetDlgItemText(IDC_EDIT6, g_objSysSettings.m_web_site);
	SetDlgItemInt(IDC_EDIT7,  g_objSysSettings.m_web_port, FALSE);
	return TRUE; 
}

void CDlgSysSettings::InitSpin()
{
	CEdit *pEdit=NULL;
	//m_ctlSpin1 //time to try to reconnect
	pEdit=(CEdit *)GetDlgItem(IDC_EDIT1);
	if(pEdit){
		CString strText;
		strText.Format(_T("%d"), g_objSysSettings.m_reconn_time);
		pEdit->SetWindowText(strText);
		m_ctlSpin1.SetBuddy(pEdit);
	}
	m_ctlSpin1.SetPos(2);
	m_ctlSpin1.SetBase(2);
	m_ctlSpin1.SetRange32(2, 65535);

	//m_ctlSpin2 //times to try to reconnect
	pEdit=(CEdit *)GetDlgItem(IDC_EDIT2);
	if(pEdit) {
		CString strText;
		strText.Format(_T("%d"), g_objSysSettings.m_reconn_times);
		pEdit->SetWindowText(strText);
		m_ctlSpin2.SetBuddy(pEdit);
	}
	m_ctlSpin2.SetPos(2);
	m_ctlSpin2.SetBase(2);
	m_ctlSpin2.SetRange32(2, 65535);

	//m_ctlSpin3 //alarm sound time length [5, 60]
	pEdit=(CEdit *)GetDlgItem(IDC_EDIT4);
	if(pEdit) {
		CString strText;
		strText.Format(_T("%d"), g_objSysSettings.m_sound_duration);
		pEdit->SetWindowText(strText);
		m_ctlSpin3.SetBuddy(pEdit);
	}
	m_ctlSpin3.SetPos(1);
	m_ctlSpin3.SetBase(1);
	m_ctlSpin3.SetRange32(1, OPT_MAX_ALARM_SOUNDTIMELEN);
}

// CDlgSysSettings 消息处理程序
void CDlgSysSettings::OnBnClickedButton1()
{
	if(!CheckBeforeSave()) return;
	CString csText;
	g_objSysSettings.m_ctrl_bit=0;
	if(m_ctlCheck1.GetCheck()) g_objSysSettings.m_ctrl_bit |=CTRLBIT_AUTO_CONNECT;
	else g_objSysSettings.m_ctrl_bit &= ~CTRLBIT_AUTO_CONNECT;

	if(m_ctlCheck2.GetCheck()) g_objSysSettings.m_ctrl_bit |=CTRLBIT_ENABLE_SEND_EMAIL;
	else g_objSysSettings.m_ctrl_bit &= ~CTRLBIT_ENABLE_SEND_EMAIL;

	if(m_ctlCheck3.GetCheck()) g_objSysSettings.m_ctrl_bit |=CTRLBIT_CONN_CAMS_WHEN_START;
	else g_objSysSettings.m_ctrl_bit &= ~CTRLBIT_CONN_CAMS_WHEN_START;

	if(m_ctlCheck4.GetCheck()) g_objSysSettings.m_ctrl_bit |=CTRLBIT_VIDEO_STRETCH;
	else g_objSysSettings.m_ctrl_bit &= ~CTRLBIT_VIDEO_STRETCH;

	if(m_ctlCheck5.GetCheck()) g_objSysSettings.m_ctrl_bit |=CTRLBIT_CLOSE_STATUS;
	else g_objSysSettings.m_ctrl_bit &= ~CTRLBIT_CLOSE_STATUS;

	if(m_ctlCheck6.GetCheck()) g_objSysSettings.m_ctrl_bit |=CTRLBIT_CONFIRM_WHEN_APP_CLOSE;
	else g_objSysSettings.m_ctrl_bit &= ~CTRLBIT_CONFIRM_WHEN_APP_CLOSE;

	g_objSysSettings.m_reconn_time =GetDlgItemInt(IDC_EDIT1, NULL, FALSE);
	g_objSysSettings.m_reconn_times=GetDlgItemInt(IDC_EDIT2, NULL, FALSE);

	GetDlgItemText(IDC_EDIT3, csText);
	g_objSysSettings.m_sound_file_path=csText;
	g_objSysSettings.m_sound_duration=GetDlgItemInt(IDC_EDIT4, NULL, FALSE);

	GetDlgItemText(IDC_EDIT5, csText);
	g_objSysSettings.m_snap_rec_file_path=csText;

	GetDlgItemText(IDC_EDIT6, csText);
	g_objSysSettings.m_web_site=csText;
	g_objSysSettings.m_web_port=GetDlgItemInt(IDC_EDIT7, NULL, FALSE);
	
	g_objSysSettings.WriteSysSettingsToDB();

	CMainFrame *pMainFrame=	(CMainFrame *)AfxGetMainWnd();
	if(pMainFrame) pMainFrame->ShowStatusBarByPara();
	CDialog::OnOK();
}

BOOL CDlgSysSettings::CheckBeforeSave()
{
	
	return TRUE;
}

void CDlgSysSettings::OnBnClickedButton2()
{
	CDialog::OnCancel();
}

void CDlgSysSettings::OnBnClickedRemoteModiPwd()
{
	CDlgEmailSetting dlg;
	dlg.DoModal();
}

void CDlgSysSettings::OnBnClickedBfolder2()
{
	static CString csFilter(_T(""));
	if(csFilter.GetLength()==0)	csFilter.LoadString(OP_INFO12);
	CString csFile;
	GetDlgItemText(IDC_EDIT3, csFile);

	CFileDialog dlg(TRUE, _T("*.wav"), NULL, OFN_PATHMUSTEXIST|OFN_HIDEREADONLY, csFilter);	
	if(PathFileExists(csFile)){
		dlg.m_ofn.lpstrInitialDir=csFile.GetBuffer(0);
		csFile.ReleaseBuffer();

	}else dlg.m_ofn.lpstrInitialDir=g_csAppPath.GetBuffer(0);

	if(dlg.DoModal()==IDOK)	SetDlgItemText(IDC_EDIT3, dlg.GetPathName());
	g_csAppPath.ReleaseBuffer();
}

void CDlgSysSettings::OnBnClickedBfolder()
{
	CString csFolder;
	GetDlgItemText(IDC_EDIT5, csFolder);

	csFolder.Replace(_T("/"), _T("\\"));
	CFolderDialog dlg(csFolder, BIF_RETURNONLYFSDIRS, this);
	if(dlg.DoModal()==IDOK)
	{
		csFolder = dlg.GetPathName();		
		SetDlgItemText(IDC_EDIT5, csFolder);
		//m_bDataChanged=TRUE;
	}
}

void CDlgSysSettings::OnDeltaposSpin1(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	
	CString strText, strTmp;
	int iValue=0;
	m_ctlSpin1.GetBuddy()->GetWindowText(strText);
	_stscanf(strText, _T("%d"), &iValue);

	if(pNMUpDown->iDelta==1){
		if(iValue>=OPT_MAX_TIME_RECONN) {
		}else{			
			strText.Format(_T("%d"), iValue+1);
			m_ctlSpin1.GetBuddy()->SetWindowText(strText);
		}		
	}else if(pNMUpDown->iDelta==-1){
		if(iValue<=OPT_MIN_TIME_RECONN) {
		}else{			
			strText.Format(_T("%d"), iValue-1);
			m_ctlSpin1.GetBuddy()->SetWindowText(strText);
		}
	}

	*pResult = 0;
}

void CDlgSysSettings::OnDeltaposSpin2(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);

	CString strText, strTmp;
	int iValue=0;
	m_ctlSpin2.GetBuddy()->GetWindowText(strText);
	_stscanf(strText, _T("%d"), &iValue);

	if(pNMUpDown->iDelta==1){
		if(iValue>=OPT_MAX_TIMES_RECONN) {

		}else{
			strText.Format(_T("%d"), iValue+1);
			m_ctlSpin2.GetBuddy()->SetWindowText(strText);
		}		
	}else if(pNMUpDown->iDelta==-1){
		if(iValue<=OPT_MIN_TIMES_RECONN) {

		}else{			
			strText.Format(_T("%d"), iValue-1);
			m_ctlSpin2.GetBuddy()->SetWindowText(strText);
		}
	}

	*pResult = 0;
}

void CDlgSysSettings::OnDeltaposSpin3(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);

	CString strText, strTmp;
	int iValue=0;
	m_ctlSpin3.GetBuddy()->GetWindowText(strText);
	_stscanf(strText, _T("%d"), &iValue);

	if(pNMUpDown->iDelta==1){
		if(iValue>=OPT_MAX_ALARM_SOUNDTIMELEN) {
		}else{			
			strText.Format(_T("%d"), iValue+1);
			m_ctlSpin3.GetBuddy()->SetWindowText(strText);
		}		
	}else if(pNMUpDown->iDelta==-1){
		if(iValue<=OPT_MIN_ALARM_SOUNDTIMELEN) {
		}else{			
			strText.Format(_T("%d"), iValue-1);
			m_ctlSpin3.GetBuddy()->SetWindowText(strText);
		}
	}

	*pResult = 0;
}
