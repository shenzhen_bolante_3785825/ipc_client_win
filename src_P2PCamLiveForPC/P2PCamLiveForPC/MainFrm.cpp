// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "P2PCamLiveForPC.h"
#include "MainFrm.h"
#include "FullScreenHandler.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define IDC_DOCKPAGEBAR 1233



// CMainFrame
IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_GETMINMAXINFO()
	ON_WM_CLOSE()
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator		
	ID_INDICATOR_S1,
	ID_INDICATOR_S2,
	ID_INDICATOR_S3,
	ID_INDICATOR_S4,
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL
};


// CMainFrame construction/destruction
CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
}

CMainFrame::~CMainFrame()
{
}


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if(CFrameWnd::OnCreate(lpCreateStruct) == -1) return -1;

	//1. status bar
	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}
	m_wndStatusBar.SetPaneInfo(1, ID_INDICATOR_S1, SBPS_NORMAL,120);
	m_wndStatusBar.SetPaneInfo(2, ID_INDICATOR_S2, SBPS_NORMAL,140);
	m_wndStatusBar.SetPaneInfo(3, ID_INDICATOR_S3, SBPS_NORMAL,190);
	m_wndStatusBar.SetPaneInfo(4, ID_INDICATOR_S4, SBPS_NORMAL,165);

	m_wndStatusBar.SetPaneText(1, _T(""));
	m_wndStatusBar.SetPaneText(2, g_csLoginUser);
	CString csText;
	COleDateTime odt(COleDateTime::GetCurrentTime());//g_paraSet.timeLastStart);
	csText.Format(_T("Login date: %s"), odt.Format(_T("%Y-%m-%d %H:%M:%S")));
	m_wndStatusBar.SetPaneText(3, csText);
	m_wndStatusBar.SetPaneText(4, _T(""));
	EnableDocking(CBRS_ALIGN_ANY);


	//2 navig bar
	if (!m_DockPageBar.Create(_T("DockPageBar"), this, CSize(270,550),TRUE,
		IDC_DOCKPAGEBAR, WS_CHILD | WS_VISIBLE|CBRS_TOOLTIPS | CBRS_GRIPPER | 
		CBRS_RIGHT| CBRS_FLYBY | CBRS_SIZE_DYNAMIC))
	{
		TRACE0("Failed to create DockPageBar.\n");
		return -1;
	}
	m_DockPageBar.DisabledBtnClose(TRUE);
	m_dlgCtrl1.Create(CCtrlPanel1::IDD, &m_DockPageBar);
	CString csIDSstr;
	csIDSstr.LoadString(APP_INFO4);
	m_DockPageBar.AddPage(&m_dlgCtrl1, csIDSstr, IDR_P2PCamLiveForPCTYPE);

// 	m_dlgCtrl2.Create(CCtrlPanel2::IDD, &m_DockPageBar);
// 	csIDSstr.LoadString(APP_INFO10);
// 	m_DockPageBar.AddPage(&m_dlgCtrl2, csIDSstr, IDR_MAINFRAME);

	ShowStatusBarByPara();
	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CFrameWnd::PreCreateWindow(cs) ) return FALSE;
	cs.hMenu = NULL;
	if(g_stWndPosSize.cx!=0 && g_stWndPosSize.cy!=0){
		cs.x  =g_stWndPosSize.x;  cs.y =g_stWndPosSize.y;
		cs.cx =g_stWndPosSize.cx; cs.cy=g_stWndPosSize.cy;
	}

	return TRUE;
}

// CMainFrame message handlers
void CMainFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x=600;
	lpMMI->ptMinTrackSize.y=450;

	CSize sz = g_objFullScrnHandle.GetMaxSize();
	lpMMI->ptMaxSize = CPoint(sz);
	lpMMI->ptMaxTrackSize = CPoint(sz);	

	CFrameWnd::OnGetMinMaxInfo(lpMMI);
}



// CMainFrame diagnostics
#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG


BOOL CMainFrame::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
{
	if(m_dlgCtrl1.OnCmdMsg(nID, nCode, pExtra, pHandlerInfo)) return TRUE;
	return CFrameWnd::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}

void CMainFrame::OnClose()
{
	if(g_objSysSettings.m_ctrl_bit & CTRLBIT_CONFIRM_WHEN_APP_CLOSE){
		int nRet=0;
		CString csIDSstr, csIDSstrTitle;
		csIDSstrTitle.LoadString(MB_TITLE_INFO);
		csIDSstr.LoadString(APP_INFO6);
		nRet=MessageBox(csIDSstr, csIDSstrTitle, MB_ICONQUESTION | MB_YESNO);
		if(nRet==IDNO) return;
	}

	CFrameWnd::OnClose();
}

void CMainFrame::ShowStatusBarByPara()
{
	if(m_wndStatusBar.GetSafeHwnd()){
		if(g_objSysSettings.m_ctrl_bit & CTRLBIT_CLOSE_STATUS) ShowControlBar(&m_wndStatusBar, FALSE, FALSE);
		else ShowControlBar(&m_wndStatusBar, TRUE, FALSE);
	}
}
