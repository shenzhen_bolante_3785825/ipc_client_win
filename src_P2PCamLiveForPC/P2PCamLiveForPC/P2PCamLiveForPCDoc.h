// P2PCamLiveForPCDoc.h : interface of the CP2PCamLiveForPCDoc class
//


#pragma once


class CP2PCamLiveForPCDoc : public CDocument
{
protected: // create from serialization only
	CP2PCamLiveForPCDoc();
	DECLARE_DYNCREATE(CP2PCamLiveForPCDoc)

// Attributes
public:

// Operations
public:

// Overrides
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementation
public:
	virtual ~CP2PCamLiveForPCDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
};


