// DlgRemoteInfo.cpp : 实现文件
//

#include "stdafx.h"
#include "P2PCamLiveForPC.h"
#include "DlgRemoteInfo.h"
#include "DevInfo.h"

// CDlgRemoteInfo 对话框

IMPLEMENT_DYNAMIC(CDlgRemoteInfo, CDialog)

CDlgRemoteInfo::CDlgRemoteInfo(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgRemoteInfo::IDD, pParent)
{
	m_pCurDevInfo=NULL;
}

CDlgRemoteInfo::~CDlgRemoteInfo()
{
}

void CDlgRemoteInfo::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST2, m_ctrlList);
}


BEGIN_MESSAGE_MAP(CDlgRemoteInfo, CDialog)
	ON_BN_CLICKED(IDC_BTN2, &CDlgRemoteInfo::OnBnClickedBtn2)
END_MESSAGE_MAP()


// CDlgRemoteInfo 消息处理程序
BOOL CDlgRemoteInfo::OnInitDialog()
{
	CDialog::OnInitDialog();

	CStringArray arrRows;
	CString csText;
	int iInitCol=0;
	m_ctrlList.InsertColumn(iInitCol++, _T("Item"),  LVCFMT_LEFT, 100);
	m_ctrlList.InsertColumn(iInitCol++, _T("Value"), LVCFMT_LEFT, 150);

	iInitCol=0;
	csText.LoadString(DEVINFO_REMOTE_COLS);
	SplitString2(csText, arrRows, ';');
	if(arrRows.GetCount()<5) {
		//Model;Version;Vender;Total size;Free size;
		m_ctrlList.InsertItem(iInitCol++, _T("Model"));
		m_ctrlList.InsertItem(iInitCol++, _T("Version"));
		m_ctrlList.InsertItem(iInitCol++, _T("Vender"));
		m_ctrlList.InsertItem(iInitCol++, _T("Total size"));
		m_ctrlList.InsertItem(iInitCol++, _T("Free size"));
	}else{
		m_ctrlList.InsertItem(iInitCol++, arrRows[0]);
		m_ctrlList.InsertItem(iInitCol++, arrRows[1]);
		m_ctrlList.InsertItem(iInitCol++, arrRows[2]);
		m_ctrlList.InsertItem(iInitCol++, arrRows[3]);
		m_ctrlList.InsertItem(iInitCol++, arrRows[4]);
	}
	m_ctrlList.SetExtendedStyle(LVS_EX_FULLROWSELECT);

	if(m_pCurDevInfo){
		CString csText;
		BSTRFromCStrU(CP_ACP, (LPCSTR)m_pCurDevInfo->m_devInfo.model, csText);
		m_ctrlList.SetItemText(0,1,csText);

		char *pValue=(char *)&m_pCurDevInfo->m_devInfo.version;
		csText.Format(_T("%d.%d.%d.%d"), *(pValue+3), *(pValue+2), *(pValue+1), *(pValue+0));
		m_ctrlList.SetItemText(1,1,csText);

		BSTRFromCStrU(CP_ACP, (LPCSTR)m_pCurDevInfo->m_devInfo.vendor, csText);
		m_ctrlList.SetItemText(2,1,csText);

		csText.Format(_T("%d MB"), m_pCurDevInfo->m_devInfo.total);
		m_ctrlList.SetItemText(3,1,csText);

		csText.Format(_T("%d MB"), m_pCurDevInfo->m_devInfo.free);
		m_ctrlList.SetItemText(4,1,csText);
		
	}
	return TRUE;
}

void CDlgRemoteInfo::OnBnClickedBtn2()
{
	CDialog::OnCancel();
}
