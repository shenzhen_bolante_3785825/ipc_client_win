// PanelInternetDev.cpp : 实现文件
//

#include "stdafx.h"
#include "P2PCamLiveForPC.h"
#include "PanelInternetDev.h"
#include <afxinet.h>

#include "json/json.h"
#include <string>
using namespace Json;

// CPanelInternetDev 对话框

IMPLEMENT_DYNAMIC(CPanelInternetDev, CDialog)

CPanelInternetDev::CPanelInternetDev(CWnd* pParent /*=NULL*/)
	: CDialog(CPanelInternetDev::IDD, pParent)
{

}

CPanelInternetDev::~CPanelInternetDev()
{
}

void CPanelInternetDev::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_GRID, m_Grid);
}


BEGIN_MESSAGE_MAP(CPanelInternetDev, CDialog)
	ON_WM_SIZE()
	ON_COMMAND(ID_TBTN_WEB, OnTbtnWeb)
	ON_UPDATE_COMMAND_UI(ID_TBTN_WEB, OnUpdateTbtnWeb)
	ON_COMMAND(ID_TBTN_REFRESH, &CPanelInternetDev::OnTbtnRefresh)
	ON_UPDATE_COMMAND_UI(ID_TBTN_REFRESH, &CPanelInternetDev::OnUpdateTbtnRefresh)
END_MESSAGE_MAP()


// CPanelInternetDev 消息处理程序
BOOL CPanelInternetDev::InitGridCtrl()
{
	m_ImageList.Create(IDB_STAT,
		48,     // pixel size
		1,
		RGB(255,255,255));
	m_Grid.SetImageList(&m_ImageList);
	m_Grid.ModifyStyleEx(WS_EX_CLIENTEDGE, WS_EX_STATICEDGE); 

	m_Grid.SetEditable(FALSE);
	m_Grid.SetRowResize(FALSE);
	m_Grid.SetListMode();
	m_Grid.SetSingleRowSelection();
	m_Grid.EnableTitleTips(TRUE);
	m_Grid.EnableDragAndDrop();
	m_Grid.SetDefCellMargin(2);
	m_Grid.SetBkColor(RGB(255,255,255));// SetTextBkColor(RGB(0xFF, 0xFF, 0xE0));
	m_Grid.SetGridLines(GVL_VERT);

	TRY{
		m_Grid.SetRowCount(MAX_CHN_NUM);
		m_Grid.SetColumnCount(GRID_COLUMN_SIZE);
		//m_Grid.SetFixedRowCount();
		//m_Grid.SetFixedColumnCount(FIXED_COLUMN_COUNT);

		m_Grid.SetColumnWidth(0, 20);	//序号
		m_Grid.SetColumnWidth(1, 90);
		m_Grid.SetColumnWidth(2, 140);
		m_Grid.SetColumnWidth(3, 0);

		m_Grid.SetRowHeight(0, DEFAULT_ROW_HEIGHT);
	}CATCH (CMemoryException, e){
		e->ReportError();
		e->Delete();
		return FALSE;
	}
	END_CATCH
		//TestAddDataRow();
		InitGridRowNo();

	return TRUE;
}

void CPanelInternetDev::InitGridRowNo()
{
	CString csTmp;
	for (int i=0; i<m_Grid.GetRowCount(); i++)
	{
		csTmp.Format(_T("%d"), i+1);
		m_Grid.SetItemText(i, 0, csTmp);
	}
	m_Grid.Refresh();
}


#define TOOLBAR_ELE_SIZE	32
void CPanelInternetDev::InitToolbar()
{
	if(!m_ToolBar.Create(this, WS_CHILD | WS_VISIBLE | CBRS_TOP| CBRS_BORDER_3D) || !m_ToolBar.LoadToolBar(IDR_TOOL_INTERNET_DEVMGR));
	else {
		m_ToolBar.ModifyStyle(0, TBSTYLE_TOOLTIPS|TBSTYLE_FLAT);

		// Set up hot bar image lists.
		CImageList	imageList;
		CBitmap		bitmap;

		// Create and set the normal toolbar image list.
		bitmap.LoadBitmap(IDB_TOOLSBAR21);
		imageList.Create(TOOLBAR_ELE_SIZE, TOOLBAR_ELE_SIZE, ILC_COLORDDB|ILC_MASK, TOOLBAR_ELE_SIZE, 1);
		imageList.Add(&bitmap, RGB(255,255,255));
		m_ToolBar.SendMessage(TB_SETIMAGELIST, 0, (LPARAM)imageList.m_hImageList);
		imageList.Detach();
		bitmap.Detach();

		// Create and set the hot toolbar image list.
		bitmap.LoadBitmap(IDB_TOOLSBAR22);
		imageList.Create(TOOLBAR_ELE_SIZE, TOOLBAR_ELE_SIZE, ILC_COLORDDB|ILC_MASK, TOOLBAR_ELE_SIZE, 1);
		imageList.Add(&bitmap, RGB(255,255,255));
		m_ToolBar.SendMessage(TB_SETHOTIMAGELIST, 0, (LPARAM)imageList.m_hImageList);
		imageList.Detach();
		bitmap.Detach();

		// Create and set the disable toolbar image list.
		bitmap.LoadBitmap(IDB_TOOLSBAR20);
		imageList.Create(TOOLBAR_ELE_SIZE, TOOLBAR_ELE_SIZE, ILC_COLORDDB|ILC_MASK, TOOLBAR_ELE_SIZE, 1);
		imageList.Add(&bitmap, RGB(255,255,255));
		m_ToolBar.SendMessage(TB_SETDISABLEDIMAGELIST, 0, (LPARAM)imageList.m_hImageList);
		imageList.Detach();
		bitmap.Detach();

		if(m_ToolBar.GetSafeHwnd()) m_ToolBar.SetWindowPos(&wndTop, 0,0,264,45, SWP_SHOWWINDOW);
		m_ToolBar.GetToolBarCtrl().SetExtendedStyle(TBSTYLE_EX_DRAWDDARROWS);
	}
}

void CPanelInternetDev::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	if(m_Grid.GetSafeHwnd()){
		CRect rcGrid;
		m_Grid.GetWindowRect(rcGrid);
		ScreenToClient(rcGrid);
		rcGrid.bottom=cy-10;
		m_Grid.MoveWindow(rcGrid);
	}
}
BOOL CPanelInternetDev::OnInitDialog()
{
	CDialog::OnInitDialog();

	InitGridCtrl();
	InitToolbar();

	return TRUE;
}

void CPanelInternetDev::OnTbtnWeb()
{
	CString csUrl;
	if(g_objSysSettings.m_web_site.IsEmpty()){
		CString csNote, csText;
		csNote.LoadString(MB_TITLE_INFO);
		csText.LoadString(SYS_INFO1);
		MessageBox(csText, csNote, MB_ICONINFORMATION | MB_OK);
		return;
	}
	csUrl.Format(_T("http://%s:%d"), g_objSysSettings.m_web_site, g_objSysSettings.m_web_port);
	ShellExecute(this->GetSafeHwnd(), _T("open"), csUrl, NULL, NULL, SW_SHOWNORMAL);
}
void CPanelInternetDev::OnUpdateTbtnWeb(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(TRUE);
}

int CPanelInternetDev::ReadJson(char *pdata)
{
	Json::Reader reader;
	Json::Value json_object;
	char* json_document=pdata;
	if(!reader.parse(json_document, json_object)) return 0;

	CString csText1;
	const char *pValue=json_object["deviceUID"].asCString();
	if(strlen(pValue)>0){
		BSTRFromCStrU(CP_ACP, pValue, csText1);
		MessageBox(csText1);
	}

	pValue=json_object["deviceName"].asCString();
	if(strlen(pValue)>0){
		BSTRFromCStrU(CP_ACP, pValue, csText1);
		MessageBox(csText1);
	}
	return 1;
}

/*
{"result":"1"}
1 是表示成功
2 连接数据库失败
3 用户名密码错误
*/
void CPanelInternetDev::OnTbtnRefresh()
{
	/*
	CInternetSession session;
	CHttpFile *file = NULL;
	CString strURL  = _T("");
	CString strHtml = _T("");

	strURL.Format(_T("http://%s:%d/tutkweb/getdevices.php?account=%s&password=%s"), 
				  g_objSysSettings.m_web_site, g_objSysSettings.m_web_port, g_csLoginUser, g_csLoginPwd);

	try{ file = (CHttpFile*)session.OpenURL(strURL); }
	catch(CInternetException *m_pException){
		file = NULL;
		m_pException->m_dwError;
		m_pException->Delete();
		session.Close();
		MessageBox(_T("Internet Exception."));
		return;
	}
	char chLine[2048]={0};
	CString strLine;
	if(file != NULL){
		int i=0;
		while(file->ReadString(strLine) != NULL){
			strHtml += strLine;
			MessageBox(strLine);
			if(i>0){
				CStrFromWSTRU(CP_ACP, strLine, strLine.GetLength(), chLine);
				ReadJson(chLine);
			}
			i++;
		}

	}else{
		MessageBox(_T("Failed to open internet session."));
	}
	session.Close();
	file->Close();
	delete file;
	file = NULL;
	*/
}

void CPanelInternetDev::OnUpdateTbtnRefresh(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(TRUE); //m_bEnableRefresh
}

void CPanelInternetDev::FillDevInfo()
{
	
}