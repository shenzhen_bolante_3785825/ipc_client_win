// DevManageWifi.cpp : 实现文件
//

#include "stdafx.h"
#include "P2PCamLiveForPC.h"
#include "DevManageWifi.h"
#include "DevInfo.h"

// CDevManageWifi 对话框

IMPLEMENT_DYNAMIC(CDevManageWifi, CDialog)

CDevManageWifi::CDevManageWifi(CWnd* pParent /*=NULL*/)
	: CDialog(CDevManageWifi::IDD, pParent)
{
	m_nCurIndex=-1;
}

CDevManageWifi::~CDevManageWifi()
{
}

void CDevManageWifi::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO1, m_ctlWiFiSSID);
}


BEGIN_MESSAGE_MAP(CDevManageWifi, CDialog)
	ON_BN_CLICKED(IDC_BTN1, &CDevManageWifi::OnBnClickedBtn1)
	ON_BN_CLICKED(IDC_BTN2, &CDevManageWifi::OnBnClickedBtn2)
	ON_CBN_SELCHANGE(IDC_COMBO1, &CDevManageWifi::OnCbnSelchangeCombo1)
END_MESSAGE_MAP()


// CDevManageWifi 消息处理程序
void CDevManageWifi::OnBnClickedBtn1()
{
	if(m_pCurDevInfo==NULL || !m_pCurDevInfo->IsConnected()) return;

	CString csPwd;
	char chPwd[128]={0};
	SMsgAVIoctrlSetWifiReq req;
	GetDlgItemText(IDC_EDIT1, csPwd);
	memset(&req, 0, sizeof(req));
	strcpy((char *)req.ssid, (const char *)m_pCurDevInfo->m_wifiAP[m_nCurIndex].ssid);
	CStrFromWSTRU(CP_ACP, csPwd, csPwd.GetLength(),chPwd);
	strncpy((char *)req.password, chPwd, sizeof(req.password)-1);
	int nRet=m_pCurDevInfo->SendIOCtrl(IOTYPE_USER_IPCAM_SETWIFI_REQ, (tkUCHAR *)&req, sizeof(req));
	if(nRet<0){
		CString csFormat, csText, csNote;
		csNote.LoadString(MB_TITLE_INFO);
		GetDlgItemText(IDC_EDIT1, csText);
		if(csText.IsEmpty()){
			csFormat.LoadString(SETTING_FAIL);
			csText.Format(csFormat, nRet);
			MessageBox(csText, csNote, MB_ICONINFORMATION | MB_OK);
			return;
		}
	}
	CDialog::OnOK();
}

void CDevManageWifi::OnBnClickedBtn2()
{
	
	CDialog::OnCancel();
}

void CDevManageWifi::GetWiFiEncTypeBy(int index, CString &csEncType)
{
	switch(index){
		case 0x00:
			csEncType=_T("Invalid");
			break;

		case 0x01:
			csEncType=_T("None");
			break;

		case 0x02:
			csEncType=_T("WEP");
			break;

		case 0x03:
			csEncType=_T("WPA TKIP");
			break;

		case 0x04:
			csEncType=_T("WPA AES");
			break;

		case 0x05:
			csEncType=_T("WPA2 TKIP");
			break;

		case 0x06:
			csEncType=_T("WPA2 AES");
			break;

		case 0x07:
			csEncType=_T("WPA PSK TKIP");
			break;

		case 0x08:
			csEncType=_T("WPA_PSK_AES");
			break;

		case 0x09:
			csEncType=_T("WPA2 PSK TKIP");
			break;

		case 0x0A:
			csEncType=_T("WPA2 PSK AES");
			break;
	}
}

BOOL CDevManageWifi::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString csSSID;
	int n=0;
	for(n=0; n<m_pCurDevInfo->m_nWifiApNum; n++){
		BSTRFromCStrU(CP_ACP, (LPCSTR)m_pCurDevInfo->m_wifiAP[n].ssid, csSSID);
		m_ctlWiFiSSID.AddString(csSSID);
		if(m_pCurDevInfo->m_wifiAP[n].status==1) m_nCurIndex=n;
	}
	if(m_nCurIndex>-1){
		m_ctlWiFiSSID.SetCurSel(m_nCurIndex);
		CString csText;
		GetWiFiEncTypeBy(m_pCurDevInfo->m_wifiAP[m_nCurIndex].enctype, csText);
		SetDlgItemText(IDC_ENCRY_TYPE, csText);
		csText.Format(_T("%d%%"), m_pCurDevInfo->m_wifiAP[m_nCurIndex].signal);
		SetDlgItemText(IDC_SIGNAL_LENGTH, csText);
	}

	return TRUE;
}

void CDevManageWifi::OnCbnSelchangeCombo1()
{
	m_nCurIndex=m_ctlWiFiSSID.GetCurSel();
	if(m_nCurIndex<0) return;

	CString csText;
	GetWiFiEncTypeBy(m_pCurDevInfo->m_wifiAP[m_nCurIndex].enctype, csText);
	SetDlgItemText(IDC_ENCRY_TYPE, csText);
	csText.Format(_T("%d%%"), m_pCurDevInfo->m_wifiAP[m_nCurIndex].signal);
	SetDlgItemText(IDC_SIGNAL_LENGTH, csText);
}
