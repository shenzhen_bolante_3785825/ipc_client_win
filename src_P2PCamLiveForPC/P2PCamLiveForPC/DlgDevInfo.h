#pragma once
#include "afxcmn.h"


// CDlgDevInfo 对话框
class CDevInfo;

class CDlgDevInfo : public CDialog
{
	DECLARE_DYNAMIC(CDlgDevInfo)

public:
	CDlgDevInfo(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CDlgDevInfo();

// 对话框数据
	enum { IDD = DLG_DEV_INFO };

protected:
	CListCtrl m_ctrlList;	
	
	BOOL CheckBeforeSave();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	void FillDevSearchedToListCtrl();
	int  ReqRemoteSettings();

	DECLARE_MESSAGE_MAP()

public:
	BOOL m_bAddDev;
	CDevInfo *m_pCurDevInfo;

	afx_msg void OnBnClickedBtn3();
	afx_msg void OnBnClickedBtn4();
	afx_msg void OnBnClickedBtn1();
	afx_msg void OnBnClickedBtn2();
	afx_msg void OnBnClickedBtn5();	
	afx_msg void OnBnClickedSearch();
	virtual BOOL OnInitDialog();
	afx_msg void OnNMDblclkList2(NMHDR *pNMHDR, LRESULT *pResult);
};
