// DlgRemoteSettings.cpp : 实现文件
//

#include "stdafx.h"
#include "P2PCamLiveForPC.h"
#include "DlgRemoteSettings.h"
#include "DevInfo.h"
#include "AVIOCtrlDefs.h"
#include "DevModifyPasswd.h"
#include "DevManageWifi.h"

// CDlgRemoteSettings 对话框
IMPLEMENT_DYNAMIC(CDlgRemoteSettings, CDialog)

CDlgRemoteSettings::CDlgRemoteSettings(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgRemoteSettings::IDD, pParent)
{
	m_pCurDevInfo=NULL;
}

CDlgRemoteSettings::~CDlgRemoteSettings()
{
}

void CDlgRemoteSettings::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO1, m_ctlVideoQuality);
	DDX_Control(pDX, IDC_COMBO4, m_ctlVideoFlip);
	DDX_Control(pDX, IDC_COMBO5, m_ctlEnvMode);
	DDX_Control(pDX, IDC_COMBO6, m_ctlMotionDetect);
	DDX_Control(pDX, IDC_COMBO7, m_ctlRecMode);
}


BEGIN_MESSAGE_MAP(CDlgRemoteSettings, CDialog)
	ON_BN_CLICKED(IDC_BTN2, &CDlgRemoteSettings::OnBnClickedBtn2)
	ON_BN_CLICKED(IDC_REMOTE_MODI_PWD,  &CDlgRemoteSettings::OnBnClickedRemoteModiPwd)
	ON_BN_CLICKED(IDC_REMOTE_MODI_PWD2, &CDlgRemoteSettings::OnBnClickedRemoteModiPwd2)
END_MESSAGE_MAP()


BOOL CDlgRemoteSettings::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_ctlVideoQuality.AddString(_T("Max"));
	m_ctlVideoQuality.AddString(_T("High"));
	m_ctlVideoQuality.AddString(_T("Medium"));
	m_ctlVideoQuality.AddString(_T("Low"));
	m_ctlVideoQuality.AddString(_T("Min"));

	m_ctlVideoFlip.AddString(_T("Normal"));
	m_ctlVideoFlip.AddString(_T("Flip"));
	m_ctlVideoFlip.AddString(_T("Mirror"));
	m_ctlVideoFlip.AddString(_T("Flip&Mirror"));

	m_ctlEnvMode.AddString(_T("Indoor(50Hz)"));
	m_ctlEnvMode.AddString(_T("Indoor(60Hz)"));
	m_ctlEnvMode.AddString(_T("Outdoor"));
	m_ctlEnvMode.AddString(_T("Night"));

	m_ctlMotionDetect.AddString(_T("Off"));
	m_ctlMotionDetect.AddString(_T("Low"));
	m_ctlMotionDetect.AddString(_T("Medium"));
	m_ctlMotionDetect.AddString(_T("High"));
	m_ctlMotionDetect.AddString(_T("Max"));

	m_ctlRecMode.AddString(_T("Off"));
	m_ctlRecMode.AddString(_T("FullTime record"));
	m_ctlRecMode.AddString(_T("Alarm record"));
	m_ctlRecMode.AddString(_T("Manual record"));

	Data2UI();
	return TRUE;
}

void CDlgRemoteSettings::Data2UI()
{
	if(m_pCurDevInfo==NULL) return;

	m_ctlVideoQuality.SetCurSel(m_pCurDevInfo->m_nVideoQuality-1);
	m_ctlVideoFlip.SetCurSel(m_pCurDevInfo->m_nVideoFlip);
	m_ctlEnvMode.SetCurSel(m_pCurDevInfo->m_nEnvMode);
	m_ctlMotionDetect.SetCurSel(m_pCurDevInfo->m_nMotionDet);
	m_ctlRecMode.SetCurSel(m_pCurDevInfo->m_nRecMode);

	CString csWiFi("");
	int i=0;
	for(i=0; i<m_pCurDevInfo->m_nWifiApNum; i++){
		if(m_pCurDevInfo->m_wifiAP[i].status>0){
			BSTRFromCStrU(CP_ACP, (LPCSTR)m_pCurDevInfo->m_wifiAP[i].ssid, csWiFi);
			break;
		}
	}
	SetDlgItemText(IDC_CUR_WIFI, csWiFi);
	if(csWiFi.IsEmpty()) SetDlgItemText(IDC_CUR_WIFI_STATUS, _T("None"));
	else{
		switch(m_pCurDevInfo->m_wifiAP[i].status){
			case 1:
				SetDlgItemText(IDC_CUR_WIFI_STATUS, _T("Connected"));
				break;
			case 2:
				SetDlgItemText(IDC_CUR_WIFI_STATUS, _T("Unmatched password"));
				break;
			case 3:
				SetDlgItemText(IDC_CUR_WIFI_STATUS, _T("Weak signal and connected"));
				break;
			case 4:
				SetDlgItemText(IDC_CUR_WIFI_STATUS, _T("Disconnected or connected but not default gateway"));
			default:;
		}
	}
}

// CDlgRemoteSettings 消息处理程序
void CDlgRemoteSettings::OnBnClickedBtn2()
{
	CDialog::OnCancel();
}

void CDlgRemoteSettings::OnBnClickedRemoteModiPwd()//modify device password
{
	if(m_pCurDevInfo==NULL) return;
	CDevModifyPasswd dlg;
	dlg.DoModal();
}

void CDlgRemoteSettings::OnBnClickedRemoteModiPwd2()//manage network
{
	if(m_pCurDevInfo==NULL) return;
	CDevManageWifi dlg;
	dlg.m_pCurDevInfo=m_pCurDevInfo;
	dlg.DoModal();
}
