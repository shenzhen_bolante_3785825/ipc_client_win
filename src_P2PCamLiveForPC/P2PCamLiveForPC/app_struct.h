
#ifndef _APP_STRUCT_
#define _APP_STRUCT_

typedef struct _SNAPSHOT{
	int iFrameSize;
	SYSTEMTIME	stTime;
	BYTE  *pBuf;	
}SNAPSHOT;

//lpszSection: "WndPosSize"
typedef struct _MWND_POSSIZE
{
	int x, y;
	int cx, cy;

	void Reset() { x=y=cx=cy=0;	}
}MWND_POSSIZE;

#endif