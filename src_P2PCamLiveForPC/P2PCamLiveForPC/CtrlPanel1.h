#pragma once

#include "UI/ButtonST/WinXPButtonST.h"
#include "afxwin.h"
#include "afxcmn.h"

#define OM_FILL_DEV_INFO	WM_USER+1

// CCtrlPanel1 对话框
class CPanelLocalDev;
class CPanelInternetDev;

class CCtrlPanel1 : public CDialog
{
	DECLARE_DYNAMIC(CCtrlPanel1)

public:
	CCtrlPanel1(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CCtrlPanel1();

// 对话框数据
	enum { IDD = BAR_CTRL_PANEL1 };

protected:
	CWinXPButtonST m_ctlLanSearch;
	CWinXPButtonST m_ctlSysSetting;
	CWinXPButtonST m_ctlHist;
	CWinXPButtonST m_ctlAbout;
	CWinXPButtonST m_ctlCheck1;
	CWinXPButtonST m_ctlCheck2;
	CWinXPButtonST m_ctlCheck3;
	CWinXPButtonST m_ctlCheck4;

protected:
	CGridCtrl	m_Grid;
	CTabCtrl	m_ctlTab;

	CPanelLocalDev	  *m_pTab1;
	CPanelInternetDev *m_pTab2;
	int m_iCurPage;

	BOOL	  m_bEnableAdd;		//OnTbtnAdd, enabled or disable
	BOOL	  m_bEnableEdit;	//OnTbt
	BOOL	  m_bEnableDel;		//OnTbtnDel, enabled or disable

	void InitUI();
	void InitTabPages();
	void TabSwitch(int iKind);

	void InitVar();
	void InitToolbar();
	afx_msg void OnMiHistRecord();
	afx_msg void OnMiHistSnap();
	afx_msg LRESULT OnFillDevInfo(WPARAM wParam, LPARAM lParam);
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedMiLansearch();
	afx_msg void OnBnClickedMiPara();

	afx_msg void OnBnClickedCheck1();
	afx_msg void OnBnClickedCheck2();
	afx_msg void OnBnClickedCheck3();
	afx_msg void OnBnClickedCheck4();
	
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
	afx_msg void OnTcnSelchangeTab1(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedAbout();
	afx_msg void OnDestroy();
};
