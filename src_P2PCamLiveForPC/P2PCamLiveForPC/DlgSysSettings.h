#pragma once
#include "afxcmn.h"
#include "afxwin.h"


// CDlgSysSettings 对话框
class CDlgSysSettings : public CDialog
{
	DECLARE_DYNAMIC(CDlgSysSettings)

public:
	CDlgSysSettings(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CDlgSysSettings();

// 对话框数据
	enum { IDD = DLG_SYS_SETTINGS };

protected:
	CSpinButtonCtrl m_ctlSpin1;
	CSpinButtonCtrl m_ctlSpin2;
	CSpinButtonCtrl m_ctlSpin3;
	CButton m_ctlCheck1;
	CButton m_ctlCheck2;
	CButton m_ctlCheck3;
	CButton m_ctlCheck4;
	CButton m_ctlCheck5;
	CButton m_ctlCheck6;

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	void InitSpin();
	BOOL CheckBeforeSave();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedRemoteModiPwd();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBfolder2();
	afx_msg void OnBnClickedBfolder();

	afx_msg void OnDeltaposSpin1(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpin2(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpin3(NMHDR *pNMHDR, LRESULT *pResult);
};
