#pragma once
#include "afxwin.h"


// CDlgRemoteSettings 对话框
class CDevInfo;
class CDlgRemoteSettings : public CDialog
{
	DECLARE_DYNAMIC(CDlgRemoteSettings)

public:
	CDlgRemoteSettings(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CDlgRemoteSettings();

// 对话框数据
	enum { IDD = DLG_DEV_REMOTE_SETTING };

protected:
	CComboBox m_ctlVideoQuality;
	CComboBox m_ctlVideoFlip;
	CComboBox m_ctlEnvMode;
	CComboBox m_ctlMotionDetect;
	CComboBox m_ctlRecMode;

	void Data2UI();
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CDevInfo *m_pCurDevInfo;

	afx_msg void OnBnClickedBtn2();
	afx_msg void OnBnClickedRemoteModiPwd();
	afx_msg void OnBnClickedRemoteModiPwd2();
	virtual BOOL OnInitDialog();
};
