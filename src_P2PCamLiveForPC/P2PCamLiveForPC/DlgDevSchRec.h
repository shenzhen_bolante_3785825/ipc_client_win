#pragma once

#include "TimeTableUI.h"

// CDlgDevSchRec 对话框
class CDlgDevSchRec : public CDialog
{
	DECLARE_DYNAMIC(CDlgDevSchRec)

public:
	CDlgDevSchRec(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CDlgDevSchRec();

// 对话框数据
	enum { IDD = DLG_DEV_SCH_REC };

protected:
	CDC *m_pDC;

	CRect		m_grouprec;
	int			m_nModelID;
	ModelInfoList m_modeList;
	virtual void  DoDataExchange(CDataExchange* pDX); // DDX/DDV 支持
	
	DECLARE_MESSAGE_MAP()

public:
	CString      m_csWeekPlan[MINIDOWNROWLINE];

	virtual BOOL OnInitDialog();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnPaint();
	afx_msg void OnBnClickedBtn2();
	afx_msg void OnBnClickedBtn1();
};
