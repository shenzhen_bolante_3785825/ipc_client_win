#pragma once
#include "afxwin.h"


// CDevManageWifi 对话框
class CDevInfo;
class CDevManageWifi : public CDialog
{
	DECLARE_DYNAMIC(CDevManageWifi)

public:
	CDevManageWifi(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CDevManageWifi();

// 对话框数据
	enum { IDD = DLG_DEV_MANAGE_WIFI };

protected:
	void GetWiFiEncTypeBy(int index, CString &csEncType);
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	int m_nCurIndex;
	CDevInfo *m_pCurDevInfo;

	afx_msg void OnBnClickedBtn1();
	afx_msg void OnBnClickedBtn2();
	virtual BOOL OnInitDialog();
	CComboBox m_ctlWiFiSSID;
	afx_msg void OnCbnSelchangeCombo1();
};
