
#include "StdAfx.h"
#include "FullScreenHandler.h"
#include "VideoWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// redfine this to nop if you don't want tracing
#define FSTRACE TRACE

// Global object handles full-screen mode
CFullScreenHandler g_objFullScrnHandle;

CFullScreenHandler::CFullScreenHandler()
{
	m_rcRestore.SetRectEmpty();
	
}

CFullScreenHandler::~CFullScreenHandler()
{
}

//////////////////
// Resize frame so view's client area fills the entire screen. Use
// GetSystemMetrics to get the screen size -- the rest is pixel
// arithmetic.
//
void CFullScreenHandler::Maximize(CFrameWnd* pFrame, CWnd* pView)
{
	if(pFrame==NULL || pView==NULL) return;

	// get view rectangle
	CRect rcv;
	pView->GetWindowRect(&rcv);

	// get frame rectangle
	pFrame->GetWindowRect(m_rcRestore); // save for restore
	const CRect& rcf = m_rcRestore;		// frame rect

	//FSTRACE("Frame=(%d,%d) x (%d,%d)\n", rcf.left, rcf.top, rcf.Width(), rcf.Height());
	//FSTRACE("View =(%d,%d) x (%d,%d)\n", rcv.left, rcv.top, rcv.Width(), rcv.Height());

	// now compute new rect
	CRect rc(0,0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN));

	//FSTRACE("Scrn =(%d,%d) x (%d,%d)\n", rc.left, rc.top, rc.Width(), rc.Height());
	rc.left  += rcf.left  - rcv.left;
	rc.top   += rcf.top   - rcv.top;
	rc.right += 5;//rcf.right - rcv.right; //-10: offset
	rc.bottom+= rcf.bottom- rcv.bottom;

	//FSTRACE("New  =(%d,%d) x (%d,%d)\n", rc.left, rc.top, rc.Width(), rc.Height());

	// move frame!	
	pFrame->SetWindowPos(&CWnd::wndTopMost, rc.left, rc.top, rc.Width(), rc.Height(), SWP_SHOWWINDOW);
}

void CFullScreenHandler::Maximize(CFrameWnd* pFrame, CWnd* pView, CVideoWnd *pVideoWnd)
{
	if(pFrame==NULL || pView==NULL || pVideoWnd==NULL) return;

	CRect rcv;	pVideoWnd->GetRect(rcv);
	CPoint rcvPt1, rcvPt2;
	rcvPt1=rcv.TopLeft();	rcvPt2=rcv.BottomRight();
	::ClientToScreen(pView->GetSafeHwnd(), &rcvPt1);
	::ClientToScreen(pView->GetSafeHwnd(), &rcvPt2);
	rcv.SetRect(rcvPt1, rcvPt2);
	
	// get frame rectangle
	pFrame->GetWindowRect(m_rcRestore); // save for restore
	const CRect& rcf = m_rcRestore;		// frame rect		
	
	FSTRACE("Frame=(%d,%d) x (%d,%d)\n", rcf.left, rcf.top, rcf.right, rcf.bottom);
	FSTRACE("View =(%d,%d) x (%d,%d)\n", rcv.left, rcv.top, rcv.right, rcv.bottom);
	

	// now compute new rect
	CRect rc(0,0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN));
	FSTRACE("rc =(%d,%d) x (%d,%d)\n", rc.left, rc.top, rc.right, rc.bottom);		

	rc.left  =  rcf.left-rcv.left;
	rc.top   =  rcf.top-rcv.top;
	rc.right += 5;
	rc.bottom+= rcf.bottom-rcv.bottom;
	
	FSTRACE("New  =(%d,%d) x (%d,%d)\n", rc.left, rc.top, rc.Width(), rc.Height());

	// move frame!;		
	pFrame->SetWindowPos(&CWnd::wndTopMost, rc.left, rc.top, rc.Width(), rc.Height(), SWP_SHOWWINDOW);	
}

void CFullScreenHandler::Restore(CFrameWnd* pFrame)
{
	const CRect& rc = m_rcRestore;
	pFrame->SetWindowPos(&CWnd::wndNoTopMost, rc.left, rc.top, rc.Width(), rc.Height(), SWP_SHOWWINDOW);
	m_rcRestore.SetRectEmpty();
}

CSize CFullScreenHandler::GetMaxSize()
{
	CRect rc(0,0,GetSystemMetrics(SM_CXSCREEN),GetSystemMetrics(SM_CYSCREEN));
	rc.InflateRect(10,50);
	return rc.Size();
}
