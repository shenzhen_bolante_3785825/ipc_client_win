#pragma once


// CCtrlPanel2 对话框

class CCtrlPanel2 : public CDialog
{
	DECLARE_DYNAMIC(CCtrlPanel2)

public:
	CCtrlPanel2(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CCtrlPanel2();

// 对话框数据
	enum { IDD = BAR_CTRL_PANEL2 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
};
