#pragma once


// CPanelLocalDev 对话框
class CPanelLocalDev : public CDialog
{
	DECLARE_DYNAMIC(CPanelLocalDev)

public:
	CPanelLocalDev(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CPanelLocalDev();

// 对话框数据
	enum { IDD = PANEL_LOCAL_DEV };

	typedef enum { DEVSTA_OFFLINE, DEVSTA_START, DEVSTA_ONLINE, DEVSTA_IPCONFLICT, } DEVSTA_E;
	enum { GRID_COLUMN_SIZE=4, GRID_COL_DEVNAME=1, GRID_COL_DEVID=2, GRID_COL_ALARM=3,};//GRID_COL_IMG=2, 
	enum { ALARM_COL_IMG_0=0, ALARM_COL_IMG_1=5, };

protected:
	CGridCtrl	m_Grid;
	CImageList  m_ImageList;
	CToolBar	m_ToolBar;

	BOOL	  m_bEnableAdd;		//OnTbtnAdd, enabled or disable
	BOOL	  m_bEnableEdit;	//OnTbt
	BOOL	  m_bEnableDel;		//OnTbtnDel, enabled or disable

	void InitGridRowNo();
	BOOL InitGridCtrl();
	void InitToolbar();

	void UpdateTbtnStatus(int iRow);
	int  GetEmptyRow();

	afx_msg void OnTbtnAdd();
	afx_msg void OnUpdateTbtnAdd(CCmdUI* pCmdUI);
	afx_msg void OnTbtnModi();
	afx_msg void OnUpdateTbtnModi(CCmdUI* pCmdUI);
	afx_msg void OnTbtnDel();
	afx_msg void OnUpdateTbtnDel(CCmdUI* pCmdUI);
	afx_msg void OnTbtnStart();
	afx_msg void OnUpdateTbtnStart(CCmdUI* pCmdUI);
	afx_msg void OnTbtnStop();
	afx_msg void OnUpdateTbtnStop(CCmdUI* pCmdUI);
	afx_msg void OnGridRClick(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnGridDBLClick(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnGridLClick(NMHDR *pNotifyStruct, LRESULT* pResult);

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	void FillDevInfo();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
};
