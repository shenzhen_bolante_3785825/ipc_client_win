// P2PCamLiveForPC.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "P2PCamLiveForPC.h"
#include "MainFrm.h"

#include "P2PCamLiveForPCDoc.h"
#include "P2PCamLiveForPCView.h"
#include "DlgSysLogin.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CP2PCamLiveForPCApp
BEGIN_MESSAGE_MAP(CP2PCamLiveForPCApp, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, &CP2PCamLiveForPCApp::OnAppAbout)
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, &CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, &CWinApp::OnFileOpen)
END_MESSAGE_MAP()


// CP2PCamLiveForPCApp construction
CP2PCamLiveForPCApp::CP2PCamLiveForPCApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

CP2PCamLiveForPCApp theApp;

void CP2PCamLiveForPCApp::GetAppPath(CString &strAppPath)
{
	TCHAR lpszModulePath[300];
	long iPos=-1;
	GetModuleFileName(NULL, lpszModulePath, 300); 
	strAppPath = (CString)lpszModulePath; 
	iPos = strAppPath.ReverseFind('\\');
	strAppPath = strAppPath.Left(iPos+1);
}

inline void VerN2Str(UINT nVer, CString &csVer)
{
	UCHAR *pVer=(UCHAR *)&nVer;
	csVer.Format(_T("%d.%d.%d.%d"), *(pVer+3), *(pVer+2), *(pVer+1), *(pVer+0));
}

void CP2PCamLiveForPCApp::CtrlEnable(HWND hDlg, UINT nID, BOOL bEnable)
{
	HWND hWndItem=GetDlgItem(hDlg, nID);
	if(hWndItem) {
		CString csText;
		TCHAR strClsName[128];
		::GetClassName(hWndItem, strClsName, 128);
		csText=CString(strClsName);
		if(csText.Find(_T("Edit"))>=0){
			if(bEnable)	SendMessage(hWndItem, EM_SETREADONLY, FALSE, 0);
			else SendMessage(hWndItem, EM_SETREADONLY, TRUE, 0);
		}else EnableWindow(hWndItem, bEnable);
	}	
}

// CP2PCamLiveForPCApp initialization
BOOL CP2PCamLiveForPCApp::InitInstance()
{
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);
	CWinApp::InitInstance();

	GetAppPath(g_csAppPath);
	InitSys();
	g_objSysSettings.ReadSysSettingsFromDB();
	if(!SysLogin()) return FALSE;

	SetRegistryKey(_T("P2PCamLiveForPC"));
	
	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CP2PCamLiveForPCDoc),
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CP2PCamLiveForPCView));
	if(!pDocTemplate) return FALSE;
	AddDocTemplate(pDocTemplate);

	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);
	if(!ProcessShellCommand(cmdInfo)) return FALSE;

	m_pMainWnd->ShowWindow(SW_MAXIMIZE);
	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();
	
	CString csTitle, csTmp, csTmp1;
	csTmp1.LoadString(APP_TITLE);
	VerN2Str(THIS_APP_VER, csTmp);
	csTitle.Format(_T("%s(V%s)"), csTmp1, csTmp);
	::SetWindowText(m_pMainWnd->GetSafeHwnd(), csTitle);

	return TRUE;
}

void CP2PCamLiveForPCApp::InitSys()
{
	//
	g_stWndPosSize.Reset();
	//lpszSection: "WndPosSize"
	g_stWndPosSize.x	=GetProfileInt(_T("WndPosSize"), _T("x"), 0);
	g_stWndPosSize.y	=GetProfileInt(_T("WndPosSize"), _T("y"), 0);
	g_stWndPosSize.cx	=GetProfileInt(_T("WndPosSize"), _T("cx"), 0);
	g_stWndPosSize.cy	=GetProfileInt(_T("WndPosSize"), _T("cy"), 0);
	if(g_stWndPosSize.cx<0) g_stWndPosSize.cx=0;
	if(g_stWndPosSize.cy<0) g_stWndPosSize.cy=0;

}

BOOL CP2PCamLiveForPCApp::SysLogin()
{
	do{
		CDlgSysLogin dlg;
		dlg.DoModal();
		if(dlg.m_nPass==LOGIN_RESTART) continue;
		else if(dlg.m_nPass==LOGIN_OK) return TRUE;
		else return FALSE;
	}while(TRUE);
}

CP2PCamLiveForPCView *CP2PCamLiveForPCApp::GetActiveView()
{
	CP2PCamLiveForPCView *pView=NULL;
	CMainFrame *pMainWnd=(CMainFrame *)AfxGetMainWnd();	
	pView=(CP2PCamLiveForPCView *)pMainWnd->GetActiveView();	

	return pView;
}







CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CAboutDlg::OnBnClickedOk)
END_MESSAGE_MAP()

// App command to run the dialog
void CP2PCamLiveForPCApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}
// CP2PCamLiveForPCApp message handlers
void CAboutDlg::OnBnClickedOk()
{
	OnOK();
}
