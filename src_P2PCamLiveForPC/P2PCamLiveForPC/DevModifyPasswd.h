#pragma once


// CDevModifyPasswd 对话框
class CDevModifyPasswd : public CDialog
{
	DECLARE_DYNAMIC(CDevModifyPasswd)

public:
	CDevModifyPasswd(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CDevModifyPasswd();

// 对话框数据
	enum { IDD = DLG_DEV_MODIFY_PASSWD };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CString m_csOldPwd;
	CString m_csNewPwd;
	CString m_csConfirmPwd;

	afx_msg void OnBnClickedBtn1();
	afx_msg void OnBnClickedBtn2();
};
