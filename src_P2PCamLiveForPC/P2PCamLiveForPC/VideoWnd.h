// VideoWnd.h: interface for the CVideoWnd class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_VIDEOWND_H__0F7A99BF_39BB_41BE_B1FE_C9B5F796F5A4__INCLUDED_)
#define AFX_VIDEOWND_H__0F7A99BF_39BB_41BE_B1FE_C9B5F796F5A4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define IMG_STD_WIDTH		160
#define IMG_STD_HEIGHT		120
#define VWND_TITLE			22
#define VWND_STATUS			18
#define TOOLBARBTN_WIDTHHEI	16
#define MARGIN_LR			2

#define TIMER_ACCURACY		1	//定义时钟分辨率，以ms为单位

#define MDETECTION_DATA_MAXSIZE	400*1024

//--{{H264-------------------------------
// #define MAX_MD_NUMBER				4
// #define DEFAULT_ALARM_COLOR			RGB(255, 0, 0)
// #define DEFAULT_MD_COLOR			RGB(0, 255, 0)
//--}}H264-------------------------------

#include <Mmsystem.h>
#pragma comment(lib, "winmm.lib")

#include "P2PAVCore.h"
#include "H264Decoder.h"


#define MAXSIZE_PCM_DATA	2560



class CStaticEx;
class CP2PCamLiveForPCView;
class CPicture;

class CVideoWnd  : public CObject
{
public:
 	typedef enum _RGN_TYPE { RGN_TITLE, RGN_VIDEO, RGN_STAT, RGN_NOUSE} RGN_TYPE_E;

	CVideoWnd(CP2PCamLiveForPCView *pParent);
	virtual ~CVideoWnd();

	#ifdef _DEBUG
		LARGE_INTEGER m_swFreq, m_swStart, m_swStop; //StopWatch(int start0stop1)
		float m_etime; //elapsed time
		void  StopWatch(int start0stop1);
	#endif

protected:
	CRITICAL_SECTION	 m_csecSnapshot; //Synchronize Snapshot
	CRITICAL_SECTION	 m_csecCount;

	CStaticEx	*m_pCtlVideoArea;
	CP2PCamLiveForPCView *m_pParent;
	CWnd		*m_pMainWnd;
	HDC			m_hDC;
	HWND		m_hWnd;
	BYTE		*m_pBufAudio;

	CFont	 m_fontStatu;
	COLORREF m_clrStatuTextFg;
	CString  m_csStatuText, m_csStatuTextFormat;
	CString  m_csWndTitle;
	CFont	 m_fontWndTitle;
	COLORREF m_clrWndTitleTextFg, m_clrWndTitleTextShd;

	//m_rgnA4: Around the excess part of this video
	//		   When the video is displayed in proportion
	CRgn m_rgnA1, m_rgnA2, m_rgnA3, m_rgnA4; //title, video area, status, no use
	BOOL m_bStretch, m_bRecordOSD, m_bSoundOSD, m_bTalkOSD, m_bResumeOSD;
	BOOL m_bHasVideo;
	unsigned short m_nCodec_id;

	CRect	m_rectWnd;
	short   m_nFixChnNo; //After initialization, will never change
	CBrush	m_brFocus, m_clrVideoBkGnd;
	bool	m_bFocus;
	CImageList	m_imgList;

	OUT_FRAME m_outFrame;
	BITMAPINFOHEADER m_bmiHead;
	BYTE	  *m_pBufBmp24;
	int		  m_nDataSize;
	tkPVOID   m_pH264Dec;
	CPicture  *m_objShow;

protected:
	void DrawTitle(CDC *pDC);
	void DrawStatus(CDC *pDC);
	void DrawButton(CDC *pDC);
	void DestoryVideoWnd();	
	BOOL CheckAndCreateDir(LPCTSTR lpPath);
	int  WriteBmp24(char *chPath, char *pBuffer, int nDataSize);

	//@retu -1: has one fullscreen window, but isnt own;
	//		 0: without fullscreen window;
	//		 1:	has one fullscreen window, but is own;
	short GetFullScrnState();	

public: //attrib
	static UINT ms_nAccuracy;	//define resolution of timer

	BOOL HitMe(CPoint point);
	void SetFocus()					{ m_bFocus=true;			}
	void KillFocus()				{ m_bFocus=false;			}
	BOOL IsFocus()					{ return m_bFocus;			}
	void SetStretch(BOOL bStretch=TRUE);
	BOOL GetStretch()				{ return m_bStretch;		}
	short GetChnNo()				{ return m_nFixChnNo;		}
	void  SetOtherVideoAreaSize(BOOL bHasWndMax);
	void  GetRect(CRect &rect);
	void  GetRGN(RGN_TYPE_E eType, CRect &rect);

	void  AVPlay(tkCHAR *pData, tkINT32 nSize, tkVOID *pFrmInfo, tkINT32 nFrmInfoSize);
	void  SetStatus(tkINT32 nType,tkPVOID pObj, tkINT32 nValue,  CString csInfo);
	void  HandleRecvIOCtrl(tkINT32 nIOType, tkCHAR *pIOData, tkINT32 nIODataSize, tkPVOID pObj);
	void  IconSetAudio(BOOL bShow);
	void  IconSetSpeak(BOOL bShow);
	int   Snapshot();

public:
	void  InitVar(short nFixChnNo, BOOL bStretch);
	void  SetRect(int left, int top, int right, int bottom);
	void  SetRect(LPRECT pRect);

	void CreateRgn(int cx, int cy);
	void CreateRgn();	//for fullscreen
	void CompuVideoRect(CRect &in_outRect);
	void DrawVWnd(CDC *pDC);
	void  OnCreate();

	short CheckClickVideBtn(UINT nFlags, CPoint point); //@retu: >0 click aim

};

#endif // !defined(AFX_VIDEOWND_H__0F7A99BF_39BB_41BE_B1FE_C9B5F796F5A4__INCLUDED_)
