// PanelLocalDev.cpp : 实现文件
//

#include "stdafx.h"
#include "P2PCamLiveForPC.h"
#include "P2PCamLiveForPCView.h"
#include "PanelLocalDev.h"
#include "DlgDevInfo.h"
#include "DevInfo.h"


// CPanelLocalDev 对话框
IMPLEMENT_DYNAMIC(CPanelLocalDev, CDialog)

CPanelLocalDev::CPanelLocalDev(CWnd* pParent /*=NULL*/)
	: CDialog(CPanelLocalDev::IDD, pParent)
{
	m_bEnableAdd =TRUE;
	m_bEnableEdit=FALSE;
	m_bEnableDel =FALSE;
}

CPanelLocalDev::~CPanelLocalDev()
{
}

void CPanelLocalDev::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_GRID, m_Grid);
}


BEGIN_MESSAGE_MAP(CPanelLocalDev, CDialog)
	ON_COMMAND(ID_TBTN_ADD, OnTbtnAdd)
	ON_UPDATE_COMMAND_UI(ID_TBTN_ADD, OnUpdateTbtnAdd)
	ON_COMMAND(ID_TBTN_MODI, &CPanelLocalDev::OnTbtnModi)
	ON_UPDATE_COMMAND_UI(ID_TBTN_MODI, &CPanelLocalDev::OnUpdateTbtnModi)
	ON_COMMAND(ID_TBTN_DEL, &CPanelLocalDev::OnTbtnDel)
	ON_UPDATE_COMMAND_UI(ID_TBTN_DEL, &CPanelLocalDev::OnUpdateTbtnDel)
	ON_COMMAND(ID_TBTN_START, &CPanelLocalDev::OnTbtnStart)
	ON_UPDATE_COMMAND_UI(ID_TBTN_START, &CPanelLocalDev::OnUpdateTbtnStart)
	ON_COMMAND(ID_TBTN_STOP, &CPanelLocalDev::OnTbtnStop)
	ON_UPDATE_COMMAND_UI(ID_TBTN_STOP, &CPanelLocalDev::OnUpdateTbtnStop)

	ON_NOTIFY(NM_CLICK, IDC_GRID,  OnGridLClick)
	ON_NOTIFY(NM_DBLCLK, IDC_GRID, OnGridDBLClick)
	ON_NOTIFY(NM_RCLICK, IDC_GRID, OnGridRClick)

	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()


// CPanelLocalDev 消息处理程序
BOOL CPanelLocalDev::OnInitDialog()
{
	CDialog::OnInitDialog();

	InitToolbar();	
	InitGridCtrl();

	return TRUE;
}

BOOL CPanelLocalDev::InitGridCtrl()
{
	m_ImageList.Create(IDB_STAT,
		48,     // pixel size
		1,
		RGB(255,255,255));
	m_Grid.SetImageList(&m_ImageList);
	m_Grid.ModifyStyleEx(WS_EX_CLIENTEDGE, WS_EX_STATICEDGE); 

	m_Grid.SetEditable(FALSE);
	m_Grid.SetRowResize(FALSE);
	m_Grid.SetListMode();
	m_Grid.SetSingleRowSelection();
	m_Grid.EnableTitleTips(TRUE);
	m_Grid.EnableDragAndDrop();
	m_Grid.SetDefCellMargin(2);
	m_Grid.SetBkColor(RGB(255,255,255));// SetTextBkColor(RGB(0xFF, 0xFF, 0xE0));
	m_Grid.SetGridLines(GVL_VERT);

	TRY{
		m_Grid.SetRowCount(MAX_CHN_NUM);
		m_Grid.SetColumnCount(GRID_COLUMN_SIZE);
		//m_Grid.SetFixedRowCount();
		//m_Grid.SetFixedColumnCount(FIXED_COLUMN_COUNT);

		m_Grid.SetColumnWidth(0, 20);	//序号
		m_Grid.SetColumnWidth(1, 90);
		m_Grid.SetColumnWidth(2, 140);
		m_Grid.SetColumnWidth(3, 0);

		m_Grid.SetRowHeight(0, DEFAULT_ROW_HEIGHT);
	}CATCH (CMemoryException, e){
		e->ReportError();
		e->Delete();
		return FALSE;
	}
	END_CATCH

	return TRUE;
}

void CPanelLocalDev::FillDevInfo()
{
	CString csTmp;
	int i=0, nTotalRow=m_Grid.GetRowCount();
	for(i=0; i<nTotalRow; i++)
	{
		csTmp.Format(_T("%d"), i+1);
		m_Grid.SetItemText(i, 0, csTmp);
	}

	CP2PCamLiveForPCView *pView=CP2PCamLiveForPCApp::GetActiveView();
	CPtrArray *ptrArray=pView->m_objVWndMgr.GetDevInfos();
	CDevInfo *pDevInfo=NULL;
	int iRow=0;
	for(i=0; i<nTotalRow; i++){
		pDevInfo=(CDevInfo *)ptrArray->GetAt(i);
		if(pDevInfo->GetUID().IsEmpty()) continue;
		m_Grid.SetItemText(iRow, GRID_COL_DEVNAME, pDevInfo->GetDevName());
		m_Grid.SetItemText(iRow, GRID_COL_DEVID,   pDevInfo->GetUID());
		m_Grid.SetItemData(iRow, GRID_COL_DEVID,   (LPARAM)pDevInfo);
		iRow++;
	}
	m_Grid.Refresh();
}

void CPanelLocalDev::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);
}

#define TOOLBAR_ELE_SIZE	32
void CPanelLocalDev::InitToolbar()
{
	if(!m_ToolBar.Create(this, WS_CHILD | WS_VISIBLE | CBRS_TOP| CBRS_BORDER_3D) || !m_ToolBar.LoadToolBar(IDR_TOOLDEVMGR));
	else {
		m_ToolBar.ModifyStyle(0, TBSTYLE_TOOLTIPS|TBSTYLE_FLAT);

		// Set up hot bar image lists.
		CImageList	imageList;
		CBitmap		bitmap;

		// Create and set the normal toolbar image list.
		bitmap.LoadBitmap(IDB_TOOLSBAR1);
		imageList.Create(TOOLBAR_ELE_SIZE, TOOLBAR_ELE_SIZE, ILC_COLORDDB|ILC_MASK, TOOLBAR_ELE_SIZE, 1);
		imageList.Add(&bitmap, RGB(255,255,255));
		m_ToolBar.SendMessage(TB_SETIMAGELIST, 0, (LPARAM)imageList.m_hImageList);
		imageList.Detach();
		bitmap.Detach();

		// Create and set the hot toolbar image list.
		bitmap.LoadBitmap(IDB_TOOLSBAR2);
		imageList.Create(TOOLBAR_ELE_SIZE, TOOLBAR_ELE_SIZE, ILC_COLORDDB|ILC_MASK, TOOLBAR_ELE_SIZE, 1);
		imageList.Add(&bitmap, RGB(255,255,255));
		m_ToolBar.SendMessage(TB_SETHOTIMAGELIST, 0, (LPARAM)imageList.m_hImageList);
		imageList.Detach();
		bitmap.Detach();

		// Create and set the disable toolbar image list.
		bitmap.LoadBitmap(IDB_TOOLSBAR0);
		imageList.Create(TOOLBAR_ELE_SIZE, TOOLBAR_ELE_SIZE, ILC_COLORDDB|ILC_MASK, TOOLBAR_ELE_SIZE, 1);
		imageList.Add(&bitmap, RGB(255,255,255));
		m_ToolBar.SendMessage(TB_SETDISABLEDIMAGELIST, 0, (LPARAM)imageList.m_hImageList);
		imageList.Detach();
		bitmap.Detach();

		if(m_ToolBar.GetSafeHwnd()) m_ToolBar.SetWindowPos(&wndTop, 0,0,264,45, SWP_SHOWWINDOW);
		m_ToolBar.GetToolBarCtrl().SetExtendedStyle(TBSTYLE_EX_DRAWDDARROWS);
	}
}

void CPanelLocalDev::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	if(m_Grid.GetSafeHwnd()){
		CRect rcGrid;
		m_Grid.GetWindowRect(rcGrid);
		ScreenToClient(rcGrid);
		rcGrid.bottom=cy-10;
		m_Grid.MoveWindow(rcGrid);
	}
}

void CPanelLocalDev::OnTbtnAdd() 
{
	CP2PCamLiveForPCView *pView=CP2PCamLiveForPCApp::GetActiveView();
	CDevInfo *pDevInfo=pView->m_objVWndMgr.GetEmptyDevInfo();
	if(pDevInfo==NULL){
		CString csIDSstr, csIDSstrTitle;
		csIDSstrTitle.LoadString(MB_TITLE_INFO);
		csIDSstr.LoadString(DEVINFO_CHN_EMPTY);
		MessageBox(csIDSstr, csIDSstrTitle, MB_ICONINFORMATION | MB_OK);
		return;
	}

	CDlgDevInfo dlg;
	dlg.m_bAddDev=TRUE;
	dlg.m_pCurDevInfo=NULL;
	if(IDOK==dlg.DoModal()){
		int iRow=GetEmptyRow();
		if(iRow>=0)	{
			m_Grid.SetItemData(iRow, GRID_COL_DEVID, (LPARAM)dlg.m_pCurDevInfo);
			m_Grid.SetItemText(iRow, GRID_COL_DEVNAME, dlg.m_pCurDevInfo->GetDevName());
			m_Grid.SetItemText(iRow, GRID_COL_DEVID, dlg.m_pCurDevInfo->GetUID());
			m_Grid.RedrawRow(iRow);
		}
	}

}
void CPanelLocalDev::OnUpdateTbtnAdd(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bEnableAdd);
}

void CPanelLocalDev::OnTbtnModi() 
{
	LPARAM lparam=NULL;
	int iRow=m_Grid.GetCurRow();
	if(iRow<0) return;
	lparam=m_Grid.GetItemData(iRow, GRID_COL_DEVID);
	if(lparam==NULL) return;

	CDlgDevInfo dlg;
	dlg.m_bAddDev=FALSE;
	dlg.m_pCurDevInfo=(CDevInfo *)m_Grid.GetItemData(iRow, GRID_COL_DEVID);
	dlg.DoModal();
}
void CPanelLocalDev::OnUpdateTbtnModi(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bEnableEdit);
}

void CPanelLocalDev::OnTbtnDel() 
{
	LPARAM lparam=NULL;
	int iRow=m_Grid.GetCurRow();
	if(iRow<0) return;
	lparam=m_Grid.GetItemData(iRow, GRID_COL_DEVID);
	if(lparam==NULL) return;

	CDevInfo *pCurDevInfo=(CDevInfo *)m_Grid.GetItemData(iRow, GRID_COL_DEVID);
	CString csNote, csText;
	csNote.LoadString(MB_TITLE_INFO);
	csText.LoadString(DEVINFO_DEL_TIPS);
	csText.Append(_T("("));
	csText.Append(pCurDevInfo->GetDevName());
	csText.Append(_T(")"));
	if(IDYES==MessageBox(csText, csNote,MB_ICONINFORMATION|MB_YESNO)){
		bool bErr=false;
		CString csDBFile, csSQL;
		csDBFile.Format(_T("%scamera.db"), g_csAppPath);
		try{
			g_SQLite3DB.open(csDBFile);
			csSQL.Format(_T("delete from device_info where dev_uid='%s'"), pCurDevInfo->GetUID());
			g_SQLite3DB.execDML(csSQL);
		}catch(...){ bErr=true; }
		g_SQLite3DB.close();

		if(!bErr){
			m_Grid.SetItemText(iRow, GRID_COL_DEVNAME,	_T(""));
			m_Grid.SetItemText(iRow, GRID_COL_DEVID,	_T(""));
			m_Grid.RedrawRow(iRow);
			m_Grid.SetItemData(iRow, GRID_COL_DEVID, NULL);
			UpdateTbtnStatus(iRow);
		}
	}
}
void CPanelLocalDev::OnUpdateTbtnDel(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bEnableDel);

}


void CPanelLocalDev::OnTbtnStart() 
{


}
void CPanelLocalDev::OnUpdateTbtnStart(CCmdUI* pCmdUI) 
{

}

void CPanelLocalDev::OnTbtnStop() 
{


}
void CPanelLocalDev::OnUpdateTbtnStop(CCmdUI* pCmdUI) 
{	

}

void CPanelLocalDev::UpdateTbtnStatus(int iRow)
{
	if(iRow<0) {
		m_bEnableDel =FALSE;
		m_bEnableEdit=FALSE;
	}else{
		CString csText;
		csText=m_Grid.GetItemText(iRow, GRID_COL_DEVID);
		if(!csText.IsEmpty()){
			m_bEnableDel =TRUE;
			m_bEnableEdit=TRUE;
		}else{
			m_bEnableDel =FALSE;
			m_bEnableEdit=FALSE;
		}
	}
}

int CPanelLocalDev::GetEmptyRow()
{
	CString csText;
	int i=0, nTotal=m_Grid.GetRowCount();
	for(i=0; i<nTotal; i++){
		csText=m_Grid.GetItemText(i, GRID_COL_DEVID);
		if(csText.IsEmpty()) break;
	}

	if(i>=nTotal) return -1;
	else return i;
}

void CPanelLocalDev::OnGridLClick(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;
	UpdateTbtnStatus(pItem->iRow);
	if(pItem->iRow < 0)  return;

	//if(pItem->iColumn==GRID_COL_ALARM) StopAlarmSound();
	//UpdateTbtnState(pItem->iRow);

	*pResult = 0;
}

void CPanelLocalDev::OnGridDBLClick(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;
	UpdateTbtnStatus(pItem->iRow);
	if(pItem->iRow<0)  return;

	CDevInfo *pCurDevInfo=(CDevInfo *)m_Grid.GetItemData(pItem->iRow, GRID_COL_DEVID);
	if(pCurDevInfo==NULL) return;
	if(pCurDevInfo->GetDevType()!=CDevInfo::DEV_TYPE_LOCAL){
		pCurDevInfo->StopAudio();
		pCurDevInfo->StopVideo();
		pCurDevInfo->StopSpeak();
		pCurDevInfo->DisconnectDev();
	}
	pCurDevInfo->ConnectDev(CDevInfo::DEV_TYPE_LOCAL, TRUE);
	*pResult = 0;
}

void CPanelLocalDev::OnGridRClick(NMHDR *pNotifyStruct, LRESULT* pResult)
{

}

