// P2PCamLiveForPCView.h : interface of the CP2PCamLiveForPCView class
//

#pragma once

#include "VideoWndMgr.h"

class CP2PCamLiveForPCDoc;
class CP2PCamLiveForPCView : public CView
{
protected: // create from serialization only
	CP2PCamLiveForPCView();
	DECLARE_DYNCREATE(CP2PCamLiveForPCView)

// Attributes
public:
	CP2PCamLiveForPCDoc* GetDocument() const;

// Operations
public:

// Overrides
public:
	CVideoWndMgr m_objVWndMgr;

	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:

// Implementation
public:
	virtual ~CP2PCamLiveForPCView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	virtual void OnInitialUpdate();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	
	void StopAllAudio();
	void StopAllSpeak();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};

#ifndef _DEBUG  // debug version in P2PCamLiveForPCView.cpp
inline CP2PCamLiveForPCDoc* CP2PCamLiveForPCView::GetDocument() const
   { return reinterpret_cast<CP2PCamLiveForPCDoc*>(m_pDocument); }
#endif

