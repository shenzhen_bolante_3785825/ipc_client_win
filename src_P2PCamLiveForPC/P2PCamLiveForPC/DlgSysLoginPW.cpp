// DlgSysLoginPW.cpp : 实现文件
//

#include "stdafx.h"
#include "P2PCamLiveForPC.h"
#include "DlgSysLoginPW.h"
#include "MD5.h"

// CDlgSysLoginPW 对话框

IMPLEMENT_DYNAMIC(CDlgSysLoginPW, CDialog)

CDlgSysLoginPW::CDlgSysLoginPW(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgSysLoginPW::IDD, pParent)
{

}

CDlgSysLoginPW::~CDlgSysLoginPW()
{
}

void CDlgSysLoginPW::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDlgSysLoginPW, CDialog)
	ON_BN_CLICKED(IDOK, &CDlgSysLoginPW::OnBnClickedOk)
END_MESSAGE_MAP()

BOOL CDlgSysLoginPW::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetDlgItemText(IDC_EDIT1, g_csLoginUser);
	return TRUE;
}

// CDlgSysLoginPW 消息处理程序
void CDlgSysLoginPW::OnBnClickedOk()
{
	bool bOk=false;
	CString csSQL, csUser, csNewPassw1, csNewPassw2;
	CString csIDSstr, csIDSstrTitle;

	csIDSstrTitle.LoadString(MB_TITLE_INFO);
	GetDlgItemText(IDC_EDIT1, csUser);
	if(csUser.IsEmpty()){
		csIDSstr.LoadString(LOGIN_INFO3);
		MessageBox(csIDSstr, csIDSstrTitle, MB_ICONINFORMATION | MB_OK);
		return;
	}
	
	GetDlgItemText(IDC_EDIT2, csNewPassw1);
	csNewPassw1.TrimLeft();	csNewPassw1.TrimRight();
	if(csNewPassw1.IsEmpty()) {
		csIDSstr.LoadString(LOGINPW_INFO1);
		MessageBox(csIDSstr, csIDSstrTitle, MB_ICONINFORMATION | MB_OK);
		return;
	}

	GetDlgItemText(IDC_EDIT3, csNewPassw2);
	csNewPassw2.TrimLeft();	csNewPassw2.TrimRight();
	if(csNewPassw2.IsEmpty()) {
		csIDSstr.LoadString(LOGINPW_INFO2);
		MessageBox(csIDSstr, csIDSstrTitle, MB_ICONINFORMATION | MB_OK);
		return;
	}else {	//The new password and confirm password is not matching.
		if(csNewPassw1!=csNewPassw2){
			csIDSstr.LoadString(LOGINPW_INFO3);
			MessageBox(csIDSstr, csIDSstrTitle, MB_ICONQUESTION | MB_OK);
			return;	
		}
	}

	CMD5 myMD5; char *pPassW=NULL;
	CString csMD5Passw, csDBFile;
	char strNewPassw2[128]={0};
	//change password
	pPassW=NULL;
	CStrFromWSTRU(CP_ACP, csNewPassw2, csNewPassw2.GetLength(), strNewPassw2);
	pPassW=myMD5.MD5_Algorithm(strNewPassw2);
	if(pPassW) BSTRFromCStrU(CP_ACP, pPassW, csMD5Passw);

	bool bErr=false;
	csDBFile.Format(_T("%scamera.db"), g_csAppPath);
	try{
		g_SQLite3DB.open(csDBFile);
		csSQL.Format(_T("update user_info set user_name='%s',user_pwd='%s' where db_id=1"), csUser, csMD5Passw);
		g_SQLite3DB.execDML(csSQL);
	}catch(...){ bErr=true; }
	g_SQLite3DB.close();

	if(bErr){
		csIDSstr.LoadString(LOGINPW_INFO4);
		MessageBox(csIDSstr, csIDSstrTitle, MB_ICONQUESTION | MB_OK);
	}else{
		theApp.WriteProfileString(_T("Settings"), _T("user"), csUser);
		g_csLoginUser=csUser;
		OnOK();
	}
}


