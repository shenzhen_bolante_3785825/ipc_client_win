// stdafx.cpp : source file that includes just the standard includes
// P2PCamLiveForPC.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

MWND_POSSIZE g_stWndPosSize;
CString		 g_csAppPath=_T("");
CString		 g_csLoginUser(_T("")), g_csLoginPwd(_T(""));
CSysSettings g_objSysSettings;
CppSQLite3DB g_SQLite3DB;



BOOL CStrFromWSTRU(IN UINT codePage, IN LPCWSTR wstr, IN UINT len, OUT char *pStr)
{
	if( len > 0 )
	{
		size_t mblen = WideCharToMultiByte(codePage, 0, wstr, len, NULL, 0, NULL, NULL);
		if( mblen > 0 )
		{
			char *buffer = (char *)CoTaskMemAlloc(mblen+1);
			ZeroMemory(buffer, mblen+1);
			if( WideCharToMultiByte(codePage, 0, wstr, len, buffer, mblen, NULL, NULL) )
			{
				//buffer[mblen] = '\0';
				memcpy(pStr, buffer, mblen);
				*(pStr+mblen)='\0';
				CoTaskMemFree(buffer);
				return TRUE;
			}
		}
	}else *pStr='\0';

	return FALSE;
};

BOOL BSTRFromCStrU(IN UINT codePage, IN LPCSTR s, OUT CString &csWStr)
{
	csWStr=_T("");
	int wideLen = MultiByteToWideChar(codePage, 0, s, -1, NULL, 0);
	if( wideLen > 0 )
	{
		WCHAR* wideStr = (WCHAR*)CoTaskMemAlloc(wideLen*sizeof(WCHAR));
		if( NULL != wideStr)
		{
			BSTR bstr;

			ZeroMemory(wideStr, wideLen*sizeof(WCHAR));
			MultiByteToWideChar(codePage, 0, s, -1, wideStr, wideLen);
			bstr = SysAllocStringLen(wideStr, wideLen-1);
			CoTaskMemFree(wideStr);

			csWStr=bstr;
			SysFreeString(bstr);
			return TRUE;
		}
	}
	return FALSE;
}

void SplitString2(CString source, CStringArray &dest, char division)
{
	if(source.IsEmpty()) return;
	int iLenSource=source.GetLength();
	TCHAR c=source.GetAt(iLenSource-1);
	if(c!=division) source+=CString(division);
	int iSize=0, i=0, j=0;

	dest.RemoveAll();
	for(i=0; i<source.GetLength(); i++){
		if(source.GetAt(i)==division){
			dest.Add(source.Left(i));
			iSize=dest.GetSize()-1;			
			for(j=0; j<iSize; j++){
				dest[iSize]=dest[iSize].Right(dest[iSize].GetLength()-dest[j].GetLength()-1);
			}
		}//if(source.GetAt(i)==division)
	}
}
