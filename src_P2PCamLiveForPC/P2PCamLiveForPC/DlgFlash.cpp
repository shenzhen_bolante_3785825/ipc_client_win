// DlgFlash.cpp : implementation file
//

#include "stdafx.h"
#include "P2PCamLiveForPC.h"
#include "DlgFlash.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgFlash dialog

#define TIMER_ID		100
#define	TIMER_TIME		2000

#define ROUND_CONER_W	16
#define ROUND_CONER_H	16
#define LWA_COLORKEY	0x00000001
#define LWA_ALPHA		0x00000002

CDlgFlash::CDlgFlash(LPCTSTR lpTipText,UINT nTimeWait_sec, CWnd* pParent /*=NULL*/)
	: CDialog(CDlgFlash::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgFlash)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_csTipInfo=lpTipText;
	m_crTipText=RGB(255,255,255);
	m_crBk=RGB(0,0,0);

	m_nTimeWait_sec=nTimeWait_sec*1000;
}


void CDlgFlash::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgFlash)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
	
}


BEGIN_MESSAGE_MAP(CDlgFlash, CDialog)
	//{{AFX_MSG_MAP(CDlgFlash)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_WM_CTLCOLOR()
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgFlash message handlers
BOOL CDlgFlash::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetDlgItemText(IDC_TEXT, m_csTipInfo);

	m_font.CreateFont(0,0,0,0,600,0,0,0,
		ANSI_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FF_SCRIPT ,_T("MS Shell Dlg"));

	SetWindowLong(GetSafeHwnd(),GWL_EXSTYLE,GetWindowLong(GetSafeHwnd(),GWL_EXSTYLE)|0x80000);
	typedef BOOL (WINAPI *FSetLayeredWindowAttributes)(HWND,COLORREF,BYTE,DWORD);
	FSetLayeredWindowAttributes SetLayeredWindowAttributes ;
	HINSTANCE hInst = LoadLibrary(_T("User32.DLL"));	
	SetLayeredWindowAttributes = (FSetLayeredWindowAttributes)GetProcAddress(hInst,"SetLayeredWindowAttributes");
	if (SetLayeredWindowAttributes) SetLayeredWindowAttributes(GetSafeHwnd(),RGB(0,0,0),128,LWA_ALPHA); 
	FreeLibrary(hInst);

	CRgn objRgn; 
	GetClientRect(m_rectClient);   
	SetWindowRgn(NULL, FALSE);
	objRgn.CreateRoundRectRgn(m_rectClient.left, m_rectClient.top, m_rectClient.right, m_rectClient.bottom,ROUND_CONER_W,ROUND_CONER_H);
	SetWindowRgn(objRgn, TRUE);
	objRgn.DeleteObject();

	if(m_nTimeWait_sec<2) m_nTimeWait_sec=2000;
	m_timerID=SetTimer(TIMER_ID, m_nTimeWait_sec, NULL);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgFlash::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent==TIMER_ID) PostMessage(WM_CLOSE);
	
	CDialog::OnTimer(nIDEvent);
}

void CDlgFlash::OnDestroy() 
{
	CDialog::OnDestroy();
	
}

BOOL CDlgFlash::PreTranslateMessage(MSG* pMsg) 
{
	if(pMsg->message==WM_KEYDOWN){
		if(pMsg->wParam==VK_ESCAPE|| pMsg->wParam==VK_RETURN) return TRUE;
	}
	
	return CDialog::PreTranslateMessage(pMsg);
}

HBRUSH CDlgFlash::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	
	if(nCtlColor==CTLCOLOR_STATIC){
		if(pWnd->GetDlgCtrlID()==IDC_TEXT)
		{
			pDC->SetTextColor(m_crTipText);
			pDC->SetBkMode(TRANSPARENT);
			return (HBRUSH)::GetStockObject(NULL_BRUSH);
		}
	}
	return hbr;
}

void CDlgFlash::OnPaint() 
{
	CPaintDC dc(this);

	CBrush brushBlue(m_crBk);
	CBrush* pOldBrush = dc.SelectObject(&brushBlue);

	CRect rect=m_rectClient;
	rect.DeflateRect(2,2,3,3);
	dc.RoundRect(rect.left, rect.top, rect.right, rect.bottom,ROUND_CONER_W,ROUND_CONER_H);

	dc.SelectObject(pOldBrush);

	// Do not call CDialog::OnPaint() for painting messages
}
