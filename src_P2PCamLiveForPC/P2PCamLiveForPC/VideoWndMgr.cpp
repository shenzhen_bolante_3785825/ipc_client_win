// VideoWndMgr.cpp: implementation of the CVideoWndMgr class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "P2PCamLiveForPC.h"
#include "VideoWndMgr.h"
#include "P2PCamLiveForPCView.h"
#include "MainFrm.h"
#include "DevInfo.h"
#include "err_IOTC.h"
#include "AVIOCtrlDefs.h"


#include <vector>
#include <algorithm>
#include <iostream>

#define	TIMERID_RECV_IOCTRL		103

//////////////////////////////////////////////////////////////////////
// CVideoWndMgr
//////////////////////////////////////////////////////////////////////
CVideoWndMgr::CVideoWndMgr(CP2PCamLiveForPCView *pParent, ENUM_MODE nVWndMode)
{
	m_pParent	=pParent;
	m_nVWndMode	=nVWndMode;
	m_nCurPageNo=0;

	ASSERT(m_pParent!=NULL);
	m_crBackground = RGB(24,100,104);//RGB(0,0,0);//GetSysColor(COLOR_3DFACE)
	m_wndbkBrush.CreateSolidBrush(m_crBackground);
	m_wndbkBrushUnusedChan.CreateSolidBrush(RGB(0,0,0));
	m_rgnExcessArea.CreateRectRgn(0,0,0,0);
	m_rgnDontUseChannelArea.CreateRectRgn(0,0,0,0);

	CVideoWnd *pVideoWnd=NULL;
	CDevInfo  *pDevInfo=NULL;
	for(int i=0; i<MAX_CHN_NUM; i++)
	{
		pVideoWnd=new CVideoWnd(m_pParent);
		pVideoWnd->InitVar(i,FALSE);
		m_ptrVWnds.Add(pVideoWnd);

		pDevInfo=new CDevInfo(i, pVideoWnd);
		apiRegCB_OnAVFrame(pDevInfo->GetP2PHandle(), OnAVFrame, this);
		apiRegCB_OnStatus(pDevInfo->GetP2PHandle(),  OnStatus,  this);
		apiRegCB_OnRecvIOCtrl(pDevInfo->GetP2PHandle(), OnRecvIOCtrl, this);
		m_ptrDevInfo.Add(pDevInfo);
	}

	//load devices from DB
	ReadDevInfosFromDB();

	pVideoWnd=(CVideoWnd *)m_ptrVWnds[0];
	pVideoWnd->SetFocus();
	
	InitializeCriticalSection(&m_critical);
	m_nIndexFullScrn=-1;

	m_bThreadExit=FALSE;
	DWORD threadID=0;
	m_hHandleIOCtrl = CreateThread(NULL, 0, ThreadHandleRecvIOCtrl, (tkPVOID)this, 0, (tkULONG *)&threadID);
	if(NULL==m_hHandleIOCtrl)  TRACE("ThreadHandleRecvIOCtrl, failed to create thread\n");
}

CVideoWndMgr::~CVideoWndMgr()
{
	m_bThreadExit=TRUE;
	if(m_wndbkBrush.GetSafeHandle()) m_wndbkBrush.DeleteObject();
	if(m_wndbkBrushUnusedChan.GetSafeHandle()) m_wndbkBrushUnusedChan.DeleteObject();
	DeleteCriticalSection(&m_critical);

	CVideoWnd *pVideoWnd=NULL;
	CDevInfo  *pDevInfo=NULL;
	for(int i=0; i<MAX_CHN_NUM; i++)
	{
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[i];
		pDevInfo =(CDevInfo *)m_ptrDevInfo[i];
		if(pVideoWnd) {
			apiRegCB_OnAVFrame(pDevInfo->GetP2PHandle(), NULL, this);
			apiRegCB_OnStatus(pDevInfo->GetP2PHandle(),  NULL,  this);
			apiRegCB_OnRecvIOCtrl(pDevInfo->GetP2PHandle(), NULL, this);
			delete pVideoWnd;
		}
		if(pDevInfo) delete pDevInfo;
	}

	if(m_hHandleIOCtrl){ WaitForSingleObject(m_hHandleIOCtrl,INFINITE); m_hHandleIOCtrl=NULL;}
	ST_QUEUE_SENDIO *pQueueSendIO=NULL;
	while(!m_ptrRecvedIOCtrl.IsEmpty()){
		pQueueSendIO=(ST_QUEUE_SENDIO *)m_ptrRecvedIOCtrl.RemoveHead();
		if(pQueueSendIO) delete pQueueSendIO;
	}
}

tkULONG WINAPI CVideoWndMgr::ThreadHandleRecvIOCtrl(tkPVOID lpPara)
{
	CVideoWndMgr *pThis=(CVideoWndMgr *)lpPara;
	pThis->DoHandleRecvIOCtrl();
	return 0L;
}

tkINT32 CVideoWndMgr::DoHandleRecvIOCtrl()
{
	CDevInfo *pDevInfo=NULL;
	ST_QUEUE_SENDIO *pQueue=NULL;
	while(!m_bThreadExit){
		if(m_ptrRecvedIOCtrl.IsEmpty()){
			Sleep(200);
			continue;
		}

		pQueue=(ST_QUEUE_SENDIO *)m_ptrRecvedIOCtrl.RemoveHead();
		TRACE(_T("DoRecvedIOCtrl, nIOType=0x%X\n"), pQueue->nIOType);
		switch(pQueue->nIOType)
		{
			case IOTYPE_USER_IPCAM_GETSTREAMCTRL_RESP:{
				SMsgAVIoctrlGetStreamCtrlResq *pResp=(SMsgAVIoctrlGetStreamCtrlResq *)pQueue->ioData;
				TRACE(_T("DoRecvedIOCtrl, IOTYPE_USER_IPCAM_GETSTREAMCTRL_RESP, nIODataSize=%d, channel=%d,quality=%d\n"), 
					pQueue->nIODataSize, pResp->channel,pResp->quality);
				pDevInfo=(CDevInfo *)m_ptrDevInfo[pQueue->nTag];
				pDevInfo->m_nVideoQuality=pResp->quality;
				}break;

			case IOTYPE_USER_IPCAM_GET_VIDEOMODE_RESP:{
					SMsgAVIoctrlGetVideoModeResp *pResp=(SMsgAVIoctrlGetVideoModeResp *)pQueue->ioData;
					TRACE(_T("DoRecvedIOCtrl, IOTYPE_USER_IPCAM_GET_VIDEOMODE_RESP, nIODataSize=%d, channel=%d, mode=%d\n"), 
						pQueue->nIODataSize, pResp->channel,pResp->mode);
					pDevInfo=(CDevInfo *)m_ptrDevInfo[pQueue->nTag];
					pDevInfo->m_nVideoFlip=pResp->mode;	
				}break;

			case IOTYPE_USER_IPCAM_GET_ENVIRONMENT_RESP:{
				SMsgAVIoctrlGetEnvironmentResp *pResp=(SMsgAVIoctrlGetEnvironmentResp *)pQueue->ioData;
				TRACE(_T("DoRecvedIOCtrl, IOTYPE_USER_IPCAM_GET_ENVIRONMENT_RESP, nIODataSize=%d, channel=%d, mode=%d\n"), 
					pQueue->nIODataSize, pResp->channel,pResp->mode);
				pDevInfo=(CDevInfo *)m_ptrDevInfo[pQueue->nTag];
				pDevInfo->m_nEnvMode=pResp->mode;
				}break;

			case IOTYPE_USER_IPCAM_GETMOTIONDETECT_RESP:{
				SMsgAVIoctrlGetMotionDetectResp *pResp=(SMsgAVIoctrlGetMotionDetectResp *)pQueue->ioData;
				TRACE(_T("DoRecvedIOCtrl, IOTYPE_USER_IPCAM_GETMOTIONDETECT_RESP, nIODataSize=%d, channel=%d,m_nMotionDet=%d\n"), 
					pQueue->nIODataSize, pResp->channel,pResp->sensitivity);
				pDevInfo=(CDevInfo *)m_ptrDevInfo[pQueue->nTag];
				if(pResp->sensitivity==0) pDevInfo->m_nMotionDet=0;
				else if(pResp->sensitivity>0  && pResp->sensitivity<=25)  pDevInfo->m_nMotionDet=1;
				else if(pResp->sensitivity>25 && pResp->sensitivity<=50)  pDevInfo->m_nMotionDet=2;
				else if(pResp->sensitivity>50 && pResp->sensitivity<=75)  pDevInfo->m_nMotionDet=3;
				else if(pResp->sensitivity>75 && pResp->sensitivity<=100) pDevInfo->m_nMotionDet=4;
				}break;

			case IOTYPE_USER_IPCAM_GETRECORD_RESP:{
				SMsgAVIoctrlGetRecordResq *pResp=(SMsgAVIoctrlGetRecordResq *)pQueue->ioData;
				TRACE(_T("DoRecvedIOCtrl, IOTYPE_USER_IPCAM_GETRECORD_RESP, nIODataSize=%d, channel=%d,recordType=%d\n"), 
					pQueue->nIODataSize, pResp->channel,pResp->recordType);
				pDevInfo=(CDevInfo *)m_ptrDevInfo[pQueue->nTag];
				if(pResp->recordType>=0 && pResp->recordType<=3) pDevInfo->m_nRecMode=pResp->recordType;
				}break;
			
			case IOTYPE_USER_IPCAM_LISTWIFIAP_RESP:{
				SMsgAVIoctrlListWifiApResp *pResp=(SMsgAVIoctrlListWifiApResp *)pQueue->ioData;
				pDevInfo=(CDevInfo *)m_ptrDevInfo[pQueue->nTag];
				tkCHAR *pAP=NULL;
				SWifiAp *pWifiAp=NULL;
				int nNum=0;
				for(int i=0; i<pResp->number; i++)
				{
					pAP=(pQueue->ioData +sizeof(pResp->number)+sizeof(SWifiAp)*i);
					pWifiAp=(SWifiAp *)pAP;
					if(strlen(pWifiAp->ssid)==0) continue;

					memcpy((tkCHAR *)&pDevInfo->m_wifiAP[nNum], pAP, sizeof(SWifiAp));
					nNum++;
				}
				pDevInfo->m_nWifiApNum=nNum;
				}break;

			case IOTYPE_USER_IPCAM_DEVINFO_RESP:{
				SMsgAVIoctrlDeviceInfoResp *pResp=(SMsgAVIoctrlDeviceInfoResp *)pQueue->ioData;
				pDevInfo=(CDevInfo *)m_ptrDevInfo[pQueue->nTag];
				memcpy(&pDevInfo->m_devInfo, pResp, sizeof(SMsgAVIoctrlDeviceInfoResp));
				}break;

			default:;
		}
		if(pQueue){
			delete pQueue;
			pQueue=NULL;
		}
	}
	return 0;
}

tkVOID WINAPI CVideoWndMgr::OnAVFrame(tkCHAR *pData, tkINT32 nSize, tkVOID *pFrmInfo, tkINT32 nFrmInfoSize, tkUINT32 nTag, tkVOID *param)
{
	CVideoWndMgr *pThis=(CVideoWndMgr *)param;
	if(pThis->m_bThreadExit) return;

	//Only for testing. In fact play audio & video in another thread
	CDevInfo *pDevInfo=(CDevInfo *)pThis->m_ptrDevInfo[nTag];
	if(pDevInfo->m_pVideoWnd) pDevInfo->m_pVideoWnd->AVPlay(pData, nSize, pFrmInfo, nFrmInfoSize);
	//pThis->AVPlay(pData, nSize, pFrmInfo, nFrmInfoSize, nTag);
}

tkVOID WINAPI CVideoWndMgr::OnStatus(tkINT32 nType, tkINT32 nValue, tkPVOID pObj, tkUINT32 nTag, tkVOID *param)
{
	CVideoWndMgr *pThis=(CVideoWndMgr *)param;
	if(pThis->m_bThreadExit) return;

	char *pMyUID=apiGetUID(pObj);
	TRACE("OnStatus, uid=%s, nType=%d, nValue=%d\n", pMyUID, nType, nValue);
	//TRACE(_T("OnStatus, DevNum=%d, cam%d, nType=%d, nValue=%d, param=0x%X\n"), 
	//		  pThis->m_ptrObj.GetCount(), nTag, nType, nValue, param);

	//EnterCriticalSection(&pThis->m_critical);
		CString csInfo("");
		CDevInfo *pDevInfo=(CDevInfo *)pThis->m_ptrDevInfo[nTag];
		if(pDevInfo==NULL) return;

		pThis->Code2Str(nValue, csInfo);
		if(nValue==SINFO_CONNECTED) pDevInfo->SetFlagConnected(TRUE);
		else if(nValue==AV_ER_SESSION_CLOSE_BY_REMOTE   || nValue==AV_ER_REMOTE_TIMEOUT_DISCONNECT ||
				nValue==IOTC_ER_SESSION_CLOSE_BY_REMOTE || nValue==IOTC_ER_REMOTE_TIMEOUT_DISCONNECT)
			pDevInfo->SetFlagConnected(FALSE);
		pDevInfo->m_pVideoWnd->SetStatus(nType, pObj, nValue, csInfo);
		//start video
		if(nValue==SINFO_CONNECTED && pDevInfo->IsAndStartVideo()){
			pDevInfo->m_pVideoWnd->IconSetAudio(FALSE);
			pDevInfo->m_pVideoWnd->IconSetSpeak(FALSE);
			pDevInfo->StartVideo();
		}
	//LeaveCriticalSection(&pThis->m_critical);
}

tkVOID CVideoWndMgr::AppendIOCtrl(tkINT32 nIOType, tkCHAR *pIOData, tkINT32 nIODataSize, tkPVOID pObj, tkUINT32 nTag)
{
	ST_QUEUE_SENDIO *pQueueSendIO=new ST_QUEUE_SENDIO;
	memset(pQueueSendIO,0,sizeof(ST_QUEUE_SENDIO));
	pQueueSendIO->nIOType =nIOType;
	memcpy(pQueueSendIO->ioData, pIOData, nIODataSize);
	pQueueSendIO->nIODataSize=nIODataSize;
	pQueueSendIO->pObj    =pObj;
	pQueueSendIO->nTag	  =nTag;
	m_ptrRecvedIOCtrl.AddTail(pQueueSendIO);
}

tkVOID WINAPI CVideoWndMgr::OnRecvIOCtrl(tkINT32 nIOType, tkCHAR *pIOData, tkINT32 nIODataSize, tkPVOID pObj, tkUINT32 nTag, tkVOID *param)
{
	TRACE(_T("OnRecvIOCtrl, nIOType=0x%X, pObj=0x%X, cam%d, param=0x%X\n"), nIOType, pObj, nTag, param);
	CVideoWndMgr *pThis=(CVideoWndMgr *)param;
	if(pThis->m_bThreadExit) return;

	CDevInfo *pDevInfo=(CDevInfo *)pThis->m_ptrDevInfo[nTag];
	pDevInfo->m_pVideoWnd->HandleRecvIOCtrl(nIOType, pIOData, nIODataSize, pObj);
	pThis->AppendIOCtrl(nIOType, pIOData, nIODataSize, pObj, nTag);
}

void CVideoWndMgr::Code2Str(int nCode, CString &csInfo)
{
	switch(nCode)
	{
	case SINFO_CONNECTING:
		csInfo=_T("Connecting...");
		break;

	case SINFO_CONNECTED:
		csInfo=_T("Connected");
		break;

	case SINFO_DISCONNECTED:
		csInfo=_T("Disconnected");
		break;

	case -22:
	case -20015:
		csInfo=_T("The remote site closed");
		break;

	case -23:
	case -20016:
		csInfo=_T("Disconnected because timeout");
		break;

	case -19:
		csInfo=_T("Offline");
		break;

	case -13:
	case -20011:
		csInfo=_T("");
		break;

	case -20009:
		csInfo=_T("Incorrect view account or password");
		break;

	default:
		csInfo.Format(_T("error code=%d"), nCode);
	}
}


void CVideoWndMgr::ReadDevInfosFromDB()
{
	CString csDBFile, csSQL=_T("select * from device_info order by db_id");
	csDBFile.Format(_T("%scamera.db"), g_csAppPath);
	try{
		int nDevCount=0;
		CString csDevName, csDevUID, csViewAcc, csViewPwd;
		CString w0,w1,w2,w3,w4,w5,w6;
		CDevInfo *pDevInfo=NULL;
		g_SQLite3DB.open(csDBFile);
		CppSQLite3Query SQLiteQuery = g_SQLite3DB.execQuery(csSQL);
		while(!SQLiteQuery.eof()){
			pDevInfo  =(CDevInfo *)m_ptrDevInfo[nDevCount];

			csDevUID  =SQLiteQuery.fieldValue(_T("dev_uid"));
			csDevName =SQLiteQuery.fieldValue(_T("dev_name"));
			csViewAcc =SQLiteQuery.fieldValue(_T("view_acc"));
			csViewPwd =SQLiteQuery.fieldValue(_T("view_pwd"));

			w0	=SQLiteQuery.fieldValue(_T("record_plan_w0"));
			w1	=SQLiteQuery.fieldValue(_T("record_plan_w1"));
			w2	=SQLiteQuery.fieldValue(_T("record_plan_w2"));
			w3	=SQLiteQuery.fieldValue(_T("record_plan_w3"));
			w4	=SQLiteQuery.fieldValue(_T("record_plan_w4"));
			w5	=SQLiteQuery.fieldValue(_T("record_plan_w5"));
			w6	=SQLiteQuery.fieldValue(_T("record_plan_w6"));
			pDevInfo->SetDevInfo(csDevName, csDevUID, csViewAcc, csViewPwd);
			pDevInfo->SetRecPlan(w0,w1,w2,w3,w4,w5,w6);

			SQLiteQuery.nextRow();
			nDevCount++;
			if(nDevCount>=MAX_CHN_NUM) break;
		}
		SQLiteQuery.finalize();
	}catch(...){}
	g_SQLite3DB.close();
}

int CVideoWndMgr::RandInThreshold()
{
    const int VECTOR_SIZE = 8;
    typedef std::vector<int> IntVector;
    typedef std::vector<int>::iterator IntVectorIt;	
    IntVector Numbers(VECTOR_SIZE);
    IntVectorIt start, end;
	
    // Initialize vector Numbers
    Numbers[0] = 20;   Numbers[1]= 30;
    Numbers[2] = 100;  Numbers[3]= 400;
    Numbers[4] = 50;  Numbers[5] = 60;
    Numbers[6] = 90;  Numbers[7] = 500;
	
    start = Numbers.begin();   // location of first
    end = Numbers.end();       // one past the location
	std::random_shuffle(start, end);
	
    return (*start);
}

void CVideoWndMgr::SetMode(ENUM_MODE nVWndMode)
{
	if(nVWndMode==m_nVWndMode) return;

	m_nCurPageNo=0;
	m_nIndexFullScrn=-1;
	switch (nVWndMode)
	{
		case MODE1:
			CalcuMode1();
			break;
			
		case MODE2:
			CalcuMode2();
			break;
			
		case MODE3:
			CalcuMode3();
			break;
			
		case MODE4:
			CalcuMode4();
			break;
			
		default:;
	}

	if(nVWndMode<m_nVWndMode){	//release excess video window from tail
		short nOldVWndIndexFocus=-1;
		CVideoWnd *pVideoWnd=NULL;
		for(short i=m_nVWndMode-1; i>=nVWndMode; i--){
			pVideoWnd=(CVideoWnd *)m_ptrVWnds[i];
			if(pVideoWnd->IsFocus()) nOldVWndIndexFocus=i;
			pVideoWnd->KillFocus();
		}
		if(nOldVWndIndexFocus!=-1){
			pVideoWnd=(CVideoWnd *)m_ptrVWnds[nVWndMode-1];
			pVideoWnd->SetFocus();
		}
	}
	
	m_nVWndMode=nVWndMode;
	if(m_pParent) m_pParent->Invalidate();
}

void CVideoWndMgr::SetWholeWndSize(LPRECT rectWholeWnd)
{
	m_rectWholeWnd.CopyRect(rectWholeWnd);
	switch (m_nVWndMode)
	{
		case MODE1:
			CalcuMode1();
			break;

		case MODE2:
			CalcuMode2();
			break;

		case MODE3:
			CalcuMode3();
			break;

		case MODE4:
			CalcuMode4();
			break;

		default:;
	}
}

void CVideoWndMgr::VideoWndRectNull(ENUM_MODE nVWndMode, int nCurPageNo)
{
	//if(nCurPageNo<=0) return;
	
	int nWndIndex1=0, nWndIndex2=0, i=0;
	nWndIndex1=nCurPageNo*(int)nVWndMode;
	nWndIndex2=nWndIndex1+(int)nVWndMode-1;
	
	CVideoWnd *pVideoWnd=NULL;
	for(i=nWndIndex1-1; i>=0; i--)
	{
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[i];
		pVideoWnd->SetRect(0,0,0,0);
	}
	
	for(i=nWndIndex2+1; i<MAX_CHN_NUM; i++)
	{
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[i];
		pVideoWnd->SetRect(0,0,0,0);
	}
}


void CVideoWndMgr::CalcuMode1()
{
	int iCenterX=m_rectWholeWnd.Width()/2;
	int iCenterY=m_rectWholeWnd.Height()/2;
	
	//CRect rectTmp;
	CRgn rgnTmp[MODE1], rgnTmp1, rgnTmp2;	
	CVideoWnd *pVideoWnd=NULL;
	VERIFY(rgnTmp1.CreateRectRgn(0, 0, 0, 0));
	VERIFY(rgnTmp2.CreateRectRgn(0, 0, 0, 0));
	m_rgnDontUseChannelArea.SetRectRgn(0,0,0,0);
	
	VideoWndRectNull(MODE1, m_nCurPageNo);
	int nIndexBase=m_nCurPageNo*MODE1;
	int nWndIndex =0;

	//1. calculate area of video window
	nWndIndex=nIndexBase+0;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[0].CreateRectRgn(0, 0, iCenterX-WNDSPACING, iCenterY-WNDSPACING));	
		pVideoWnd->SetRect(0,0,iCenterX-WNDSPACING, iCenterY-WNDSPACING);
	}else{
		rgnTmp2.SetRectRgn(0, 0, iCenterX-WNDSPACING, iCenterY-WNDSPACING);
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}
		
	nWndIndex=nIndexBase+1;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[1].CreateRectRgn(iCenterX+WNDSPACING, 0, m_rectWholeWnd.Width(), iCenterY-WNDSPACING));
		pVideoWnd->SetRect(iCenterX+WNDSPACING, 0, m_rectWholeWnd.Width(), iCenterY-WNDSPACING);
	}else{
		rgnTmp2.SetRectRgn(iCenterX+WNDSPACING, 0, m_rectWholeWnd.Width(), iCenterY-WNDSPACING);
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}

	nWndIndex=nIndexBase+2;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[2].CreateRectRgn(0, iCenterY+WNDSPACING, iCenterX-WNDSPACING, m_rectWholeWnd.Height()));
		pVideoWnd->SetRect(0, iCenterY+WNDSPACING, iCenterX-WNDSPACING, m_rectWholeWnd.Height());
	}else{
		rgnTmp2.SetRectRgn(0, iCenterY+WNDSPACING, iCenterX-WNDSPACING, m_rectWholeWnd.Height());
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}

	nWndIndex=nIndexBase+3;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[3].CreateRectRgn(iCenterX+WNDSPACING, iCenterY+WNDSPACING, m_rectWholeWnd.Width(), m_rectWholeWnd.Height()));
		pVideoWnd->SetRect(iCenterX+WNDSPACING, iCenterY+WNDSPACING, m_rectWholeWnd.Width(), m_rectWholeWnd.Height());
	}else{
		rgnTmp2.SetRectRgn(iCenterX+WNDSPACING, iCenterY+WNDSPACING, m_rectWholeWnd.Width(), m_rectWholeWnd.Height());
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}

	//2. calculate excess area of video window
	rgnTmp1.CombineRgn(&rgnTmp[0],  &rgnTmp[1], RGN_OR);
	rgnTmp1.CombineRgn(&rgnTmp1, &rgnTmp[2], RGN_OR);
	rgnTmp1.CombineRgn(&rgnTmp1, &rgnTmp[3], RGN_OR);
	m_rgnExcessArea.SetRectRgn(&m_rectWholeWnd);
	m_rgnExcessArea.CombineRgn(&m_rgnExcessArea, &rgnTmp1, RGN_XOR);
	m_rgnExcessArea.CombineRgn(&m_rgnExcessArea, &m_rgnDontUseChannelArea, RGN_XOR);
}

void CVideoWndMgr::CalcuMode2()
{
	int iEqualW=m_rectWholeWnd.Width()/3,  iWTmp=0;
	int iEqualH=m_rectWholeWnd.Height()/3, iHTmp=0;
	if((m_rectWholeWnd.Width() % 3)==2)  iWTmp=1;
	if((m_rectWholeWnd.Height() % 3)==2) iHTmp=1;
	
	CPoint ptRef1, ptRef2;
	//1. calculate reference point
	ptRef1.x=iEqualW+iWTmp;		ptRef1.y=iEqualH+iWTmp;
	ptRef2.x=iEqualW*2+iWTmp;	ptRef2.y=iEqualH*2+iWTmp;
	
	//CRect rectTmp;
	CRgn rgnTmp[MODE2], rgnTmp1, rgnTmp2;
	CVideoWnd *pVideoWnd=NULL;
	VERIFY(rgnTmp1.CreateRectRgn(0, 0, 0, 0));
	VERIFY(rgnTmp2.CreateRectRgn(0, 0, 0, 0));
	m_rgnDontUseChannelArea.SetRectRgn(0,0,0,0);
	
	VideoWndRectNull(MODE2, m_nCurPageNo);
	int nIndexBase=m_nCurPageNo*MODE2;
	int nWndIndex=0;

	//2. calculate area of video window
	nWndIndex=nIndexBase+0;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[0].CreateRectRgn(0, 0, ptRef2.x-WNDSPACING, ptRef2.y-WNDSPACING));
		pVideoWnd->SetRect(0, 0, ptRef2.x-WNDSPACING, ptRef2.y-WNDSPACING);	
	}else{
		rgnTmp2.SetRectRgn(0, 0, ptRef2.x-WNDSPACING, ptRef2.y-WNDSPACING);
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}
	
	nWndIndex=nIndexBase+1;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[1].CreateRectRgn(ptRef2.x+WNDSPACING, 0, m_rectWholeWnd.Width(), ptRef1.y-WNDSPACING));
		pVideoWnd->SetRect(ptRef2.x+WNDSPACING, 0, m_rectWholeWnd.Width(), ptRef1.y-WNDSPACING);
	}else{
		rgnTmp2.SetRectRgn(ptRef2.x+WNDSPACING, 0, m_rectWholeWnd.Width(), ptRef1.y-WNDSPACING);
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}
		
	nWndIndex=nIndexBase+2;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[2].CreateRectRgn(ptRef2.x+WNDSPACING, ptRef1.y+WNDSPACING, m_rectWholeWnd.Width(), ptRef2.y-WNDSPACING));
		pVideoWnd->SetRect(ptRef2.x+WNDSPACING, ptRef1.y+WNDSPACING, m_rectWholeWnd.Width(), ptRef2.y-WNDSPACING);
	}else{
		rgnTmp2.SetRectRgn(ptRef2.x+WNDSPACING, ptRef1.y+WNDSPACING, m_rectWholeWnd.Width(), ptRef2.y-WNDSPACING);
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}
		
	nWndIndex=nIndexBase+3;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[3].CreateRectRgn(0, ptRef2.y+WNDSPACING, ptRef1.x-WNDSPACING, m_rectWholeWnd.Height()));
		pVideoWnd->SetRect(0, ptRef2.y+WNDSPACING, ptRef1.x-WNDSPACING, m_rectWholeWnd.Height());
	}else{
		rgnTmp2.SetRectRgn(0, ptRef2.y+WNDSPACING, ptRef1.x-WNDSPACING, m_rectWholeWnd.Height());
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}
		
	nWndIndex=nIndexBase+4;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[4].CreateRectRgn(ptRef1.x+WNDSPACING, ptRef2.y+WNDSPACING, ptRef2.x-WNDSPACING, m_rectWholeWnd.Height()));
		pVideoWnd->SetRect(ptRef1.x+WNDSPACING, ptRef2.y+WNDSPACING, ptRef2.x-WNDSPACING, m_rectWholeWnd.Height());
	}else{
		rgnTmp2.SetRectRgn(ptRef1.x+WNDSPACING, ptRef2.y+WNDSPACING, ptRef2.x-WNDSPACING, m_rectWholeWnd.Height());
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}
		
	nWndIndex=nIndexBase+5;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[5].CreateRectRgn(ptRef2.x+WNDSPACING, ptRef2.y+WNDSPACING, m_rectWholeWnd.Width(), m_rectWholeWnd.Height()));
		pVideoWnd->SetRect(ptRef2.x+WNDSPACING, ptRef2.y+WNDSPACING, m_rectWholeWnd.Width(), m_rectWholeWnd.Height());
	}else{
		rgnTmp2.SetRectRgn(ptRef2.x+WNDSPACING, ptRef2.y+WNDSPACING, m_rectWholeWnd.Width(), m_rectWholeWnd.Height());
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}
		
	//3. calculate excess area of video window
	rgnTmp1.CombineRgn(&rgnTmp[0],  &rgnTmp[1], RGN_OR);
	rgnTmp1.CombineRgn(&rgnTmp1, &rgnTmp[2], RGN_OR);
	rgnTmp1.CombineRgn(&rgnTmp1, &rgnTmp[3], RGN_OR);
	rgnTmp1.CombineRgn(&rgnTmp1, &rgnTmp[4], RGN_OR);
	rgnTmp1.CombineRgn(&rgnTmp1, &rgnTmp[5], RGN_OR);	
	m_rgnExcessArea.SetRectRgn(&m_rectWholeWnd);
	m_rgnExcessArea.CombineRgn(&m_rgnExcessArea, &rgnTmp1, RGN_XOR);
	m_rgnExcessArea.CombineRgn(&m_rgnExcessArea, &m_rgnDontUseChannelArea, RGN_XOR);
}

void CVideoWndMgr::CalcuMode3()
{
	int iEqualW=m_rectWholeWnd.Width()/3,  iWTmp=0;
	int iEqualH=m_rectWholeWnd.Height()/3, iHTmp=0;
	if((m_rectWholeWnd.Width() % 3)==2)  iWTmp=1;
	if((m_rectWholeWnd.Height() % 3)==2) iHTmp=1;

	CPoint ptRef1, ptRef2;
	//1. calculate reference point
	ptRef1.x=iEqualW+iWTmp;		ptRef1.y=iEqualH+iWTmp;
	ptRef2.x=iEqualW*2+iWTmp;	ptRef2.y=iEqualH*2+iWTmp;

	//CRect rectTmp;
	CRgn rgnTmp[MODE3], rgnTmp1, rgnTmp2;	
	CVideoWnd *pVideoWnd=NULL;
	VERIFY(rgnTmp1.CreateRectRgn(0, 0, 0, 0));
	VERIFY(rgnTmp2.CreateRectRgn(0, 0, 0, 0));
	m_rgnDontUseChannelArea.SetRectRgn(0,0,0,0);
	
	VideoWndRectNull(MODE3, m_nCurPageNo);
	int nIndexBase=m_nCurPageNo*MODE3;
	int nWndIndex=0;

	//2. calculate area of video window
	//row1---
	nWndIndex=nIndexBase+0;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[0].CreateRectRgn(0, 0, ptRef1.x-WNDSPACING, ptRef1.y-WNDSPACING));
		pVideoWnd->SetRect(0, 0, ptRef1.x-WNDSPACING, ptRef1.y-WNDSPACING);
	}else{
		rgnTmp2.SetRectRgn(0, 0, ptRef1.x-WNDSPACING, ptRef1.y-WNDSPACING);
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}
	
	nWndIndex=nIndexBase+1;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[1].CreateRectRgn(ptRef1.x+WNDSPACING, 0, ptRef2.x-WNDSPACING, ptRef1.y-WNDSPACING));
		pVideoWnd->SetRect(ptRef1.x+WNDSPACING, 0, ptRef2.x-WNDSPACING, ptRef1.y-WNDSPACING);
	}else{
		rgnTmp2.SetRectRgn(ptRef1.x+WNDSPACING, 0, ptRef2.x-WNDSPACING, ptRef1.y-WNDSPACING);
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}
	
	nWndIndex=nIndexBase+2;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[2].CreateRectRgn(ptRef2.x+WNDSPACING, 0, m_rectWholeWnd.Width(), ptRef1.y-WNDSPACING));
		pVideoWnd->SetRect(ptRef2.x+WNDSPACING, 0, m_rectWholeWnd.Width(), ptRef1.y-WNDSPACING);
	}else{
		rgnTmp2.SetRectRgn(ptRef2.x+WNDSPACING, 0, m_rectWholeWnd.Width(), ptRef1.y-WNDSPACING);
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}
	
	//row2---
	nWndIndex=nIndexBase+3;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[3].CreateRectRgn(0,ptRef1.y+WNDSPACING, ptRef1.x-WNDSPACING, ptRef2.y-WNDSPACING));
		pVideoWnd->SetRect(0,ptRef1.y+WNDSPACING, ptRef1.x-WNDSPACING, ptRef2.y-WNDSPACING);
	}else{
		rgnTmp2.SetRectRgn(0,ptRef1.y+WNDSPACING, ptRef1.x-WNDSPACING, ptRef2.y-WNDSPACING);
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}

	nWndIndex=nIndexBase+4;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[4].CreateRectRgn(ptRef1.x+WNDSPACING, ptRef1.y+WNDSPACING, ptRef2.x-WNDSPACING, ptRef2.y-WNDSPACING));
		pVideoWnd->SetRect(ptRef1.x+WNDSPACING, ptRef1.y+WNDSPACING, ptRef2.x-WNDSPACING, ptRef2.y-WNDSPACING);
	}else{
		rgnTmp2.SetRectRgn(ptRef1.x+WNDSPACING, ptRef1.y+WNDSPACING, ptRef2.x-WNDSPACING, ptRef2.y-WNDSPACING);
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}

	nWndIndex=nIndexBase+5;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[5].CreateRectRgn(ptRef2.x+WNDSPACING, ptRef1.y+WNDSPACING, m_rectWholeWnd.Width(), ptRef2.y-WNDSPACING));
		pVideoWnd->SetRect(ptRef2.x+WNDSPACING, ptRef1.y+WNDSPACING, m_rectWholeWnd.Width(), ptRef2.y-WNDSPACING);
	}else{
		rgnTmp2.SetRectRgn(ptRef2.x+WNDSPACING, ptRef1.y+WNDSPACING, m_rectWholeWnd.Width(), ptRef2.y-WNDSPACING);
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}

	//row3---
	nWndIndex=nIndexBase+6;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[6].CreateRectRgn(0, ptRef2.y+WNDSPACING, ptRef1.x-WNDSPACING, m_rectWholeWnd.Height()));
		pVideoWnd->SetRect(0, ptRef2.y+WNDSPACING, ptRef1.x-WNDSPACING, m_rectWholeWnd.Height());
	}else{
		rgnTmp2.SetRectRgn(0, ptRef2.y+WNDSPACING, ptRef1.x-WNDSPACING, m_rectWholeWnd.Height());
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}
		
	nWndIndex=nIndexBase+7;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[7].CreateRectRgn(ptRef1.x+WNDSPACING, ptRef2.y+WNDSPACING, ptRef2.x-WNDSPACING, m_rectWholeWnd.Height()));
		pVideoWnd->SetRect(ptRef1.x+WNDSPACING, ptRef2.y+WNDSPACING, ptRef2.x-WNDSPACING, m_rectWholeWnd.Height());
	}else {
		rgnTmp2.SetRectRgn(ptRef1.x+WNDSPACING, ptRef2.y+WNDSPACING, ptRef2.x-WNDSPACING, m_rectWholeWnd.Height());
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}
	nWndIndex=nIndexBase+8;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[8].CreateRectRgn(ptRef2.x+WNDSPACING, ptRef2.y+WNDSPACING, m_rectWholeWnd.Width(), m_rectWholeWnd.Height()));
		pVideoWnd->SetRect(ptRef2.x+WNDSPACING, ptRef2.y+WNDSPACING, m_rectWholeWnd.Width(), m_rectWholeWnd.Height());
	}else{
		rgnTmp2.SetRectRgn(ptRef2.x+WNDSPACING, ptRef2.y+WNDSPACING, m_rectWholeWnd.Width(), m_rectWholeWnd.Height());
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}

	//3. calculate excess area of video window
	rgnTmp1.CombineRgn(&rgnTmp[0],  &rgnTmp[1], RGN_OR);
	rgnTmp1.CombineRgn(&rgnTmp1, &rgnTmp[2], RGN_OR);
	rgnTmp1.CombineRgn(&rgnTmp1, &rgnTmp[3], RGN_OR);
	rgnTmp1.CombineRgn(&rgnTmp1, &rgnTmp[4], RGN_OR);
	rgnTmp1.CombineRgn(&rgnTmp1, &rgnTmp[5], RGN_OR);
	rgnTmp1.CombineRgn(&rgnTmp1, &rgnTmp[6], RGN_OR);
	rgnTmp1.CombineRgn(&rgnTmp1, &rgnTmp[7], RGN_OR);
	rgnTmp1.CombineRgn(&rgnTmp1, &rgnTmp[8], RGN_OR);
	m_rgnExcessArea.SetRectRgn(&m_rectWholeWnd);
	m_rgnExcessArea.CombineRgn(&m_rgnExcessArea, &rgnTmp1, RGN_XOR);
	m_rgnExcessArea.CombineRgn(&m_rgnExcessArea, &m_rgnDontUseChannelArea, RGN_XOR);
}

void CVideoWndMgr::CalcuMode4()
{
	int iEqualW=m_rectWholeWnd.Width()/4,  iWTmp=0;
	int iEqualH=m_rectWholeWnd.Height()/4, iHTmp=0;
	if((m_rectWholeWnd.Width() % 4)==3)  iWTmp=1;
	if((m_rectWholeWnd.Height() % 4)==3) iHTmp=1;
	
	CPoint ptRef1, ptRef2, ptRef3;
	//1. calculate reference point
	ptRef1.x=iEqualW+iWTmp;		ptRef1.y=iEqualH+iWTmp;
	ptRef2.x=iEqualW*2+iWTmp;	ptRef2.y=iEqualH*2+iWTmp;
	ptRef3.x=iEqualW*3+iWTmp;	ptRef3.y=iEqualH*3+iWTmp;
	
	//CRect rectTmp;
	CRgn rgnTmp[MODE4], rgnTmp1, rgnTmp2;
	CVideoWnd *pVideoWnd=NULL;
	VERIFY(rgnTmp1.CreateRectRgn(0, 0, 0, 0));
	VERIFY(rgnTmp2.CreateRectRgn(0, 0, 0, 0));
	m_rgnDontUseChannelArea.SetRectRgn(0,0,0,0);
	
	VideoWndRectNull(MODE4, m_nCurPageNo);
	int nIndexBase=m_nCurPageNo*MODE4;
	int nWndIndex=0;
	//2. calculate area of video window
	//row1---
	nWndIndex=nIndexBase+0;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[0].CreateRectRgn(0, 0, ptRef1.x-WNDSPACING, ptRef1.y-WNDSPACING));
		pVideoWnd->SetRect(0, 0, ptRef1.x-WNDSPACING, ptRef1.y-WNDSPACING);
	}else{
		rgnTmp2.SetRectRgn(0, 0, ptRef1.x-WNDSPACING, ptRef1.y-WNDSPACING);
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}
		
	nWndIndex=nIndexBase+1;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[1].CreateRectRgn(ptRef1.x+WNDSPACING, 0, ptRef2.x-WNDSPACING, ptRef1.y-WNDSPACING));
		pVideoWnd->SetRect(ptRef1.x+WNDSPACING, 0, ptRef2.x-WNDSPACING, ptRef1.y-WNDSPACING);
	}else{
		rgnTmp2.SetRectRgn(ptRef1.x+WNDSPACING, 0, ptRef2.x-WNDSPACING, ptRef1.y-WNDSPACING);
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}

	nWndIndex=nIndexBase+2;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[2].CreateRectRgn(ptRef2.x+WNDSPACING, 0, ptRef3.x-WNDSPACING, ptRef1.y-WNDSPACING));
		pVideoWnd->SetRect(ptRef2.x+WNDSPACING, 0, ptRef3.x-WNDSPACING, ptRef1.y-WNDSPACING);
	}else{
		rgnTmp2.SetRectRgn(ptRef2.x+WNDSPACING, 0, ptRef3.x-WNDSPACING, ptRef1.y-WNDSPACING);
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}
	
	nWndIndex=nIndexBase+3;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[3].CreateRectRgn(ptRef3.x+WNDSPACING, 0, m_rectWholeWnd.Width(), ptRef1.y-WNDSPACING));
		pVideoWnd->SetRect(ptRef3.x+WNDSPACING, 0, m_rectWholeWnd.Width(), ptRef1.y-WNDSPACING);
	}else{
		rgnTmp2.SetRectRgn(ptRef3.x+WNDSPACING, 0, m_rectWholeWnd.Width(), ptRef1.y-WNDSPACING);
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}
	
	//row2---
	nWndIndex=nIndexBase+4;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[4].CreateRectRgn(0, ptRef1.y+WNDSPACING, ptRef1.x-WNDSPACING, ptRef2.y-WNDSPACING));
		pVideoWnd->SetRect(0, ptRef1.y+WNDSPACING, ptRef1.x-WNDSPACING, ptRef2.y-WNDSPACING);
	}else{
		rgnTmp2.SetRectRgn(0, ptRef1.y+WNDSPACING, ptRef1.x-WNDSPACING, ptRef2.y-WNDSPACING);
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}

	nWndIndex=nIndexBase+5;
	if(nWndIndex<MAX_CHN_NUM){	
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[5].CreateRectRgn(ptRef1.x+WNDSPACING, ptRef1.y+WNDSPACING, ptRef2.x-WNDSPACING, ptRef2.y-WNDSPACING));
		pVideoWnd->SetRect(ptRef1.x+WNDSPACING, ptRef1.y+WNDSPACING, ptRef2.x-WNDSPACING, ptRef2.y-WNDSPACING);
	}else{
		rgnTmp2.SetRectRgn(ptRef1.x+WNDSPACING, ptRef1.y+WNDSPACING, ptRef2.x-WNDSPACING, ptRef2.y-WNDSPACING);
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}
	
	nWndIndex=nIndexBase+6;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[6].CreateRectRgn(ptRef2.x+WNDSPACING, ptRef1.y+WNDSPACING, ptRef3.x-WNDSPACING, ptRef2.y-WNDSPACING));
		pVideoWnd->SetRect(ptRef2.x+WNDSPACING, ptRef1.y+WNDSPACING, ptRef3.x-WNDSPACING, ptRef2.y-WNDSPACING);
	}else{
		rgnTmp2.SetRectRgn(ptRef2.x+WNDSPACING, ptRef1.y+WNDSPACING, ptRef3.x-WNDSPACING, ptRef2.y-WNDSPACING);
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}
	
	nWndIndex=nIndexBase+7;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[7].CreateRectRgn(ptRef3.x+WNDSPACING, ptRef1.y+WNDSPACING, m_rectWholeWnd.Width(), ptRef2.y-WNDSPACING));
		pVideoWnd->SetRect(ptRef3.x+WNDSPACING, ptRef1.y+WNDSPACING, m_rectWholeWnd.Width(), ptRef2.y-WNDSPACING);
	}else{
		rgnTmp2.SetRectRgn(ptRef3.x+WNDSPACING, ptRef1.y+WNDSPACING, m_rectWholeWnd.Width(), ptRef2.y-WNDSPACING);
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}
	
	//row3---
	nWndIndex=nIndexBase+8;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[8].CreateRectRgn(0, ptRef2.y+WNDSPACING, ptRef1.x-WNDSPACING, ptRef3.y-WNDSPACING));
		pVideoWnd->SetRect(0, ptRef2.y+WNDSPACING, ptRef1.x-WNDSPACING, ptRef3.y-WNDSPACING);
	}else{
		rgnTmp2.SetRectRgn(0, ptRef2.y+WNDSPACING, ptRef1.x-WNDSPACING, ptRef3.y-WNDSPACING);
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}
	
	nWndIndex=nIndexBase+9;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[9].CreateRectRgn(ptRef1.x+WNDSPACING, ptRef2.y+WNDSPACING, ptRef2.x-WNDSPACING, ptRef3.y-WNDSPACING));
		pVideoWnd->SetRect(ptRef1.x+WNDSPACING, ptRef2.y+WNDSPACING, ptRef2.x-WNDSPACING, ptRef3.y-WNDSPACING);
	}else{
		rgnTmp2.SetRectRgn(ptRef1.x+WNDSPACING, ptRef2.y+WNDSPACING, ptRef2.x-WNDSPACING, ptRef3.y-WNDSPACING);
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}

	nWndIndex=nIndexBase+10;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[10].CreateRectRgn(ptRef2.x+WNDSPACING, ptRef2.y+WNDSPACING, ptRef3.x-WNDSPACING, ptRef3.y-WNDSPACING));
		pVideoWnd->SetRect(ptRef2.x+WNDSPACING, ptRef2.y+WNDSPACING, ptRef3.x-WNDSPACING, ptRef3.y-WNDSPACING);
	}else{
		rgnTmp2.SetRectRgn(ptRef2.x+WNDSPACING, ptRef2.y+WNDSPACING, ptRef3.x-WNDSPACING, ptRef3.y-WNDSPACING);
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}

	nWndIndex=nIndexBase+11;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[11].CreateRectRgn(ptRef3.x+WNDSPACING, ptRef2.y+WNDSPACING, m_rectWholeWnd.Width(), ptRef3.y-WNDSPACING));
		pVideoWnd->SetRect(ptRef3.x+WNDSPACING, ptRef2.y+WNDSPACING, m_rectWholeWnd.Width(), ptRef3.y-WNDSPACING);
	}else{
		rgnTmp2.SetRectRgn(ptRef3.x+WNDSPACING, ptRef2.y+WNDSPACING, m_rectWholeWnd.Width(), ptRef3.y-WNDSPACING);
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}

	//row4---
	nWndIndex=nIndexBase+12;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[12].CreateRectRgn(0, ptRef3.y+WNDSPACING, ptRef1.x-WNDSPACING, m_rectWholeWnd.Height()));
		pVideoWnd->SetRect(0, ptRef3.y+WNDSPACING, ptRef1.x-WNDSPACING, m_rectWholeWnd.Height());
	}else{
		rgnTmp2.SetRectRgn(0, ptRef3.y+WNDSPACING, ptRef1.x-WNDSPACING, m_rectWholeWnd.Height());
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}
	
	nWndIndex=nIndexBase+13;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[13].CreateRectRgn(ptRef1.x+WNDSPACING, ptRef3.y+WNDSPACING, ptRef2.x-WNDSPACING, m_rectWholeWnd.Height()));
		pVideoWnd->SetRect(ptRef1.x+WNDSPACING, ptRef3.y+WNDSPACING, ptRef2.x-WNDSPACING, m_rectWholeWnd.Height());
	}else{
		rgnTmp2.SetRectRgn(ptRef1.x+WNDSPACING, ptRef3.y+WNDSPACING, ptRef2.x-WNDSPACING, m_rectWholeWnd.Height());
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}
		
	nWndIndex=nIndexBase+14;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[14].CreateRectRgn(ptRef2.x+WNDSPACING, ptRef3.y+WNDSPACING, ptRef3.x-WNDSPACING, m_rectWholeWnd.Height()));
		pVideoWnd->SetRect(ptRef2.x+WNDSPACING, ptRef3.y+WNDSPACING, ptRef3.x-WNDSPACING, m_rectWholeWnd.Height());
	}else{
		rgnTmp2.SetRectRgn(ptRef2.x+WNDSPACING, ptRef3.y+WNDSPACING, ptRef3.x-WNDSPACING, m_rectWholeWnd.Height());
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}
		
	nWndIndex=nIndexBase+15;
	if(nWndIndex<MAX_CHN_NUM){
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
		VERIFY(rgnTmp[15].CreateRectRgn(ptRef3.x+WNDSPACING, ptRef3.y+WNDSPACING, m_rectWholeWnd.Width(), m_rectWholeWnd.Height()));
		pVideoWnd->SetRect(ptRef3.x+WNDSPACING, ptRef3.y+WNDSPACING, m_rectWholeWnd.Width(), m_rectWholeWnd.Height());
	}else{
		rgnTmp2.SetRectRgn(ptRef3.x+WNDSPACING, ptRef3.y+WNDSPACING, m_rectWholeWnd.Width(), m_rectWholeWnd.Height());
		m_rgnDontUseChannelArea.CombineRgn(&m_rgnDontUseChannelArea, &rgnTmp2, RGN_OR);
	}
	
	//3. calculate excess area of video window
	rgnTmp1.CombineRgn(&rgnTmp[0], &rgnTmp[1], RGN_OR);
	rgnTmp1.CombineRgn(&rgnTmp1, &rgnTmp[2], RGN_OR);
	rgnTmp1.CombineRgn(&rgnTmp1, &rgnTmp[3], RGN_OR);

	rgnTmp1.CombineRgn(&rgnTmp1, &rgnTmp[4], RGN_OR);
	rgnTmp1.CombineRgn(&rgnTmp1, &rgnTmp[5], RGN_OR);
	rgnTmp1.CombineRgn(&rgnTmp1, &rgnTmp[6], RGN_OR);
	rgnTmp1.CombineRgn(&rgnTmp1, &rgnTmp[7], RGN_OR);

	rgnTmp1.CombineRgn(&rgnTmp1, &rgnTmp[8], RGN_OR);
	rgnTmp1.CombineRgn(&rgnTmp1, &rgnTmp[9], RGN_OR);
	rgnTmp1.CombineRgn(&rgnTmp1, &rgnTmp[10], RGN_OR);
	rgnTmp1.CombineRgn(&rgnTmp1, &rgnTmp[11], RGN_OR);

	rgnTmp1.CombineRgn(&rgnTmp1, &rgnTmp[12], RGN_OR);
	rgnTmp1.CombineRgn(&rgnTmp1, &rgnTmp[13], RGN_OR);
	rgnTmp1.CombineRgn(&rgnTmp1, &rgnTmp[14], RGN_OR);
	rgnTmp1.CombineRgn(&rgnTmp1, &rgnTmp[15], RGN_OR);

	m_rgnExcessArea.SetRectRgn(&m_rectWholeWnd);
	m_rgnExcessArea.CombineRgn(&m_rgnExcessArea, &rgnTmp1, RGN_XOR);
	m_rgnExcessArea.CombineRgn(&m_rgnExcessArea, &m_rgnDontUseChannelArea, RGN_XOR);
}

void CVideoWndMgr::GetExcessRgn(CRgn &rgnExcessArea)
{
	rgnExcessArea.CreateRectRgnIndirect(&m_rectWholeWnd);
}

void CVideoWndMgr::KillFocusAllWnd()
{
	CVideoWnd *pVideoWnd=NULL;
	for(int i=0; i<MAX_CHN_NUM; i++)
	{
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[i];
		if(!pVideoWnd) continue;	// || (pVideoWnd && pVideoWnd->GetChannelNo()<0)
		pVideoWnd->KillFocus();
	}	
}

void CVideoWndMgr::SetFocusBy(CPoint point)
{
	CVideoWnd *pVideoWnd=NULL;
	for(int i=0; i<MAX_CHN_NUM; i++)
	{
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[i];
		if(!pVideoWnd) continue;
		if(pVideoWnd->HitMe(point)) {
			pVideoWnd->SetFocus();
			break;
		}
	}	
}

//@para nCurWndIndex in which be going to set focus
//@retu old focus wndIndex
short CVideoWndMgr::SetFocusBy(short nCurWndIndex, bool bDoTalkSound)
{
	short nOldFocusWndIndex=-1;

	CVideoWnd *pVideoWnd=NULL;
	for(int i=0; i<MAX_CHN_NUM; i++)
	{
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[i];
		if(!pVideoWnd) continue;

		if(pVideoWnd->IsFocus()) nOldFocusWndIndex=i;
		if(nCurWndIndex==i) pVideoWnd->SetFocus();
		else pVideoWnd->KillFocus();
	}
	return nOldFocusWndIndex;
}

short CVideoWndMgr::GetFocusWndIndex()
{
	if(m_nIndexFullScrn!=-1) return m_nIndexFullScrn;

	CVideoWnd *pVideoWnd=NULL;
	for(int i=0; i<MAX_CHN_NUM; i++)
	{
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[i];
		if(!pVideoWnd) continue;
		if(pVideoWnd->IsFocus()) return (short)i;
	}
	return (short)-1;
}

short CVideoWndMgr::GetHitWndIndexBy(CPoint point)
{
	if(m_nIndexFullScrn!=-1) return m_nIndexFullScrn;

	CVideoWnd *pVideoWnd=NULL;
	for(int i=0; i<MAX_CHN_NUM; i++)
	{
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[i];
		if(pVideoWnd && pVideoWnd->HitMe(point)) return (short)i;
	}

	return (short)-1;
}

CVideoWnd *CVideoWndMgr::GetVideoWndBy(CPoint point)
{
	if(m_nIndexFullScrn!=-1) return (CVideoWnd *)m_ptrVWnds[m_nIndexFullScrn];
	
	CVideoWnd *pVideoWnd=NULL;
	for(int i=0; i<MAX_CHN_NUM; i++)
	{
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[i];
		if(pVideoWnd && pVideoWnd->HitMe(point)) return pVideoWnd;
	}
	
	return NULL;
}

CVideoWnd *CVideoWndMgr::GetVideoWndBy(short nWndIndex)
{
	if(nWndIndex<0 || nWndIndex>MAX_CHN_NUM) return NULL;
	else return (CVideoWnd *)m_ptrVWnds[nWndIndex];
}

CVideoWnd *CVideoWndMgr::GetVideoWndByFixChn(short nFixChnNo)
{
	CVideoWnd *pVideoWnd=NULL;
	for(int i=0; i<MAX_CHN_NUM; i++)
	{
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[i];
		if(pVideoWnd && pVideoWnd->GetChnNo()==nFixChnNo) return pVideoWnd;
	}
	return NULL;
}

void  CVideoWndMgr::StretchVWnd(short nWndIndex) //nWndIndex=-1: all video window
{
	
}

//@return	oldWndIndex
//
short CVideoWndMgr::LButtonDown(UINT nFlags, CPoint point, short nCurWndIndex)
{
	short nOldFocusWndIndex=SetFocusBy(nCurWndIndex, true);
	if(m_pParent) m_pParent->Invalidate();
	return nOldFocusWndIndex;
}

void CVideoWndMgr::DrawVWnd(CDC *pDC)
{
	pDC->FillRgn(&m_rgnExcessArea, &m_wndbkBrush);
	pDC->FillRgn(&m_rgnDontUseChannelArea, &m_wndbkBrushUnusedChan);

	CVideoWnd *pVideoWnd=NULL;
	for(int i=0; i<MAX_CHN_NUM; i++)
	{
		pVideoWnd=(CVideoWnd *)m_ptrVWnds[i];
		if(pVideoWnd) pVideoWnd->DrawVWnd(pDC);
	}
}

//return -1 when no wnd idled
CVideoWnd *CVideoWndMgr::GetIdleWndIndex(short &nWndIndex)
{
	short i=0;
	CVideoWnd *pVWnd=NULL;
	for(i=0; i<MAX_CHN_NUM; i++){
		pVWnd=(CVideoWnd *)m_ptrVWnds[i];
// 		if(pVWnd->GetChannelNo()<0) {
// 			continue;
// 		}
		//if(!pVWnd->HasAllocDev()) break;
	}
	if(i==MAX_CHN_NUM) {
		nWndIndex=-1;
		return NULL;
	}else { nWndIndex=i; return pVWnd; }
}

CVideoWnd *CVideoWndMgr::GetVideoWndFocused()
{
	CVideoWnd *pVWnd=NULL;
	for(int i=0; i<MAX_CHN_NUM; i++){
		pVWnd=(CVideoWnd *)m_ptrVWnds[i];
		if(pVWnd->IsFocus()) return pVWnd;
	}
	return NULL;	
}

CVideoWnd *CVideoWndMgr::AllocVideoWndBy(IN short nWndIndex)
{
	if(nWndIndex<0 || nWndIndex>=MAX_CHN_NUM) return NULL;
	CVideoWnd *pVWnd=(CVideoWnd *)m_ptrVWnds[nWndIndex];
	return pVWnd;
}

void CVideoWndMgr::ChgOtherWndSize(short nOpType, short nWndIndexExcluded)
{
	short i=0, nFixChanNo=-1;
	CVideoWnd *pVWnd=NULL;
	if(nOpType==1){ //nWndIndexExcluded maximize
		for(i=0; i<MAX_CHN_NUM; i++){
			pVWnd=(CVideoWnd *)m_ptrVWnds[i];
			nFixChanNo=pVWnd->GetChnNo();
			//TRACE("max: nFixChanNo=%d, nWndIndexExcluded=%d\n", nFixChanNo, nWndIndexExcluded);
			if(nFixChanNo==nWndIndexExcluded) continue;

			if(nFixChanNo>=0) pVWnd->SetOtherVideoAreaSize(TRUE);
		}
	}else if(nOpType==0){//nWndIndexExcluded restore
		for(i=0; i<MAX_CHN_NUM; i++){
			pVWnd=(CVideoWnd *)m_ptrVWnds[i];
			nFixChanNo=pVWnd->GetChnNo();
			//TRACE("restore: nFixChanNo=%d, nWndIndexExcluded=%d\n", nFixChanNo, nWndIndexExcluded);
			if(nFixChanNo==nWndIndexExcluded) continue;
			
			if(nFixChanNo>=0) pVWnd->SetOtherVideoAreaSize(FALSE);
		}
	}
}


CDevInfo* CVideoWndMgr::GetEmptyDevInfo()
{
	CDevInfo *pDevInfo=NULL;
	for(int i=0; i<MAX_CHN_NUM; i++)
	{
		pDevInfo=(CDevInfo *)m_ptrDevInfo[i];
		if(pDevInfo->IsEmpty()) return pDevInfo;
	}
	return NULL;
}

