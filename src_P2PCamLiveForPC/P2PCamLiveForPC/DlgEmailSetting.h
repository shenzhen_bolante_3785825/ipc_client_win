#pragma once


// CDlgEmailSetting 对话框

class CDlgEmailSetting : public CDialog
{
	DECLARE_DYNAMIC(CDlgEmailSetting)

public:
	CDlgEmailSetting(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CDlgEmailSetting();

// 对话框数据
	enum { IDD = DLG_EMAIL_SETTING };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtn1();
	afx_msg void OnBnClickedTestsmtp();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBtn2();
};
