#pragma once


// CDlgFlash 对话框
class CDevInfo;
class CDlgPTZ : public CDialog
{
	DECLARE_DYNAMIC(CDlgPTZ)

public:
	CDlgPTZ(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CDlgPTZ();

// 对话框数据
	enum { IDD = IDD_PTZ };

public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);


protected:
	UINT	m_timerID;
	CFont	m_font;
	CRect   m_rectClient;
	COLORREF m_crTipText, m_crBk;
	UINT    m_nCount;
	UINT	m_nTimerID_PTControl;

	// Generated message map functions
	//{{AFX_MSG(CDlgFlash)
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDestroy();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnPaint();

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()

public:
	CDevInfo *m_pDevInfo;

	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
};
