// DlgFlash.cpp : ʵ���ļ�
//

#include "stdafx.h"
#include "P2PCamLiveForPC.h"
#include "DlgPTZ.h"
#include "DevInfo.h"
#include "AVIOCTRLDEFs.h"

#define TIMER_ID		100
#define	TIMER_TIME		1000

#define ROUND_CONER_W	16
#define ROUND_CONER_H	16
#define LWA_COLORKEY	0x00000001
#define LWA_ALPHA		0x00000002

#define TIMERID_PT_CONTROL		102


IMPLEMENT_DYNAMIC(CDlgPTZ, CDialog)

CDlgPTZ::CDlgPTZ(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgPTZ::IDD, pParent)
{
	m_crTipText=RGB(255,255,255);
	m_crBk=RGB(0,0,0);
	m_pDevInfo=NULL;
	m_nTimerID_PTControl=0;
}

CDlgPTZ::~CDlgPTZ()
{

}

void CDlgPTZ::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDlgPTZ, CDialog)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_WM_CTLCOLOR()
	ON_WM_PAINT()
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_BUTTON1, &CDlgPTZ::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CDlgPTZ::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CDlgPTZ::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &CDlgPTZ::OnBnClickedButton4)
END_MESSAGE_MAP()

BOOL CDlgPTZ::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_font.CreateFont(0,0,0,0,600,0,0,0,
		ANSI_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,FF_SCRIPT ,_T("MS Shell Dlg"));

	SetWindowLong(GetSafeHwnd(),GWL_EXSTYLE,GetWindowLong(GetSafeHwnd(),GWL_EXSTYLE)|0x80000);
	typedef BOOL (WINAPI *FSetLayeredWindowAttributes)(HWND,COLORREF,BYTE,DWORD);
	FSetLayeredWindowAttributes SetLayeredWindowAttributes ;
	HINSTANCE hInst = LoadLibrary(_T("User32.DLL"));	
	SetLayeredWindowAttributes = (FSetLayeredWindowAttributes)GetProcAddress(hInst,"SetLayeredWindowAttributes");
	if (SetLayeredWindowAttributes) SetLayeredWindowAttributes(GetSafeHwnd(),RGB(0,0,0),128,LWA_ALPHA); 
	FreeLibrary(hInst);

	CRgn objRgn; 
	GetClientRect(m_rectClient);   
	SetWindowRgn(NULL, FALSE);
	objRgn.CreateRoundRectRgn(m_rectClient.left, m_rectClient.top, m_rectClient.right, m_rectClient.bottom,ROUND_CONER_W,ROUND_CONER_H);
	SetWindowRgn(objRgn, TRUE);
	objRgn.DeleteObject();

	m_timerID=SetTimer(TIMER_ID, TIMER_TIME, NULL);

	return TRUE;
}

void CDlgPTZ::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent==TIMER_ID){
		if(m_nCount>=3) {
			PostMessage(WM_CLOSE);
			m_nCount=0;
			return;
		}
		m_nCount++;

	}else if(nIDEvent==TIMERID_PT_CONTROL){
		KillTimer(TIMERID_PT_CONTROL);

		if(m_pDevInfo){
			SMsgAVIoctrlPtzCmd req;
			memset(&req, 0, sizeof(req));
			req.control=AVIOCTRL_PTZ_STOP;
			m_pDevInfo->SendIOCtrl(IOTYPE_USER_IPCAM_PTZ_COMMAND, (tkUCHAR *)&req, sizeof(req));
		}
		m_nTimerID_PTControl=0;
	}

	CDialog::OnTimer(nIDEvent);
}

void CDlgPTZ::OnDestroy() 
{
	KillTimer(m_timerID);
	CDialog::OnDestroy();
}

BOOL CDlgPTZ::PreTranslateMessage(MSG* pMsg) 
{
	if(pMsg->message==WM_KEYDOWN){
		if(pMsg->wParam==VK_ESCAPE|| pMsg->wParam==VK_RETURN) return TRUE;
	}

	return CDialog::PreTranslateMessage(pMsg);
}

HBRUSH CDlgPTZ::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	return hbr;
}

void CDlgPTZ::OnPaint() 
{
	CPaintDC dc(this);

	CBrush brushBlue(m_crBk);
	CBrush* pOldBrush = dc.SelectObject(&brushBlue);

	CRect rect=m_rectClient;
	rect.DeflateRect(2,2,3,3);
	dc.RoundRect(rect.left, rect.top, rect.right, rect.bottom,ROUND_CONER_W,ROUND_CONER_H);

	dc.SelectObject(pOldBrush);

	// Do not call CDialog::OnPaint() for painting messages
}


void CDlgPTZ::OnMouseMove(UINT nFlags, CPoint point)
{
	m_nCount=0;
	CDialog::OnMouseMove(nFlags, point);
}

void CDlgPTZ::OnBnClickedButton1() //left
{
	if(m_pDevInfo==NULL) return;

	SMsgAVIoctrlPtzCmd req;
	memset(&req, 0, sizeof(req));
	req.control=AVIOCTRL_PTZ_LEFT;
	m_pDevInfo->SendIOCtrl(IOTYPE_USER_IPCAM_PTZ_COMMAND, (tkUCHAR *)&req, sizeof(req));
	if(m_nTimerID_PTControl==0) m_nTimerID_PTControl=SetTimer(TIMERID_PT_CONTROL, 500, NULL);
}

void CDlgPTZ::OnBnClickedButton2()//up
{
	if(m_pDevInfo==NULL) return;

	SMsgAVIoctrlPtzCmd req;
	memset(&req, 0, sizeof(req));
	req.control=AVIOCTRL_PTZ_UP;
	m_pDevInfo->SendIOCtrl(IOTYPE_USER_IPCAM_PTZ_COMMAND, (tkUCHAR *)&req, sizeof(req));
	if(m_nTimerID_PTControl==0) m_nTimerID_PTControl=SetTimer(TIMERID_PT_CONTROL, 500, NULL);
}

void CDlgPTZ::OnBnClickedButton3()//right
{
	if(m_pDevInfo==NULL) return;
	
	SMsgAVIoctrlPtzCmd req;
	memset(&req, 0, sizeof(req));
	req.control=AVIOCTRL_PTZ_RIGHT;
	m_pDevInfo->SendIOCtrl(IOTYPE_USER_IPCAM_PTZ_COMMAND, (tkUCHAR *)&req, sizeof(req));
	if(m_nTimerID_PTControl==0) m_nTimerID_PTControl=SetTimer(TIMERID_PT_CONTROL, 500, NULL);
}

void CDlgPTZ::OnBnClickedButton4()//down
{
	if(m_pDevInfo==NULL) return;
	
	SMsgAVIoctrlPtzCmd req;
	memset(&req, 0, sizeof(req));
	req.control=AVIOCTRL_PTZ_DOWN;
	m_pDevInfo->SendIOCtrl(IOTYPE_USER_IPCAM_PTZ_COMMAND, (tkUCHAR *)&req, sizeof(req));
	if(m_nTimerID_PTControl==0) m_nTimerID_PTControl=SetTimer(TIMERID_PT_CONTROL, 500, NULL);
}
