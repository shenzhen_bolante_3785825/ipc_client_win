#pragma once
#include "afxcmn.h"


// CDlgRemoteInfo 对话框
class CDevInfo;
class CDlgRemoteInfo : public CDialog
{
	DECLARE_DYNAMIC(CDlgRemoteInfo)

public:
	CDlgRemoteInfo(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CDlgRemoteInfo();

// 对话框数据
	enum { IDD = DLG_DEV_REMOTE_INFO };

protected:
	CListCtrl m_ctrlList;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()

public:
	CDevInfo *m_pCurDevInfo;

	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBtn2();
};
