// P2PCamLiveForPC.h : main header file for the P2PCamLiveForPC application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// CAboutDlg dialog used for App About
class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

	// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	// Implementation
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};


// CP2PCamLiveForPCApp:
// See P2PCamLiveForPC.cpp for the implementation of this class
//
class CP2PCamLiveForPCView;
class CP2PCamLiveForPCApp : public CWinApp
{
public:
	CP2PCamLiveForPCApp();

// Overrides
public:
	virtual BOOL InitInstance();
	
	BOOL SysLogin();
	void InitSys();
	void GetAppPath(CString &strAppPath);
	static void CtrlEnable(HWND hDlg, UINT nID, BOOL bEnable);
	static CP2PCamLiveForPCView* GetActiveView();

// Implementation
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CP2PCamLiveForPCApp theApp;