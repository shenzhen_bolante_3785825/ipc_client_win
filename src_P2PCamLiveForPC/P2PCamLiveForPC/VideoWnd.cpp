// VideoWnd.cpp: implementation of the CVideoWnd class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VideoWnd.h"
#include "P2PCamLiveForPC.h"
#include "P2PCamLiveForPCView.h"
#include "MainFrm.h"
#include "StaticEx.h"
#include "DataLock.h"
#include "AVFRAMEINFO.h"
#include "Picture.h"
#include "DevInfo.h"
#include "adpcm.h"
#include "wave_out.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


UINT CVideoWnd::ms_nAccuracy=0;	//define resolution of timer
CVideoWnd::CVideoWnd(CP2PCamLiveForPCView *pParent)
{
	m_pParent=pParent;
	ASSERT(m_pParent!=NULL);
	VERIFY(m_clrVideoBkGnd.CreateSolidBrush(RGB(0,0,0)));//RGB(16, 76, 104)
	VERIFY(m_brFocus.CreateSolidBrush(RGB(255,255,0)));
	
	InitializeCriticalSection(&m_csecSnapshot);
	InitializeCriticalSection(&m_csecCount);

	m_rgnA1.CreateRectRgn(0,0,0,0);
	m_rgnA2.CreateRectRgn(0,0,0,0);
	m_rgnA3.CreateRectRgn(0,0,0,0);
	m_rgnA4.CreateRectRgn(0,0,0,0);

	if(ms_nAccuracy==0){
		TIMECAPS tc;
		if(timeGetDevCaps(&tc,sizeof(TIMECAPS))==TIMERR_NOERROR) 	
		{
			ms_nAccuracy=min(max(tc.wPeriodMin, TIMER_ACCURACY),tc.wPeriodMax);
			timeBeginPeriod(ms_nAccuracy); //set resolution of timer
		}
	}
#ifdef _DEBUG
	QueryPerformanceFrequency(&m_swFreq);
	m_etime = 0.0;
#endif
	m_bStretch	=FALSE;
	m_bRecordOSD=FALSE;
	m_bSoundOSD	=FALSE;
	m_bTalkOSD	=FALSE;
	m_bResumeOSD=FALSE;
	
	m_bHasVideo=FALSE;
	m_nCodec_id=MEDIA_CODEC_VIDEO_H264;

	m_pMainWnd	=NULL;
	m_pH264Dec	=NULL;
	memset(&m_outFrame, 0, sizeof(OUT_FRAME));

	m_bmiHead.biSize  = sizeof(BITMAPINFOHEADER);
	m_bmiHead.biWidth = 0;
	m_bmiHead.biHeight= 0;
	m_bmiHead.biPlanes= 1;
	m_bmiHead.biBitCount = 24;
	m_bmiHead.biCompression = BI_RGB;
	m_bmiHead.biSizeImage   = 0;
	m_bmiHead.biXPelsPerMeter = 0;
	m_bmiHead.biYPelsPerMeter = 0;
	m_bmiHead.biClrUsed       = 0;
	m_bmiHead.biClrImportant  = 0;
	m_pBufBmp24=new BYTE[2765800];
	m_nDataSize=0;
	m_objShow=NULL;

	m_pBufAudio=new BYTE[MAXSIZE_PCM_DATA];
	memset(m_pBufAudio,0, MAXSIZE_PCM_DATA);

	Set_WIN_Params(INVALID_FILEDESC, 8000.0f, 16, 1);
}

CVideoWnd::~CVideoWnd()
{
	DestoryVideoWnd();

	if(m_clrVideoBkGnd.m_hObject) m_clrVideoBkGnd.DeleteObject();
	if(m_brFocus.m_hObject) m_brFocus.DeleteObject();

	m_pCtlVideoArea->DestroyWindow();
	delete m_pCtlVideoArea;
	if(m_pH264Dec){
			apiH264ReleaseObj(&m_pH264Dec);
			m_pH264Dec=NULL;
	}

	if(m_pBufBmp24) {
		delete[] m_pBufBmp24;
		m_pBufBmp24=NULL;
	}
	if(m_objShow){
		delete m_objShow;
		m_objShow=NULL;
	}
	DeleteCriticalSection(&m_csecSnapshot);
	DeleteCriticalSection(&m_csecCount);
	if(m_pBufAudio){
		delete[] m_pBufAudio;
		m_pBufAudio=NULL;
	}
}

void CVideoWnd::InitVar(short nFixChnNo, BOOL bStretch)
{
	m_nFixChnNo=nFixChnNo;	
	m_bFocus	=false;
	m_bStretch	=bStretch;

	m_imgList.Create(IDB_TOOLSBAR, 16, 1, RGB(255,255,255));
	
	if(m_fontStatu.m_hObject) m_fontStatu.DeleteObject();
	m_fontStatu.CreateFont(12, 0, 0, 0, 
		FW_NORMAL,	//nIsBold,
		FALSE,		//bIsItalic,
		FALSE,		//bIsUnderline,
		0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
		DEFAULT_QUALITY, DEFAULT_PITCH, _T("MS Shell Dlg"));
	m_clrStatuTextFg=RGB(0,255,0);	
	m_csStatuText	=_T("");
	m_csStatuTextFormat=_T("%0.2f FPS");

	m_csWndTitle=_T("");
	if(m_fontWndTitle.m_hObject) m_fontWndTitle.DeleteObject();
	m_fontWndTitle.CreateFont(14, 0, 0, 0, 
	 	FW_BOLD,//nIsBold,
	 	FALSE,	//bIsItalic,
	 	FALSE,	//bIsUnderline,
	 	0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
 		DEFAULT_QUALITY, DEFAULT_PITCH, _T("MS Shell Dlg"));
	m_clrWndTitleTextFg =RGB(0,255,0);//129, 255, 129; GetSysColor(COLOR_GRAYTEXT); //default text color goes here
	m_clrWndTitleTextShd=RGB(0,0,0);

	m_pCtlVideoArea=new CStaticEx(RGB(0,255,0));
	m_hDC	=NULL;
	m_hWnd	=NULL;

	m_pH264Dec=apiH264CreateObj(1280, 720, ROTATION_NONE);
}


void CVideoWnd::OnCreate()
{
	CString csText;
	csText.Format(_T("static%d"), m_nFixChnNo);
	m_pCtlVideoArea->Create(csText, WS_CHILD | WS_VISIBLE|SS_OWNERDRAW, CRect(0,0,0,0), m_pParent);
	m_hDC	=m_pCtlVideoArea->GetDC()->GetSafeHdc();
	m_hWnd	=m_pCtlVideoArea->GetSafeHwnd();
}


void CVideoWnd::DestoryVideoWnd()
{
	timeEndPeriod(ms_nAccuracy);
}

#ifdef _DEBUG
void CVideoWnd::StopWatch(int start0stop1)
{
	if (start0stop1==0)	QueryPerformanceCounter(&m_swStart);
	else {
		QueryPerformanceCounter(&m_swStop);
		if (m_swFreq.LowPart==0 && m_swFreq.HighPart==0) m_etime = -1;
		else {
			m_etime = (float)(m_swStop.LowPart - m_swStart.LowPart);
			if (m_etime < 0) m_etime += 2^32;
			m_etime /= (m_swFreq.LowPart+m_swFreq.HighPart * 2^32);
		}
	}
}
#endif

void  CVideoWnd::GetRect(CRect &rect)
{
	short nFullScrnStat=GetFullScrnState();
	if(nFullScrnStat==1) {
		CRect rc(0,0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN));
		rect.CopyRect(rc);
	}else if(nFullScrnStat==0) rect.CopyRect(m_rectWnd);
	else rect.SetRectEmpty();
}

void  CVideoWnd::GetRGN(RGN_TYPE_E eType, CRect &rect)
{
	switch (eType)
	{
		case RGN_TITLE:
			m_rgnA1.GetRgnBox(&rect);
			break;

		case RGN_VIDEO:
			m_rgnA2.GetRgnBox(&rect);
			break;

		case RGN_STAT:
			m_rgnA3.GetRgnBox(&rect);
			break;

		case RGN_NOUSE:
			m_rgnA4.GetRgnBox(&rect);
			break;

		default:;
	}
}


BOOL CVideoWnd::HitMe(CPoint point)
{
	short nWndIndexFullScrn=m_pParent->m_objVWndMgr.GetWndIndexFullScrn();
	if(nWndIndexFullScrn==-1) return m_rectWnd.PtInRect(point);
	else return TRUE;
}

void CVideoWnd::SetStretch(BOOL bStretch)
{
	m_bStretch=bStretch;

	short nFullScrn=0;
	nFullScrn=GetFullScrnState();
	if(nFullScrn==1) CreateRgn();
	else CreateRgn(m_rectWnd.Width(), m_rectWnd.Height());

	if(!m_bStretch)	m_pParent->InvalidateRect(&m_rectWnd);
	//else if(nFullScrn==1) m_pParent->Invalidate();
}

void  CVideoWnd::CreateRgn(int cx, int cy)
{
	if(cx==0 || cy==0){
		if(m_pCtlVideoArea->GetSafeHwnd()){
			m_pCtlVideoArea->MoveWindow(0,0,0,0, FALSE);
			m_pCtlVideoArea->SetWndEmpty();
		}
		return;
	}

	CPoint ptVideo(m_rectWnd.left, m_rectWnd.top);
	m_rgnA1.SetRectRgn(ptVideo.x, ptVideo.y, ptVideo.x+cx, ptVideo.y+VWND_TITLE);
	
	CRect rect2(ptVideo.x, ptVideo.y+VWND_TITLE,  ptVideo.x+cx, ptVideo.y+cy-VWND_STATUS);
	if(!m_bStretch)	CompuVideoRect(rect2);
	else {
		m_rgnA4.SetRectRgn(&rect2);
		rect2.left+=MARGIN_LR;	rect2.right-=MARGIN_LR;
		m_rgnA2.SetRectRgn(&rect2);
		
		int nCombineResult=m_rgnA4.CombineRgn(&m_rgnA4, &m_rgnA2, RGN_XOR);
		ASSERT(nCombineResult != ERROR || nCombineResult != NULLREGION);
	}
	
	m_rgnA3.SetRectRgn(ptVideo.x, ptVideo.y+cy-VWND_STATUS, ptVideo.x+cx, ptVideo.y+cy);
  	if(m_pCtlVideoArea->GetSafeHwnd()){
 		CRect rectVideo;
  		m_rgnA2.GetRgnBox(&rectVideo);
 		m_pCtlVideoArea->MoveWindow(&rectVideo, FALSE);
		m_pCtlVideoArea->SetWndEmpty(FALSE);
  	}
}

void  CVideoWnd::CreateRgn()	//for fullscreen
{
	CRect rc(0,0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN));
	int cx=rc.Width(), cy=rc.Height();
	m_rgnA1.SetRectRgn(0, 0, cx, VWND_TITLE);
	
	CRect rect2(0, VWND_TITLE,  cx, cy-VWND_STATUS);
	if(!m_bStretch)	CompuVideoRect(rect2);
	else {
		m_rgnA4.SetRectRgn(&rect2);
		rect2.left+=MARGIN_LR;	rect2.right-=MARGIN_LR;
		m_rgnA2.SetRectRgn(&rect2);
		
		int nCombineResult=m_rgnA4.CombineRgn(&m_rgnA4, &m_rgnA2, RGN_XOR);
		ASSERT(nCombineResult != ERROR || nCombineResult != NULLREGION);
	}	
	m_rgnA3.SetRectRgn(0, cy-VWND_STATUS, cx, cy);

 	if(m_pCtlVideoArea->GetSafeHwnd()){
 		CRect rectVideo;
 		m_rgnA2.GetRgnBox(&rectVideo);
 		m_pCtlVideoArea->MoveWindow(&rectVideo, FALSE);
		m_pCtlVideoArea->SetWndEmpty(FALSE);
  	}
}

//@desc compute base IMG_STD_WIDTH, IMG_STD_HEIGHT
//
void CVideoWnd::CompuVideoRect(CRect &in_outRect)
{
	m_rgnA4.SetRectRgn(&in_outRect);
	
	in_outRect.left+=MARGIN_LR;	in_outRect.right-=MARGIN_LR;
	int	nDispWidth=in_outRect.Width(), nDispHeight=in_outRect.Height();
	int nTmp=0;
	if(nDispWidth > nDispHeight){	//base nDispHeight
		nTmp=nDispHeight*IMG_STD_WIDTH/IMG_STD_HEIGHT;	//calcu Width
		nTmp=(nDispWidth-nTmp)/2;
		if(nTmp<0) nTmp=0;
		in_outRect.left +=nTmp;
		in_outRect.right-=nTmp;
	}else{							//base nDispWitdh
		nTmp=nDispWidth*IMG_STD_HEIGHT/IMG_STD_WIDTH;	//calcu Height
		nTmp=(nDispHeight-nTmp)/2;
		if(nTmp<0) nTmp=0;		
		in_outRect.top		+=nTmp;
		in_outRect.bottom	-=nTmp;
	}
	m_rgnA2.SetRectRgn(&in_outRect);
	
	int nCombineResult=m_rgnA4.CombineRgn(&m_rgnA4, &m_rgnA2, RGN_XOR);
	ASSERT(nCombineResult != ERROR || nCombineResult != NULLREGION);
}

void CVideoWnd::SetRect(int left, int top, int right, int bottom)
{
	m_rectWnd.left		=left;
	m_rectWnd.top		=top;
	m_rectWnd.right		=right;
	m_rectWnd.bottom	=bottom;
	CreateRgn(m_rectWnd.Width(), m_rectWnd.Height());
}

void CVideoWnd::SetRect(LPRECT pRect)
{
	if(pRect==NULL) return;

	m_rectWnd.CopyRect(pRect);
	CreateRgn(m_rectWnd.Width(), m_rectWnd.Height());
}

short CVideoWnd::CheckClickVideBtn(UINT nFlags, CPoint point)
{
	CRect rcBtn1, rcBtn2, rcBtn3, rcBtn4,rcBtn5;
	//--{{CompuBtnRect--------------------------------------
	m_rgnA3.GetRgnBox(rcBtn1);
	rcBtn1.left	=rcBtn1.right -TOOLBARBTN_WIDTHHEI-5;
	
	rcBtn2=rcBtn1;
	rcBtn2.right=rcBtn1.left-3;
	rcBtn2.left	=rcBtn2.right-TOOLBARBTN_WIDTHHEI;
	
	//----
	rcBtn3=rcBtn2;	
	rcBtn3.right=rcBtn2.left-6;
	rcBtn3.left	=rcBtn3.right-TOOLBARBTN_WIDTHHEI;
		
	rcBtn4=rcBtn3;
	rcBtn4.right=rcBtn3.left-3;
	rcBtn4.left	=rcBtn4.right-TOOLBARBTN_WIDTHHEI;
	
	rcBtn5=rcBtn4;
	rcBtn5.right=rcBtn4.left-3;
	rcBtn5.left	=rcBtn5.right-TOOLBARBTN_WIDTHHEI;	
	//--}}CompuBtnRect--------------------------------------
	
	short nClickBtn=-1;
	if(rcBtn1.PtInRect(point)) nClickBtn=2;
	else if(rcBtn2.PtInRect(point)) nClickBtn=3;
	else if(rcBtn3.PtInRect(point)) nClickBtn=4; //sound
	else if(rcBtn4.PtInRect(point)) nClickBtn=5; //talk
	//else if(rcBtn5.PtInRect(point)) nClickBtn=6; //resume-pause video
	return nClickBtn;
}

void CVideoWnd::DrawButton(CDC *pDC)
{
	CRect rcBtn1, rcBtn2, rcBtn3, rcBtn4,rcBtn5;
	//--{{CompuBtnRect--------------------------------------
	m_rgnA3.GetRgnBox(rcBtn1);	
	rcBtn1.left	=rcBtn1.right -TOOLBARBTN_WIDTHHEI-5;
	
	rcBtn2=rcBtn1;
	rcBtn2.right=rcBtn1.left-3;
	rcBtn2.left	=rcBtn2.right-TOOLBARBTN_WIDTHHEI;
	
	//------------
	rcBtn3=rcBtn2;	
	rcBtn3.right=rcBtn2.left-6;
	rcBtn3.left	=rcBtn3.right-TOOLBARBTN_WIDTHHEI;	
		
	rcBtn4=rcBtn3;
	rcBtn4.right=rcBtn3.left-3;
	rcBtn4.left	=rcBtn4.right-TOOLBARBTN_WIDTHHEI;

	rcBtn5=rcBtn4;
	rcBtn5.right=rcBtn4.left-3;
	rcBtn5.left	=rcBtn5.right-TOOLBARBTN_WIDTHHEI;
	//--}}CompuBtnRect--------------------------------------
	
	CPoint point(rcBtn1.left, rcBtn1.top);
	short nBtnBmpIndex=0;
	//if(m_nStatusCode>=STA_SUC_RATE0) nBtnBmpIndex=1;
	//short nBtnBmpIndex=1; //for 530
	//0,2,4,6 gray;	1,3,5,7 green;	8 recording;
	//9  sound off, 10 sound on
	//11 talk off,  12 talk on
	
	CDevInfo *pDevInfo=NULL;
	pDevInfo=m_pParent->m_objVWndMgr.GetDevInfoBy(m_nFixChnNo);
	if(pDevInfo->IsStartConnect()){
		//record
		point.x=rcBtn1.left;	point.y=rcBtn1.top;
		if(m_bRecordOSD) m_imgList.Draw(pDC, nBtnBmpIndex+7, point, ILD_TRANSPARENT);
		else m_imgList.Draw(pDC, nBtnBmpIndex+6, point, ILD_TRANSPARENT);

		//capture tool
		point.x=rcBtn2.left;	point.y=rcBtn2.top;		
		m_imgList.Draw(pDC, nBtnBmpIndex+3, point, ILD_TRANSPARENT);

		//sound 
		point.x=rcBtn3.left;	point.y=rcBtn3.top;	
		if(m_bSoundOSD) m_imgList.Draw(pDC, nBtnBmpIndex+10, point, ILD_TRANSPARENT);
		else m_imgList.Draw(pDC, nBtnBmpIndex+9, point, ILD_TRANSPARENT);

		//talk 
		point.x=rcBtn4.left;	point.y=rcBtn4.top;
		if(m_bTalkOSD) m_imgList.Draw(pDC, nBtnBmpIndex+12, point, ILD_TRANSPARENT);
		else m_imgList.Draw(pDC, nBtnBmpIndex+11, point, ILD_TRANSPARENT);
	}
}

void CVideoWnd::DrawStatus(CDC *pDC)
{
	CRect rcStatus;
	m_rgnA3.GetRgnBox(rcStatus);
	rcStatus.top +=2;
	rcStatus.left+=10;
	//rcStatus.top =rcStatus.bottom-TOOLBARBTN_WIDTHHEI;

	pDC->SetBkMode(TRANSPARENT);
	CFont* pfontOld;
	pfontOld = pDC->SelectObject(&m_fontStatu);
	
	//draw text
	pDC->SetTextColor(m_clrStatuTextFg);

	pDC->DrawText(m_csStatuText, rcStatus,  DT_SINGLELINE| DT_NOCLIP);	
	pDC->SelectObject(pfontOld);
}

void CVideoWnd::DrawTitle(CDC *pDC)
{
	pDC->FillRgn(&m_rgnA1, &m_clrVideoBkGnd);
	CRect rectTitle;	m_rgnA1.GetRgnBox(rectTitle);
	
	pDC->SetBkMode(TRANSPARENT);
	CFont* pfontOld= pDC->SelectObject(&m_fontWndTitle);

	CString csTitle;
	CDevInfo *pDevInfo=NULL;
	pDevInfo=m_pParent->m_objVWndMgr.GetDevInfoBy(m_nFixChnNo);
	if(!pDevInfo->IsStartConnect()) csTitle.Format(_T("%d"), m_nFixChnNo+1);
	else csTitle.Format(_T("%s"), pDevInfo->GetDevName());
	rectTitle.top+=3;	rectTitle.left+=10;	//Title position

	//draw text
	pDC->SetTextColor(m_clrWndTitleTextFg);
	pDC->DrawText(csTitle, rectTitle,  DT_SINGLELINE);//DT_VCENTER  | DT_CENTER| DT_NOCLIP
	pDC->SelectObject(pfontOld);
}

//@desc
//@retu -1: has one fullscreen window, but isnt own;
//		 0: without fullscreen window;
//		 1:	has one fullscreen window, but is own;
short CVideoWnd::GetFullScrnState()
{
	short nStatus=-1;
	short nWndNoFullScrn=m_pParent->m_objVWndMgr.GetWndIndexFullScrn();
	if(nWndNoFullScrn==-1) nStatus=0;
	else if(nWndNoFullScrn==m_nFixChnNo) nStatus=1;
	else nStatus=-1; //nWndNoFullScrn!=-1 && nWndNoFullScrn!=m_nFixChnNo

	return nStatus;
}

void CVideoWnd::DrawVWnd(CDC *pDC)
{
	short nFullScrn=0;
	nFullScrn=GetFullScrnState();
	if(nFullScrn==-1) return;	

	pDC->FillRgn(&m_rgnA4, &m_clrVideoBkGnd);
	pDC->FillRgn(&m_rgnA3, &m_clrVideoBkGnd);

	DrawButton(pDC);//draw button
	DrawStatus(pDC);
	DrawTitle(pDC); //??? may draw when startCam or stopCam?

	CRect rect2;
	m_pCtlVideoArea->GetClientRect(rect2);
	if(!m_bHasVideo) ::FillRect(m_hDC, &rect2, (HBRUSH)m_clrVideoBkGnd.GetSafeHandle());

	if(m_bFocus && nFullScrn==0){
		CRect focuRect(&m_rectWnd);
		focuRect.DeflateRect(1,1);
		pDC->FrameRect(focuRect, &m_brFocus);
	}
}

void CVideoWnd::SetOtherVideoAreaSize(BOOL bHasWndMax)
{
	if(bHasWndMax){
		m_pCtlVideoArea->MoveWindow(0,0,0,0, FALSE);
		m_pCtlVideoArea->SetWndEmpty();
		//TRACE("MoveWindow(0,0,0,0, FALSE)\n");
	}else{
		CRect rectVideo;
		m_rgnA2.GetRgnBox(&rectVideo);		
		m_pCtlVideoArea->MoveWindow(&rectVideo, FALSE);
		m_pCtlVideoArea->SetWndEmpty(FALSE);
		//TRACE("m_pCtlVideoArea->MoveWindow(&rectVideo, FALSE)\n");
	}
}

void  CVideoWnd::IconSetAudio(BOOL bShow)
{
	m_bSoundOSD=bShow;
	m_pParent->Invalidate();
}

void  CVideoWnd::IconSetSpeak(BOOL bShow)
{
	m_bTalkOSD=bShow;
	m_pParent->Invalidate();
}

BOOL CVideoWnd::CheckAndCreateDir(LPCTSTR lpPath)
{
	CString csPath(lpPath);
	if(csPath.IsEmpty()) return FALSE;//return----------------------------

	if(csPath.Find(_T("/"))!=-1) csPath.Replace(_T("/"), _T("\\"));
	CStringArray destArr;
	SplitString2(csPath, destArr, '\\');

	//such as: e:\\aa\\bb\cc\\ee\\dd
	//e:; aa; bb; cc; ee; dd
	CString csFullPath, csTmp;
	int nCount=destArr.GetSize();
	if(nCount>0){
		int i=0;
		for(i=0; i<nCount; i++){
			if(i==0) {
				csFullPath=destArr[i]+_T("\\");					
				if(!PathFileExists(csFullPath)) return FALSE;//return----
				else csTmp=csFullPath;
			}else {
				csFullPath=csTmp+_T("\\")+destArr[i];
				if(!PathFileExists(csFullPath)){ //create dir
					CreateDirectory(csFullPath, NULL);
					csTmp=csFullPath;
				}else csTmp=csFullPath+_T("\\");
			}
		}
	}else return FALSE; //return-----------------------------------------

	return TRUE;
}

int CVideoWnd::WriteBmp24(char *chPath, char *pBuffer, int nDataSize)
{
	BITMAPFILEHEADER bfh;
	BITMAPINFOHEADER bih;

	FILE *fp=NULL;
	fp=fopen(chPath, "wb");
	if(fp==NULL) return -2;

	memset( &bfh, 0, sizeof( bfh ) );
	bfh.bfType = 'MB';
	bfh.bfSize = sizeof( bfh ) + nDataSize + sizeof(BITMAPINFOHEADER);
	bfh.bfOffBits = sizeof( BITMAPINFOHEADER ) + sizeof(BITMAPFILEHEADER);
	fwrite(&bfh, sizeof( bfh ), 1, fp);

	memset(&bih, 0, sizeof(BITMAPINFOHEADER));
	memcpy(&bih, &m_bmiHead, sizeof(bih));
	fwrite(&bih, sizeof(bih), 1, fp);

	// Write the bitmap bits
	fwrite(pBuffer, 1, nDataSize, fp);

	fclose(fp);
	return 0;
}

int  CVideoWnd::Snapshot()
{
	CString csPath, csFilename;
	if(!m_bHasVideo) return -1;
	if(g_objSysSettings.m_snap_rec_file_path.IsEmpty()) csPath.Format(_T("%sSnapRecord"), g_csAppPath);
	else csPath=g_objSysSettings.m_snap_rec_file_path;
	if(!PathFileExists(csPath)) CheckAndCreateDir(csPath);
	
	COleDateTime odt(COleDateTime::GetCurrentTime());
	if(m_nCodec_id==MEDIA_CODEC_VIDEO_MJPEG) {
		csFilename.Format(_T("%s\\%s_%s.jpg"), csPath, odt.Format(_T("%Y-%m-%d")), odt.Format(_T("%H-%M-%S")));
		char chFilename[256]={0};
		CStrFromWSTRU(CP_ACP, csFilename, csFilename.GetLength(), chFilename);
		
		FILE *fp=NULL;
		fp=fopen(chFilename, "wb");
		if(fp==NULL) return -2;
		fwrite(m_pBufBmp24, 1, m_nDataSize, fp);
		fflush(fp);
		fclose(fp);
		return 0;

	}else{
		csFilename.Format(_T("%s\\%s_%s.bmp"), csPath, odt.Format(_T("%Y-%m-%d")), odt.Format(_T("%H-%M-%S")));
		char chFilename[256]={0};
		CStrFromWSTRU(CP_ACP, csFilename, csFilename.GetLength(), chFilename);
		return WriteBmp24(chFilename, (char *)m_pBufBmp24, m_nDataSize);
	}
}

void  CVideoWnd::AVPlay(tkCHAR *pData, tkINT32 nSize, tkVOID *pFrmInfo, tkINT32 nFrmInfoSize)
{
	if(pData==NULL || pFrmInfo==NULL || nSize<=0 || m_pH264Dec==NULL) return;
	FRAMEINFO_t *pstFrmInfo=(FRAMEINFO_t *)pFrmInfo;
	m_nCodec_id=pstFrmInfo->codec_id;
	m_bHasVideo=TRUE;
	switch(pstFrmInfo->codec_id)
	{
	case MEDIA_CODEC_VIDEO_H264:{
		memset(&m_outFrame, 0, sizeof(OUT_FRAME));
		int nRet=apiH264Decode(m_pH264Dec, (tkUCHAR *)pData, nSize, &m_outFrame);
		if(nRet<0) {
			TRACE(_T("AVPlay, apiH264Decode=%d,flags=%d, nSize=%d, %dX%d\n"), 
				nRet, pstFrmInfo->flags, nSize, m_outFrame.nWidth, m_outFrame.nHeight);
			return;
		}
		bool bGet=YUV420toRGB24(m_outFrame.pY,m_outFrame.pU,m_outFrame.pV, m_pBufBmp24, m_outFrame.nWidth, m_outFrame.nHeight);
		//TRACE(_T("AVPlay, flags=%d, codec_id=%d, nSize=%d, %dX%d, YUV420toRGB24=%d\n"), 
		//	pstFrmInfo->flags, pstFrmInfo->codec_id , nSize,m_outFrame.nWidth, m_outFrame.nHeight, bGet);
		if(bGet) {
			m_bmiHead.biWidth	=m_outFrame.nWidth;
			m_bmiHead.biHeight	=m_outFrame.nHeight;
			m_bmiHead.biSizeImage=m_outFrame.nWidth*m_outFrame.nHeight*3;
			m_nDataSize=m_bmiHead.biSizeImage;
			if(m_objShow==NULL) m_objShow=new CPicture();
			if(m_objShow->PushData(m_pBufBmp24, &m_bmiHead)) {
				CRect rect;
				m_pCtlVideoArea->GetClientRect(&rect);
				m_objShow->Show(m_hDC, &rect);
			}
		}
		}break;

	case MEDIA_CODEC_VIDEO_MJPEG:
		memcpy(m_pBufBmp24, pData, nSize);
		m_nDataSize=nSize;
		if(m_objShow==NULL) m_objShow=new CPicture();
		if(m_objShow->PushData((BYTE*)pData, nSize)) {
			CRect rect;
			m_pCtlVideoArea->GetClientRect(&rect);
			if(m_objShow->Show(m_hDC, &rect)){
				m_outFrame.nWidth =m_objShow->GetVWidth();
				m_outFrame.nHeight=m_objShow->GetVHeight();
			}
		}
		break;

	case MEDIA_CODEC_AUDIO_PCM:{
 		WIN_Play_Samples(pData, nSize);
 		}break;

	case MEDIA_CODEC_AUDIO_ADPCM:{
		int nPCMSize=nSize*4;
		Decode((char *)pData, nSize, (char *)m_pBufAudio);
		//TRACE(_T("AVPlay, MEDIA_CODEC_AUDIO_ADPCM, nSize=%d, nPCMSize=%d\n"), nSize, nPCMSize);
		WIN_Play_Samples(m_pBufAudio, nPCMSize);
		}break;

	case MEDIA_CODEC_AUDIO_G726:{
		TRACE(_T("AVPlay, MEDIA_CODEC_AUDIO_G726, nSize=%d\n"), nSize);
		}break;

	default:;
	}
}

void  CVideoWnd::SetStatus(tkINT32 nType, tkPVOID pObj, tkINT32 nValue,  CString csInfo)
{
	static CString CONN_MODE[]={_T("P2P"), _T("RLY"), _T("LAN")};

	if(nValue==SINFO_DISCONNECTED) m_csStatuText=_T("");
	else m_csStatuText=csInfo;

	if(nValue==SINFO_DISCONNECTED || nValue<0) m_bHasVideo=FALSE;
	if(nValue==SINFO_FPS_ETC_READY){
		tkUCHAR nOnlineNum=0, nConnMode=0;
		tkFLOAT fFrameRate=0.0f, fBytesRate=0.0f;

		apiGetObjInfo(pObj, GOI_OnlineNum, &nOnlineNum);
		apiGetObjInfo(pObj, GOI_ConnectMode, &nConnMode);
		apiGetObjInfo(pObj, GOI_FrameRate, &fFrameRate);
		apiGetObjInfo(pObj, GOI_BytesRate, &fBytesRate);
		m_csStatuText.Format(_T("%s  %dX%d  \nOnline=%d  %0.2f FPS  %0.3f kbps"), 
			CONN_MODE[nConnMode],
			m_outFrame.nWidth, m_outFrame.nHeight, nOnlineNum, fFrameRate,fBytesRate*8);
	}
	if(m_pParent->GetSafeHwnd()) {
		InvalidateRgn(m_pParent->GetSafeHwnd(), (HRGN)m_rgnA1.GetSafeHandle(), FALSE);
		InvalidateRgn(m_pParent->GetSafeHwnd(), (HRGN)m_rgnA3.GetSafeHandle(), FALSE);
	}
}

void  CVideoWnd::HandleRecvIOCtrl(tkINT32 nIOType, tkCHAR *pIOData, tkINT32 nIODataSize, tkPVOID pObj)
{

}

