#ifndef _COLORSC_H
#define _COLORSC_H

typedef   UCHAR		uint8_t;
typedef   ULONGLONG uint64_t;

// API declaration
#ifdef  __cplusplus
extern "C" {
#endif

	void yuv422To420(const uint8_t *src, uint8_t *ydst, uint8_t *udst, uint8_t *vdst,
					 long width, long height,
					 long lumStride, long chromStride, long srcStride);
	
	void yuv2rgb24(uint8_t *puc_y,      int stride_y, 
				   uint8_t *puc_u,      uint8_t *puc_v, int stride_uv, 
				   uint8_t *puc_out,    int width_y,    int height_y,   int stride_out);

#ifdef  __cplusplus
			}
#endif


#endif

