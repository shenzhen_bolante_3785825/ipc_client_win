// LogFile.cpp: implementation of the CLogFile class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "LogFile.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLogFile::CLogFile()
{
	m_bReady				= FALSE;
	m_nNo					= 0;
	m_csPreFileName			= "";
	m_csFirstLogPath		= "";
	m_csFirstFileNamePrefix	= "";

	InitializeCriticalSection(&m_critSec);
}

CLogFile::~CLogFile()
{
	DeleteCriticalSection(&m_critSec);
	if(m_bReady) {
		m_File.Close();
		m_bReady=FALSE;
	}
	m_nNo=0;
}

//
BOOL CLogFile::CreateLogFile(LPCTSTR lpPath, LPCTSTR lpFileNamePrefix)
{
	if(lpPath==NULL) return FALSE;

CDataLock cs(m_critSec, "WriteLog");
	if(m_bReady) m_File.Close();
	m_bReady=FALSE;
	
	CString csFileName, csPath(lpPath), csText;	
	if(csPath.Right(1)!="\\") csPath+="\\";
	if(!PathFileExists(csPath)) return FALSE;
			
	csText.Format(_T("%sLog"), csPath); //Dir
	if(!PathFileExists(csText)) CreateDirectory(csText, NULL);
	csPath=csText+_T("\\");
	
	m_csFirstLogPath=csPath;

	SYSTEMTIME st;	::GetLocalTime(&st);
	COleDateTime odt(st);
	//csText=odt.Format("%Y%m%d_%H%M%S");
	csText=odt.Format(_T("%Y%m%d"));
	if(lpFileNamePrefix==NULL) {
		csFileName.Format(_T("%s%s_%d.txt"), csPath, csText, m_nNo);
		m_csPreFileName.Format(_T("%s_%d.txt"), csText, m_nNo);
	}else {
		m_csFirstFileNamePrefix=lpFileNamePrefix;
		csFileName.Format(_T("%s%s%s_%d.txt"), csPath, lpFileNamePrefix, csText, m_nNo);
		m_csPreFileName.Format(_T("%s%s_%d.txt"), lpFileNamePrefix, csText, m_nNo);
	}

	m_bReady=m_File.Open(csFileName, CFile::modeReadWrite | CFile::shareDenyWrite);	
	if(m_bReady) m_File.SeekToEnd();
	else m_bReady=m_File.Open(csFileName, CFile::modeCreate | CFile::modeReadWrite | CFile::shareDenyWrite);
	
	if(m_bReady) m_nNo++;
	return m_bReady;
}

void CLogFile::WriteLog(LPCTSTR lpszFormat, ...)
{	
	if(!m_bReady || lpszFormat==NULL) return; //return----------

CDataLock cs(m_critSec, "WriteLog");
	CString csInText, csLog;
	if(!AfxIsValidString(lpszFormat)){
		COleDateTime odt;
		csInText.Format(_T("Format string(%s) isn't valid.\n"), lpszFormat);

		odt=COleDateTime::GetCurrentTime();
		csLog.Format(_T("%s %s\n"), odt.Format(_T("%Y-%m-%d %H:%M:%S")), csInText);
		m_File.WriteString(csLog);
		m_File.Flush();
		return; //return----------------------------------------
	}

	va_list argList;
	va_start(argList, lpszFormat);
	csInText.FormatV(lpszFormat, argList);
	va_end(argList);

	//----------------------------------------------------------
	if(m_File.GetLength()>=ONELOG_MAXLEN) {
		CString csText, csFileName, csTmp;
		SYSTEMTIME st;	::GetLocalTime(&st);
		COleDateTime odt(st);
		csText=odt.Format(_T("%Y%m%d"));
		csTmp.Format(_T("%s%s"), m_csFirstFileNamePrefix, csText);
		if(m_csPreFileName.Find(csTmp)!=-1){ //find
			csFileName.Format(_T("%s%s%s_%d.txt"), m_csFirstLogPath, m_csFirstFileNamePrefix, csText, m_nNo);
			m_csPreFileName.Format(_T("%s%s_%d.txt"), m_csFirstFileNamePrefix, csText, m_nNo);
		}else {
			m_nNo=0;
			csFileName.Format(_T("%s%s%s_%d.txt"), m_csFirstLogPath, m_csFirstFileNamePrefix, csText, m_nNo);				
			m_csPreFileName.Format(_T("%s%s_%d.txt"), m_csFirstFileNamePrefix, csText, m_nNo);				
		}
		CloseLogFile();

		m_bReady=m_File.Open(csFileName, CFile::modeReadWrite | CFile::shareDenyWrite);	
		if(m_bReady) m_File.SeekToEnd();
		else m_bReady=m_File.Open(csFileName, CFile::modeCreate | CFile::modeReadWrite | CFile::shareDenyWrite);
		if(m_bReady) m_nNo++;
		
		if(!m_bReady) {
			CloseLogFile();
			return; //return------------------------------------
		}
	}

	//odt=COleDateTime::GetCurrentTime();
	//csLog.Format("%s %s\n", odt.Format("%Y-%m-%d %H:%M:%S"), csInText);
	//odt.GetAsSystemTime();
	SYSTEMTIME st;	::GetLocalTime(&st);
	csLog.Format(_T("%04d-%02d-%02d %02d:%02d:%02d.%03d %s\n"), 
		st.wYear, st.wMonth, st.wDay, st.wHour,st.wMinute,st.wSecond,st.wMilliseconds, csInText);

	m_File.WriteString(csLog);
	m_File.Flush();
}

void CLogFile::CloseLogFile()
{
	if(m_bReady) m_File.Close();
	m_bReady=FALSE;
}
