
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//--{{TCP--------------------------------------
#define RECVBUF_MAX_SIZE	1024*8
#define JPGBUF_MAX_SIZE		1024*1024

#define PACKBUF_HEAD		1024
#define PACKBUF_BODY		1024*255
//--}}TCP--------------------------------------

//224 bytes
typedef struct _DDNS_CONF
{
	unsigned char srvName[64];	//DDNS服务器名
	unsigned char dn2[128];		//DDNS动态二级域名
	unsigned char username[16];	//ASCII的用户名
 	unsigned char passw[16];	//ASCII的密码	
	
}CONF_DDNS, *PCONF_DDNS;

//96 bytes
typedef struct _PLATF_CONF
{
	unsigned char srvName[64];	//平台服务器名	
 	unsigned char username[16];	//ASCII的用户名
 	unsigned char passw[16];	//ASCII的密码
	
}CONF_PLATF, *PCONF_PLATF;

//240+224+96 byte
typedef struct _DEV_CONF_INFO
{
	unsigned char mac[6];		//MAC 设备物理地址, 更新设备信息时, 不允许更改
	unsigned char cnfa;			//config a
	unsigned char cnfb;			//config b
	
// 	unsigned char reserve0[4];//must set to 0x00 
// 	unsigned char reserve1[4];//must set to 0x00 //factsIP Comment by juju on 090625
// 							  //如果平台服务器域名栏直接填写IP地址,
// 							  //1>需把该IP转换成unsigned long 赋值给这个4字节数组.
// 							  //2>且把第三方DDNS域名字符串项置0.
// 	unsigned char reserve2[4];//must set to 0x00 
// 	unsigned char reserve3[2];//must set to 0x00
	unsigned char ddnsserverip[4];//must set to 0x00 //ddnsIP  Comment by juju on 090625
	unsigned char p2pserverip[4]; //reserve1[4];//must set to 0x00 //factsIP Comment by juju on 090625
								//如果平台服务器域名栏直接填写IP地址,
								//1>需把该IP转换成unsigned long 赋值给这个4字节数组.
								//2>且把第三方DDNS域名字符串项置0
	//091106 removed unsigned char reserve2[4];//must set to 0x00 //secuIP1 Comment by juju on 090625
	unsigned char reserve2[1];	//must set to 0x00 
	unsigned char udpport[2];	//P2P-UDP 端口. 如果填0 默认=28500
	unsigned char fpsctl;		//2009-8-20 16:58 新增帧速控制: 0-15  0--全速 15--最慢	
	unsigned char serverport[2]; //平台服务器的TCP端口. 如果填0 默认=80. 

	unsigned char cnfc;
	unsigned char cmpRatio;		//图像压缩率

	unsigned char firmVer[2];	//当前是:[0]=0x26 [1]=0x00; 更新设备信息时, 不允许更改
	unsigned char httpPort[2];	//{[0]:Hight [1]:Low }=HTTP port 设备HTTP端口
	unsigned char ip[4];		//my ip 设备的内网IP

	unsigned char subnet[4];	//net mask 子网掩码
	unsigned char gw[4];		//gateway ip 网关IP

	unsigned char dnsip1[4];	//DNS1 IP
	unsigned char dnsip2[4];	//DNS2 IP
	
	unsigned char username[64];	//user for watching
	unsigned char passw[64];	//password for watching
	unsigned char ppoeUser[32];	//备用
	unsigned char ppoePw[32];	//备用

	CONF_DDNS	  cnfDDNS;	//224 bytes
	CONF_PLATF	  cnfPlatf;	//96 bytes
	
}DEV_CONF_INFO, *PDEV_CONF_INFO;


//for cnfa========================================================================
								//0x1100 0000
#define CNFA_PIX1       0x80	//pix[1]:pix[0]: 分辨率0:0==160*120 0:1==320*240 1:0==640*480
#define CNFA_PIX0		0X40	//pix[0]
	#define CNFA_PIX_ALL	0xC0
	#define CNFA_PIX_640	0x80
	#define CNFA_PIX_320	0x40
	#define CNFA_PIX_160	0x00
#define CNFA_ENVI1      0x20	//bit5 =1: 室外; =0: 室内
#define CNFA_ENVI0      0x10	//bit4 =1: 60Hz; =0: 50Hz
								//00:室内50Hz 01:室内60Hz 02:室外&50Hz 03:室外&60Hz
								//室内50Hz:开机默认室内50Hz,适合大陆
								//室内60Hz:开机默认室内60Hz,适合?
								//室外&50Hz:开机默认室外,在用户点击网页的"室内"按钮时切换到室内50Hz.适合大陆
								//室外&60Hz:开机默认室外,在用户点击网页的"室内"按钮时切换到室内60Hz.适合?
//{{FirmVer>=0x26-----------------------------------------
#define CNFA_LIGHT_NIGHT  0x08	//夜视 Enable

	//cnfc
#define CNFC_FLIP		  0X80	//图像上下颠倒
#define CNFC_MIRROR		  0x40	//图像水平翻转
#define CNFC_FPS_LIM      0x10	//帧速限制在15FPS(2424MHz)
#define CNFC_P2P_DIS      0x08	//0: P2P enable    1:P2P disable
#define CNFC_UPNP_EN      0x04	//0: UPNP disable  1:UPNP enable

	//cmpRatio
#define CMPRATIO_0		  0xE0
#define CMPRATIO_1		  0x89
#define CMPRATIO_2		  0x59
#define CMPRATIO_3		  0x39
#define CMPRATIO_4		  0x29

//cmpRatio:0X29~0XE0
	//压缩率越大,帧速越快(对公网影响较大),而图像质量降低.
	//可分 "最高" "高" "中" "低" "最低"五个档次,
	//分别取值:0x29, 0x39, 0x59, 0x89, 0xE0
//}}FirmVer>=0x26-----------------------------------------

//FirmVer>=0x26 for cnfa low 2bits
//FirmVer <0x26 for cnfa low 4bits
#define CFG1_NEVER_LOGIN  0		//2小时向平台发送注册包
#define CFG1_TIMER_LOGIN  1		//3分钟向平台发送注册包
//#define CNFA_ADSL_TYPE     0x01//0-ADSL按时计费用户 1-包月用户

//for cnfb========================================================================
#define CNFB_DHCP_EN      0X80	//DHCP ON.自动获取IP
#define CNFB_RESERVE0     0X20	//保留.must set to 0
#define CNFB_UPLOADI_EN   0X10	//启用向平台服务器发送报警消息

//for cnfb:LOW 3bit.设置串口波特率
#define B2400             0
#define B4800             1
#define B9600             2
#define B115200           3

typedef struct _SDKVER
{
	union{
		struct {unsigned char v1,v2,v3,v4;}ver_b;
		UINT ver_l;
	};
}SDKVER;

//////////////////////////////////////////////////////////////////////////
//interface
//////////////////////////////////////////////////////////////////////////
#ifndef  _IPCAMSDK_
#define  _IPCAMSDK_

#ifdef IPCAMSDK_EXPORT
	#define  DLLEXPORT __declspec(dllexport)
#else 
	#define  DLLEXPORT 
#endif

extern "C"
{
	typedef void (CALLBACK *PFUN_DEVINFO) (DEV_CONF_INFO *pDevInfo, LPVOID lPara);// //test by juju on 090820
	typedef void (CALLBACK *PFUN_GETCURFRAME)(BYTE *pFrame, unsigned long nFrameSize, LPVOID lpParameter);

	DLLEXPORT int  __stdcall SEGetSDKVer(void);
	DLLEXPORT BOOL __stdcall LoadSockDll(void);
	DLLEXPORT void __stdcall UnLoadSockDll(void);
	DLLEXPORT void __stdcall SEUdpCreateEnv2(void *pUdpEnv, PFUN_DEVINFO pfunDevInfo, LPVOID lPara, 
									short *pOutErrCode, unsigned short nPort);

	//--{{工作在内网,用于对设备的搜索和配置.-----------------------------------------------
	DLLEXPORT void  *SEUdpCreateEnv(PFUN_DEVINFO pfunDevInfo, LPVOID lPara, 
									short *pOutErrCode, unsigned short nPort=62001);
	DLLEXPORT void  __stdcall SEUdpDestroyEnv(void *pUdpEnv);
	DLLEXPORT short __stdcall SEUdpQueryDev(void *pUdpEnv);

			//nFirmVer={将DEV_CONF_INFO, firmVer[0]值先转成十六进制串, 在把这个十六进制串转成int类型}
	DLLEXPORT short __stdcall SEUdpUpdateDev(void *pUdpEnv, DEV_CONF_INFO *pDevInfo, int nFirmVer=29);
			//nFirmVer={DEV_CONF_INFO, firmVer[0]}, 不用转换
	DLLEXPORT short __stdcall SEUdpUpdateDev2(void *pUdpEnv, DEV_CONF_INFO *pDevInfo, int nFirmVer);
	//--}}工作在内网,用于对设备的搜索和配置.-----------------------------------------------


	//--{{工作在内、外网, 用于获取视频流.--------------------------------------------------
	DLLEXPORT void  *SETcpCreateEnv1();
			//@para pUserPw: base64Code of user:passw
	DLLEXPORT void  *SETcpCreateEnv2(PFUN_GETCURFRAME pfunGetCurFrame, LPVOID lPara,
									char *pDevIP, unsigned short nDevPort, char *pUserPw,
									short nTimeReconn=10, short nTimes=5);
			//@para pUserPw: base64Code of user:passw
	DLLEXPORT short  __stdcall SETcpSetEnvPara(void *pTcpEnv, PFUN_GETCURFRAME pfunGetCurFrame, LPVOID lPara,
									char *pDevIP, unsigned short nDevPort, char *pUserPw,
									short nTimeReconn=10, short nTimes=5);
			//
			//@return >0 1: success
			//		  <0 failture
			//			-1: sys is already running
			//			-2: lpDevIP==NULL, nDevPort==0
			//			-3: connect dev failture
			//			-4: create socket failture
			//			-5: send data failture
	DLLEXPORT short __stdcall SETcpStart(void *pTcpEnv);
	DLLEXPORT void  __stdcall SETcpStop(void *pTcpEnv);
	DLLEXPORT BOOL  __stdcall SETcpHasVideo(void *pTcpEnv);

		//@desc applicable for firmver<30
		//@para nPara	0x0000	0 00 0
		//				320/640(1bit)	low/std/height(2bit)	in/out(1bit)
		//				0/1				0/1/2					0/1		
		//		nType	1: condition; 2: bright; 3: resolution
	DLLEXPORT BOOL  __stdcall SETcpSetVideoPara(void *pTcpEnv, short nType, BYTE nPara);

	//default=0x00 00 00 10
	//low byte2=1 outer	//cmmd=IE01
	//		   =2 inner	//cmmd=IE00
	//		   =4 night	//cmmd=1D01
	//low byte3, low 4 bit,bright=1 low		//cmmd=2001
	//							 =2 std		//cmmd=2003
	//							 =4 hight	//cmmd=2005
	//			 high 4bit, reso
	//							 =1 160X120	//cmmd=1800
	//							 =2 320X240	//cmmd=1801
	//							 =4 640X480	//cmmd=1802
	
	//low byte4, low 1 bit, flip pic
	//							=0 FALSE; =1 TRUE	//1: cmmd=2201; 0:2200
	//			 low 2 bit, mirroe pic		
	//							=0 FALSE; =1 TRUE	//1: cmmd=2101; 0:2100
	//
	//			 high 5 bit video quality		>>27bits
	//							=0x08 lowest	0x01	//cmmd=13E0
	//							=0x10 low		0x02	//cmmd=1389
	//							=0x20 middle	0x04	//cmmd=1359
	//							=0x40 hight		0x08	//cmmd=1339
	//							=0x80 highest	0x10	//cmmd=1329
	DLLEXPORT BOOL  __stdcall SETcpSetVideoPara2(void *pTcpEnv, DWORD nVideoPara, int nFirmVer=30);

	DLLEXPORT void  __stdcall SETcpDestroyEnv(void *pTcpEnv);
	//--}}工作在内、外网, 用于获取视频流.--------------------------------------------------

		//inUserPw=user:passw
	DLLEXPORT short __stdcall SEEnBase64(char *inUserPw, char *outBase64, int outBase64Len);
	DLLEXPORT short __stdcall SEDeBase64(char *outUser, char*outPW, char*inStr);
}

#endif


