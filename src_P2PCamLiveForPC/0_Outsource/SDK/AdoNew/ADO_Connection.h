// ADO_Connection.h: interface for the CADO_Connection class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ADO_CONNECTION_H__2EB0A240_FBF4_491C_8A98_EF116DE2B0EE__INCLUDED_)
#define AFX_ADO_CONNECTION_H__2EB0A240_FBF4_491C_8A98_EF116DE2B0EE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Shlwapi.h"
#pragma comment(lib, "Shlwapi.lib")

#ifdef _WIN32_WCE
	#include <COMDEF.H>
	#include <ADOCE30.H>
#else	
	#pragma warning(disable:4146)
	#import "C:\Program Files\Common Files\System\ADO\msado15.dll" named_guids rename("EOF","adoEOF"), rename("BOF","adoBOF")
	#pragma warning(default:4146)
	using namespace ADODB;

	#include <icrsint.h>

	class CADO_Connection;
	#include "ADO_Recordset.h"
	#include "ADO_Command.h"

	typedef enum tagEmDBType{
		DBT_ACCESS	=1,
		DBT_SQL2000	=2,
		DBT_MYSQL	=3,
	}EM_DBTYPE;
#endif

extern CADO_Connection* g_pDBConn;

// 数值类型转换 -----------------------------------
COleDateTime vartodate(const _variant_t& var);
COleCurrency vartocy(const _variant_t& var);
bool		 vartobool(const _variant_t& var);
BYTE		 vartoby(const _variant_t& var);
short		 vartoi(const _variant_t& var);
long		 vartol(const _variant_t& var);
double		 vartof(const _variant_t& var);
CString		 vartostr(const _variant_t& var);


// ------------------------------------------------
// CADO_Connection class
// ------------------------------------------------
class CADO_Connection
{
public:
	CADO_Connection(LPCTSTR pcszProvider);
	virtual ~CADO_Connection();
	
protected:
	void Release();
		
public:
	_ConnectionPtr& GetConnection() { return m_pConnection; }
	
	CString	  GetLastErrorText();
	ErrorsPtr GetErrors();
	ErrorPtr  GetError(long index);

	CString GetConnectionText() { return m_strConnect; }

	CString GetProviderName();
	CString GetVersion();
	CString GetDefaultDatabase();
		
	BOOL IsOpen();
	long GetState();
	
	// 连接模式 ----------------------------------
	ConnectModeEnum GetMode();
	BOOL SetMode(ConnectModeEnum mode);
	
	// 连接时间 ----------------------------------
	long GetConnectTimeOut();
	BOOL SetConnectTimeOut(long lTime = 5);

	_RecordsetPtr OpenSchema(SchemaEnum QueryType);	// 数据源信息

public:
	BOOL Open(LPCTSTR lpszConnect =_T(""), long lOptions = adConnectUnspecified);
	void Close();
	
	long BeginTrans();
	BOOL RollbackTrans();
	BOOL CommitTrans();

	_RecordsetPtr Execute(LPCTSTR strSQL, long *pRecNumAffected=NULL, long lOptions = adCmdText);
	BOOL Cancel();

protected:
	CString			m_strConnect;
	_ConnectionPtr	m_pConnection;
};

#endif // !defined(AFX_ADO_CONNECTION_H__2EB0A240_FBF4_491C_8A98_EF116DE2B0EE__INCLUDED_)
