// ADO_Command.h: interface for the CADO_Command class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ADO_COMMAND_H__F29C9CEF_0845_4F11_9814_D253FE0B8E0E__INCLUDED_)
#define AFX_ADO_COMMAND_H__F29C9CEF_0845_4F11_9814_D253FE0B8E0E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ADO_Connection.h"

/*########################################################################
			------------------------------------------------
						   CADO_Parameter class
			------------------------------------------------
  ########################################################################*/
class CADO_Parameter
{
// 构建/折构 -------------------------------------------
public:
	CADO_Parameter();
	CADO_Parameter(DataTypeEnum DataType, long lSize, ParameterDirectionEnum Direction, CString strName);
	virtual ~CADO_Parameter();
// 属性 ------------------------------------------------
public:
	_ParameterPtr GetParameter() {return m_pParameter;}

	// 参数精度 ---------------------------
	BOOL SetPrecision(char nPrecision);

	// 参数小数位数 -----------------------
	BOOL SetNumericScale(int nScale);

	// 参数类型 ---------------------------
	ParameterDirectionEnum GetDirection();
	BOOL SetDirection(ParameterDirectionEnum Direction);
	
	// 参数名称 ---------------------------
	CString GetName();
	BOOL SetName(CString strName);

	// 参数长度 ---------------------------
	int GetSize();
	BOOL SetSize(int size);

	// 参数据类型 -------------------------
	DataTypeEnum GetType();
	BOOL SetType(DataTypeEnum DataType);

// 方法 ------------------------------------------------
public:	
	BOOL GetValue(COleDateTime &value);
	BOOL GetValue(CString &value);
	BOOL GetValue(double &value);
	BOOL GetValue(long &value);
	BOOL GetValue(int &value);
	BOOL GetValue(short &value);
	BOOL GetValue(BYTE &value);
	BOOL GetValue(bool &value);

	BOOL SetValue(const float &value);
	BOOL SetValue(const short &value);
	BOOL SetValue(const BYTE &value);
	BOOL SetValue(const COleDateTime &value);
	BOOL SetValue(const CString &value);
	BOOL SetValue(const double &value);
	BOOL SetValue(const long &value);
	BOOL SetValue(const int &value);
	BOOL SetValue(const bool &value);
	BOOL SetValue(const _variant_t &value);

//其他方法 ------------------------------
public:
	_ParameterPtr& operator =(_ParameterPtr& pParameter);

// 数据 ------------------------------------------------
protected:
	_ParameterPtr m_pParameter;
	CString m_strName;
	DataTypeEnum m_nType;
};


/*########################################################################
			------------------------------------------------
						   CADO_Command class
			------------------------------------------------
  ########################################################################*/

class CADO_Command
{
// 构建/折构 -------------------------------------------
public:
	CADO_Command();
	CADO_Command(CADO_Connection* pAdoConnection, CString strCommandText = _T(""), CommandTypeEnum CommandType = adCmdStoredProc);
	virtual ~CADO_Command();

// 属性 ------------------------------------------------
public:
	_variant_t GetValue(LPCTSTR lpstrName);
	_variant_t GetValue(long index);

	_ParameterPtr GetParameter(long index);
	_ParameterPtr GetParamter(LPCTSTR lpstrName);

	BOOL Append(_ParameterPtr param);

	ParametersPtr GetParameters();
	
	BOOL SetCommandTimeOut(long lTime);

	long GetState();

	BOOL SetCommandType(CommandTypeEnum CommandType);
	BOOL SetCommandText(LPCTSTR lpstrCommand);

	CADO_Parameter operator [](int index);
	CADO_Parameter operator [](LPCTSTR lpszParamName);

// 实现 ------------------------------------------------
public:
	_ParameterPtr CreateParameter(LPCTSTR lpstrName, DataTypeEnum Type, 
								  ParameterDirectionEnum Direction, 
								  long Size, _variant_t Value);
	_RecordsetPtr CADO_Command::Execute(long Options = adCmdStoredProc);
	BOOL Cancel();
	
// 其他方法 --------------------------------------------
public:
	_CommandPtr& GetCommand();
	BOOL SetConnection(CADO_Connection *pConnect);

// 数据 ------------------------------------------------
protected:
	void Release();
	_CommandPtr		m_pCommand;
	CString			m_strSQL;
};

#endif // !defined(AFX_ADO_COMMAND_H__F29C9CEF_0845_4F11_9814_D253FE0B8E0E__INCLUDED_)
