#pragma once

class CLangStr
{
public:
	enum{ DOLANG_NONE, DOLANG_ANSI, DOLANG_UNICODE, };
public:
	CLangStr(TCHAR chPrefix='$');
	virtual ~CLangStr(void);

	//nType=1:
	//		[CurrentLang]
	//			Lang=LangName
	//			Ver=x.x.x.x
	//		[LangName]
	//			$xxxx=xxxx
	//
	//nType=2:
	//		[Lang]
	//			Name=LangName
	//			Ver=x.x.x.x
	//			#xxxx=xxxx
	//
	BOOL	LoadLangFile(LPCTSTR lpPathFileIni, LPCTSTR lpLangIni, UINT uResID, 
						 BOOL bDelTmpFile=TRUE, short nType=1);
	//Unicode Ini don't translate ANSI
	BOOL	LoadLangFileU(LPCTSTR lpPathFileIni, LPCTSTR lpLangIni, UINT uResID, 
						  BOOL bDelTmpFile=TRUE, short nType=1);
	// doWhatEncode:
	//	1: by ANSI
	//	2: by UNICODE
	BOOL	LoadLangFile2(LPCTSTR lpPathFileIni, LPCTSTR lpLangIni,
						  UINT uResID, short doWhatEncode, BOOL bDelTmpFile=TRUE, short nType=1);
	void	LoadUIStr(HWND hCurWnd, bool bSetCurWndTitle=false);
	void	RemoveAll()							{	m_mapLang.RemoveAll();	}

	LPCTSTR GetCurLangName()					{	return m_csCurLang;		}
	LPCTSTR GetLangAt(LPCTSTR lpKey);

	void	SetTransFlag(BOOL bTrans)			{	m_bTrans=bTrans;		}
	BOOL	GetTransFlag()						{	return m_bTrans;		}
	int     GetLangItemNum();
	CMapStringToString *GetMapStringToString()	{	return &m_mapLang;		}
	void	SetPrefix(TCHAR chPrefix)			{	m_chPrefix=chPrefix;	}
	TCHAR	GetPrefix()							{	return m_chPrefix;		}

private:
	void	SetCurLangName(LPCTSTR lpLangName)	{	m_csCurLang=lpLangName;	}
	void	SetLangAt(LPCTSTR lpKey, LPCTSTR lpValue);

	BOOL CStrFromWSTRU(IN UINT codePage, IN LPCWSTR wstr, IN UINT len, OUT char *pStr);
	void LoadLangStr(LPCTSTR lpLangFileTmp, BOOL bUnicode, short nType);
	BOOL SplitStr(LPCTSTR lpStr, CString &csKey, CString &csValue);
	int  LangTrans(LPCTSTR lpConfFileName);
	int  LangTransU(LPCTSTR lpConfFileName);
	BOOL Unicode2Ansi(unsigned short *pUnicodeSrc, int nUnicodeSrcSize, 
					  char *pOutBuf, int *pnOutBufSize);
	BOOL	m_bTrans;
	TCHAR	m_chPrefix;
	CString m_csCurLang;
	CString m_csPathFileIni;
	BOOL	m_bDelTmpFile;
	short	m_nDoWhatEncode;
	CMapStringToString m_mapLang;

};
