#ifndef AV_DECODER_LIB_HEADER_H
#define AV_DECODER_LIB_HEADER_H

#ifdef AV_DECODE_EXPORTS
#define AVDECODE_API __declspec(dllexport)
#else
#define AVDECODE_API __declspec(dllimport)
#endif

#include <windows.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

	AVDECODE_API LONG InitialPlayer(HWND hWnd) ;
	AVDECODE_API BOOL UninitalPlayer(LONG lHandler);
	AVDECODE_API BOOL AddFrameData(LONG lHandler, BYTE* buff, int nSize, void *pFrmInfo);
	AVDECODE_API BOOL UpdateOSDData(LONG lHandler, const TCHAR* buff, int nSize);
	AVDECODE_API BOOL SaveBitmap(LONG lHandler, const TCHAR *inFilePath);

#ifdef __cplusplus
}
#endif

#endif