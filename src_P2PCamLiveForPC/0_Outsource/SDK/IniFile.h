
#ifndef _INIFILE_H_
#define _INIFILE_H_

#include <vector>
#include <afxtempl.h>
//用户接口说明:在成员函数SetVarStr和SetVarInt函数中,当iType等于零,则如果用户制定的参数在ini文件中不存在,
//则就写入新的变量.当iType不等于零,则如果用户制定的参数在ini文件中不存在,就不写入新的变量，而是直接返回FALSE;
class CIniFile
{
public:
	CIniFile();
	CIniFile(BOOL bWriteBack);
	virtual ~CIniFile();

private:
	CIniFile(const CIniFile &);
	CIniFile & operator = (const CIniFile &);

public:	
	BOOL Create(const CString &strFileName, BOOL bUnicode);//创建函数
	//Get all content in the strSection
	BOOL GetVarStr(IN const CString &strSection, OUT CString &strReturnValue);
	CArray<CString,CString>* GetVarStr(IN LPCTSTR pstrSection, OUT std::vector<int> &vecValidRows);

	//得到变量字符串型数值
	BOOL GetVarStr(const CString &strSection,const CString &strVarName,CString &strReturnValue);
	//得到变量整数型数值
	BOOL GetVarInt(const CString &strSection,const CString &strVarName,int &iValue);
	//重新设置变量字符串型数值
	BOOL SetVarStr(const CString &strSection,const CString &strVarName,const CString &strValue,const int iType=1);
	//重新设置变量整数型数值
	BOOL SetVarInt(const CString &strSection,const CString &strVarName,const int &iValue,const int iType=1);

private:
	BOOL GetVar(const CString &strSection, const CString &strVarName,CString &strReturnValue);	

	BOOL SetVar(const CString &strSection, const CString &strVarName,const CString &strVar, const int iType=1);	
	int SearchLine(const CString &strSection, const CString & strVarName);
	int SearchLine(const CString &strSection, std::vector<int> &vecValidRows);

private:
//	vector <CString>  FileContainer;
	CArray <CString,CString> FileContainer;
	BOOL  bFileExsit;
	CStdioFile stfFile;
	CString strInIFileName;

	BOOL	m_bWriteBack;
};

#endif