
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//================================================================================
// UDP
// 1.Get new version
//	 server udp port:	#define	DEFAULT_UDP_PORT		30003
//================================================================================
#define MSGTYPE_GETCLINEWVER		121
#define MSGTYPE_GETCLINEWVERSUC		122
#define MSGTYPE_GETCLINEWVERFAI		123
	//Failture: ackno =1: no new ver; =2: has new ver but file has question
	//Success:  ackno=0;


//================================================================================
// TCP
//	[111, 116] for IPCamCltFac.exe
// 2.Get File
//================================================================================
#define DFILEPACKET_HEADLEN		8
#define	APP_BUFFER_SIZE			(1024*9)	//接收数据缓冲区
#define READONCE_BUFFER_SIZE	(1024*8)

//server, client
//SO_SNDBUF, SO_RCVBUF=1024*9
typedef struct _DFILEPACKET
{
	//该结构必须 4 字节对齐. BigEndian排列,
	//Windows客户端须做高低字节转换
	unsigned char	verId;		//=0xa0
	unsigned char	msgType;
	unsigned short	ackno;
	unsigned long	payLoad;	//getReq[]字符串长度	
	//8 bytes
	unsigned char	getReq[READONCE_BUFFER_SIZE];
}DFILEPACKET, *PDFILEPACKET;

//--{{util fun-------------------------------------------------------------
inline void Pack_BigEndian(DFILEPACKET &stPacket)
{
	stPacket.payLoad=htonl(stPacket.payLoad);
	stPacket.ackno =htons(stPacket.ackno);
}

inline void Pack_LittleEndian(DFILEPACKET &stPacket)
{	
	stPacket.payLoad =ntohl(stPacket.payLoad);
	stPacket.ackno  =ntohs(stPacket.ackno);	
}
//--}}util fun-------------------------------------------------------------
// other the same as tcp_proto.h, p2p_proto.h




//server
#define	TCP_PORT_DOWNLOAD		30006

#define MAX_SUBTHREAD_SIZE		(5)
#define	OVERLAPPEDPLUSLEN	(sizeof(IO_OPERATION_DATA))

//I/O操作类型
#define		IORead			10				//接收包
#define		IOWriteCMD		11				//发送命令
#define		IOExit			12				//客户端退出


// I/O data structure
typedef	struct tag_io_operation_data 
{
	OVERLAPPED	overlapped;
	UINT		IOType;						//operator type
	char		recvBuf[APP_BUFFER_SIZE];
	DWORD		realSize;					//actual size of buffer
}IO_OPERATION_DATA, *PIO_OPERATION_DATA;


//client
#define MSGTYPE_VERFILESLEN		121
#define MSGTYPE_VERFILESLEN_RS	122
#define MSGTYPE_VERFILESDATA	123



