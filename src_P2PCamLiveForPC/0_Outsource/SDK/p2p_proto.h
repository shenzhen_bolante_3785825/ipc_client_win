
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "update_proto.h"


//for server
#define	DEFAULT_UDP_PORT		30003

#define TIMER_IDEVENT			102
#define TIMEOUT_DEVOFFLINE		5*60		//unit: s
#define TIMER_LOOP_CHKDEVONLINE	2*60*1000	//unit: ms

//for client
//#define PORT_CLIENT_BEGIN		36000
//#define PORT_CLIENT_END			46000			

#define TIMER_LOOP_DISPPIC		50			//unit: ms
#define TIME_RECONN_TIMES		5			//unit: pcs
#define TIME_SELECT_WAIT		(1000)		//unit: ms //timeout

#define PROTO_HEADER_LEN		12
#define PROTO_HEADER_VERID		0xA0

#define MEM_GLOBAL_MAX			1024*1024

//================================================================================
// udp
//================================================================================
//
//----------------------------------------------------
// 下面的约定用于客户端,设备与服务器间, 的通信
//----------------------------------------------------
//value of iMsgType
#define MSGTYPE_REG			0x1 //设备发往P2P服务器的注册包
#define MSGTYPE_HPREQ		0x2 //客户端请求服务器, 命令设备发出UDP打洞包(Hole Punching)
#define MSGTYPE_HPCMD		0x3 //服务器命令设备发出打洞包
#define MSGTYPE_HPPKT		0x4 //客户端获得设备的ip:port后,发给设备的UDP打洞包(Hole Punching)
#define MSGTYPE_HPFAI		0x5 //服务器收到客户端的打洞请求包后回复客户端:向设备发送Hole包失败
#define MSGTYPE_HPSUC		0x6 //服务器收到客户端的打洞请求包后回复客户端:向设备发送Hole包成功
#define MSGTYPE_REGRS		0x7 //服务器回复设备的注册结果.成功:Update-OK 
								//失败:ERRIDS_SERVER_NOAUTH:没有权限

#define MSGTYPE_IMGINF		0x20 //这是当前帧图像的信息包,即HTTP头
#define MSGTYPE_IMGSTR		0x21 //这是当前帧图像(JPG文件)的第一包数据
#define MSGTYPE_IMGMID		0x22 //这是当前帧图像数据的中间包
#define MSGTYPE_IMGEND		0x23 //这是当前帧图像(JPG文件)的最后一包数据.当客户端收到这个包时, 
								 //必须回复ackno.
								 //ackno等于该包的seqno+payLoad.

#define MSGTYPE_AUTREQ		0x25 //访问要求输入密码
#define MSGTYPE_AESSTR		0X26 //设备发给客户端 16 字节的RNS编码 ,开始认证过程//2011-11-25
#define MSGTYPE_AESREQ		0X27 //客户端发给设备 16 字节的AES 编码 //2011-11-25
#define MSGTYPE_AESOK		0X28 //设备发给客户端 AES 认证 OK //2011-11-25
#define MSGTYPE_AESERR		0x29 //设备的观看密码错误 //2011-11-25

#define MSGTYPE_WEBDAT		0x2A //no use//这是网页数据包
#define MSGTYPE_ACK			0x2C //确认包.payLoad=0
#define MSGTYPE_FIN			0x2D //结束连接.非数据包.payLoad=0

#define HTTP_CMD_BRIG		0x20 //设置亮度命令
#define HTTP_CMD_COND		0x1E //设置环境命令
#define HTTP_CMD_RESOLUT	0X18 //设置分辨率命令
#define HTTP_CMD_P2PHLA		0x50 //P2P打洞的IP地址

#define MSGTYPE_AUTHBAD		41 //=0x29 no use//用户设置了用户密码,设备要求客户端附带Authorization: Basic .
#define MSGTYPE_OTHDAT		44 //=0x2c 其他数据包

//--{{query localP2PServ, relayServ--2010-08-11------
#define MSGTYPE_SERVERWHO  0x60 //client send to www.p2pipcam.com:30002(UDP)
#define MSGTYPE_SERVERANS  0x61 //www.p2pipcam.com response

//--}}query localP2PServ, relayServ------------------

//--{{relay service--2010-08-06----------------------
#define MSGTYPE_RLREQ  0x64	// req RelayService
#define MSGTYPE_RLSTR  0x66 // RelayServ reponse: Service Start  
#define MSGTYPE_RLACK  0x65	// send to RelayServ when receive MSGTYPE_RLSTR
#define MSGTYPE_RLEND  0x67 // Relay Service End

//--}}relay service----------------------------------

//--{{get version from main P2PServ--2010-09-24------
//client send to www.p2pipcam.com:30002(UDP)
#define MSGTYPE_VERIONINFO		0x68 
#define MSGTYPE_VERIONINFO_R	0x69

//--}}get version from main P2PServ--2010-09-24------

//MSGTYPE_xxxx   0xC0---0xC9,0xD1 defined on IPCamSDK_tutk.h, used to TST.exe

//--{{SE--+100(2010-09-24)-----------------------------------------------------------
#define MSGTYPE_REQDEV_INFO			104+100 //客户端请求某设备ip, port
#define MSGTYPE_REQDEV_INFO_SUC		105+100 //客户端请求某设备ip, port; 服务器返回
#define MSGTYPE_REQDEV_INFO_FAIL	106+100 //客户端请求某设备ip, port; 服务器返回

#define MSGTYPE_HPCMD2				107+100	//服务器命令客户端发出打洞包

#define MSGTYPE_GETDEVSNUM			108+100
#define MSGTYPE_GETDEVSNUM_ACK		109+100
#define MSGTYPE_GETDEVSLIST			110+100
#define MSGTYPE_GETDEVSLIST_ACK		111+100

//--{{to solve one problem that ocx connect device by tcp in Lan-----------------
#define MSGTYPE_REQDEV_INFO2		114+100 //客户端请求某设备ip, port; 服务器器返回的信息多了4项, lanIP,lanPort...
#define MSGTYPE_REQDEV_INFO2_SUC	115+100 //客户端请求某设备ip, port; 服务器返回
#define MSGTYPE_REQDEV_INFO2_FAIL	116+100 //客户端请求某设备ip, port; 服务器返回
//--}}to solve one problem that ocx connect device by tcp in Lan-----------------
//================================================================================
// TCP
//================================================================================
// #define MSGTYPE_FAC_REG				101	//厂家设备注册(首次建立设备资料)
// #define MSGTYPE_FAC_REGSUC			102	//厂家设备注册(首次建立设备资料),成功
// #define MSGTYPE_FAC_REGFAI			103	//厂家设备注册(首次建立设备资料),失败
//--}}SE-----------------------------------------------------------------------------

// www.p2pipcam.com response(MSGTYPE_SERVERANS) to client
typedef struct _stServerAnsMessage
{
	char username[20];
	char localP2P[40];
	char localRelay[40];
}ServerAnsMessage_t;

typedef struct stVersionInfoMessage
{
	unsigned char language_ID;
				// language_ID = 0, ENG
				// language_ID = 1, CHT
				// language_ID = 2, CHS
				// language_ID = 3, KR
	unsigned char reserved[3];
}VersionInfoMessage_t;

typedef struct stVersionInfoRMessage
{
	unsigned char Version[4];
	char hyperlink[128];
}VersionInfoRMessage_t;


// 
// typedef struct _ST_REGMSG
// {
// 	char devID[16];
// 	char password[16];
// 
// }ST_REGMSG;

//node of dev
typedef struct _ST_DEVNODE
{
	char			devID[16];
	ULONG			ip;		//in_addr 中的 S_un.S_addr
	unsigned short	port;	//ntohs(.)---littleEndian

	SYSTEMTIME		m_lastRegTime;

}ST_DEVNODE;

//================================================================================
// 客户端之间, 客户端与服务器间收发信息统一协议
//================================================================================
// len=12+1536
typedef struct _avFrame
{
	//该结构必须 4 字节对齐. BigEndian排列,Windows客户端须做高低字节转换
	unsigned char	verId;		//=0xa0
	unsigned char	MsgType;
	unsigned short	payLoad;	//getReq[]字符串长度
	unsigned long	seqno;
	unsigned long	ackno;
	//12 bytes
			//客户端发给设备的GET请求,例如:GET /image HTTP/1.1	
	unsigned char	getReq[1536];	//1.5k/package //CompressDevInfo(..)
}avFrame, *PavFrame;

inline void Pack_BigEndian(avFrame &st_avframe)
{	
	st_avframe.payLoad	=htons(st_avframe.payLoad);
	st_avframe.seqno	=htonl(st_avframe.seqno);
	st_avframe.ackno	=htonl(st_avframe.ackno);
}

inline void Pack_LittleEndian(avFrame &st_avframe)
{	
	st_avframe.payLoad	=ntohs(st_avframe.payLoad);
	st_avframe.seqno	=ntohl(st_avframe.seqno);
	st_avframe.ackno	=ntohl(st_avframe.ackno);
}