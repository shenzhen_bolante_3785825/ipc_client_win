/**********************************************************************
* 版权所有 (C)2008, 广州长正公司									  *
*                                                                     *
* 文件名称：CommonFun.h                                               *
* 文件标识：													      *
* 内容摘要：常用公共函数											  *
* 其它说明：														  *
*                                                                     *
* 修改日期      版本号      修改人      修改内容                      *
* ------------------------------------------------------------------- *
* 2008.03.07    V1.00.01    zrJU         create                       *
* 2008.07.08    V1.00.01    zrJU         add part function from cblai *
* 2009.02.10    V1.00.02	zrJU		 add base64Code				  *
**********************************************************************/
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_COMMONFUN_H__D0E22A75_0ECA_474F_ADA9_68B77940D6AA__INCLUDED_)
#define AFX_COMMONFUN_H__D0E22A75_0ECA_474F_ADA9_68B77940D6AA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#pragma comment(lib, "Version.lib")


#include <vector>
#define VEC_INT		std::vector<int>

#define WIN_FIREWALL

#ifdef WIN_FIREWALL
	#include <netfw.h>
	#include "Shlwapi.h"
	#pragma comment(lib, "Shlwapi.lib")
#endif

#define CHARTOVALUE(c)			(c <= '9' ? c - '0' : c - 'A' + 0X0A)
#define DOUBLECHARTOVALUE(pc)	(((CHARTOVALUE(*(pc))) << 4) | (CHARTOVALUE(*(pc + 1))))

		//能被4整除但不能被100整除, 可是却能被400整除的年是闰年
#define isLeap(y) (((((y) % 4) == 0 && ((y) % 100) != 0) || ((y) % 400) == 0))

class CCommonFun  
{
public:
	CCommonFun();
	virtual ~CCommonFun();

	static CString GetClassNameFromID(HWND hWnd, UINT nCtlID);
	static CString GetDateOfIF(COleDateTime &odtCurDate, int nIFIndex);
	static int  GetDayOfMon(int iYear, int iMon);
	static BOOL IsValidDateFormat(LPCTSTR lpszDate);
	static BOOL SplitStrDate(TCHAR *pszDate, int &iYear, int &iMon, int &iDay);
	static BOOL NormalizeStrDate(CString &strDate);
	static void SplitString(CString source,CStringArray &dest, char division=',');

#ifdef ADO_CONNECTION_H
	static long GetSNFromDB(CADO_Connection* pConn, LPCTSTR sFieldTabName, 
							LPCTSTR sFieldName, CString &strTable_Code);
	static BOOL UpdateDBSN(CADO_Connection* pConn, LPCTSTR sFieldTabName, LPCTSTR sFieldName);
#endif

	static BOOL IsDigit(CString &strText);	
	//static string GetTimeStr(const SYSTEMTIME& st);
	
			//--{{cblai----------------------------------------------------------------------
	static byte DoubleCharToByte(LPCTSTR psValue);
	static int  HexStrToDec(LPCTSTR pHexText);
	static void Acs2Hex(const CString& src, LPTSTR des);
	static void TinyInt2HexStr(int MacroNumberVersion,	TCHAR des[3]);
	static void Hex2Asc(CString& src);
		
	static BOOL AnsiStrToWideStr(const char* pszInString, WCHAR** ptrOutWStr, UINT& nSizeCount);
	static BOOL WideStrToAnsiStr(const WCHAR* pwzInString, char** ptrOutStr, UINT& nSizeCount);
	static BOOL Utf8StrToWideStr(const char* pszInString, WCHAR** ptrOutWStr, UINT& nSizeCount);
	static BOOL WideStrToUtf8Str(const WCHAR* pwzInString, char** ptrOutStr, UINT& nSizeCount);
	static BOOL WideStrToMBStr(const WCHAR* pwzInString, char** ptrOutStr, unsigned int nCodePage, UINT& nSizeCount);
	static BOOL MBStrToWideStr(const char* pszInString, WCHAR** ptrOutWStr, unsigned int nCodePage,UINT& nSizeCount);
	
	static BOOL IsPositiveNumber(LPCTSTR pszIn);
	static CString& MakeIntField(CString& strResult, LPCTSTR pszIn, int FieldLength);
	static CString& MakeStrField(CString& strResult, LPCTSTR pszIn, int FieldLength);
	static CString& MakeAddressField(CString& strResult, LPCTSTR pszIn, 
									 int FieldLength = 10, unsigned short sAddrFlag = 1);
	
	static int MakeCheckSum(CString& strSource);
	static BOOL DoCheckSum(const CString& strSource);

	static CString& Bin2OmniBin(const TCHAR* src, CString& des);
	static CString& Bin2OmniBin(const TCHAR* src, UINT nLen, char cSepa,CString& strDec);
	static CString& OmniBin2Bin(const TCHAR* src, CString& des);
	static CString& Bin2BrewBin(const TCHAR* src, CString& strDec);
	static CString& BrewBin2Bin(const TCHAR* src, CString& strDec);
	static CString& MakeBrewMacroName(const TCHAR* src, CString& strDec);
	
	static void WaitForQueue(int* pvalue, int limit, int interval = 10 /*ms*/);
	static void WaitForComplete(int* pvalue, int flag = 0, int interval = 10 /*ms*/);
		//--}}cblai----------------------------------------------------------------------
	static BOOL _CStrFromWSTRU(IN UINT codePage, IN LPCWSTR wstr, IN UINT len, OUT char *pStr);
	static BOOL _BSTRFromCStrU(IN UINT codePage, IN LPCSTR s, OUT CString &csWStr);

		//must be strlen(outUser+outPW)<=128
	static void DeBase64(char *outUser, char*outPW, char*inStr);
		//Encode a buffer from "string" into "outbuf"
	static void EnBase64(char *string, char *outbuf, int outlen);
		//dont work when in _DEBUG
	static HRESULT CreateShotcutLink(LPCTSTR lpszPathObj, LPCTSTR lpszPathLink, LPCTSTR lpszDesc);

	static void GetAppVer(CString &strVersion);
	static HWND FindWndHandleByTitle(LPCTSTR lpPartTitle);

	//--{{byte order-------------------------------------------
	static BOOL CCommonFun::IsBigEndian();
	//result converted: Big-endian
	static void Big_Word2Bytes(IN WORD nValue,		OUT BYTE *pByte);
	static void Big_Bytes2Word(OUT WORD &nValue,	IN BYTE *pByte);
	static void Big_Int42Bytes(IN int nValue,		OUT BYTE *pByte);
	static void Big_Bytes2Int4(OUT int &nValue,		IN BYTE *pByte);
	static void Big_ULONG2Bytes(IN ULONG nValue,	OUT BYTE *pByte);
	static void Big_Bytes2ULONG(OUT ULONG &nValue,	IN BYTE *pByte);
	
	//result converted: Little-endian
	static void Little_Word2Bytes(IN WORD nValue,	OUT BYTE *pByte);
	static void Little_Bytes2Word(OUT WORD &nValue, IN BYTE *pByte);
	static void Little_Int42Bytes(IN int nValue,	OUT BYTE *pByte);
	static void Little_Bytes2Int4(OUT int &nValue,	IN BYTE *pByte);
	static void Little_ULONG2Bytes(IN ULONG nValue, OUT BYTE *pByte);
	static void Little_Bytes2ULONG(OUT ULONG &nValue, IN BYTE *pByte);
	//--}}byte order-------------------------------------------

	static void ReverseXBitOfByte(BYTE &n, short xBitReversed);
	static void KeepXBitOfByte(BYTE &n, short xBitKeeped);

	static void CtrlEnable(HWND hDlg, UINT nID, BOOL bEnable);

	//lpRows: "1,3,5-7"
	//nMaxRowNoExisted, max row no. after sort by row no., that is no. marked before row
	static void GetDestRows(IN LPCTSTR lpRows, IN int nRowNoExcluded, IN int nMaxRowNoExisted, OUT VEC_INT &vecSrcDestRows);
	static void DestRowsVec2Str(IN VEC_INT &vecSrcDestRows, OUT CString &csDestRows);
	
	static BOOL IsRealIp(IN OUT CString &csDomainIP);
	static BOOL GetIPFrom(IN CString csDomainEntered, OUT CString &csIP);
	static void GetWANIP(OUT CString &csExtraIP, char chHeadDelimiter='[', char chTailDelimiter=']');

	//--{{WIN_FIREWALL-------------------------------------------------------------------
#ifdef WIN_FIREWALL
	static HRESULT	Initialize(OUT INetFwProfile **fwProfile);
	static void		Cleanup(IN INetFwProfile *fwProfile);
	static HRESULT	IsOn(IN INetFwProfile *fwProfile, OUT BOOL *fwOn);
	static HRESULT	TurnOn(IN INetFwProfile *fwProfile);
	static HRESULT	TurnOff(IN INetFwProfile *fwProfile);
	static HRESULT	AppIsEnabled(IN INetFwProfile *fwProfile, IN const wchar_t *fwProcessImageFileName, OUT BOOL *fwAppEnabled);
	static HRESULT	AddApp(IN INetFwProfile *fwProfile, IN const wchar_t *fwProcessImageFileName,IN const wchar_t *fwName);
	static HRESULT	PortIsEnabled(IN INetFwProfile *fwProfile, IN LONG portNumber, IN NET_FW_IP_PROTOCOL ipProtocol, OUT BOOL *fwPortEnabled);
	
	static HRESULT  PortAdd(IN INetFwProfile *fwProfile, IN LONG portNumber, IN NET_FW_IP_PROTOCOL ipProtocol, IN const wchar_t *name);
	
	static int		Test();
	static BOOL		RegFireWall(LPCTSTR lpFullFileName, LPCTSTR lpRegName); //stdafx.h #define _WIN32_WINNT 0x0400
#endif
	//--}}WIN_FIREWALL-------------------------------------------------------------------
	//last character has '\\'
	static void GetAppPath(CString &strAppPath);

private:
	static void to64frombits(unsigned char *outBase64, const unsigned char *inUserPw, int inUserPWLen);
	static int  from64tobits(char *out, const char *in, int maxlen);

};

//@desc ========================================================
//
class CDataLock
{
public:
	CDataLock(CRITICAL_SECTION& cs, const CString& strFunc)
	{
		m_strFunc = strFunc;
		m_pcs = &cs;
		Lock();
	}
	virtual ~CDataLock()
	{
		Unlock();
	}
	
	void Lock()
	{
		//TRACE(_T("EC %d %s\n") , GetCurrentThreadId(), m_strFunc);
		EnterCriticalSection(m_pcs);
	}
	void Unlock()
	{
		LeaveCriticalSection(m_pcs);
		//TRACE(_T("LC %d %s\n") , GetCurrentThreadId() , m_strFunc);
	}
	
protected:
	CRITICAL_SECTION*	m_pcs;
	CString				m_strFunc;
};


#include <process.h>
typedef unsigned (__stdcall *PTHREAD_START) (void *);
#define chBEGINTHREADEX(psa, cbStack, pfnStartAddr,		\
	pvParam, fdwCreate, pdwThreadID)   \
	((HANDLE)_beginthreadex(           \
	(void *) (psa),                    \
	(unsigned) (cbStack),              \
	(PTHREAD_START) (pfnStartAddr),    \
	(void *) (pvParam),                \
	(unsigned) (fdwCreate),            \
	(unsigned *) (pdwThreadID)))



typedef struct _SOFTVER
{
	union{
		struct {unsigned char v1,v2,v3,v4;}ver_b;
		UINT ver_l;
	};
}SOFTVER;
inline void VerN2Str(UINT nVer, CString &csVer)
{
	SOFTVER sdkVer;
	sdkVer.ver_l=nVer;
	csVer.Format(_T("%d.%d.%d.%d"), sdkVer.ver_b.v4, sdkVer.ver_b.v3, 
		sdkVer.ver_b.v2, sdkVer.ver_b.v1);
}
inline void VerN2Str_3(UINT nVer, CString &csVer)
{
	SOFTVER sdkVer;
	sdkVer.ver_l=nVer;
	csVer.Format(_T("%d.%d.%d"), sdkVer.ver_b.v4, sdkVer.ver_b.v3, 
		sdkVer.ver_b.v2);
}

inline bool VerStr2N(UINT &nVer, LPCTSTR lpVer)
{
	CString csVer(lpVer);
	if(csVer.IsEmpty()) { nVer=0; return false; }		//return--------

	CStringArray arrStr;
	CCommonFun::SplitString(csVer, arrStr, '.');
	if(arrStr.GetSize()!=4) { nVer=0; return false; }	//return--------

	SOFTVER sdkVer;
	int n=0;
	_stscanf(arrStr[0], _T("%d"), &n); sdkVer.ver_b.v4=(BYTE)n;
	_stscanf(arrStr[1], _T("%d"), &n); sdkVer.ver_b.v3=(BYTE)n;
	_stscanf(arrStr[2], _T("%d"), &n); sdkVer.ver_b.v2=(BYTE)n;
	_stscanf(arrStr[3], _T("%d"), &n); sdkVer.ver_b.v1=(BYTE)n;

	nVer=sdkVer.ver_l;
	return true;
}

#endif // !defined(AFX_COMMONFUN_H__D0E22A75_0ECA_474F_ADA9_68B77940D6AA__INCLUDED_)


