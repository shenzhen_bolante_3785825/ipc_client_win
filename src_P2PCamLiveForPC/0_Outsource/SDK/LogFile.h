// LogFile.h: interface for the CLogFile class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LOGFILE_H__4C480C07_6428_45F2_B19B_6FFF63FD8447__INCLUDED_)
#define AFX_LOGFILE_H__4C480C07_6428_45F2_B19B_6FFF63FD8447__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Shlwapi.h"
#pragma comment(lib, "shlwapi.lib")

#define  ONELOG_MAXLEN	 1048576	//uint: Byte; 1048576=1MB

class CLogFile  
{
public:
	CLogFile();
	virtual ~CLogFile();

protected:
	CStdioFile	m_File;	//generate log file
	BOOL		m_bReady;
	CString		m_csPreFileName; //dont include path, pure filename
	CString		m_csFirstLogPath, m_csFirstFileNamePrefix;
	int			m_nNo;
	CRITICAL_SECTION	m_critSec;

public:
	BOOL CreateLogFile(LPCTSTR lpPath, LPCTSTR lpFileNamePrefix);
	void CloseLogFile();
	void WriteLog(LPCTSTR lpszFormat, ...);

	BOOL IsReady()	{ return m_bReady; }
	void GetCurLogFileName(CString &csLogFileName) { csLogFileName=m_csPreFileName;  }
	void GetCurLogFilePath(CString &csLogFilePath) { csLogFilePath=m_csFirstLogPath; }
};

#endif // !defined(AFX_LOGFILE_H__4C480C07_6428_45F2_B19B_6FFF63FD8447__INCLUDED_)
