#ifndef _P2PAVCoreType_H_
#define _P2PAVCoreType_H_

//#ifndef UNICODE
typedef char			tkCHAR;
typedef unsigned char	tkUCHAR;
//#else
//typedef wchar_t			tkCHAR;
//typedef unsigned wchar_t tkUCHAR;
//#endif

typedef short			tkINT16;
typedef unsigned short  tkUINT16;

typedef int				tkINT32;
typedef unsigned int	tkUINT32;

typedef long			tkLONG;
typedef unsigned long	tkULONG;

typedef float			tkFLOAT;
typedef double			tkDOUBLE;

typedef void	tkVOID;
typedef void*	tkPVOID;
typedef void*	tkHandle;

#endif  //_P2PAVCoreType_H_