#include "StdAfx.h"
#include "IOTCCore.h"
#include "AVAPIs.h"
#include "AVIOCTRLDEFs.h"
#include "AVFRAMEINFO.h"
#include "adpcm.h"

#pragma comment(lib, "IOTCAPIs.lib")
#pragma comment(lib, "AVApis.lib")

//--var-------------------------
static tkUINT32	 gInit=0;
CRITICAL_SECTION gLockCommon;
CRITICAL_SECTION gLockLog;

//--define----------------------
#define TIME_sec_avClientStart	8 //8 s
#define MAX_SIZE_RECV_VIDEO		262144
#define MAX_SIZE_RECV_AUDIO		5120
#define MAX_SIZE_RECV_IOCTRL	5120

#define AUDIO_OK				1
#define AUDIO_BUFFER_LEN		1024
#define OM_THREAD_QUIT			WM_USER+100
#define OM_STARTTALK			WM_USER+101
#define OM_STOPTALK				WM_USER+102

#define DEFAULT_COLLECT_INTERVAL_s 2 //s

#ifdef _DEBUG
	//#define OUTPUT_DEBUG_MSG
#endif
//#define OUT_LOG_FILE	("d:\\P2PAVCore.txt")

void TRACE_DEBUG(char *format, ...)
{
#if defined(OUTPUT_DEBUG_MSG)
	static unsigned long gTickout=0L;
	static char fmt[320]={0};
	va_list vlist;
	unsigned long hour=0L, minute=0L, second=0L, ms=0L;
	unsigned long nTmp=0L;
	if(gTickout==0L) gTickout=GetTickCount();
EnterCriticalSection(&gLockLog);
	ms=(GetTickCount()-gTickout);
	nTmp=ms/1000;
	second=nTmp % 60;
	minute=(nTmp/60) % 60;
	hour  =(nTmp/3600) % 24;
	sprintf_s(fmt, 320, "%02d:%02d:%02d.%03d ", hour, minute,second, ms%1000);

	va_start(vlist, format);
	vsprintf(&fmt[13], format, vlist);
	va_end(vlist);

  #if defined(OUT_LOG_FILE)
	{
		FILE *fp = NULL;
		fp = fopen(OUT_LOG_FILE,"a+");
		if(fp == NULL) return;
		fprintf(fp,"%s",fmt);
		fclose(fp);
	}
  #else
	OutputDebugStr(fmt);
  #endif
LeaveCriticalSection(&gLockLog);
#endif
}

CIOTCCore::CIOTCCore(tkVOID)
{
	m_pstConnInfo=new tkConnInfo();
	memset(m_pstConnInfo, 0, sizeof(tkConnInfo));
	m_pstConnInfo->mpSInfo=new struct st_SInfo();
	memset(m_pstConnInfo->mpSInfo, 0, sizeof(struct st_SInfo));
	m_pstConnInfo->mnSID=-1;
	m_pstConnInfo->mnAVChannel=-1;
	m_pstConnInfo->mConnMode=0xFF;

	m_hConnectDev=NULL;
	m_hRecvIOCtrl=NULL;
	m_hRecvVideo =NULL;
	m_hRecvAudio =NULL;
	m_hSampleAudio=NULL;
	m_threadIDSampleAudio=0L;
	m_hSendAudio =NULL;
	m_hIdle=NULL;

	m_bAudioInOpen	=FALSE;
	m_bRecvingIOCtrl=m_bRecvingVideo= 0;
	m_bRecvingAudio =m_bSendingAudio=0;
	m_bSamplingAudio=0;
	m_bIdleRunning=0;
	m_nCollect_interval=0;
	m_pBufAudioEncoded=new tkUCHAR[AUDIO_BUFFER_LEN];
	m_fifoSpeakData=core_block_FifoNew();
	m_fifoHandleIOCtrl=core_block_FifoNew();
}

CIOTCCore::~CIOTCCore(tkVOID)
{
	if(m_pBufAudioEncoded){
		delete []m_pBufAudioEncoded;
		m_pBufAudioEncoded=NULL;
	}
	if(m_pstConnInfo!=NULL) {
		if(m_pstConnInfo->mpSInfo) delete m_pstConnInfo->mpSInfo;
		delete m_pstConnInfo;
		m_pstConnInfo=NULL;
	}
	core_block_FifoRelease(&m_fifoSpeakData);
	core_block_FifoRelease(&m_fifoHandleIOCtrl);
}

tkVOID CIOTCCore::OnStatus(tkINT32 nType, tkINT32 nValue)
{
	if(m_pstConnInfo->mCBStatus) m_pstConnInfo->mCBStatus(nType, nValue, this, m_pstConnInfo->mnTag, m_pstConnInfo->mCBStatusParam);
}

tkVOID CIOTCCore::AppendIOCtrl(tkINT32 nIOType, tkCHAR *pIOData, tkINT32 nIODataSize)
{
	ST_QUEUE_IOCTRL *pQueueSendIO=new ST_QUEUE_IOCTRL;
	memset(pQueueSendIO,0,sizeof(ST_QUEUE_IOCTRL));
	pQueueSendIO->nIOType =nIOType;
	memcpy(pQueueSendIO->ioData, pIOData, nIODataSize);
	pQueueSendIO->nIODataSize=nIODataSize;

	core_block_t *pBlock=(core_block_t *)new core_block_t;
	if(pBlock){
		if(core_block_Alloc(pBlock, (char *)pQueueSendIO, sizeof(ST_QUEUE_IOCTRL))>0){
			core_block_FifoPut(m_fifoHandleIOCtrl, pBlock);
		}else delete pBlock;
	}
}

tkVOID CIOTCCore::AppendIOCtrl(ST_QUEUE_IOCTRL *pQueueSendIO)
{
	core_block_t *pBlock=(core_block_t *)new core_block_t;
	if(pBlock){
		if(core_block_Alloc(pBlock, (char *)pQueueSendIO, sizeof(ST_QUEUE_IOCTRL))>0){
			core_block_FifoPut(m_fifoHandleIOCtrl, pBlock);
		}else delete pBlock;
	}
}

tkULONG WINAPI CIOTCCore::ThreadIdle(tkPVOID lpPara)
{
	CIOTCCore *pThis=(CIOTCCore *)lpPara;
	pThis->DoIdle();
	TRACE_DEBUG("%s ThreadIdle exited\n", pThis->m_pstConnInfo->mszUID);
	return 0L;
}

tkINT32 CIOTCCore::DoIdle()
{
	core_block_t *pBlock=NULL;
	ST_QUEUE_IOCTRL *pIOCtrl=NULL;

	TRACE_DEBUG("ThreadIdle going...\n");
	m_bIdleRunning=1;
	while(m_bIdleRunning)
	{
		pBlock=core_block_FifoGet(m_fifoHandleIOCtrl);
		if(pBlock!=NULL){
			pIOCtrl=(ST_QUEUE_IOCTRL *)pBlock->p_buffer;
			avSendIOCtrl(m_pstConnInfo->mnAVChannel, pIOCtrl->nIOType, pIOCtrl->ioData, pIOCtrl->nIODataSize);
			TRACE_DEBUG("DoIdle: avSendIOCtrl(.)=0x%X\n", pIOCtrl->nIOType);
			core_block_Release(&pBlock);
		}
		Sleep(40);
	}
	return 0;
}

tkULONG WINAPI CIOTCCore::ThreadConnectDev(tkPVOID lpPara)
{
	CIOTCCore *pThis=(CIOTCCore *)lpPara;
	pThis->DoConnectDev();
	TRACE_DEBUG("%s ThreadConnectDev exited\n", pThis->m_pstConnInfo->mszUID);
	return 0L;
}

tkINT32 CIOTCCore::DoConnectDev()
{
	TRACE_DEBUG("%s connecting...\n", m_pstConnInfo->mszUID);
	tkINT32 nSID=-1, nAVChannel=-1, nRet=-1;
	EnterCriticalSection(&gLockCommon);
	if(strlen(m_pstConnInfo->mszUID)>0) nSID=IOTC_Connect_ByUID(m_pstConnInfo->mszUID);
	LeaveCriticalSection(&gLockCommon);

	if(nSID<0){
		OnStatus(TYPE_STATUS_ERRCODE, nSID);
		return -1;
	}else{
		struct st_SInfo *pSInfo=m_pstConnInfo->mpSInfo;
		nRet=IOTC_Session_Check(nSID, pSInfo);
		if(nRet==0){
			m_pstConnInfo->mConnMode=pSInfo->Mode;
			tkULONG nServType=0L;
			nAVChannel=avClientStart(nSID, m_pstConnInfo->mszUser,m_pstConnInfo->mszPwd,TIME_sec_avClientStart, &nServType, 0);
			if(nAVChannel>=0) {
				m_pstConnInfo->mnSID	  =nSID;
				m_pstConnInfo->mnAVChannel=nAVChannel;
				m_pstConnInfo->mnServType =nServType;

				if(m_hRecvIOCtrl==NULL){
					tkULONG threadID=0L;
					m_hRecvIOCtrl = CreateThread(NULL, 0, ThreadRecvIOCtrl, (tkPVOID)this, 0, (tkULONG *)&threadID);
					if(NULL==m_hRecvIOCtrl) {
						TRACE_DEBUG("ThreadRecvIOCtrl, failed to create thread\n");
						OnStatus(TYPE_STATUS_ERRCODE, SINFO_FAIL_CREATE_THREAD);
					}else{
						m_hIdle = CreateThread(NULL, 0, ThreadIdle, (tkPVOID)this, 0, (tkULONG *)&threadID);
						if(NULL==m_hIdle){
							TRACE_DEBUG("ThreadIdle, failed to create thread\n");
							OnStatus(TYPE_STATUS_ERRCODE, SINFO_FAIL_CREATE_THREAD);
						}
					}
				}
				OnStatus(TYPE_STATUS_INFO, SINFO_CONNECTED);
				
			}else{
				IOTC_Session_Close(nSID);
				OnStatus(TYPE_STATUS_ERRCODE, nAVChannel);
				return -1;
			}					
		}else {
			OnStatus(TYPE_STATUS_ERRCODE, nRet);
			return -1;
		}
	}	
	return 0;
}

tkULONG WINAPI CIOTCCore::ThreadRecvIOCtrl(tkPVOID lpPara)
{
	CIOTCCore *pThis=(CIOTCCore *)lpPara;
	TRACE_DEBUG("---ThreadRecvIOCtrl going...\n");
	pThis->DoRecvIOCtrl();
	TRACE_DEBUG("---ThreadRecvIOCtrl exited\n");
	return 0L;
}

tkINT32 CIOTCCore::DoRecvIOCtrl()
{
	char ioCtrlBuf[MAX_SIZE_RECV_IOCTRL];
	int	nRecvSize=0;
	unsigned int ioType=0;

	m_bRecvingIOCtrl=1;
	while(m_bRecvingIOCtrl){
		nRecvSize=avRecvIOCtrl(m_pstConnInfo->mnAVChannel, &ioType, ioCtrlBuf, sizeof(ioCtrlBuf), 200);
		if(nRecvSize>=0){
			if(ioType==IOTYPE_USER_IPCAM_GET_FLOWINFO_REQ){
				SMsgAVIoctrlGetFlowInfoReq *pReq=(SMsgAVIoctrlGetFlowInfoReq *)ioCtrlBuf;
				if(pReq->collect_interval<0) pReq->collect_interval=DEFAULT_COLLECT_INTERVAL_s;
				m_nCollect_interval=pReq->collect_interval*1000; //ms

				ST_QUEUE_IOCTRL *pQueueSendIO=new ST_QUEUE_IOCTRL;
				memset(pQueueSendIO, 0, sizeof(ST_QUEUE_IOCTRL));
				pQueueSendIO->nIOType =IOTYPE_USER_IPCAM_GET_FLOWINFO_RESP;
				memset(pQueueSendIO->ioData, 0, sizeof(pQueueSendIO->ioData));				
				memcpy(pQueueSendIO->ioData+sizeof(pReq->channel), (char *)&m_nCollect_interval, sizeof(pReq->collect_interval));
				pQueueSendIO->nIODataSize=sizeof(SMsgAVIoctrlGetFlowInfoResp);
				AppendIOCtrl(pQueueSendIO);

			}else if(m_pstConnInfo->mCBRecvIOCtrl) 
				m_pstConnInfo->mCBRecvIOCtrl(ioType, ioCtrlBuf, nRecvSize, this, m_pstConnInfo->mnTag, m_pstConnInfo->mCBRecvIOCtrlParam);

		}else if(nRecvSize==AV_ER_SESSION_CLOSE_BY_REMOTE || nRecvSize==AV_ER_REMOTE_TIMEOUT_DISCONNECT) OnStatus(TYPE_STATUS_ERRCODE, nRecvSize);
	}
	return 0;
}

tkULONG WINAPI CIOTCCore::ThreadRecvVideo(tkPVOID lpPara)
{
	CIOTCCore *pThis=(CIOTCCore *)lpPara;
	TRACE_DEBUG("---ThreadRecvVideo going...\n");
	pThis->DoRecvVideo();
	TRACE_DEBUG("---ThreadRecvVideo exited\n");
	return 0L;
}

tkINT32 CIOTCCore::DoRecvVideo()
{
	FRAMEINFO_t frmInfo;
	tkINT32  nSIZE_FRMINFO=sizeof(frmInfo);
	tkCHAR   *pRecvBuf=new tkCHAR[MAX_SIZE_RECV_VIDEO];
	tkINT32  nRecvSize=0;
	tkUINT32 nFrmNo=0, nFrmNoPrev=0;
	tkINT32  nFrmCount=0, nIncompleteFrmCount=0;
	tkINT32  nBytesCount=0;
	tkULONG  nTick1=0L, nTick2=0L, nTickSpan=0L;

	tkULONG  nTickCollect1=0L, nTickCollect2=0L, nTickSpanCollect=0L;
	tkCHAR   bSendFirstIFrame=0;
	tkINT32  nFrmCountCollect=0, nIncompleteFrmCountCollect=0;
	tkINT32  total_expected_frame_size=0, total_actual_frame_size=0;
	tkINT32  nActualFrameSize=0, nExpectedFrameSize=0, nActualFrameInfoSize=0;

	memset(&frmInfo, 0, sizeof(frmInfo));
	m_bRecvingVideo=1;
	while(m_bRecvingVideo){
		nExpectedFrameSize=0;
		nActualFrameSize=0;
		nRecvSize=avRecvFrameData2(m_pstConnInfo->mnAVChannel, pRecvBuf, MAX_SIZE_RECV_VIDEO, 
								   &nActualFrameSize, &nExpectedFrameSize, (tkCHAR *)&frmInfo, nSIZE_FRMINFO, &nActualFrameInfoSize, &nFrmNo);		
		total_expected_frame_size+=nExpectedFrameSize;
		total_actual_frame_size  +=nActualFrameSize;
		//TRACE_DEBUG("   avRecvFrameData2 1 nRecvSize=%d, nExpected=%d, nActual=%d\n", nRecvSize, nExpectedFrameSize, nActualFrameSize);
		TRACE_DEBUG("   avRecvFrameData2 2 total_expected_frame_size=%d, total_actual_frame_size=%d\n", total_expected_frame_size, total_actual_frame_size);
		if(nRecvSize>0){
			if(!bSendFirstIFrame && frmInfo.flags==IPC_FRAME_FLAG_IFRAME){
				bSendFirstIFrame=1;
				ST_QUEUE_IOCTRL *pQueueSendIO=new ST_QUEUE_IOCTRL;
				memset(pQueueSendIO, 0, sizeof(ST_QUEUE_IOCTRL));
				pQueueSendIO->nIOType =IOTYPE_USER_IPCAM_RECEIVE_FIRST_IFRAME;
				memset(pQueueSendIO->ioData, 0, sizeof(pQueueSendIO->ioData));
				pQueueSendIO->nIODataSize=sizeof(SMsgAVIoctrlReceiveFirstIFrame);
				AppendIOCtrl(pQueueSendIO);
			}
			
			nBytesCount+=nRecvSize;
			if(frmInfo.flags==IPC_FRAME_FLAG_IFRAME || nFrmNo==(nFrmNoPrev+1)){
				nFrmNoPrev=nFrmNo;
				nFrmCount++;
				nFrmCountCollect++;
				m_pstConnInfo->mOnlineNum=frmInfo.onlineNum;
				if(m_pstConnInfo->mCBAVFrame) {
					m_pstConnInfo->mCBAVFrame(pRecvBuf, nRecvSize,(tkUCHAR *)&frmInfo, sizeof(frmInfo), 
											  m_pstConnInfo->mnTag, m_pstConnInfo->mCBAVFrameParam);
				}
			}else {
				nIncompleteFrmCount++;
				nIncompleteFrmCountCollect++;
			}
		}else if(nRecvSize == AV_ER_INCOMPLETE_FRAME){
			nIncompleteFrmCount++;
			nIncompleteFrmCountCollect++;

		}else if(nRecvSize == AV_ER_LOSED_THIS_FRAME){
			nIncompleteFrmCount++;
			nIncompleteFrmCountCollect++;

		}else if(nRecvSize == AV_ER_DATA_NOREADY) {
			Sleep(32);

		}else if(nRecvSize==AV_ER_REMOTE_TIMEOUT_DISCONNECT || nRecvSize==AV_ER_SESSION_CLOSE_BY_REMOTE ||nRecvSize==AV_ER_INVALID_SID){
				OnStatus(TYPE_STATUS_ERRCODE, nRecvSize);
				break;
		}else {
			Sleep(10);
		}

		nTick2=GetTickCount();
		nTickSpan=nTick2-nTick1;
		if(nTickSpan>=3000){
			nTick1=nTick2;
			m_pstConnInfo->mfFrameRate=nFrmCount*1000.0/nTickSpan;
			m_pstConnInfo->mfByteRate=(m_nBytesCountAudio+nBytesCount)*1.0/nTickSpan;

			nFrmCount=0;
			nBytesCount=0;
			m_nBytesCountAudio=0;
			OnStatus(TYPE_STATUS_INFO, SINFO_FPS_ETC_READY);					
		}

		nTickCollect2=GetTickCount();
		nTickSpanCollect=nTickCollect2-nTickCollect1;
		if(m_nCollect_interval!=0 && nTickSpanCollect>=m_nCollect_interval){
			nTickCollect1=nTickCollect2;
			ST_QUEUE_IOCTRL *pQueueSendIO=new ST_QUEUE_IOCTRL;
			memset(pQueueSendIO, 0, sizeof(ST_QUEUE_IOCTRL));
			SMsgAVIoctrlCurrentFlowInfo *pFlowInfo=(SMsgAVIoctrlCurrentFlowInfo *)pQueueSendIO->ioData;
			pFlowInfo->total_frame_count=nFrmCountCollect+nIncompleteFrmCountCollect;
			pFlowInfo->lost_incomplete_frame_count=nIncompleteFrmCountCollect;
			pFlowInfo->total_expected_frame_size=total_expected_frame_size;
			pFlowInfo->total_actual_frame_size=total_actual_frame_size;
			pFlowInfo->timestamp_ms=GetTickCount();
			
			TRACE_DEBUG("   avRecvFrameData2 3 total_expected_frame_size=%d, total_actual_frame_size=%d\n", total_expected_frame_size, total_actual_frame_size);

			pQueueSendIO->nIOType =IOTYPE_USER_IPCAM_CURRENT_FLOWINFO;			
			memcpy(pQueueSendIO->ioData, (char *)pFlowInfo, sizeof(ST_QUEUE_IOCTRL));
			pQueueSendIO->nIODataSize=sizeof(SMsgAVIoctrlCurrentFlowInfo);
			AppendIOCtrl(pQueueSendIO);
			
			nFrmCountCollect=0;
			nIncompleteFrmCountCollect=0;
			total_expected_frame_size=0;
			total_actual_frame_size=0;
		}
	}
	delete pRecvBuf;
	pRecvBuf=NULL;

	return 0;
}

tkULONG WINAPI CIOTCCore::ThreadRecvAudio(tkPVOID lpPara)
{
	CIOTCCore *pThis=(CIOTCCore *)lpPara;
	TRACE_DEBUG("---ThreadRecvAudio going...\n");
	pThis->DoRecvAudio();
	TRACE_DEBUG("---ThreadRecvAudio exited\n");
	return 0L;
}

tkINT32 CIOTCCore::DoRecvAudio()
{
	FRAMEINFO_t frmInfo;
	tkINT32  nSIZE_FRMINFO=sizeof(frmInfo);
	tkCHAR   *pRecvBuf=new tkCHAR[MAX_SIZE_RECV_AUDIO];
	tkINT32  nRecvSize=0;
	tkUINT32 nFrmNo=0, nFrmNoPrev=0;
	tkINT32  nRet=0;

	m_nBytesCountAudio=0;
	m_bRecvingAudio=1;
	while(m_bRecvingAudio){
		nRet=avCheckAudioBuf(m_pstConnInfo->mnAVChannel);
		if(nRet<0)break;
		else if(nRet<6){
			Sleep(10);
			continue;
		}
		nRecvSize=avRecvAudioData(m_pstConnInfo->mnAVChannel, pRecvBuf, MAX_SIZE_RECV_AUDIO, (tkCHAR *)&frmInfo, nSIZE_FRMINFO, &nFrmNo);
		if(nRecvSize>0){
			m_nBytesCountAudio+=nRecvSize;
			if(m_pstConnInfo->mCBAVFrame) 
				m_pstConnInfo->mCBAVFrame(pRecvBuf, nRecvSize,(tkUCHAR *)&frmInfo, sizeof(frmInfo), m_pstConnInfo->mnTag, m_pstConnInfo->mCBAVFrameParam);
		}else if(nRecvSize == AV_ER_INCOMPLETE_FRAME){

		}else if(nRecvSize == AV_ER_LOSED_THIS_FRAME){

		}else if(nRecvSize == AV_ER_DATA_NOREADY) {
			Sleep(20);

		}else if(nRecvSize==AV_ER_REMOTE_TIMEOUT_DISCONNECT || nRecvSize==AV_ER_SESSION_CLOSE_BY_REMOTE ||	nRecvSize==AV_ER_INVALID_SID){
			OnStatus(TYPE_STATUS_ERRCODE, nRecvSize);
			break;
		}else {
			Sleep(10);
		}
	}
	delete pRecvBuf;
	pRecvBuf=NULL;
	TRACE_DEBUG("---DoRecvAudio over\n");
	return 0;
}

tkULONG WINAPI CIOTCCore::ThreadSampleAudio(tkPVOID lpPara)
{
	CIOTCCore *pThis=(CIOTCCore *)lpPara;	
	tkConnInfo *pstConnInfo=pThis->m_pstConnInfo;

	pstConnInfo->mnAVChannelSpeak = avServStart(pstConnInfo->mnSID, NULL, NULL, 0, 0, pstConnInfo->mnIOTCChIdSpeak);
	TRACE_DEBUG("---ThreadSampleAudio going...mnIOTCChIdSpeak=%d, mnAVChannel=%d,mnAVChannelSpeak=%d\n", 
				pstConnInfo->mnIOTCChIdSpeak, pstConnInfo->mnAVChannel, pstConnInfo->mnAVChannelSpeak);
	if(pstConnInfo->mnAVChannelSpeak>=0){
		pThis->InitWIEnv();
		pThis->DoSampleAudio();

		avServStop(pstConnInfo->mnAVChannelSpeak);
		pstConnInfo->mnAVChannelSpeak=-1;
	}
	
	TRACE_DEBUG("---ThreadSampleAudio exited\n");
	return 0L;
}

//@para  >0 success
//		 <0 failure
//			-1: m_iThreadIDAudio2==0
//			-2: MMSYSERR_NOERROR != woOpenRet
//
tkINT32 CIOTCCore::InitWIEnv()
{
	WAVEFORMATEX WaveFormatEx;
	memset(&WaveFormatEx, 0, sizeof(WaveFormatEx));
	WaveFormatEx.wFormatTag		= WAVE_FORMAT_PCM;
	WaveFormatEx.nChannels		= 1;
	WaveFormatEx.nSamplesPerSec = 8000;
	WaveFormatEx.wBitsPerSample = 16;
	WaveFormatEx.nAvgBytesPerSec= 16384;
	WaveFormatEx.nBlockAlign	= 2;
	MMRESULT wiOpenRet=waveInOpen(&m_hWI, WAVE_MAPPER, &WaveFormatEx, m_threadIDSampleAudio, 0, CALLBACK_THREAD);
	if(MMSYSERR_NOERROR != wiOpenRet) {
		TRACE_DEBUG("CAudio_adpcm::InitWIEnv(): waveInOpen=failure\n");
		return -2;//return--------------------------------------------
	}
	adpcmResetEncode();
	TRACE_DEBUG("CIOTCCore::InitWIEnv(): waveInOpen=success\n");

	return 1;
}

tkINT32 CIOTCCore::DoSampleAudio()
{
	bool bTalk = false;
	char bufferin1[640], bufferin2[640];
	WAVEHDR *pwavehdrin1=NULL, *pwavehdrin2=NULL, *pwavehdrin=NULL;

	pwavehdrin1 = new WAVEHDR;
	pwavehdrin1->lpData  = bufferin1;
	pwavehdrin1->dwBufferLength = 640;
	pwavehdrin1->dwBytesRecorded = 0;
	pwavehdrin1->dwUser  = 0;
	pwavehdrin1->dwLoops = 0;
	pwavehdrin1->dwFlags = WHDR_DONE;
	pwavehdrin1->lpNext  = NULL;
	pwavehdrin1->reserved= 0;
	waveInPrepareHeader(m_hWI, pwavehdrin1,sizeof(WAVEHDR));
	pwavehdrin2 = new WAVEHDR;
	pwavehdrin2->lpData = bufferin2;
	pwavehdrin2->dwBufferLength = 640;
	pwavehdrin2->dwBytesRecorded= 0;
	pwavehdrin2->dwUser = 0;
	pwavehdrin2->dwLoops= 0;
	pwavehdrin2->dwFlags= WHDR_DONE;
	pwavehdrin2->lpNext = NULL;
	pwavehdrin2->reserved = 0;
	waveInPrepareHeader(m_hWI, pwavehdrin2, sizeof(WAVEHDR));

	m_bSamplingAudio=1;
	MSG msg;
	while(GetMessage(&msg, NULL, 0, 0))
	{
		if(!m_bSamplingAudio) goto EXIT_FUN; //goto--------------		
		switch(msg.message) 
		{
		case OM_STARTTALK:
			if(msg.wParam == AUDIO_OK && m_bAudioInOpen)
			{
				bTalk = true;
				waveInAddBuffer(m_hWI, pwavehdrin1, sizeof(WAVEHDR));
				waveInAddBuffer(m_hWI, pwavehdrin2, sizeof(WAVEHDR));
				waveInStart(m_hWI);
				TRACE_DEBUG("audio OM_STARTTALK\n");
			}
			break;

		case OM_STOPTALK:
			bTalk = false;
			if(m_bAudioInOpen) waveInReset(m_hWI);				
			break;

		case MM_WIM_OPEN:
			TRACE_DEBUG("audio MM_WIM_OPEN\n");
			m_bAudioInOpen=TRUE;
			if(m_threadIDSampleAudio>0) PostThreadMessage(m_threadIDSampleAudio, OM_STARTTALK, AUDIO_OK, 0L);
			break;

		case MM_WIM_DATA:
			pwavehdrin = (WAVEHDR *)msg.lParam;
			if(bTalk) {
				core_block_t *pBlock=(core_block_t *)new core_block_t;
				if(pBlock){
					if(core_block_Alloc(pBlock, pwavehdrin->lpData, 640)>0) {
						core_block_FifoPut(m_fifoSpeakData, pBlock);
					}else delete pBlock;
				}
				waveInAddBuffer(m_hWI, pwavehdrin, sizeof(WAVEHDR));
			}
			break;

		case MM_WIM_CLOSE:
			break;

		case OM_THREAD_QUIT:
			TRACE_DEBUG("audio OM_THREAD_QUIT\n");
			goto EXIT_FUN; //goto----------------------------------

		default:;
		}
	}

EXIT_FUN:
	waveInReset(m_hWI);
	waveInUnprepareHeader(m_hWI,pwavehdrin1,sizeof(WAVEHDR));
	waveInUnprepareHeader(m_hWI,pwavehdrin2,sizeof(WAVEHDR));
	waveInClose(m_hWI);
	delete pwavehdrin1;
	delete pwavehdrin2;

	m_bAudioInOpen	=FALSE;
	TRACE_DEBUG("delete pwavehdr1,2\n");
	return 0;
}

tkULONG WINAPI CIOTCCore::ThreadSendAudio(tkPVOID lpPara)
{
	CIOTCCore *pThis=(CIOTCCore *)lpPara;
	TRACE_DEBUG("---ThreadSendAudio going...\n");
	pThis->DoSendAudio();
	TRACE_DEBUG("---ThreadSendAudio exited\n");
	return 0L;
}

tkINT32 CIOTCCore::DoSendAudio()
{
	core_block_t *pBlock=NULL;

	m_bSendingAudio=1;
	while(m_bSendingAudio)
	{
		pBlock=core_block_FifoGet(m_fifoSpeakData);
		if(pBlock!=NULL){
			SendSpeakData(pBlock->p_buffer, pBlock->i_buffer);
			core_block_Release(&pBlock);
			//TRACE_DEBUG("DoSendAudio: number=%d\n", core_block_FifoCount(m_fifoSpeakData));
		}
		Sleep(30);
	}
	return 0L;
}

FRAMEINFO_t g_frameInfo;
tkVOID CIOTCCore::SendSpeakData(tkCHAR *pAudioData, tkINT32 nDataSize)
{
	switch(m_pstConnInfo->nCodeID_audioSpeak)
	{
		case MEDIA_CODEC_AUDIO_ADPCM:{
			adpcmEncode((tkUCHAR *)pAudioData, nDataSize, m_pBufAudioEncoded);
			
			memset(&g_frameInfo, 0, sizeof(g_frameInfo));
			g_frameInfo.codec_id = MEDIA_CODEC_AUDIO_ADPCM;
			g_frameInfo.flags = (AUDIO_SAMPLE_8K << 2) | (AUDIO_DATABITS_16 << 1) | AUDIO_CHANNEL_MONO;
			g_frameInfo.timestamp=GetTickCount();
			int nRet=avSendAudioData(m_pstConnInfo->mnAVChannelSpeak, (char*)m_pBufAudioEncoded, (nDataSize/4), (void *)&g_frameInfo, sizeof(g_frameInfo));
			//TRACE_DEBUG("SendSpeakData: nRet=%d\n", nRet);
			}break;

		case MEDIA_CODEC_AUDIO_PCM:{

			}break;

		case MEDIA_CODEC_AUDIO_SPEEX:{
			}break;

		case MEDIA_CODEC_AUDIO_MP3:{
			}break;

		case MEDIA_CODEC_AUDIO_G726:{
			}break;

		default:;
	}
}

tkVOID WINAPI CIOTCCore::GetIOTCVer(tkUINT32 *pArrayVer, tkUINT32 nArrMaxSize)
{
	if(pArrayVer==NULL) return;

	tkULONG  iotcVer=0L;
	tkUINT32 avapiVer=0;
	IOTC_Get_Version(&iotcVer);
	avapiVer=avGetAVApiVer();
	if(nArrMaxSize>=2) {
		memcpy((char *)pArrayVer, (char *)&iotcVer, sizeof(tkUINT32));
		memcpy((char *)(pArrayVer)+sizeof(tkUINT32), (char *)&avapiVer, sizeof(tkUINT32));
	}else if(nArrMaxSize>=1) memcpy((char *)pArrayVer, (char *)&iotcVer, sizeof(tkUINT32));
}

tkINT32	CIOTCCore::Init()
{
	int nRet=0;
	EnterCriticalSection(&gLockCommon);
		do{
			if(gInit==0){
				//int nRet=IOTC_Initialize2(0);
				int nRet=IOTC_Initialize(0, "50.19.254.134", "122.248.234.207", "m4.iotcplatform.com", "m5.iotcplatform.com");
				if(!(nRet==IOTC_ER_ALREADY_INITIALIZED || nRet==IOTC_ER_NoERROR)) {
					nRet=-1;
					break;
				}
				avInitialize(128);				
				TRACE_DEBUG("Inited\n");
			}
			gInit++;
			TRACE_DEBUG("Init, gInit=%d\n", gInit);
		}while(0);
	LeaveCriticalSection(&gLockCommon);
	return nRet;
}

tkVOID CIOTCCore::Deinit()
{	
	EnterCriticalSection(&gLockCommon);
		gInit--;
		if(gInit==0){
			avDeInitialize();
			IOTC_DeInitialize();			
			TRACE_DEBUG("Deinited\n");
		}
		TRACE_DEBUG("Deinit, gInit=%d\n", gInit);
		if(gInit<0) gInit=0;
	LeaveCriticalSection(&gLockCommon);	
}

void CIOTCCore::SetConnInfo(tkUINT32 nTag, tkCHAR *szUID, tkCHAR *szUser, tkCHAR *szPwd)
{
	m_pstConnInfo->mnTag=nTag;
	memcpy_s(m_pstConnInfo->mszUID, sizeof(m_pstConnInfo->mszUID), szUID,	strlen(szUID));
	memcpy_s(m_pstConnInfo->mszUser,sizeof(m_pstConnInfo->mszUser),szUser,strlen(szUser));
	memcpy_s(m_pstConnInfo->mszPwd, sizeof(m_pstConnInfo->mszPwd), szPwd,	strlen(szPwd));
}

tkVOID	CIOTCCore::Connect()
{
	TRACE_DEBUG("Connect, m_hConnectDev=0x%X\n", m_hConnectDev);
	if(m_hConnectDev==NULL){
		OnStatus(TYPE_STATUS_INFO, SINFO_CONNECTING);
		tkULONG threadID=0L;
		m_hConnectDev = CreateThread(NULL, 0, ThreadConnectDev, (tkPVOID)this, 0, (tkULONG *)&threadID);
		if(NULL==m_hConnectDev) {
			TRACE_DEBUG("Connect, failed to create thread\n");
			OnStatus(TYPE_STATUS_ERRCODE, SINFO_FAIL_CREATE_THREAD);
		}
	}
}

tkINT32	CIOTCCore::Disconnect(tkCHAR bForceStop)
{
	if(bForceStop) IOTC_Connect_Stop();
	TRACE_DEBUG("Disconnect, 1, m_hConnectDev=0x%X %s\n", m_hConnectDev, m_pstConnInfo->mszUID);

	m_bRecvingIOCtrl=0;
	m_bRecvingVideo =0;
	m_bRecvingAudio =0;
	m_bSamplingAudio=0;
	m_bSendingAudio =0;
	m_bIdleRunning  =0;
	if(m_hIdle)
	{
		WaitForSingleObject(m_hIdle,INFINITE);
		m_hIdle=NULL;
	}
	if(m_hConnectDev){ WaitForSingleObject(m_hConnectDev,INFINITE); m_hConnectDev=NULL;}
	if(m_hRecvIOCtrl){ WaitForSingleObject(m_hRecvIOCtrl,INFINITE); m_hRecvIOCtrl=NULL;}
	if(m_hRecvVideo) { WaitForSingleObject(m_hRecvVideo, INFINITE); m_hRecvVideo=NULL; }
	if(m_hRecvAudio) { WaitForSingleObject(m_hRecvAudio, INFINITE); m_hRecvAudio=NULL; }
	if(m_hSampleAudio) { 
		WaitForSingleObject(m_hSampleAudio, INFINITE); 
		m_hSampleAudio=NULL; 
		m_threadIDSampleAudio=0L;
	}
	if(m_hSendAudio) { 
		WaitForSingleObject(m_hSendAudio, INFINITE); 
		m_hSendAudio=NULL; 
	}

	if(m_pstConnInfo->mnAVChannel>=0){
		avClientStop(m_pstConnInfo->mnAVChannel);
		m_pstConnInfo->mnAVChannel=-1;
	}
	if(m_pstConnInfo->mnSID>=0){
		IOTC_Session_Close(m_pstConnInfo->mnSID);
		m_pstConnInfo->mnSID=-1;
		OnStatus(TYPE_STATUS_INFO, SINFO_DISCONNECTED);
	}
	
	TRACE_DEBUG("Disconnect, 2, m_hConnectDev=0x%X %s\n", m_hConnectDev, m_pstConnInfo->mszUID);
	return 0;
}

tkVOID CIOTCCore::RegCB_OnAVFrame(cbOnAVFrame cbFun,tkPVOID lParam)
{
	m_pstConnInfo->mCBAVFrame=cbFun;
	m_pstConnInfo->mCBAVFrameParam=lParam;
}

tkVOID CIOTCCore::RegCB_OnStatus(cbOnStatus cbFun,tkPVOID lParam)
{
	m_pstConnInfo->mCBStatus=cbFun;
	m_pstConnInfo->mCBStatusParam=lParam;
}

tkVOID CIOTCCore::RegCB_OnRecvIOCtrl(cbOnRecvIOCtrl cbFun, tkPVOID lParam)
{
	m_pstConnInfo->mCBRecvIOCtrl=cbFun;
	m_pstConnInfo->mCBRecvIOCtrlParam=lParam;
}

tkINT32	CIOTCCore::StartVideo()
{
	SMsgAVIoctrlAVStream req;
	memset(&req, 0, sizeof(req));
	int nRet=avSendIOCtrl(m_pstConnInfo->mnAVChannel,IOTYPE_USER_IPCAM_START,(char *)&req, sizeof(req));
	if(nRet==AV_ER_NoERROR && m_hRecvVideo==NULL) {
		tkULONG threadID=0L;
		m_hRecvVideo=CreateThread(NULL, 0, ThreadRecvVideo, this, 0, &threadID);
		if(m_hRecvVideo==NULL) {
			TRACE_DEBUG("StartVideo, failed to create thread\n");
			OnStatus(TYPE_STATUS_ERRCODE, SINFO_FAIL_CREATE_THREAD);
		}	
	}
	return nRet;
}

tkVOID CIOTCCore::StopVideo()
{
	SMsgAVIoctrlAVStream req;
	memset(&req, 0, sizeof(req));
	int nRet=avSendIOCtrl(m_pstConnInfo->mnAVChannel,IOTYPE_USER_IPCAM_STOP,(char *)&req, sizeof(req));
	TRACE_DEBUG("StopVideo, avSendIOCtrl,nRet=%d\n", nRet);

	if(m_hRecvVideo) { 
		m_bRecvingVideo=0;
		WaitForSingleObject(m_hRecvVideo, INFINITE);
		m_hRecvVideo=NULL; 
	}
}

tkINT32	CIOTCCore::StartAudio()
{
	SMsgAVIoctrlAVStream req;
	memset(&req, 0, sizeof(req));
	int nRet=avSendIOCtrl(m_pstConnInfo->mnAVChannel,IOTYPE_USER_IPCAM_AUDIOSTART,(char *)&req, sizeof(req));
	if(nRet==AV_ER_NoERROR && m_hRecvAudio==NULL) {
		tkULONG threadID=0L;
		m_hRecvAudio=CreateThread(NULL, 0, ThreadRecvAudio, this, 0, &threadID);
		if(m_hRecvAudio==NULL) {
			TRACE_DEBUG("StartAudio, failed to create thread\n");
			OnStatus(TYPE_STATUS_ERRCODE, SINFO_FAIL_CREATE_THREAD);
		}	
	}
	return nRet;
}

tkVOID CIOTCCore::StopAudio()
{
	SMsgAVIoctrlAVStream req;
	memset(&req, 0, sizeof(req));
	int nRet=avSendIOCtrl(m_pstConnInfo->mnAVChannel,IOTYPE_USER_IPCAM_AUDIOSTOP,(char *)&req, sizeof(req));
	TRACE_DEBUG("StopAudio, avSendIOCtrl,nRet=%d\n", nRet);

	if(m_hRecvAudio) { 
		m_bRecvingAudio=0;
		WaitForSingleObject(m_hRecvAudio, INFINITE);
		m_hRecvAudio=NULL; 
	}
}

tkINT32	CIOTCCore::StartSpeak(tkINT32 nCodeID_audio)
{
	if(nCodeID_audio<MEDIA_CODEC_AUDIO_ADPCM || nCodeID_audio>MEDIA_CODEC_AUDIO_G726) return -1;
	m_pstConnInfo->nCodeID_audioSpeak=nCodeID_audio;
	SMsgAVIoctrlAVStream req;
	memset(&req, 0, sizeof(req));
	req.channel=IOTC_Session_Get_Free_Channel(m_pstConnInfo->mnSID);
	if(req.channel<0) return -2;

	m_pstConnInfo->mnIOTCChIdSpeak=req.channel;
	int nRet=avSendIOCtrl(m_pstConnInfo->mnAVChannel,IOTYPE_USER_IPCAM_SPEAKERSTART,(char *)&req, sizeof(req));
	if(nRet==AV_ER_NoERROR){
		if(m_hSampleAudio==NULL) {			
			m_hSampleAudio=CreateThread(NULL, 0, ThreadSampleAudio, this, 0, &m_threadIDSampleAudio);
			if(m_hSampleAudio==NULL) {
				m_threadIDSampleAudio=0L;
				TRACE_DEBUG("StartSpeak, failed to create thread\n");
				OnStatus(TYPE_STATUS_ERRCODE, SINFO_FAIL_CREATE_THREAD);
			}
		}

		if(m_hSendAudio==NULL) {
			DWORD threadIDSendAudio;
			m_hSendAudio=CreateThread(NULL, 0, ThreadSendAudio, this, 0, &threadIDSendAudio);
			if(m_hSendAudio==NULL) {
				TRACE_DEBUG("StartSpeak, failed to create thread\n");
				OnStatus(TYPE_STATUS_ERRCODE, SINFO_FAIL_CREATE_THREAD);
			}
		}
	}
	return nRet;
}

tkVOID CIOTCCore::StopSpeak()
{
	if(m_threadIDSampleAudio>0)PostThreadMessage(m_threadIDSampleAudio, OM_THREAD_QUIT, 0,0);
	
	SMsgAVIoctrlAVStream req;
	memset(&req, 0, sizeof(req));
	int nRet=avSendIOCtrl(m_pstConnInfo->mnAVChannel,IOTYPE_USER_IPCAM_SPEAKERSTOP,(char *)&req, sizeof(req));
	TRACE_DEBUG("StopSpeak, avSendIOCtrl,nRet=%d\n", nRet);

	if(m_hSampleAudio) {
		m_bSamplingAudio=0;
		WaitForSingleObject(m_hSampleAudio, INFINITE);
		m_hSampleAudio=NULL; 
	}
	if(m_hSendAudio) {
		m_bSendingAudio=0;
		WaitForSingleObject(m_hSendAudio, INFINITE);
		m_hSendAudio=NULL; 
	}
	core_block_FifoEmpty(m_fifoSpeakData);
}

tkINT32	CIOTCCore::SendIOCtrl(tkINT32 nIOType, tkUCHAR *pIOCtrlData, tkUINT32 nDataSize)
{
	if(nIOType==IOTYPE_USER_IPCAM_START		   || nIOType==IOTYPE_USER_IPCAM_STOP	   ||
	   nIOType==IOTYPE_USER_IPCAM_AUDIOSTART   || nIOType==IOTYPE_USER_IPCAM_AUDIOSTOP ||
	   nIOType==IOTYPE_USER_IPCAM_SPEAKERSTART || nIOType==IOTYPE_USER_IPCAM_SPEAKERSTOP) return -1;
	tkINT32 nRet=avSendIOCtrl(m_pstConnInfo->mnAVChannel, nIOType, (const char *)pIOCtrlData, nDataSize);
	return nRet;
}
