// P2PAVCore.cpp
//

#include "stdafx.h"
#include "P2PAVCore.h"
#include "IOTCCore.h"

#define API_VER_P2PAVCore	0x00010004

P2PAVCORE_API tkVOID apiGetIOTCVer(tkUINT32 *pArrayVer, tkUINT32 nArrMaxSize)
{
	CIOTCCore::GetIOTCVer(pArrayVer, nArrMaxSize);
}

P2PAVCORE_API tkUINT32 apiGetVersion()
{
	return API_VER_P2PAVCore;
}

P2PAVCORE_API tkHandle apiCreateObj(tkUINT32 nTag, tkCHAR *szUID, tkCHAR *szUser, tkCHAR *szPwd)
{
	CIOTCCore *pObj=new CIOTCCore();
	if(pObj->Init()>=0) {
		pObj->SetConnInfo(nTag, szUID, szUser, szPwd);
		return pObj;
	}else {
		delete pObj;
		return NULL;
	}
}

P2PAVCORE_API tkVOID apiReleaseObj(tkHandle *ppObj)
{
	if(ppObj==NULL || *ppObj==NULL) return;
	CIOTCCore *p=(CIOTCCore *)(*ppObj);
	p->Disconnect(1);
	p->Deinit();
	delete p;
	p = NULL;
}

P2PAVCORE_API tkVOID apiSetConnInfo(tkHandle pObj, tkUINT32 nTag, tkCHAR *szUID, tkCHAR *szUser, tkCHAR *szPwd)
{
	if(pObj==NULL) return;
	CIOTCCore *p=(CIOTCCore *)pObj;
	p->SetConnInfo(nTag, szUID, szUser, szPwd);
}

P2PAVCORE_API tkINT32 apiConnect(tkHandle pObj)
{
	if(pObj==NULL) return -1;
	CIOTCCore *p=(CIOTCCore *)pObj;
	p->Connect();
	return 0;
}

P2PAVCORE_API tkINT32 apiDisconnect(tkHandle pObj, tkCHAR bForceStop)
{
	if(pObj==NULL) return -1;
	CIOTCCore *p=(CIOTCCore *)pObj;
	return p->Disconnect(bForceStop);
}

P2PAVCORE_API tkVOID apiRegCB_OnAVFrame(tkHandle pObj, cbOnAVFrame cbFun,	tkPVOID lParam)
{
	if(pObj==NULL) return;
	CIOTCCore *p=(CIOTCCore *)pObj;
	p->RegCB_OnAVFrame(cbFun, lParam);
}

P2PAVCORE_API tkVOID apiRegCB_OnStatus(tkHandle pObj, cbOnStatus cbFun,	tkPVOID lParam)
{
	if(pObj==NULL) return;
	CIOTCCore *p=(CIOTCCore *)pObj;
	p->RegCB_OnStatus(cbFun, lParam);
}

P2PAVCORE_API tkVOID apiRegCB_OnRecvIOCtrl(tkHandle pObj, cbOnRecvIOCtrl cbFun,	tkPVOID lParam)
{
	if(pObj==NULL) return;
	CIOTCCore *p=(CIOTCCore *)pObj;
	p->RegCB_OnRecvIOCtrl(cbFun, lParam);
}

P2PAVCORE_API tkINT32 apiStartVideo(tkHandle pObj)
{
	if(pObj==NULL) return -1;
	CIOTCCore *p=(CIOTCCore *)pObj;
	return p->StartVideo();
}

P2PAVCORE_API tkVOID	apiStopVideo(tkHandle pObj)
{
	if(pObj==NULL) return;
	CIOTCCore *p=(CIOTCCore *)pObj;
	return p->StopVideo();
}

P2PAVCORE_API tkINT32	apiStartAudio(tkHandle pObj)
{
	if(pObj==NULL) return -1;
	CIOTCCore *p=(CIOTCCore *)pObj;
	return p->StartAudio();
}

P2PAVCORE_API tkVOID	apiStopAudio(tkHandle pObj)
{
	if(pObj==NULL) return;
	CIOTCCore *p=(CIOTCCore *)pObj;
	p->StopAudio();
}

P2PAVCORE_API tkINT32	apiStartSpeak(tkHandle pObj, tkINT32 nCodeID_audio)
{
	if(pObj==NULL) return -1;
	CIOTCCore *p=(CIOTCCore *)pObj;
	return p->StartSpeak(nCodeID_audio);
}

P2PAVCORE_API tkVOID	apiStopSpeak(tkHandle pObj)
{
	if(pObj==NULL) return;
	CIOTCCore *p=(CIOTCCore *)pObj;
	p->StopSpeak();
}

P2PAVCORE_API tkINT32	apiSendIOCtrl(tkHandle pObj, tkINT32 nIOType, tkUCHAR *pIOCtrlData, tkUINT32 nDataSize)
{
	if(pObj==NULL) return -1;
	CIOTCCore *p=(CIOTCCore *)pObj;
	return p->SendIOCtrl(nIOType, pIOCtrlData, nDataSize);
}

P2PAVCORE_API tkCHAR* apiGetUID(tkHandle pObj)
{
	if(pObj==NULL) return NULL;
	CIOTCCore *p=(CIOTCCore *)pObj;
	return p->GetUID();
}

P2PAVCORE_API tkVOID apiGetObjInfo(tkHandle pObj, tkUINT32 nGOIType, tkVOID *out_pObj)
{
	if(pObj==NULL) return;
	CIOTCCore *p=(CIOTCCore *)pObj;
	p->GetObjInfo(nGOIType, out_pObj);
}

P2PAVCORE_API int apiGetLanObj(CORE_STLanSearchInfo *psLanSearchInfo, int nArrayLen, int nWaitTimeMs)
{
	int nRet=-1;
	CIOTCCore *pObj=new CIOTCCore();
	if(pObj->Init()>=0) {
		struct st_LanSearchInfo *pLAN=(struct st_LanSearchInfo *)psLanSearchInfo;
		nRet=IOTC_Lan_Search(pLAN, nArrayLen, nWaitTimeMs);
	}
	if(pObj){
		pObj->Deinit();
		delete pObj;
		pObj=NULL;
	}
	return nRet;
}

