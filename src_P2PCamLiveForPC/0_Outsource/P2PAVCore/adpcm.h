
#ifndef _ADPCM_H_
#define _ADPCM_H_

#ifdef __cplusplus
extern "C" {
#endif

void adpcmResetEncode();
void adpcmEncode(unsigned char *pRaw, int nLenRaw, unsigned char *pBufEncoded);
void adpcmResetDecode();
void adpcmDecode(char *pDataCompressed, int nLenData, char *pDecoded);

#ifdef __cplusplus
}
#endif

#endif