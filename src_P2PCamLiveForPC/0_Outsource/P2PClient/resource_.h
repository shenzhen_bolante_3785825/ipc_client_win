//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by P2PClient.rc
//
#define IDCANCEL                        2
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDS_STR_DEV_UN_LINE             102
#define IDD_P2PCLIENT_DIALOG            102
#define IDS_STR_DEV_MGR                 103
#define IDD_VIDEOPLAYDIALOG             103
#define IDS_STR_DEV_SEARCH              104
#define IDD_USERLOGINDIALOG             104
#define IDS_STR_DEV_CAPTION             105
#define IDS_STR_INPUT_USER_NAME         106
#define IDD_SYSMANAGERDIALOG            106
#define IDS_STR_LOGIN_CAPTION           107
#define IDD_DEVICEINFODIALOG            107
#define IDS_STR_LOGIN_ERROR             108
#define IDD_REMOTEWEBDIALOG             108
#define IDS_STR_LOGIN_PWD_ERR           109
#define IDD_VIDEORECORDPLANDIALOG       109
#define IDS_STR_NET_PARAM_ERR           110
#define IDD_VIDEORECORDPLAYDIALOG       110
#define IDS_STR_NET_DEVICE_LIST         111
#define IDD_EQUIPMENTCONFIGDIALOG       111
#define IDS_STR_NET_SERVER_DISCONNECT   112
#define IDD_EQUIPMENTSEARCHDIALOG       112
#define IDS_STR_NET_PAGE_NOT_OPEN       113
#define IDS_STR_NET_DATA_SEND_ERR       114
#define IDS_STR_NET_SERVER_HANDLE_ERR   115
#define IDS_STR_NET_EXCEPTION_ERR       116
#define IDS_STR_NET_UNKNOW_ERR          117
#define IDS_STR_SEARCH_DEVICE_IP        118
#define IDS_STR_SEARCH_DEVICE_ID        119
#define IDS_STR_SEARCH_DEVICE_IN_USE    120
#define IDS_STR_SEARCH_ERROR            121
#define IDS_STR_SEARCH_END              122
#define IDS_STR_MAIN_REAL_VIDEO         123
#define IDS_STR_MAIN_RECORD_VIDEO       124
#define IDS_STR_MAIN_LOCAL_CONFIG       125
#define IDS_STR_MAIN_NET_DEV_MGR        126
#define IDS_STR_SYS_MODIFY_PWD_ERR      127
#define IDR_MAINFRAME                   128
#define IDS_STR_SYS_CONFIG_CAPTION      128
#define IDS_STR_SYS_DEVICE_NOT_IN_USE   129
#define IDB_TREEIMAGE                   130
#define IDS_STR_SYS_DELETE_DEVICE       130
#define IDS_STR_SYS_NET_SERVER_INFO_ERR 131
#define IDS_STR_SYS_MESSAGE_INFO        132
#define IDS_STR_SYS_DEVICE_LIST_ID      133
#define IDS_STR_SYS_DEIVCE_LIST_NAME    134
#define IDS_STR_SYS_DEVICE_LIST_USER_NAME 135
#define IDS_STR_SYS_DEVICE_LIST_DEVICE_ID 136
#define IDS_STR_SYS_DEVICE_LIST_DEVICE_TXT 137
#define IDS_STR_REAL_VIDEO_CAPTION      138
#define IDS_STR_REAL_NET_PARAM_ERR      139
#define IDS_STR_REAL_UPDATE_DEV_LIST    140
#define IDS_STR_REAL_UPDATE_OK          141
#define IDS_STR_REAL_UPDATE_NET_DEV_LIST 142
#define IDS_STR_LOGIN_CHANGE_LAN        143
#define IDS_STR_REAL_DEVICE_ROOT        144
#define IDS_STR_REAL_NET_DEVICE_LIST    145
#define IDS_STR_REAL_DEVICE_LIST_TAB    146
#define IDS_STR_REAL_NET_DEVICE_LIST_TAB 147
#define IDS_STR_STATUS_DEV_CONNECTING   148
#define IDS_STR_STATUS_DEV_DISCONNECT   149
#define IDS_STR_STATUS_DEV_UID_ERR      150
#define IDS_STR_STATUS_DEV_PWD_ERR      151
#define IDS_STR_STATUS_DEV_NOT_ON_LINE  152
#define IDS_STR_RECORD_SEARCH_CHANNEL   153
#define IDS_STR_RECORD_SEARCH_CAPTION   154
#define IDC_CAPTION_TAB                 1000
#define IDC_DEVICE_TREE                 1001
#define IDC_SCREEN1_BTN                 1002
#define IDC_SCREEN4_BTN                 1003
#define IDC_USER_NAME_EDIT              1003
#define IDC_SCREEN9_BTN                 1004
#define IDC_USER_PWD_EDIT               1004
#define IDC_SCREEN6_BTN                 1005
#define IDC_LIST_DEIVCE_INFO            1005
#define IDC_SCREEN8_BTN                 1006
#define IDC_EDIT_OLD_PWD                1006
#define IDC_SCREEN16_BTN                1007
#define IDC_EDIT_NEW_PWD1               1007
#define IDC_SCREEN25_BTN                1008
#define IDC_EDIT_NEW_PWD2               1008
#define IDC_SCREEN36_BTN                1009
#define IDC_NETDEVICE_TREE              1010
#define IDC_UPDATE_BTN                  1010
#define IDC_ADD_DEV_BTN                 1011
#define IDC_MODIFY_DEV_BTN              1012
#define IDC_DEL_DEV_BTN                 1013
#define IDC_SYSTEM_MSG                  1014
#define IDC_NET_SAVE_BTN                1015
#define IDC_DEVICE_NAME_EDIT            1015
#define IDC_TREELIST_TAB                1016
#define IDC_MOD_DEV_BTN                 1016
#define IDC_DEVICE_UID_EDIT             1016
#define IDC_SEARCH_DEV_BTN              1017
#define IDC_WEB_EXPLORER                1017
#define IDC_DEVICE_PWD_EDIT             1017
#define IDC_VIDEO_PLAN                  1018
#define IDC_DEVICE_USER_EDIT            1018
#define IDC_VIDEO_RECORD_PLAN_BTN       1019
#define IDC_VIDEO_TREE                  1020
#define IDC_VIDEO_SAVE_PATH_EDIT        1021
#define IDC_SAVE_PATH_BTN               1022
#define IDC_VIDEO_CAPACITY_EDIT         1023
#define IDC_LEFT_CONTROL_BTN            1023
#define IDC_TOP_CONTROL_BTN             1024
#define IDC_VIDEO_RECORD_LIST           1025
#define IDC_BOTTOM_CONTROL__BTN         1025
#define IDC_START_DTP                   1026
#define IDC_RIGTH_CONTROL_BTN           1026
#define IDC_END_DTP                     1027
#define IDC_START_DTP2                  1027
#define IDC_SEARCH_BTN                  1028
#define IDC_NET_URL_EDIT                1029
#define IDC_NET_PORT_EDIT               1030
#define IDC_STATIC_USER_NAME            1032
#define IDC_STATIC_OLD_PWD              1033
#define IDC_STATIC_CON_PWD              1034
#define IDC_STATIC_NEW_PWD              1035
#define IDC_STATIC_NET_ADDR             1036
#define IDC_STATIC_NET_PORT             1037
#define IDC_STATIC_RECORD_PATH          1038
#define IDC_STATIC_RECORD_TIME          1039
#define IDC_STATIC_RECORD_MIN           1040
#define IDC_STATIC_START_TIME           1041
#define IDC_STATIC_END_TIME             1042
#define IDC_STATIC_SPLIT_SCREEN         1043
#define IDC_UPDATE_NET_BTN              1044
#define IDC_STATIC_EQUIPMENT__MSG       1045
#define IDC_NEW_PWD_EDIT                1046
#define IDC_MODIFY_DEV_PWD_BTN          1047
#define IDC_VIDEO_QU_COMBO              1048
#define IDC_TURN_MODE_COMBO             1049
#define IDC_EN_MODE_COMBO               1050
#define IDC_MODIFY_DEV_MODE_BTN         1051
#define IDC_RECORD_MODE_COMBO           1052
#define IDC_DETECT_MODE_COMBO           1053
#define IDC_WIFI_SSID_EDIT              1054
#define IDC_AP_MODE_COMBO               1055
#define IDC_WIFI_ENCTYPE_COMBO          1056
#define IDC_WIFI_SIGNAL_PROGRESS        1057
#define IDC_IPCAM_MODE_EDIT             1058
#define IDC_MANUFACTURER_EDIT           1059
#define IDC_FIRMWARE_EDIT               1060
#define IDC_TOTAL_STATUS_EDIT           1061
#define IDC_SD_FREE_SPACE_EDIT          1062
#define IDC_WIFI_STATUS_EDIT            1063
#define IDC_MODIFY_WIFI_MSG_BTN         1064
#define IDC_SEARCH_DEV_LIST             1065
#define ID_DEV_SEARCH_BTN               1066
#define ID_DEV_ADD_BTN                  1067
#define IDC_COMBO_LAN                   1067

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1068
#define _APS_NEXT_SYMED_VALUE           113
#endif
#endif
