// SendData.cpp: implementation of the CSendData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "P2PClient.h"
#include "SendData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSendData::CSendData()
{
	m_lpUTF8 = NULL;		//UTF8编码缓存指针
	m_lpMultiByte = NULL;		//
	m_wpWideChar = NULL;		//Widechar编码缓存指针
	//服务器默认值
	_tcscpy(m_tzServerName,_T("www.lilu.name"));
	_tcscpy(m_tzObjectName,_T("/posttest.asp"));
	m_nPort = INTERNET_DEFAULT_HTTP_PORT;
}

CSendData::~CSendData()
{
	//如果原先的数据没有删除
	if (m_lpUTF8)
	{
		delete [] m_lpUTF8;
		m_lpUTF8 = NULL;
	}
	//如果原先的数据没有删除
	if (m_lpMultiByte)
	{
		delete [] m_lpMultiByte;
		m_lpMultiByte = NULL;
	}
	//宽字节代码指针
	if (m_wpWideChar)
	{
		delete []m_wpWideChar;
		m_wpWideChar = NULL;
	}

}
//设置服务器参数
void CSendData::SetServerParam(TCHAR ServerName[], TCHAR ObjectName[], WORD Port)
{
	_tcscpy(m_tzServerName,ServerName);
	_tcscpy(m_tzObjectName,ObjectName);
	m_nPort = Port;
}
//发送数据
//编码参数：1表示GB2312，2表示UTF8
DWORD CSendData::PostDataMethod2(LPCTSTR strDataSend, 
								 int DataSize, 
								 LPTSTR *pRecvData, 
								 DWORD &RecvSize,
								 int SendCode,	//发送时的编码
								 int RecvCode	//接收返回时的编码
								 )	
{
	//返回错误代码列表：
	//100：正常成功
	//101：服务器无法连接
	//102：提交页面无法打开
	//103：数据发送失败
	//104：服务器处理失败
	//500：异常错误

	CInternetSession	InternetSession;
	CHttpConnection		*pHttp = NULL;
	CHttpFile			*pFile = NULL;
	DWORD				nRetCode	= 100;	//返回值
	TCHAR				tzErrMsg[MAX_PATH] = {0};	//	捕捉异常
	string				strResponse;	//接收的数据
	
	try
	{
		pHttp = InternetSession.GetHttpConnection(m_tzServerName,m_nPort);
		if (pHttp == NULL)
		{
			return 101;
		}
		pFile = pHttp->OpenRequest(CHttpConnection::HTTP_VERB_POST,m_tzObjectName,NULL);
		if (pFile == NULL)
		{
			return 102;
		}
		CString	strHeader = _T("Content-Type: application/x-www-form-urlencoded");
		//////////////////////////////////////////////////////////////////////////
		//发送时采用GB2312
		if (SendCode == 1)
		{
			#ifdef UNICODE
				WideToMulti((LPCWSTR)(LPCTSTR)strDataSend,DataSize);
			#else
				MultiToMulti((LPCSTR)(LPCTSTR)strDataSend,DataSize);				
			#endif
			//发送
			if (pFile->SendRequest(strHeader,m_lpMultiByte,m_nMultiByteLen) == FALSE)
			{
				return 103;
			}
		}
		//发送时选择UTF8
		else if (SendCode == 2)
		{
			#ifdef UNICODE
				WideCharToUTF8((LPCWSTR)(LPCTSTR)strDataSend,DataSize);
			#else
				MultiByteToUTF8((LPCSTR)(LPCTSTR)strDataSend,DataSize);
			#endif
			//发送
			if (pFile->SendRequest(strHeader,m_lpUTF8,m_nUTF8Len) == FALSE)
			{
				return 103;
			}
		}
		
		//查询状态
		DWORD	dwStatusCode = 0;
		pFile->QueryInfoStatusCode(dwStatusCode);
		if (dwStatusCode != 200)
		{
			return 104;
		}
		
		//////////////////////////////////////////////////////////////////////////
		//接收数据
		char	szRead[1024*10] = {0};
		while((pFile->Read(szRead,1024*10)) > 0)
		{
			strResponse += szRead;
		}
		//转换接收的数据
		//接收采用GB2312编码
		if (RecvCode == 1)
		{
			#ifdef UNICODE
				MultiToWide(strResponse.c_str(),strResponse.length());
				*pRecvData = (LPTSTR)m_wpWideChar;
				RecvSize = m_nWideCharLen;
			#else
				MultiToMulti(strResponse.c_str(),strResponse.length());
				*pRecvData = (LPTSTR)m_lpMultiByte;
				RecvSize = m_nMultiByteLen;
			#endif
		}
		//接收采用UTF8编码
		else if (RecvCode == 2)
		{
			#ifdef UNICODE
				UTF8ToWideChar(strResponse.c_str(),strResponse.length());
				*pRecvData = (LPTSTR)m_wpWideChar;
				RecvSize = m_nWideCharLen;
			#else
				UTF8ToMultiByte(strResponse.c_str(),strResponse.length());
				*pRecvData = (LPTSTR)m_lpMultiByte;
				RecvSize = m_nMultiByteLen;
			#endif
		}
		//返回值
		nRetCode = 100;
	}
	//捕获异常
	catch (CInternetException* e)
	{
		e->GetErrorMessage(tzErrMsg,sizeof(tzErrMsg));
		e->Delete();
		nRetCode = 500;
	}
	
	//////////////////////////////////////////////////////////////////////////
	//关闭
	pFile->Close();
	delete	pFile;
	pHttp->Close();
	delete pHttp;
	InternetSession.Close();
	
	return nRetCode;
}
//UTF-8转为宽字符
void CSendData::UTF8ToWideChar(LPCSTR pUTF8,int UTF8Len)
{
	
	if (m_wpWideChar)
	{
		delete [] m_wpWideChar;
		m_wpWideChar = NULL;
	}
	
	m_nWideCharLen = MultiByteToWideChar(CP_UTF8,0,pUTF8,UTF8Len,NULL,0);
	
	m_wpWideChar = new WCHAR[m_nWideCharLen + 1];
	m_wpWideChar[m_nWideCharLen] = 0;
	
	MultiByteToWideChar(CP_UTF8,0,pUTF8,UTF8Len,m_wpWideChar,m_nWideCharLen);
	
}
//将UTF8转为多字节
void CSendData::UTF8ToMultiByte(LPCSTR pUTF8, int UTF8Len)
{
	//这个转换也要分两步，先将UTF8转为宽字符，
	//再将宽字符转为多字节。
	
	if (m_lpMultiByte)
	{
		delete [] m_lpMultiByte;
		m_lpMultiByte = NULL;
	}
	
	//UTF8转宽字符
	UTF8ToWideChar(pUTF8,UTF8Len);
	//宽字符转为多字节
	m_nMultiByteLen = WideCharToMultiByte(CP_ACP,0,m_wpWideChar,m_nWideCharLen,NULL,0,NULL,NULL);
	m_lpMultiByte = new char[m_nMultiByteLen + 1];
	m_lpMultiByte[m_nMultiByteLen] = 0;
	
	WideCharToMultiByte(CP_UTF8,0,m_wpWideChar,m_nWideCharLen,m_lpMultiByte,m_nMultiByteLen,NULL,NULL);
	
}
//将宽字符转为UTF-8
void CSendData::WideCharToUTF8(LPCWSTR wpWideData, int WideLen)
{
	//如果原先的数据没有删除
	if (m_lpUTF8)
	{
		delete [] m_lpUTF8;
		m_lpUTF8 = NULL;
	}
	
	m_nUTF8Len = WideCharToMultiByte(CP_UTF8,0,wpWideData,WideLen,NULL,0,NULL,NULL);
	
	
	m_lpUTF8 = new char[m_nUTF8Len + 1];
	m_lpUTF8[m_nUTF8Len] = 0;
	
	
	WideCharToMultiByte(CP_UTF8,0,wpWideData,WideLen,m_lpUTF8,m_nUTF8Len,NULL,NULL);
	
}


//将多字节转为UTF8
void CSendData::MultiByteToUTF8(LPCSTR lpMultiData, int MultiLen)
{
	//这个转换要麻烦点，要先将多字节转为宽字符，再将宽字符转为UTF8
	//其实如果全部是英文的话，是没有必要进行转换了，如果有中文，就必须转换
	//如果原先的数据没有删除
	if (m_lpUTF8)
	{
		delete [] m_lpUTF8;
		m_lpUTF8 = NULL;
	}
	if (m_wpWideChar)
	{
		delete [] m_wpWideChar;
		m_wpWideChar = NULL;
	}
	//先转为宽字符	
	m_nWideCharLen = MultiByteToWideChar(CP_ACP,0,lpMultiData,MultiLen,NULL,0);
	
	m_wpWideChar = new WCHAR[m_nWideCharLen + 1];
	m_wpWideChar[m_nWideCharLen] = 0;
	
	
	MultiByteToWideChar(CP_UTF8,0,lpMultiData,MultiLen,m_wpWideChar,m_nWideCharLen);
	//再将宽字符转为UTF8
	WideCharToUTF8(m_wpWideChar,m_nWideCharLen - 1);
}

void CSendData::WideToMulti(LPCWSTR wpWideData, int WideLen)
{
	if (m_lpMultiByte)
	{
		delete [] m_lpMultiByte;
		m_lpMultiByte = NULL;
	}

	m_nMultiByteLen = WideCharToMultiByte(CP_ACP,0,wpWideData,WideLen,NULL,0,NULL,NULL);
	m_lpMultiByte = new char[m_nMultiByteLen + 1];
	m_lpMultiByte[m_nMultiByteLen] = '\0';

	WideCharToMultiByte(CP_ACP,0,wpWideData,WideLen,m_lpMultiByte,m_nMultiByteLen,NULL,NULL);

}

void CSendData::MultiToMulti(LPCSTR strDataSend, int DataSize)
{
	if (m_lpMultiByte)
	{
		delete [] m_lpMultiByte;
		m_lpMultiByte = NULL;
	}
	
	m_nMultiByteLen = DataSize;
	m_lpMultiByte = new	char[m_nMultiByteLen + 1];
	m_lpMultiByte[m_nMultiByteLen] = '\0';

	memcpy(m_lpMultiByte,(LPCSTR)strDataSend,m_nMultiByteLen);

}

void CSendData::MultiToWide(LPCSTR strDataSend, int DataSize)
{
	if (m_wpWideChar)
	{
		delete [] m_wpWideChar;
		m_wpWideChar = NULL;
	}

	m_nWideCharLen = MultiByteToWideChar(CP_ACP,0,strDataSend,DataSize,NULL,0);

	m_wpWideChar = new wchar_t[m_nWideCharLen + 1];
	m_wpWideChar[m_nWideCharLen] = 0;

	MultiByteToWideChar(CP_ACP,0,strDataSend,DataSize,m_wpWideChar,m_nWideCharLen);

}
