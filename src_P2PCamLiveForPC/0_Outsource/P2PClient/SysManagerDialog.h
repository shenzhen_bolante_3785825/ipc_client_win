#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "EquipmentConfigDialog.h"

// CSysManagerDialog 对话框

class CSysManagerDialog : public CSkinDialog
{
	DECLARE_DYNAMIC(CSysManagerDialog)

public:
	CSysManagerDialog(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CSysManagerDialog();

// 对话框数据
	enum { IDD = IDD_SYSMANAGERDIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedUpdateBtn();
public:
	CString m_csOldUserPwd;
public:
	CString m_csNewUserPwd1;
public:
	CString m_csNewUserPwd2;
public:
	virtual BOOL OnInitDialog();
public:
	CListCtrl m_DeviceListCtrl;
public:
	afx_msg void OnBnClickedAddDevBtn();
public:
	afx_msg void OnBnClickedModifyDevBtn();
public:
	afx_msg void OnBnClickedDelDevBtn();
public:
	CString m_csSystemMsg;

	int		m_nCurID;
	CString	m_csDeviceID;

private:
	void InitDeviceInfoList();
	afx_msg void OnHdnItemchangedListDeivceInfo(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClickListDeivceInfo(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnStnClickedSystemMsg();
	afx_msg void OnBnClickedVideoRecordPlanBtn();
	CString m_csNetURLEdit;
	int m_nNetPortEdit;
	afx_msg void OnBnClickedNetSaveBtn();
	CSkinEdit m_OldPwdEdit;
	CSkinEdit m_NewPwdEdit1;
	CSkinEdit m_NewPwdEdit2;
	CSkinButton m_btnUpdate;
	CSkinButton m_btnVideoRecordPlan;
	CSkinStatic m_pwd1;
	CSkinStatic m_pwd2;
	CSkinStatic m_pwd3;
	CSkinStatic m_netport;
	CSkinStatic m_netaddr;
	CSkinButton m_btnAddDev;
	CSkinButton m_btnMOdifyDev;
	CSkinButton m_btnDelDev;
	CSkinStatic m_systemmsg;
	CSkinButton m_btnNetSave;
	CSkinButton m_btnModDev;
	afx_msg void OnBnClickedModDevBtn();
	CStatic m_staticEquipmentMsg;
	afx_msg void OnSize(UINT nType, int cx, int cy);

	CEquipmentConfigDialog m_EquipmentConfigDialog;
	CSkinButton m_SearchDevBtn;
	afx_msg void OnBnClickedSearchDevBtn();
protected:
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
public:
	afx_msg void OnBnClickedPictureCaptureBtn();
public:
	CSkinButton m_PictureCaptureBtn;
public:
	afx_msg void OnBnClickedEmailConfigBtn();
public:
	CSkinButton m_EmailConfigBtn;
};
