#include "StdAfx.h"
#include "P2PClient.h"
#include "RealStreamManager.h"
#include "NvsModelPlanUI.h"
#include <time.h>
#include <shlwapi.h>


RealStreamManager::RealStreamManager(void)
{
	m_fOnAVFrame	= NULL;
	m_pParam		= NULL;
	m_nRecIndex		= -1;
	m_dwLeastSpace	= 10000;
	m_dwStartRecordTime = 0;
	m_pRecordFile	= NULL;
	memset(m_sVideoRecordPath, 0, 256);
	memset(m_sDeviceID, 0, 256);
	memset(m_sDeviceName, 0, 256);

	m_bPlaying		= FALSE;
	m_bRecording	= FALSE;

	m_objPlayStream	= NULL;
}

RealStreamManager::~RealStreamManager(void)
{
	StopRecVideo();
}

tkVOID WINAPI RealStreamManager::OnAVFrame(tkCHAR *pData, tkINT32 nSize, tkVOID *pFrmInfo, tkINT32 nFrmInfoSize, tkUINT32 nTag, tkVOID *param)
{
#ifdef _DEBUG
	TRACE(_T("RealStreamManager::OnAVFrame\n"));
#endif
	RealStreamManager *pThis = (RealStreamManager *)param;
	//转发至实时视频播放
	if(pThis->m_fOnAVFrame)
		pThis->m_fOnAVFrame(pData, nSize, pFrmInfo, nFrmInfoSize, nTag, pThis->m_pParam);

	//转发至录像
	if( pThis->m_nRecIndex < 0 || pThis->m_nRecIndex > theApp.m_modeList.totalNum)
		return ;

	pThis->DealRecVideo(pData, nSize, pFrmInfo, nFrmInfoSize, nTag, param);
}

int	RealStreamManager::PlayStream(tkPVOID objPlayStream, fOnAVFrame subOnAVFrame, void *param)
{
	m_fOnAVFrame	= subOnAVFrame;
	m_pParam		= param;
	m_objPlayStream = objPlayStream;

	if(m_bPlaying == FALSE)
	{
		apiRegCB_OnAVFrame(objPlayStream, OnAVFrame, this);
		apiStartVideo(objPlayStream);
		m_bPlaying = TRUE;
	}
	return 0;
}

int	RealStreamManager::StopStream()
{
	if(m_objPlayStream)
	{
		apiRegCB_OnAVFrame(m_objPlayStream, NULL, NULL);
		m_objPlayStream = NULL;
	}
	m_bPlaying = FALSE;
	m_fOnAVFrame	= NULL;
	m_pParam		= NULL;

	return 0;
}
int	RealStreamManager::StopRecVideo()
{
	if(m_bRecording == FALSE)
		return 0;
	m_bRecording = FALSE;
	m_bPlaying = FALSE;
	m_fOnAVFrame	= NULL;
	m_pParam		= NULL;

	SYSTEMTIME stUTC;
	::GetSystemTime(&stUTC);

	if(m_pRecordFile)
	{
		fclose(m_pRecordFile);
		m_pRecordFile = NULL;
	}

	m_csEndRecordTime.Format(_T("%04d-%02d-%02d %02d:%02d:%02d"), stUTC.wYear, stUTC.wMonth, stUTC.wDay, stUTC.wHour, stUTC.wMinute, stUTC.wSecond);
	//写录像记录到数据库
	try
	{
		theApp.m_SQLite3DB.open(_T("SystemDB"));
		TCHAR sSelectSQL[256] = {0};
		wsprintf(sSelectSQL, _T("select * from RecordInfo where UserID = %d order by uID desc"), theApp.m_nCurUserID);
		CppSQLite3Query SQLiteQuery = theApp.m_SQLite3DB.execQuery(sSelectSQL);
		int m_nCurID = 0;
		if(!SQLiteQuery.eof())
		{
			m_nCurID = ::_ttoi(SQLiteQuery.fieldValue(_T("uID"))) + 1;
		}
		SQLiteQuery.finalize();

		CString csDeleteSQL;
		csDeleteSQL.Format(_T("insert into RecordInfo(uID, DeviceID, RecordTime, RecordPath, StartTime, EndTime, UserID) values(%d, '%s', %d, '%s', '%s', '%s', %d)"),  
			m_nCurID, m_sDeviceID, theApp.m_uVideoFileTime, m_sVideoRecordPath, m_csStartRecordTime, m_csEndRecordTime, theApp.m_nCurUserID);
		theApp.m_SQLite3DB.execDML(csDeleteSQL.GetBuffer());

	}
	catch(CppSQLite3Exception ce)
	{
		int i = 0;
	}
	theApp.m_SQLite3DB.close();
	return 0;
}

void RealStreamManager::SetRecIndex(int nIndex, TCHAR *sDeviceID, TCHAR *sDeviceName)
{
	m_nRecIndex		= nIndex;

	if(sDeviceID)
		_tcscpy(m_sDeviceID, sDeviceID);
	if(sDeviceName)
		_tcscpy(m_sDeviceName, sDeviceName);
}

void RealStreamManager::DealRecVideo(tkCHAR *pData, tkINT32 nSize, tkVOID *pFrmInfo, tkINT32 nFrmInfoSize, tkUINT32 nTag, tkVOID *param)
{
	ModelInfo *pModelInfo = &theApp.m_modeList.recModelInfo[m_nRecIndex];
	time_t curTime = ::time(NULL);
	tm *tt = localtime(&curTime);
	int tHour = 0;
	if(tt->tm_min < 30)
		tHour = tt->tm_hour*2;
	else
		tHour = tt->tm_hour * 2 + 1;
	if(pModelInfo->recPlan[tt->tm_wday][tHour] == true)
	{
		//1、判断磁盘还剩余多少空间，如果空间不足，即删除最旧录像，确保有足够的存储空间
		m_bRecording = true;
		bool bDiskSpaceOver = false;
		ULARGE_INTEGER totalspace;
        ULARGE_INTEGER freespace;
        ULARGE_INTEGER freebyte;
		int nDiskNum = PathGetDriveNumber(theApp.m_csVideoRecordPath);
		CString csDiskName;
		switch(nDiskNum)
		{
		case 0:
			csDiskName.Format(_T("A:"));
			break;
		case 1:
			csDiskName.Format(_T("B:"));
			break;
		case 2:
			csDiskName.Format(_T("C:"));
			break;
		case 3:
			csDiskName.Format(_T("D:"));
			break;
		case 4:
			csDiskName.Format(_T("E:"));
			break;
		case 5:
			csDiskName.Format(_T("F:"));
			break;
		case 6:
			csDiskName.Format(_T("G:"));
			break;
		case 7:
			csDiskName.Format(_T("H:"));
			break;
		case 8:
			csDiskName.Format(_T("I:"));
			break;
		case 9:
			csDiskName.Format(_T("J:"));
			break;
		case 10:
			csDiskName.Format(_T("K:"));
			break;
		}
		if (GetDiskFreeSpaceEx(csDiskName, &freebyte, &totalspace, &freespace))
        {
			
            DWORD dwDiskSpace = (DWORD)(freespace.QuadPart/(ULONGLONG)1024/(ULONGLONG)1024);
            if ( dwDiskSpace < m_dwLeastSpace )//单位为：M
            {
                bDiskSpaceOver = true;
            }
        }

		//2、判断录像文件大小是否已经超过设置，如果超过设置，即保存该文件，并重新写录像文件
		
		 SYSTEMTIME stUTC;
			::GetSystemTime(&stUTC);
		if(m_dwStartRecordTime == 0)
		{
			m_dwStartRecordTime = ::time(NULL);
			
			m_csStartRecordTime.Format(_T("%04d-%02d-%02d %02d:%02d:%02d"), stUTC.wYear, stUTC.wMonth, stUTC.wDay, stUTC.wHour, stUTC.wMinute, stUTC.wSecond);
		}
		if(m_pRecordFile == NULL)
		{
			wsprintf(m_sVideoRecordPath, _T("%s\\%04d_%02d_%02d_%02d_%02d_%02d.mp4"), theApp.m_csVideoRecordPath.GetBuffer(), 
				stUTC.wYear, stUTC.wMonth, stUTC.wDay, stUTC.wHour, stUTC.wMinute, stUTC.wSecond);
			m_pRecordFile = _tfopen(m_sVideoRecordPath, _T("ab+"));

			int i = 0;
			
		}
		DWORD dwCurTime = ::time(NULL);
		int nTimeInteval = dwCurTime - m_dwStartRecordTime;
		if( nTimeInteval > (theApp.m_uVideoFileTime * 60) )//默认录像时长为30分钟
		{
			//如果时间到
			if(m_pRecordFile)
			{
				fclose(m_pRecordFile);
				m_pRecordFile = NULL;
			}

			m_csEndRecordTime.Format(_T("%04d-%02d-%02d %02d:%02d:%02d"), stUTC.wYear, stUTC.wMonth, stUTC.wDay, stUTC.wHour, stUTC.wMinute, stUTC.wSecond);
			//写录像记录到数据库
			try
			{
				theApp.m_SQLite3DB.open(_T("SystemDB"));
				TCHAR sSelectSQL[256] = {0};
				wsprintf(sSelectSQL, _T("select * from RecordInfo where UserID = %d order by uID desc"), theApp.m_nCurUserID);
				CppSQLite3Query SQLiteQuery = theApp.m_SQLite3DB.execQuery(sSelectSQL);
				int m_nCurID = 0;
				if(!SQLiteQuery.eof())
				{
					m_nCurID = ::_ttoi(SQLiteQuery.fieldValue(_T("uID"))) + 1;
				}
				SQLiteQuery.finalize();

				CString csDeleteSQL;
				csDeleteSQL.Format(_T("insert into RecordInfo(uID, DeviceID, RecordTime, RecordPath, StartTime, EndTime, UserID) values(%d, '%s', %d, '%s', '%s', '%s', %d)"),  
					m_nCurID, m_sDeviceID, theApp.m_uVideoFileTime, m_sVideoRecordPath, m_csStartRecordTime, m_csEndRecordTime, theApp.m_nCurUserID);
				theApp.m_SQLite3DB.execDML(csDeleteSQL.GetBuffer());

			}
			catch(CppSQLite3Exception ce)
			{
				int i = 0;
			}
			theApp.m_SQLite3DB.close();

			//新建文件，并开始写入
			m_dwStartRecordTime = ::time(NULL);
			 SYSTEMTIME stUTC;
			::GetSystemTime(&stUTC);
			m_csStartRecordTime.Format(_T("%04d-%02d-%02d %02d:%02d:%02d"), stUTC.wYear, stUTC.wMonth, stUTC.wDay, stUTC.wHour, stUTC.wMinute, stUTC.wSecond);

			wsprintf(m_sVideoRecordPath, _T("%s\\%04d_%02d_%02d_%02d_%02d_%02d.mp4"), theApp.m_csVideoRecordPath.GetBuffer(), 
				stUTC.wYear, stUTC.wMonth, stUTC.wDay, stUTC.wHour, stUTC.wMinute, stUTC.wSecond);
			m_pRecordFile = _tfopen(m_sVideoRecordPath, _T("ab+"));

			
		}
		//写录像文件
		if(m_pRecordFile)
		{
			char sPacketInfo[8] = {0};
			memcpy(sPacketInfo, &nSize, 4);
			fwrite(sPacketInfo, 4, 1, m_pRecordFile);
			fwrite(pData, nSize, 1, m_pRecordFile);
		}
	}
	else
	{
		m_bRecording = FALSE;
	}
}