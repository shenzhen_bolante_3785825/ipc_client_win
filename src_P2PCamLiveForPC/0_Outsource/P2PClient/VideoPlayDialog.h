#pragma once
#include "ScreenPannel.h"
#include "afxcmn.h"
#include "P2PAVCore.h"
#include <map>
#include <string>
#include "RealStreamManager.h"
#include "ControlButton.h"
#include "afxwin.h"

using namespace std;
// CVideoPlayDialog 对话框

typedef struct __CAMERA_INFO
{
	TCHAR sDevUID[64];
	TCHAR sUserName[128];
	TCHAR sUserPwd[128];

	HTREEITEM	hCurItem;
};

typedef struct __PLAY_INFO
{
	tkPVOID		objPlayStream;
	int			nConnected;
	BOOL		bPlaying;
	BOOL		bRecording;
	RealStreamManager *fRealStreamMgr;
	HTREEITEM	hCurItem;
	TCHAR		sDeviceID[128];
	TCHAR		sError[256];
};

class CVideoPlayDialog : public CSkinDialog
{
	DECLARE_DYNAMIC(CVideoPlayDialog)

public:
	CVideoPlayDialog(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CVideoPlayDialog();

// 对话框数据
	enum { IDD = IDD_VIDEOPLAYDIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()

public:
	int m_iCurWndNum;	//screen split mode 1, 4, 9, 16, 25, 36
	int m_iCurWndIndex; //current selected split window index, start from 0
private:
	CScreenPannel		m_screenPannel;		//播放屏幕底板－子窗口
	int m_curScreen;
	BOOL m_bFullScreen;
	CWnd *m_hWndParent;
	int		m_nScreenNum;
public:

	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);

	void	ExitThread();
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	void FullScreen();
protected:
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
public:
	CTreeCtrl m_deviceTree;
	CImageList m_image;
private:
	void InitTreeView();

	void InitNetTreeView();

	void ConnectAllDevice(int nInitFlag = 0);

	void SetConnectStatus(tkPVOID objPlayStream, int nConnected, BOOL bRecord = FALSE, const TCHAR *pErrorCode = NULL);

	HTREEITEM	m_hParentItem;
	HTREEITEM	m_hNetParentItem;
	afx_msg void OnBnClickedScreen6Btn();
	afx_msg void OnBnClickedScreen9Btn();
	afx_msg void OnBnClickedScreen4Btn();
	afx_msg void OnBnClickedScreen1Btn();
	afx_msg void OnBnClickedScreen8Btn();
	afx_msg void OnBnClickedScreen16Btn();
	afx_msg void OnBnClickedScreen25Btn();
	afx_msg void OnBnClickedScreen36Btn();
	CSkinTabCtrl m_TreeListTab;
	afx_msg void OnTcnSelchangeTreelistTab(NMHDR *pNMHDR, LRESULT *pResult);
	CTreeCtrl m_netDeviceTree;
	afx_msg void OnNMDblclkDeviceTree(NMHDR *pNMHDR, LRESULT *pResult);

	static tkVOID WINAPI OnStatus(tkINT32 nType, tkINT32 nValue, tkPVOID pObj, tkUINT32 nTag, tkVOID *param);
	static tkVOID WINAPI OnRecvIOCtrl(tkINT32 nIOType, tkCHAR *pIOData, tkINT32 nIODataSize, tkPVOID pObj, tkUINT32 nTag, tkVOID *param);
	static DWORD WINAPI ThreadConnectDev(LPVOID lpPara);
	void SendAlarmEmail();
private:
	tkPVOID							m_objPlayStream;
#ifndef UNICODE
	map<string, __PLAY_INFO>			m_curDeviceMap;
	map<string, __CAMERA_INFO*>			m_curNetDeviceMap;

	map<string, HTREEITEM>				m_curDeviceItemMap;
#else
	map<wstring, __PLAY_INFO>			m_curDeviceMap;
	map<wstring, __CAMERA_INFO*>			m_curNetDeviceMap;

	map<wstring, HTREEITEM>				m_curDeviceItemMap;
#endif

	BOOL							m_bThreadFlag;
	CControlButton m_leftControlBtn;
	CControlButton m_rigthControlBtn;
	CControlButton m_topControlBtn;
	CControlButton m_bottomControlBtn;

	CString	m_ObjectName;
	UINT	m_nPort;
	CString	m_PostData;
	CString	m_RecvData;
	CString	m_ServerName;
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg LRESULT OnUpdateDataMessage(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnUpdateNetDataMessage(WPARAM wParam, LPARAM lParam);
	CSkinButton m_screen1btn;
	CSkinButton m_screen4btn;
	CSkinButton m_screen9btn;
	CSkinButton m_screen6btn;
	CSkinButton m_screen8btn;
	CSkinButton m_screen16btn;
	CSkinButton m_screen25btn;
	CSkinButton m_screen36btn;
	CStatic m_splitscreen;
	afx_msg void OnNMDblclkNetdeviceTree(NMHDR *pNMHDR, LRESULT *pResult);
	CSkinButton m_updateNetBtn;
	afx_msg void OnTvnBegindragDeviceTree(NMHDR *pNMHDR, LRESULT *pResult);

private:
	CImageList* m_pDragImage; //拖动时显示的图象列表
	HTREEITEM m_hItemDragS; //被拖动的标签
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	int				m_nDragWinIndex;
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	BOOL			m_bDeviceTreeDrag;

	BOOL			m_bInitNetDevices;

public:
#ifdef UNICODE
	tkPVOID FindDeviceObjFromUID(std::wstring sUID, int &nStatus);
#else
	tkPVOID FindDeviceObjFromUID(std::string sUID, int &nStatus);
#endif

public:
	afx_msg void OnTvnBeginrdragNetdeviceTree(NMHDR *pNMHDR, LRESULT *pResult);
public:
	afx_msg void OnTvnBegindragNetdeviceTree(NMHDR *pNMHDR, LRESULT *pResult);
public:
	afx_msg void OnDestroy();
};

