// P2PClientDlg.h : 头文件
//

#pragma once
#include "afxcmn.h"
#include "VideoPlayDialog.h"
#include "SysManagerDialog.h"
#include "RemoteWebDialog.h"
#include "VideoRecordPlayDialog.h"

// CP2PClientDlg 对话框
class CP2PClientDlg : public CSkinDialog
{
// 构造
public:
	CP2PClientDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_P2PCLIENT_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
public:
	CSkinTabCtrl m_ctrlCaptionTab;
public:
	afx_msg void OnTcnSelchangeCaptionTab(NMHDR *pNMHDR, LRESULT *pResult);
private:
	CVideoPlayDialog		*m_pVideoPlayDialog;
	CSysManagerDialog		*m_pSystemMgrDialog;
	CRemoteWebDialog		*m_pRemoteWebDialog;
	VideoRecordPlayDialog	*m_pVideoRecordPlayDialog;
};
