// UserLoginDialog.cpp : 实现文件
//

#include "stdafx.h"
#include "P2PClient.h"
#include "UserLoginDialog.h"
#include "SendData.h"
#include "ChineseCode.h"
#include "tinyxml.h"
#include <Shlwapi.h>
#pragma  comment(lib,"Shlwapi.lib") 


// CUserLoginDialog 对话框

IMPLEMENT_DYNAMIC(CUserLoginDialog, CSkinDialog)

CUserLoginDialog::CUserLoginDialog(CWnd* pParent /*=NULL*/)
	: CSkinDialog(CUserLoginDialog::IDD, pParent)
	, m_csUserName(_T(""))
	, m_csUserPwd(_T(""))
	, m_ObjectName(_T("/interface/login_mb.ds"))
	, m_PostData(_T("user=001&&password=123"))
{

}

CUserLoginDialog::~CUserLoginDialog()
{
}

void CUserLoginDialog::DoDataExchange(CDataExchange* pDX)
{
	CSkinDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_USER_NAME_EDIT, m_csUserName);
	DDX_Text(pDX, IDC_USER_PWD_EDIT, m_csUserPwd);
	DDX_Control(pDX, IDOK, m_btnUserLogin);
	DDX_Control(pDX, IDCANCEL, m_btnLoginCancel);
	DDX_Control(pDX, IDC_STATIC_USER_NAME, m_UserNameStatic);
	DDX_Control(pDX, IDC_START_DTP, m_passwordStatic);
	DDX_Control(pDX, IDC_COMBO_LAN, m_SysLanCombo);
}


BEGIN_MESSAGE_MAP(CUserLoginDialog, CSkinDialog)
	ON_BN_CLICKED(IDOK, &CUserLoginDialog::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CUserLoginDialog::OnBnClickedCancel)
	ON_CBN_SELCHANGE(IDC_COMBO_LAN, &CUserLoginDialog::OnCbnSelchangeComboLan)
END_MESSAGE_MAP()


// CUserLoginDialog 消息处理程序

void CUserLoginDialog::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	if(!PathFileExists(_T("SystemDB")))
	{
		//theApp.m_SQLite3DB.open(_T("SystemDB"), _T("p2pavcore"));
		theApp.m_SQLite3DB.open(_T("SystemDB"));

		TCHAR strCreateSQL[256] = {0};
		wsprintf(strCreateSQL, _T("Create  TABLE UserInfo(uID INTEGER PRIMARY KEY,uUserName TEXT, uUserPwd TEXT)"));
		theApp.m_SQLite3DB.execDML(strCreateSQL);
	}
	else
	{
		//theApp.m_SQLite3DB.open(_T("SystemDB"), _T("p2pavcore"));
		theApp.m_SQLite3DB.open(_T("SystemDB"));
	}
	
	UpdateData(true);
	if(m_csUserName.GetLength() == 0)
	{
		//MessageBox("用户名没有输入！", "客户端", MB_ICONWARNING);

		CString strText;
		strText.LoadString(IDS_STR_INPUT_USER_NAME);
		CString strCaption;
		strCaption.LoadString(IDS_STR_LOGIN_CAPTION);
		MessageBox(strText, strCaption, MB_ICONWARNING);
		return ;
	}
	TCHAR strInsertSQL[256] = {0};
	wsprintf(strInsertSQL, _T("select * from UserInfo where uUserName = '%s'"), m_csUserName.GetBuffer());
	CppSQLite3Query SQLiteQuery = theApp.m_SQLite3DB.execQuery(strInsertSQL);
	if(SQLiteQuery.eof())
	{
		//int nRet = MessageBox("对不起，该用户没有注册！现在注册？", "客户端", MB_YESNO);
		//if (nRet == IDYES)
		if(m_csUserName == "admin")
		{
			wsprintf(strInsertSQL, _T("select * from UserInfo order by uID desc"));
			CppSQLite3Query SQLiteQuery2 = theApp.m_SQLite3DB.execQuery(strInsertSQL);
			if(!SQLiteQuery2.eof())
			{
				theApp.m_nCurUserID = ::_ttoi(SQLiteQuery2.fieldValue(_T("uID")));
				theApp.m_nCurUserID++;
			}
			SQLiteQuery2.finalize();
			wsprintf(strInsertSQL, _T("insert into UserInfo(uID, uUserName, uUserPwd) values (%d, '%s', '%s')"), theApp.m_nCurUserID, m_csUserName.GetBuffer(), m_csUserPwd.GetBuffer());
			theApp.m_SQLite3DB.execDML(strInsertSQL);
			theApp.m_bFirstTime = true;

			SQLiteQuery.finalize();
			theApp.m_SQLite3DB.close();
			return ;
		}
		//else
		//{
		//	SQLiteQuery.finalize();
		//	theApp.m_SQLite3DB.close();
		//	//OnCancel();
		//	return ;
		//}
		if(!LoginFromNetwork())
		{
			CString strText;
			strText.LoadString(IDS_STR_LOGIN_ERROR);
			CString strCaption;
			strCaption.LoadString(IDS_STR_LOGIN_CAPTION);
			MessageBox(strText, strCaption);
			return ;
		}
		
	}
	else
	{
#ifdef UNICODE
		if(wcscmp(m_csUserPwd.GetBuffer(), SQLiteQuery.fieldValue(_T("uUserPwd"))) != 0)
#else
		if(strcmp(m_csUserPwd.GetBuffer(), SQLiteQuery.fieldValue(_T("uUserPwd"))) != 0)
#endif
		{
			//MessageBox("对不起，密码错误！", "客户端", MB_ICONWARNING);
			if(!LoginFromNetwork())
			{
				CString strText;
				strText.LoadString(IDS_STR_LOGIN_PWD_ERR);
				CString strCaption;
				strCaption.LoadString(IDS_STR_LOGIN_CAPTION);
				MessageBox(strText, strCaption, MB_ICONWARNING);
				SQLiteQuery.finalize();
				theApp.m_SQLite3DB.close();
				return ;
			}
		}

		
		theApp.m_nCurUserID = ::_ttoi(SQLiteQuery.fieldValue(_T("uID")));
		theApp.m_bFirstTime = false;
	}
	theApp.m_csUserName = m_csUserName;
	theApp.m_csUserPwd = m_csUserPwd;
	SQLiteQuery.finalize();
	theApp.m_SQLite3DB.close();
	OnOK();
}

void CUserLoginDialog::OnBnClickedCancel()
{
	// TODO: 在此添加控件通知处理程序代码
	OnCancel();
}

BOOL CUserLoginDialog::OnInitDialog()
{
	BOOL bRet = CSkinDialog::OnInitDialog();

	// TODO:  在此添加额外的初始化
	this->UserDefSkin(false, true, true);
	this->SetBKSkinImage(_T("image\\bk_body.png"), BF_PNG);
	this->SetCaptionImage(_T("image\\bk_body.png"), BF_PNG);
	this->SetSysBtnImage(_T("image\\sys_btn_close.png"), BF_PNG, 3);
	this->SetSysBtnImage(_T("image\\sys_btn_max.png"), BF_PNG, 2);
	this->SetSysBtnImage(_T("image\\sys_btn_min.png"), BF_PNG, 1);
	
	switch(theApp.m_nCurLanIndex)
	{
	case 0:
		{
			m_SysLanCombo.InsertString(0, _T("简体中文"));
			m_SysLanCombo.InsertString(1, _T("繁体中文"));
			m_SysLanCombo.InsertString(2, _T("英语"));
		}
		break;
	case 1:
		{
			m_SysLanCombo.InsertString(0, _T("簡體中文"));
			m_SysLanCombo.InsertString(1, _T("繁體中文"));
			m_SysLanCombo.InsertString(2, _T("英語"));
		}
		break;
	case 2:
		{
			m_SysLanCombo.InsertString(0, _T("Simplified Chinese"));
			m_SysLanCombo.InsertString(1, _T("Traditional Chinese"));
			m_SysLanCombo.InsertString(2, _T("English"));
		}
		break;
	}
	m_SysLanCombo.SetCurSel(theApp.m_nCurLanIndex);
	return bRet;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

BOOL CUserLoginDialog::LoginFromNetwork()
{
	try
	{
		UpdateData();
		m_PostData.Format(_T("user=%s&&password=%s"), m_csUserName, m_csUserPwd);
		CSendData	MySend;

		TCHAR		*pRecvData = NULL;
		DWORD		dwRecvSize = 0;
		DWORD		nRetCode = 0;
		int			SendCode = 1;//1ASSII
		int			RecvCode = 1;//2 UNCODE

		if(theApp.m_ServerName.GetLength() <= 0 || theApp.m_nPort < 0 || m_ObjectName.GetLength() <= 0)
		{
			//MessageBox("网络设备参数错误，请配置正确！");
			CString strText;
			strText.LoadString(IDS_STR_NET_PARAM_ERR);
			CString strCaption;
			strCaption.LoadString(IDS_STR_LOGIN_CAPTION);
			MessageBox(strText, strCaption, MB_ICONWARNING);
			return FALSE;;
		}

		MySend.SetServerParam((LPTSTR)(LPCTSTR)theApp.m_ServerName,(LPTSTR)(LPCTSTR)m_ObjectName, theApp.m_nPort);


		nRetCode = MySend.PostDataMethod2(m_PostData,m_PostData.GetLength(),
			&pRecvData,dwRecvSize,SendCode,RecvCode);
		//返回错误代码列表：
		//100：正常成功
		//101：服务器无法连接
		//102：提交页面无法打开
		//103：数据发送失败
		//104：服务器处理失败
		//500：异常错误
		if (nRetCode == 100)
		{
			
		}
		else if (nRetCode == 101)
		{
			//MessageBox(_T("服务器无法连接"), _T("网络设备列表"));
			CString strText;
			strText.LoadString(IDS_STR_NET_SERVER_DISCONNECT);
			CString strCaption;
			strCaption.LoadString(IDS_STR_NET_DEVICE_LIST);
			MessageBox(strText, strCaption, MB_ICONWARNING);
			return FALSE;
		}
		else if (nRetCode == 102)
		{
			//MessageBox(_T("提交页面无法打开"), _T("网络设备列表"));
			CString strText;
			strText.LoadString(IDS_STR_NET_PAGE_NOT_OPEN);
			CString strCaption;
			strCaption.LoadString(IDS_STR_NET_DEVICE_LIST);
			MessageBox(strText, strCaption, MB_ICONWARNING);
			return FALSE;
		}
		else if (nRetCode == 103)
		{
			//MessageBox(_T("数据发送失败"), _T("网络设备列表"));
			CString strText;
			strText.LoadString(IDS_STR_NET_DATA_SEND_ERR);
			CString strCaption;
			strCaption.LoadString(IDS_STR_NET_DEVICE_LIST);
			MessageBox(strText, strCaption, MB_ICONWARNING);
			return FALSE;
		}
		else if (nRetCode == 104)
		{
			//MessageBox(_T("服务器处理失败"), _T("网络设备列表"));
			CString strText;
			strText.LoadString(IDS_STR_NET_SERVER_HANDLE_ERR);
			CString strCaption;
			strCaption.LoadString(IDS_STR_NET_DEVICE_LIST);
			MessageBox(strText, strCaption, MB_ICONWARNING);
			return FALSE;
		}
		else if (nRetCode == 500)
		{
			//MessageBox(_T("异常错误"), _T("网络设备列表"));
			CString strText;
			strText.LoadString(IDS_STR_NET_EXCEPTION_ERR);
			CString strCaption;
			strCaption.LoadString(IDS_STR_NET_DEVICE_LIST);
			MessageBox(strText, strCaption, MB_ICONWARNING);
			return FALSE;
		}
		else
		{
			//MessageBox(_T("未知错误"), _T("网络设备列表"));
			CString strText;
			strText.LoadString(IDS_STR_NET_UNKNOW_ERR);
			CString strCaption;
			strCaption.LoadString(IDS_STR_NET_DEVICE_LIST);
			MessageBox(strText, strCaption, MB_ICONWARNING);
			return FALSE;
		}

		//因为网页返回的XML数据有问题，临时解决办法
#ifdef UNICODE
		std::wstring strXML = pRecvData;
#else
		std::string strXML = pRecvData;
#endif
		int nEndPos		= strXML.find(_T("</rest>"));
#ifdef UNICODE
		std::wstring strResult = strXML.substr(nEndPos - 1, 1);
#else
		std::string strResult = strXML.substr(nEndPos - 1, 1);
#endif
		if(strResult == _T("1"))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
		//int nPos = strXML.find_last_of('>');
		//char sXML[1024*20] = {0};
		//if(nPos > 0)
		//{
		//	strXML = strXML.substr(0, nPos+1);

		//	CChineseCode::GB2312ToUTF_8(sXML, (char*)strXML.c_str(), strXML.length());
		//}

		//TiXmlDocument* myDocument = new TiXmlDocument();
		//const char *pData = myDocument->Parse((const char*)sXML, NULL, TIXML_ENCODING_UTF8);
		////myDocument->LoadFile("test.xml");
		//TiXmlElement* rootElement = myDocument->FirstChildElement();
		//if(rootElement)
		//{
		//	TiXmlElement *headElement = rootElement->FirstChildElement();
		//	//TiXmlElement *dataElement = headElement->NextSiblingElement();
		//}
	}
	catch(...)
	{

	}
	return TRUE;
}
void CUserLoginDialog::OnCbnSelchangeComboLan()
{
	// TODO: 在此添加控件通知处理程序代码
	CString strTxt;
	strTxt.LoadString(IDS_STR_LOGIN_CHANGE_LAN);
	CString strCaption;
	strCaption.LoadString(IDS_STR_LOGIN_CAPTION);
	if(IDYES == MessageBox(strTxt, strCaption, MB_YESNO))
	{
		theApp.m_bLanChange = TRUE;
		theApp.m_nCurLanIndex = m_SysLanCombo.GetCurSel();
		OnOK();
	}
}
