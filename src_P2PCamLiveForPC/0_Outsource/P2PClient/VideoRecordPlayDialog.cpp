// VideoRecordPlayDialog.cpp : 实现文件
//

#include "stdafx.h"
#include "P2PClient.h"
#include "VideoRecordPlayDialog.h"

VideoRecordPlayDialog			*g_pVideoReocrdPlayDialog = NULL;
// VideoRecordPlayDialog 对话框

IMPLEMENT_DYNAMIC(VideoRecordPlayDialog, CSkinDialog)

VideoRecordPlayDialog::VideoRecordPlayDialog(CWnd* pParent /*=NULL*/)
	: CSkinDialog(VideoRecordPlayDialog::IDD, pParent)
	, m_pRecordFile(NULL)
{

}

VideoRecordPlayDialog::~VideoRecordPlayDialog()
{
}

void VideoRecordPlayDialog::DoDataExchange(CDataExchange* pDX)
{
	CSkinDialog::DoDataExchange(pDX);
	//DDX_Control(pDX, IDC_DEVICE_TREE, m_DeviceTree);
	DDX_Control(pDX, IDC_VIDEO_RECORD_LIST, m_VideoRecordList);
	DDX_Control(pDX, IDC_START_DTP, m_StartDataTimeCtrl);
	DDX_Control(pDX, IDC_END_DTP, m_EndDataTimeCtrl);
	DDX_Control(pDX, IDC_SEARCH_BTN, m_btnSearchRecord);
	DDX_Control(pDX, IDC_STATIC_START_TIME, m_startTimestaitc);
	DDX_Control(pDX, IDC_STATIC_END_TIME, m_endtimestatic);
	DDX_Control(pDX, IDC_RECORD_TREE, m_RecordTree);
}


BEGIN_MESSAGE_MAP(VideoRecordPlayDialog, CSkinDialog)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_SEARCH_BTN, &VideoRecordPlayDialog::OnBnClickedSearchBtn)
	ON_WM_TIMER()
	ON_NOTIFY(HDN_ITEMDBLCLICK, 0, &VideoRecordPlayDialog::OnHdnItemdblclickVideoRecordList)
	ON_NOTIFY(NM_DBLCLK, IDC_VIDEO_RECORD_LIST, &VideoRecordPlayDialog::OnNMDblclkVideoRecordList)
	ON_MESSAGE(WM_USER_UPDATE_DATA, OnUpdateDataMessage)
	ON_NOTIFY(TVN_SELCHANGED, IDC_DEVICE_TREE, &VideoRecordPlayDialog::OnTvnSelchangedDeviceTree)
END_MESSAGE_MAP()


// VideoRecordPlayDialog 消息处理程序

void VideoRecordPlayDialog::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 在此处添加消息处理程序代码
	// 不为绘图消息调用 CSkinDialog::OnPaint()
	if(m_RecordTree.GetSafeHwnd())
	{
		CRect rcClient;
		GetClientRect(&rcClient);
		rcClient.left += 10;
		rcClient.top += 150;
		rcClient.right = 220;
		rcClient.bottom -= 10;
		m_RecordTree.MoveWindow(&rcClient);
	}

	if(m_VideoRecordList.GetSafeHwnd())
	{
		CRect rcClient;
		GetClientRect(&rcClient);
		rcClient.left += 230;
		rcClient.top = rcClient.bottom - 250;
		rcClient.right -= 10;
		rcClient.bottom -= 10;
		m_VideoRecordList.MoveWindow(&rcClient);
	}

	if(m_screenPannel.GetSafeHwnd())
	{
		CRect rcClient;
		GetClientRect(&rcClient);
		rcClient.left += 230;
		//rcClient.top += 10;
		rcClient.right -= 10;
		rcClient.bottom -= 255;
		m_screenPannel.MoveWindow(&rcClient);
	}
}

BOOL VideoRecordPlayDialog::OnInitDialog()
{
	CSkinDialog::OnInitDialog();

	this->UserDefSkin(false, false, false);
	this->SetBKSkinImage(_T("image\\bk_body.png"), BF_PNG);
	this->SetSysBtnImage(_T("image\\sys_btn_close (2).png"), BF_PNG, 3);
	this->SetSysBtnImage(_T("image\\sys_btn_max.png"), BF_PNG, 2);
	this->SetSysBtnImage(_T("image\\sys_btn_min.png"), BF_PNG, 1);
	//this->SetCaptionImage(_T("image\\bk_body.png"), BF_PNG);

	m_image.Create(IDB_TREEIMAGE,16,1,RGB(255,255,255));
	// TODO:  在此添加额外的初始化
	m_screenPannel.Create(
		NULL,
		NULL,
		WS_CHILD|WS_VISIBLE,  
		CRect(0,0,0,0), 
		this, 
		1982);
	RECT rect;
	this->GetClientRect(&rect);	
	//m_bFullScreen=FALSE;
	m_nScreenNum = 4;
	m_screenPannel.SetShowPlayWin(m_nScreenNum, 0, 0);
	rect.left += 230;
	rect.top += 10;
	rect.right -= 10;
	rect.bottom -= 255;
	m_screenPannel.MoveWindow(&rect);
	m_screenPannel.ShowWindow(SW_SHOW);

	DWORD dwStyle = m_VideoRecordList.GetExtendedStyle();
	dwStyle |= LVS_EX_FULLROWSELECT;//选中某行使整行高亮（只适用与report风格的listctrl）
	dwStyle |= LVS_EX_GRIDLINES;//网格线（只适用与report风格的listctrl）
	//dwStyle |= LVS_EX_CHECKBOXES;//item前生成checkbox控件
	m_VideoRecordList.SetExtendedStyle(dwStyle); //设置扩展风格

	if(theApp.m_nCurLanIndex == 0)
	{
		m_VideoRecordList.InsertColumn(0, _T("序号"), LVCFMT_CENTER, 30 );
		m_VideoRecordList.InsertColumn(1, _T("设备名称"), LVCFMT_CENTER, 100 );
		m_VideoRecordList.InsertColumn(2, _T("设备UID"), LVCFMT_CENTER, 180 );
		m_VideoRecordList.InsertColumn(3, _T("开始时间"), LVCFMT_CENTER, 180 );
		m_VideoRecordList.InsertColumn(4, _T("结束时间"), LVCFMT_CENTER, 180 );
		m_VideoRecordList.InsertColumn(5, _T("录像路径"), LVCFMT_CENTER, 300 );
	}
	else if(theApp.m_nCurLanIndex == 0)
	{
		m_VideoRecordList.InsertColumn(0, _T("序號"), LVCFMT_CENTER, 30 );
		m_VideoRecordList.InsertColumn(1, _T("裝置名稱"), LVCFMT_CENTER, 100 );
		m_VideoRecordList.InsertColumn(2, _T("裝置UID"), LVCFMT_CENTER, 180 );
		m_VideoRecordList.InsertColumn(3, _T("開始時間"), LVCFMT_CENTER, 180 );
		m_VideoRecordList.InsertColumn(4, _T("結束時間"), LVCFMT_CENTER, 180 );
		m_VideoRecordList.InsertColumn(5, _T("錄影路徑"), LVCFMT_CENTER, 300 );
	}
	else if(theApp.m_nCurLanIndex == 0)
	{
		m_VideoRecordList.InsertColumn(0, _T("NO."), LVCFMT_CENTER, 30 );
		m_VideoRecordList.InsertColumn(1, _T("DeviceName"), LVCFMT_CENTER, 100 );
		m_VideoRecordList.InsertColumn(2, _T("DeviceUID"), LVCFMT_CENTER, 180 );
		m_VideoRecordList.InsertColumn(3, _T("StartTime"), LVCFMT_CENTER, 180 );
		m_VideoRecordList.InsertColumn(4, _T("EndTime"), LVCFMT_CENTER, 180 );
		m_VideoRecordList.InsertColumn(5, _T("RecordPath"), LVCFMT_CENTER, 300 );
	}

	dwStyle=GetWindowLong(m_RecordTree.m_hWnd ,GWL_STYLE);//获得树的信息
	dwStyle|=TVS_HASBUTTONS|TVS_HASLINES|TVS_LINESATROOT;//设置风格
	::SetWindowLong (m_RecordTree.m_hWnd ,GWL_STYLE,dwStyle);
	m_RecordTree.SetImageList(&m_image,TVSIL_NORMAL );
	if(theApp.m_nCurLanIndex == 0)
		m_hParentItem = m_RecordTree.InsertItem(_T("设备列表"), IMAGE_INDEX_ROOT, IMAGE_INDEX_ROOT,TVI_ROOT);
	else if(theApp.m_nCurLanIndex == 1)
		m_hParentItem = m_RecordTree.InsertItem(_T("裝置串列"), IMAGE_INDEX_ROOT, IMAGE_INDEX_ROOT,TVI_ROOT);
	else if(theApp.m_nCurLanIndex == 2)
		m_hParentItem = m_RecordTree.InsertItem(_T("Device List"), IMAGE_INDEX_ROOT, IMAGE_INDEX_ROOT,TVI_ROOT);
	InitTreeView();
	

	g_pVideoReocrdPlayDialog = this;
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void VideoRecordPlayDialog::OnBnClickedSearchBtn()
{
	// TODO: 在此添加控件通知处理程序代码
	
	HTREEITEM curNode = m_RecordTree.GetSelectedItem();
	if(!curNode)
	{
		CString strText;
		strText.LoadString(IDS_STR_RECORD_SEARCH_CHANNEL);
		CString strCaption;
		strCaption.LoadString(IDS_STR_RECORD_SEARCH_CAPTION);
		MessageBox(strText, strCaption, MB_ICONWARNING);
		return;
	}

	
	m_VideoRecordList.DeleteAllItems();
	theApp.m_SQLite3DB.open(_T("SystemDB"));
	theApp.m_SQLite3DB.execDML(_T("BEGIN TRANSACTION;"));

	 CTime startTime;
	 m_StartDataTimeCtrl.GetTime(startTime);
	 CString strStartTime=startTime.Format(_T("%Y-%m-%d 00:00:00"));

	 CTime endTime;
	 m_EndDataTimeCtrl.GetTime(endTime);
	 CString strEndTime=endTime.Format(_T("%Y-%m-%d 23:59:59"));
	
	TCHAR sQuerySQL[256] = {0};
	if( curNode == m_hParentItem)
	{
		wsprintf(sQuerySQL, _T("select * from RecordInfo where EndTime > '%s' and EndTime < '%s'"), 
			strStartTime.GetBuffer(), strEndTime.GetBuffer());
	}
	else
	{
		TCHAR *sDeviceID = (TCHAR*)m_RecordTree.GetItemData(curNode);
		wsprintf(sQuerySQL, _T("select * from RecordInfo where DeviceID = '%s' and EndTime > '%s' and EndTime < '%s' "), 
			sDeviceID, strStartTime.GetBuffer(), strEndTime.GetBuffer());
	}
	//sprintf(sQuerySQL, "select * from RecordInfo ");

	CppSQLite3Query SQLiteQuery = theApp.m_SQLite3DB.execQuery(sQuerySQL);
	
	int nRow = 0;
	while(!SQLiteQuery.eof())
	{
		int nID = 1;
		TCHAR sID[16] = {0};
		wsprintf(sID, _T("%d"), nRow + 1);
		nRow = m_VideoRecordList.InsertItem(nRow, sID);//插入行
		m_VideoRecordList.SetItemText(nRow, nID++, _T("test1"));
		m_VideoRecordList.SetItemText(nRow, nID++, SQLiteQuery.fieldValue(_T("DeviceID")));
		m_VideoRecordList.SetItemText(nRow, nID++, SQLiteQuery.fieldValue(_T("StartTime")));
		m_VideoRecordList.SetItemText(nRow, nID++, SQLiteQuery.fieldValue(_T("EndTime")));
		m_VideoRecordList.SetItemText(nRow, nID++, SQLiteQuery.fieldValue(_T("RecordPath")));
		SQLiteQuery.nextRow();
	}
	
	theApp.m_SQLite3DB.execDML(_T("COMMIT TRANSACTION;"));
	SQLiteQuery.finalize();
	theApp.m_SQLite3DB.close();
}

void VideoRecordPlayDialog::InitTreeView()
{
	theApp.m_SQLite3DB.open(_T("SystemDB"));
	TCHAR strQuerySQL[256] = {0};
	try
	{
		wsprintf(strQuerySQL, _T("select * from DeviceInfo order by uID asc")/*, theApp.m_nCurUserID*/);
		CppSQLite3Query SQLiteQuery = theApp.m_SQLite3DB.execQuery(strQuerySQL);

		int nListRow = 0;
		while( !SQLiteQuery.eof())
		{
			CString name = SQLiteQuery.fieldValue(_T("DeviceName"));
			HTREEITEM hCurItem = m_RecordTree.InsertItem(SQLiteQuery.fieldValue(_T("DeviceName")), IMAGE_INDEX_NO_PTZ, IMAGE_INDEX_BADPIC, m_hParentItem);

			/*__CAMERA_INFO *pCameraInfo = new __CAMERA_INFO();
			strcpy(pCameraInfo->sDevUID, SQLiteQuery.fieldValue("DeviceID"));
			strcpy(pCameraInfo->sUserName, SQLiteQuery.fieldValue("DeviceUser"));
			strcpy(pCameraInfo->sUserPwd, SQLiteQuery.fieldValue("DevicePwd"));*/
			TCHAR *pDeviceID = new TCHAR[64];
			memset(pDeviceID, 0, 64);
			_tcscpy(pDeviceID, SQLiteQuery.fieldValue(_T("DeviceID")));

			m_RecordTree.SetItemData(hCurItem, (DWORD_PTR)pDeviceID);
			
			SQLiteQuery.nextRow();
		}
		SQLiteQuery.finalize();
	}
	catch(...)
	{
	}
	theApp.m_SQLite3DB.close();

	m_RecordTree.Expand(m_hParentItem, TVE_EXPAND);
}

void VideoRecordPlayDialog::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	//if(nIDEvent == 10001)
	{
		//读取录像文件
		char sPacketInfo[8] = {0};
		int nSize = fread(sPacketInfo, 1, 4, m_pRecordFile);
		int nDataLen = 0;
		memcpy(&nDataLen, sPacketInfo, 4);
		TCHAR *sBuffer = new TCHAR[nDataLen + 1];
		memset(sBuffer, 0, nDataLen + 1);
		nSize = fread(sBuffer, 1, nDataLen, m_pRecordFile);
		if(nSize <= 0)
		{
			KillTimer(nIDEvent);
			return ;
		}
		//播放视频
		CPlayWnd *pWnd = (CPlayWnd*)m_screenPannel.GetPage(nIDEvent);
		if( pWnd)
		{
			pWnd->OnRecordAVFrame(sBuffer, nSize);
		}

		delete []sBuffer;
		sBuffer = NULL;
	}

	CSkinDialog::OnTimer(nIDEvent);
}

void VideoRecordPlayDialog::OnHdnItemdblclickVideoRecordList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMHEADER phdr = reinterpret_cast<LPNMHEADER>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	
	*pResult = 0;
}

void VideoRecordPlayDialog::OnNMDblclkVideoRecordList(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 在此添加控件通知处理程序代码
	
	if(m_pRecordFile)
	{
		fclose(m_pRecordFile);
		m_pRecordFile = NULL;
	}

	CPlayWnd *pWnd = (CPlayWnd*)m_screenPannel.GetPage(m_screenPannel.GetCurScreen());
	if( pWnd)
	{
		SetTimer(m_screenPannel.GetCurScreen(), 100, NULL);
		pWnd->PlayRecord();
	}

	int nRow = m_VideoRecordList.GetSelectionMark();
	CString str = m_VideoRecordList.GetItemText(nRow, 5);
	m_pRecordFile = _tfopen(str.GetBuffer(), _T("ab+"));

	*pResult = 0;
}

LRESULT VideoRecordPlayDialog::OnUpdateDataMessage(WPARAM wParam, LPARAM lParam)
{
	//if(IDYES == MessageBox("马上更新网络设备列表数据？", "更新设备列表", MB_YESNO))
	{
		m_RecordTree.DeleteAllItems();
		//m_hParentItem = m_DeviceTree.InsertItem(_T("设备列表"));
		if(theApp.m_nCurLanIndex == 0)
			m_hParentItem = m_RecordTree.InsertItem(_T("设备列表"), IMAGE_INDEX_ROOT, IMAGE_INDEX_ROOT,TVI_ROOT);
		else if(theApp.m_nCurLanIndex == 1)
			m_hParentItem = m_RecordTree.InsertItem(_T("裝置串列"), IMAGE_INDEX_ROOT, IMAGE_INDEX_ROOT,TVI_ROOT);
		else if(theApp.m_nCurLanIndex == 2)
			m_hParentItem = m_RecordTree.InsertItem(_T("Device List"), IMAGE_INDEX_ROOT, IMAGE_INDEX_ROOT,TVI_ROOT);

		InitTreeView();
	}
	return 0;
}
void VideoRecordPlayDialog::OnTvnSelchangedDeviceTree(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	
	*pResult = 0;
}

BOOL VideoRecordPlayDialog::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 在此添加专用代码和/或调用基类
	if(pMsg->message==WM_KEYDOWN&&pMsg->wParam==VK_RETURN)    
        return TRUE; 
    if(pMsg->message==WM_KEYDOWN&&pMsg->wParam==VK_ESCAPE)    
        return TRUE; 
	return CSkinDialog::PreTranslateMessage(pMsg);
}
