// RemoteWebDialog.cpp : 实现文件
//

#include "stdafx.h"
#include "P2PClient.h"
#include "RemoteWebDialog.h"


// CRemoteWebDialog 对话框

IMPLEMENT_DYNAMIC(CRemoteWebDialog, CSkinDialog)

CRemoteWebDialog::CRemoteWebDialog(CWnd* pParent /*=NULL*/)
	: CSkinDialog(CRemoteWebDialog::IDD, pParent)
{	
}

CRemoteWebDialog::~CRemoteWebDialog()
{
}

void CRemoteWebDialog::DoDataExchange(CDataExchange* pDX)
{
	CSkinDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_WEB_EXPLORER, m_webExplorer);
}


BEGIN_MESSAGE_MAP(CRemoteWebDialog, CSkinDialog)
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CRemoteWebDialog 消息处理程序

BOOL CRemoteWebDialog::OnInitDialog()
{
	CSkinDialog::OnInitDialog();
	this->UserDefSkin(false, false, false);
	// TODO:  在此添加额外的初始化
	try
	{
		CString strURL;
		switch(theApp.m_nCurLanIndex)
		{
		case 0:
			{
				strURL.Format(_T("http://%s:%d/device_manage/device_list.ds?lang=cn"), theApp.m_ServerName, theApp.m_nPort);
			}
			break;
		case 1:
			{
				strURL.Format(_T("http://%s:%d/device_manage/device_list.ds?lang=tra"), theApp.m_ServerName, theApp.m_nPort);
			}
			break;
		case 2:
			{
				strURL.Format(_T("http://%s:%d/device_manage/device_list.ds?lang=en"), theApp.m_ServerName, theApp.m_nPort);
			}
			break;
		}
		m_webExplorer.Navigate(strURL, NULL, NULL, NULL, NULL);
	}
	catch(...)
	{

	}
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CRemoteWebDialog::OnSize(UINT nType, int cx, int cy)
{
	CSkinDialog::OnSize(nType, cx, cy);

	// TODO: 在此处添加消息处理程序代码
	if(m_webExplorer.GetSafeHwnd())
	{
		CRect rcClient;
		//rcClient.right = cx;
		//rcClient.bottom = cy;
		GetClientRect(&rcClient);
		m_webExplorer.MoveWindow(&rcClient);
	}
}
