// SysManagerDialog.cpp : 实现文件
//

#include "stdafx.h"
#include "P2PClient.h"
#include "SysManagerDialog.h"
#include "DeviceInfoDialog.h"
#include "VideoRecordPlayDialog.h"
#include "VideoPlayDialog.h"
#include "EquipmentSearchDialog.h"
#include "AlarmDetectConfigDialog.h"


extern VideoRecordPlayDialog			*g_pVideoReocrdPlayDialog;
extern CVideoPlayDialog					*g_pVideoPlayDialog;

// CSysManagerDialog 对话框

IMPLEMENT_DYNAMIC(CSysManagerDialog, CSkinDialog)

CSysManagerDialog::CSysManagerDialog(CWnd* pParent /*=NULL*/)
	: CSkinDialog(CSysManagerDialog::IDD, pParent)
	, m_csOldUserPwd(_T(""))
	, m_csNewUserPwd1(_T(""))
	, m_csNewUserPwd2(_T(""))
	, m_csSystemMsg(_T(""))
	, m_nCurID(0)
	, m_csNetURLEdit(_T("sztutk.3322.org"))
	, m_nNetPortEdit(81)
	, m_csDeviceID("")
{
	theApp.m_ServerName		= m_csNetURLEdit;
	theApp.m_nPort			= m_nNetPortEdit;
}

CSysManagerDialog::~CSysManagerDialog()
{
}

void CSysManagerDialog::DoDataExchange(CDataExchange* pDX)
{
	CSkinDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_OLD_PWD, m_csOldUserPwd);
	DDX_Text(pDX, IDC_EDIT_NEW_PWD1, m_csNewUserPwd1);
	DDX_Text(pDX, IDC_EDIT_NEW_PWD2, m_csNewUserPwd2);
	DDX_Control(pDX, IDC_LIST_DEIVCE_INFO, m_DeviceListCtrl);
	DDX_Text(pDX, IDC_SYSTEM_MSG, m_csSystemMsg);
	DDX_Text(pDX, IDC_NET_URL_EDIT, m_csNetURLEdit);
	DDX_Text(pDX, IDC_NET_PORT_EDIT, m_nNetPortEdit);
	DDX_Control(pDX, IDC_EDIT_OLD_PWD, m_OldPwdEdit);
	DDX_Control(pDX, IDC_EDIT_NEW_PWD1, m_NewPwdEdit1);
	DDX_Control(pDX, IDC_EDIT_NEW_PWD2, m_NewPwdEdit2);
	DDX_Control(pDX, IDC_UPDATE_BTN, m_btnUpdate);
	DDX_Control(pDX, IDC_VIDEO_RECORD_PLAN_BTN, m_btnVideoRecordPlan);
	DDX_Control(pDX, IDC_STATIC_OLD_PWD, m_pwd1);
	DDX_Control(pDX, IDC_STATIC_NEW_PWD, m_pwd2);
	DDX_Control(pDX, IDC_STATIC_CON_PWD, m_pwd3);
	DDX_Control(pDX, IDC_STATIC_NET_PORT, m_netport);
	DDX_Control(pDX, IDC_STATIC_NET_ADDR, m_netaddr);
	DDX_Control(pDX, IDC_ADD_DEV_BTN, m_btnAddDev);
	DDX_Control(pDX, IDC_MODIFY_DEV_BTN, m_btnMOdifyDev);
	DDX_Control(pDX, IDC_DEL_DEV_BTN, m_btnDelDev);
	DDX_Control(pDX, IDC_SYSTEM_MSG, m_systemmsg);
	DDX_Control(pDX, IDC_NET_SAVE_BTN, m_btnNetSave);
	DDX_Control(pDX, IDC_MOD_DEV_BTN, m_btnModDev);
	DDX_Control(pDX, IDC_STATIC_EQUIPMENT__MSG, m_staticEquipmentMsg);
	DDX_Control(pDX, IDC_SEARCH_DEV_BTN, m_SearchDevBtn);
	DDX_Control(pDX, IDC_PICTURE_CAPTURE_BTN, m_PictureCaptureBtn);
	DDX_Control(pDX, IDC_EMAIL_CONFIG_BTN, m_EmailConfigBtn);
}


BEGIN_MESSAGE_MAP(CSysManagerDialog, CSkinDialog)
	ON_BN_CLICKED(IDC_UPDATE_BTN, &CSysManagerDialog::OnBnClickedUpdateBtn)
	ON_BN_CLICKED(IDC_ADD_DEV_BTN, &CSysManagerDialog::OnBnClickedAddDevBtn)
	ON_BN_CLICKED(IDC_MODIFY_DEV_BTN, &CSysManagerDialog::OnBnClickedModifyDevBtn)
	ON_BN_CLICKED(IDC_DEL_DEV_BTN, &CSysManagerDialog::OnBnClickedDelDevBtn)
	ON_NOTIFY(HDN_ITEMCHANGED, 0, &CSysManagerDialog::OnHdnItemchangedListDeivceInfo)
	ON_NOTIFY(NM_CLICK, IDC_LIST_DEIVCE_INFO, &CSysManagerDialog::OnNMClickListDeivceInfo)
	ON_STN_CLICKED(IDC_SYSTEM_MSG, &CSysManagerDialog::OnStnClickedSystemMsg)
	ON_BN_CLICKED(IDC_VIDEO_RECORD_PLAN_BTN, &CSysManagerDialog::OnBnClickedVideoRecordPlanBtn)
	ON_BN_CLICKED(IDC_NET_SAVE_BTN, &CSysManagerDialog::OnBnClickedNetSaveBtn)
	ON_BN_CLICKED(IDC_MOD_DEV_BTN, &CSysManagerDialog::OnBnClickedModDevBtn)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_SEARCH_DEV_BTN, &CSysManagerDialog::OnBnClickedSearchDevBtn)
	ON_BN_CLICKED(IDC_PICTURE_CAPTURE_BTN, &CSysManagerDialog::OnBnClickedPictureCaptureBtn)
	ON_BN_CLICKED(IDC_EMAIL_CONFIG_BTN, &CSysManagerDialog::OnBnClickedEmailConfigBtn)
END_MESSAGE_MAP()


// CSysManagerDialog 消息处理程序

void CSysManagerDialog::OnBnClickedUpdateBtn()
{
	// TODO: 在此添加控件通知处理程序代码
	try
	{
		//theApp.m_SQLite3DB.open(_T("SystemDB"), _T("p2pavcore"));
		theApp.m_SQLite3DB.open(_T("SystemDB"));

		TCHAR strUpdateSQL[256] = {0};
		wsprintf(strUpdateSQL, _T("select * from UserInfo where uID = %d"), theApp.m_nCurUserID);
		CppSQLite3Query SQLiteQuery = theApp.m_SQLite3DB.execQuery(strUpdateSQL);
		if(!SQLiteQuery.eof())
		{
#ifdef UNICODE
			if(wcscmp(m_csNewUserPwd1.GetBuffer(), m_csNewUserPwd2.GetBuffer()) == 0)
#else
			if(/*strcmp(m_csOldUserPwd.GetBuffer(), SQLiteQuery.fieldValue("")) == 0 
				&& */strcmp(m_csNewUserPwd1.GetBuffer(), m_csNewUserPwd2.GetBuffer()) == 0)
#endif
			{
				wsprintf(strUpdateSQL, _T("update UserInfo set uUserPwd = '%s' where uID = %d"), 
					m_csNewUserPwd1.GetBuffer(), theApp.m_nCurUserID);
				theApp.m_SQLite3DB.execDML(strUpdateSQL);
			}
			else
			{
				//MessageBox("修改密码失败！", "客户端", MB_ICONWARNING);
				CString strText;
				strText.LoadString(IDS_STR_SYS_MODIFY_PWD_ERR);
				CString strCaption;
				strCaption.LoadString(IDS_STR_SYS_CONFIG_CAPTION);
				MessageBox(strText, strCaption, MB_ICONWARNING);
			}
		}
		SQLiteQuery.finalize();
		theApp.m_SQLite3DB.close();
	}
	catch(...)
	{

	}
}

BOOL CSysManagerDialog::OnInitDialog()
{
	CSkinDialog::OnInitDialog();

	this->UserDefSkin(false, false, false);
	m_csSystemMsg.LoadString(IDS_STR_SYS_MESSAGE_INFO);
	m_csSystemMsg += theApp.m_csUserName;
	UpdateData(false);

	DWORD dwStyle = m_DeviceListCtrl.GetExtendedStyle();
	dwStyle |= LVS_EX_FULLROWSELECT;//选中某行使整行高亮（只适用与report风格的listctrl）
	dwStyle |= LVS_EX_GRIDLINES;//网格线（只适用与report风格的listctrl）
	//dwStyle |= LVS_EX_CHECKBOXES;//item前生成checkbox控件
	m_DeviceListCtrl.SetExtendedStyle(dwStyle); //设置扩展风格

	CString strID;
	strID.LoadString(IDS_STR_SYS_DEVICE_LIST_ID);
	m_DeviceListCtrl.InsertColumn(0, strID, LVCFMT_CENTER, 100 );
	CString strDeviceName;
	strDeviceName.LoadString(IDS_STR_SYS_DEIVCE_LIST_NAME);
	m_DeviceListCtrl.InsertColumn(1, strDeviceName, LVCFMT_CENTER, 180 );
	CString strDeviceUserName;
	strDeviceUserName.LoadString(IDS_STR_SYS_DEVICE_LIST_USER_NAME);
	m_DeviceListCtrl.InsertColumn(2, strDeviceUserName, LVCFMT_CENTER, 180 );
	CString strDeviceID;
	strDeviceID.LoadString(IDS_STR_SYS_DEVICE_LIST_DEVICE_ID);
	m_DeviceListCtrl.InsertColumn(3, strDeviceID, LVCFMT_CENTER, 180 );
	CString strDeviceTXT;
	strDeviceTXT.LoadString(IDS_STR_SYS_DEVICE_LIST_DEVICE_TXT);
	m_DeviceListCtrl.InsertColumn(4, strDeviceTXT, LVCFMT_CENTER, 300 );
	//int nRow = m_DeviceListCtrl.InsertItem(0, "测试");//插入行
	//int num = 1;
	//m_DeviceListCtrl.SetItemText(nRow, num++, "测试1");
	//m_DeviceListCtrl.SetItemText(nRow, num++, "测试2");
	//m_DeviceListCtrl.SetItemText(nRow, num++, "测试3");

	// TODO:  在此添加额外的初始化
	InitDeviceInfoList();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CSysManagerDialog::OnBnClickedAddDevBtn()
{
	// TODO: 在此添加控件通知处理程序代码
	CDeviceInfoDialog deviceInfoDlg;
	if( IDOK == deviceInfoDlg.DoModal())
	{
		CString csDeviceName		= deviceInfoDlg.m_csDeviceName;
		CString csDeviceUID			= deviceInfoDlg.m_csDeivceUID;
		CString csDeviceUser		= deviceInfoDlg.m_csDeviceUser;
		CString csDeivcePwd			= deviceInfoDlg.m_csDevicePwd;
		
		try
		{
			//theApp.m_SQLite3DB.open(_T("SystemDB"), _T("p2pavcore"));
			theApp.m_SQLite3DB.open(_T("SystemDB"));

			theApp.m_SQLite3DB.execDML(_T("BEGIN TRANSACTION;"));
			CString csUpdateSQL;
			csUpdateSQL.Format(_T("select * from DeviceInfo where DeviceID = '%s'"), csDeviceUID);
			CppSQLite3Query SQLiteQuery = theApp.m_SQLite3DB.execQuery(csUpdateSQL.GetBuffer());
			if(!SQLiteQuery.eof())
			{
				//MessageBox("已经存在该设备！", "客户端", MB_ICONWARNING);
				CString strText;
				strText.LoadString(IDS_STR_SEARCH_DEVICE_IN_USE);
				CString strCaption;
				strCaption.LoadString(IDS_STR_SYS_CONFIG_CAPTION);
				MessageBox(strText, strCaption, MB_ICONWARNING);
			}
			else
			{
				csUpdateSQL.Format(_T("select * from DeviceInfo order by uID desc"));
				CppSQLite3Query SQLiteQuery2 = theApp.m_SQLite3DB.execQuery(csUpdateSQL.GetBuffer());
				int nID = 1;
				if(!SQLiteQuery2.eof())
				{
					nID = ::_ttoi(SQLiteQuery2.fieldValue(_T("uID")));
					nID++;
				}
				SQLiteQuery2.finalize();
				csUpdateSQL.Format(_T("insert into DeviceInfo(uID, DeviceName, DeviceID, DeviceUser, DevicePwd, UserID) values(%d, '%s', '%s', '%s', '%s', %d)"), 
					nID, csDeviceName, csDeviceUID, csDeviceUser, csDeivcePwd, theApp.m_nCurUserID);

				theApp.m_SQLite3DB.execDML(csUpdateSQL.GetBuffer());
			}
			SQLiteQuery.finalize();
			theApp.m_SQLite3DB.execDML(_T("COMMIT TRANSACTION;"));
			theApp.m_SQLite3DB.close();
		}
		catch(CppSQLite3Exception ce)
		{
			theApp.m_SQLite3DB.close();
		}

		InitDeviceInfoList();

		if(g_pVideoPlayDialog)
		{
			g_pVideoPlayDialog->PostMessage(WM_USER_UPDATE_DATA, 0, 0);
		}

		if(g_pVideoReocrdPlayDialog)
		{
			g_pVideoReocrdPlayDialog->PostMessage(WM_USER_UPDATE_DATA, 0, 0);
		}
	}
}

void CSysManagerDialog::OnBnClickedModifyDevBtn()
{
	// TODO: 在此添加控件通知处理程序代码

	try
	{
		//theApp.m_SQLite3DB.open(_T("SystemDB"), _T("p2pavcore"));
		theApp.m_SQLite3DB.open(_T("SystemDB"));

		CString csUpdateSQL;
		csUpdateSQL.Format(_T("select * from DeviceInfo where uID = %d"), m_nCurID);
		CppSQLite3Query SQLiteQuery = theApp.m_SQLite3DB.execQuery(csUpdateSQL.GetBuffer());
		if(!SQLiteQuery.eof())
		{
			CDeviceInfoDialog deviceInfoDlg;

			deviceInfoDlg.m_csDeviceName	= SQLiteQuery.fieldValue(_T("DeviceName"));
			deviceInfoDlg.m_csDeivceUID		= SQLiteQuery.fieldValue(_T("DeviceID"));
			deviceInfoDlg.m_csDeviceUser	= SQLiteQuery.fieldValue(_T("DeviceUser"));
			deviceInfoDlg.m_csDevicePwd		= SQLiteQuery.fieldValue(_T("DevicePwd"));

			SQLiteQuery.finalize();
			if( IDOK == deviceInfoDlg.DoModal())
			{
				CString csDeviceName		= deviceInfoDlg.m_csDeviceName;
				CString csDeviceUID			= deviceInfoDlg.m_csDeivceUID;
				CString csDeviceUser		= deviceInfoDlg.m_csDeviceUser;
				CString csDeivcePwd			= deviceInfoDlg.m_csDevicePwd;

				csUpdateSQL.Format(_T("update DeviceInfo set DeviceName = '%s', DeviceID = '%s', DeviceUser = '%s', DevicePwd = '%s' where uID = %d"),
					csDeviceName, csDeviceUID, csDeviceUser, csDeivcePwd, m_nCurID);

				theApp.m_SQLite3DB.execDML(csUpdateSQL.GetBuffer());
				int nIndex = m_DeviceListCtrl.GetSelectionMark();
				int iCount = 1;
				m_DeviceListCtrl.SetItemText(nIndex, iCount++, csDeviceName);
				m_DeviceListCtrl.SetItemText(nIndex, iCount++, csDeviceUser);
				m_DeviceListCtrl.SetItemText(nIndex, iCount++, csDeviceUID);
				m_DeviceListCtrl.SetItemText(nIndex, iCount++, csDeivcePwd);
				//InitDeviceInfoList();


				if(g_pVideoPlayDialog)
				{
					g_pVideoPlayDialog->PostMessage(WM_USER_UPDATE_DATA, 0, 0);
				}

				if(g_pVideoReocrdPlayDialog)
				{
					g_pVideoReocrdPlayDialog->PostMessage(WM_USER_UPDATE_DATA, 0, 0);
				}

				m_DeviceListCtrl.SetSelectionMark(nIndex);
			}
		}
		else
		{
			SQLiteQuery.finalize();
			//MessageBox("未找到该设备信息！", "设备配置");	
			CString strText;
			strText.LoadString(IDS_STR_SYS_DEVICE_NOT_IN_USE);
			CString strCaption;
			strCaption.LoadString(IDS_STR_SYS_CONFIG_CAPTION);
			MessageBox(strText, strCaption, MB_ICONWARNING);
		}
		
		theApp.m_SQLite3DB.close();
	}
	catch(...)
	{

	}
}

void CSysManagerDialog::OnBnClickedDelDevBtn()
{
	// TODO: 在此添加控件通知处理程序代码
	CString strText;
	strText.LoadString(IDS_STR_SYS_DELETE_DEVICE);
	CString strCaption;
	strCaption.LoadString(IDS_STR_SYS_CONFIG_CAPTION);
	//MessageBox(strText, strCaption, MB_ICONWARNING);
	if( IDYES == MessageBox(strText, strCaption, MB_YESNO))
	{
		//theApp.m_SQLite3DB.open(_T("SystemDB"), _T("p2pavcore"));
		theApp.m_SQLite3DB.open(_T("SystemDB"));
		CString csDeleteSQL;
		csDeleteSQL.Format(_T("delete from DeviceInfo where uID = %d "),  m_nCurID);
		theApp.m_SQLite3DB.execDML(csDeleteSQL.GetBuffer());
		theApp.m_SQLite3DB.close();

		InitDeviceInfoList();

		if(g_pVideoPlayDialog)
		{
			g_pVideoPlayDialog->PostMessage(WM_USER_UPDATE_DATA, (WPARAM)m_csDeviceID.GetBuffer(), (LPARAM)3);
		}

		if(g_pVideoReocrdPlayDialog)
		{
			g_pVideoReocrdPlayDialog->PostMessage(WM_USER_UPDATE_DATA, (WPARAM)m_csDeviceID.GetBuffer(), (LPARAM)3);
		}
	}
	
}

void CSysManagerDialog::InitDeviceInfoList()
{
	m_DeviceListCtrl.DeleteAllItems();

	//theApp.m_SQLite3DB.open(_T("SystemDB"), _T("p2pavcore"));
	theApp.m_SQLite3DB.open(_T("SystemDB"));
	TCHAR strQuerySQL[256] = {0};
	try
	{
		wsprintf(strQuerySQL, _T("select * from DeviceInfo order by uID desc"));
		CppSQLite3Query SQLiteQuery = theApp.m_SQLite3DB.execQuery(strQuerySQL);

		int nListRow = 0;
		while( !SQLiteQuery.eof())
		{
			nListRow = m_DeviceListCtrl.InsertItem(nListRow, SQLiteQuery.fieldValue(_T("uID")));
			int nRow = 1;
			m_DeviceListCtrl.SetItemText(nListRow, nRow++, SQLiteQuery.fieldValue(_T("DeviceName")));
			m_DeviceListCtrl.SetItemText(nListRow, nRow++, SQLiteQuery.fieldValue(_T("DeviceUser")));
			m_DeviceListCtrl.SetItemText(nListRow, nRow++, SQLiteQuery.fieldValue(_T("DeviceID")));
			m_DeviceListCtrl.SetItemText(nListRow, nRow++, SQLiteQuery.fieldValue(_T("UserID")));
			
			SQLiteQuery.nextRow();
		}
		SQLiteQuery.finalize();
	}
	catch(...)
	{//Create  TABLE UserInfo(uID INTEGER PRIMARY KEY,uUserName TEXT, uUserPwd TEXT)"
		wsprintf(strQuerySQL, _T("Create  TABLE DeviceInfo(uID INTEGER PRIMARY KEY,DeviceName TEXT, DeviceID TEXT, DeviceUser TEXT, DevicePwd TEXT, UserID INTEGER)"));
		theApp.m_SQLite3DB.execDML(strQuerySQL);

		memset(strQuerySQL, 0, 256);
		wsprintf(strQuerySQL, _T("Create  TABLE RecordInfo(uID INTEGER PRIMARY KEY,DeviceID TEXT, RecordTime INTEGER, RecordPath TEXT, StartTime TEXT, EndTime TEXT, UserID INTEGER)"));
		theApp.m_SQLite3DB.execDML(strQuerySQL);
	}
	theApp.m_SQLite3DB.close();
}
void CSysManagerDialog::OnHdnItemchangedListDeivceInfo(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMHEADER phdr = reinterpret_cast<LPNMHEADER>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	*pResult = 0;
}

void CSysManagerDialog::OnNMClickListDeivceInfo(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 在此添加控件通知处理程序代码
	int nRow = m_DeviceListCtrl.GetSelectionMark();
	CString str = m_DeviceListCtrl.GetItemText(nRow, 0);
	m_nCurID = ::_ttoi(str.GetBuffer());
	m_csDeviceID = m_DeviceListCtrl.GetItemText(nRow, 3);
	*pResult = 0;
}

void CSysManagerDialog::OnStnClickedSystemMsg()
{
	// TODO: 在此添加控件通知处理程序代码
	
}

void CSysManagerDialog::OnBnClickedVideoRecordPlanBtn()
{
	// TODO: 在此添加控件通知处理程序代码
	VideoRecordPlanDialog m_VideoRecordPlanDlg;
	int nResult = m_VideoRecordPlanDlg.DoModal();
	if(nResult == IDOK)
	{
		theApp.m_modeList.bStartRec = true;
	}
	else
	{
		theApp.m_modeList.bStartRec = false;
	}
}

void CSysManagerDialog::OnBnClickedNetSaveBtn()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData();
	if(m_csNetURLEdit.Find(_T("http://")) > 0)
	{
		//MessageBox("网络地址格式应该为www.xxx.com或者192.168.0.168");
		CString strText;
		strText.LoadString(IDS_STR_SYS_NET_SERVER_INFO_ERR);
		CString strCaption;
		strCaption.LoadString(IDS_STR_SYS_CONFIG_CAPTION);
		MessageBox(strText, strCaption, MB_ICONWARNING);
		return ;
	}
	theApp.m_ServerName		= m_csNetURLEdit;
	theApp.m_nPort			= m_nNetPortEdit;

	if(g_pVideoPlayDialog)
	{
		g_pVideoPlayDialog->PostMessage(WM_USER_UPDATE_NET_DATA, 0, 0);
	}
}

void CSysManagerDialog::OnBnClickedModDevBtn()
{
	// TODO: 在此添加控件通知处理程序代码
	theApp.m_SQLite3DB.open(_T("SystemDB"));

	CString csUpdateSQL;
	csUpdateSQL.Format(_T("select * from DeviceInfo where uID = %d"), m_nCurID);
	CppSQLite3Query SQLiteQuery = theApp.m_SQLite3DB.execQuery(csUpdateSQL.GetBuffer());
	if(!SQLiteQuery.eof())
	{
		m_btnModDev.EnableWindow(FALSE);
		CEquipmentConfigDialog equipmentDialog;
		equipmentDialog.m_csOldDevPwd = SQLiteQuery.fieldValue(_T("DevicePwd"));
		equipmentDialog.m_sDeviceUID = SQLiteQuery.fieldValue(_T("DeviceID"));
		equipmentDialog.DoModal();
		m_btnModDev.EnableWindow(TRUE);
	}
}

void CSysManagerDialog::OnSize(UINT nType, int cx, int cy)
{
	CSkinDialog::OnSize(nType, cx, cy);

	// TODO: 在此处添加消息处理程序代码
	CRect rcClient(0, 150, cx, cy);
	rcClient.InflateRect(-10, 0, -10, -10);
	if(m_staticEquipmentMsg.GetSafeHwnd())
	{
		m_staticEquipmentMsg.MoveWindow(&rcClient);
	}

	CRect rcClientList(0, 165, cx, cy);
	rcClientList.InflateRect(-15, 0, -15, -55);
	if(m_DeviceListCtrl.GetSafeHwnd())
	{
		m_DeviceListCtrl.MoveWindow(&rcClientList);
	}

	CRect rcAddDev;
	if(m_btnAddDev.GetSafeHwnd())
	{
		
		m_btnAddDev.GetClientRect(&rcAddDev);
		rcAddDev.left = 15;
		rcAddDev.top = cy - 50;
		rcAddDev.bottom = cy - 5;
		m_btnAddDev.MoveWindow(&rcAddDev);
	}

	if(m_btnMOdifyDev.GetSafeHwnd())
	{
		CRect rcModifyDev;
		m_btnMOdifyDev.GetClientRect(&rcModifyDev);
		rcModifyDev.left = 120;
		rcModifyDev.right = rcModifyDev.left + rcAddDev.Width();
		rcModifyDev.top = cy - 50;
		rcModifyDev.bottom = cy - 5;
		m_btnMOdifyDev.MoveWindow(&rcModifyDev);
	}

	if(m_btnDelDev.GetSafeHwnd())
	{
		CRect rcDelDev;
		m_btnDelDev.GetClientRect(&rcDelDev);
		rcDelDev.left = 225;
		rcDelDev.right = rcDelDev.left + rcAddDev.Width();
		rcDelDev.top = cy - 50;
		rcDelDev.bottom = cy - 5;
		m_btnDelDev.MoveWindow(&rcDelDev);
	}

	if(m_btnModDev.GetSafeHwnd())
	{
		CRect rcModDev;
		m_btnModDev.GetClientRect(&rcModDev);
		rcModDev.left =330;
		rcModDev.right = rcModDev.left + rcAddDev.Width();
		rcModDev.top = cy - 50;
		rcModDev.bottom = cy - 5;
		m_btnModDev.MoveWindow(&rcModDev);
	}

	if(m_SearchDevBtn.GetSafeHwnd())
	{
		CRect rcModDev;
		m_SearchDevBtn.GetClientRect(&rcModDev);
		rcModDev.left =435;
		rcModDev.right = rcModDev.left + rcAddDev.Width();
		rcModDev.top = cy - 50;
		rcModDev.bottom = cy - 5;
		m_SearchDevBtn.MoveWindow(&rcModDev);
	}
}

void CSysManagerDialog::OnBnClickedSearchDevBtn()
{
	// TODO: 在此添加控件通知处理程序代码
	CEquipmentSearchDialog searchDlg;
	searchDlg.DoModal();
	if(searchDlg.m_bAddDevice)
	{
		InitDeviceInfoList();

		if(g_pVideoPlayDialog)
		{
			g_pVideoPlayDialog->PostMessage(WM_USER_UPDATE_DATA, (WPARAM)m_csDeviceID.GetBuffer(), (LPARAM)3);
		}

		if(g_pVideoReocrdPlayDialog)
		{
			g_pVideoReocrdPlayDialog->PostMessage(WM_USER_UPDATE_DATA, (WPARAM)m_csDeviceID.GetBuffer(), (LPARAM)3);
		}

		searchDlg.m_bAddDevice = FALSE;
	}
}

LRESULT CSysManagerDialog::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	// TODO: 在此添加专用代码和/或调用基类

	return CSkinDialog::DefWindowProc(message, wParam, lParam);
}

BOOL CSysManagerDialog::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 在此添加专用代码和/或调用基类
	if(pMsg->message==WM_KEYDOWN&&pMsg->wParam==VK_RETURN)    
        return TRUE; 
    if(pMsg->message==WM_KEYDOWN&&pMsg->wParam==VK_ESCAPE)    
        return TRUE; 
	return CSkinDialog::PreTranslateMessage(pMsg);
}

void CSysManagerDialog::OnBnClickedPictureCaptureBtn()
{
	// TODO: 在此添加控件通知处理程序代码
	CString sFolderPath;
	BROWSEINFO bi;
	TCHAR Buffer[MAX_PATH];
	//初始化入口参数bi开始
	bi.hwndOwner = NULL;
	bi.pidlRoot =NULL;//初始化制定的root目录很不容易，
	bi.pszDisplayName = Buffer;//此参数如为NULL则不能显示对话框
	if(theApp.m_nCurLanIndex == 0)
	{
		bi.lpszTitle = _T("修改拍照存储路径");
	}
	else if(theApp.m_nCurLanIndex == 1)
	{
		bi.lpszTitle = _T("修改拍照儲存路徑");
	}
	else if(theApp.m_nCurLanIndex == 2)
	{
		bi.lpszTitle = _T("modify the picture storage path");
	}
	//bi.ulFlags = BIF_BROWSEINCLUDEFILES;//包括文件
	bi.ulFlags = BIF_EDITBOX;//包括文件

	bi.lpfn = NULL;
	bi.iImage=IDR_MAINFRAME;
	//初始化入口参数bi结束
	LPITEMIDLIST pIDList = SHBrowseForFolder(&bi);//调用显示选择对话框
	if(pIDList)
	{
		SHGetPathFromIDList(pIDList, Buffer);
		//取得文件夹路径到Buffer里
		sFolderPath = Buffer;//将路径保存在一个CString对象里
		theApp.m_csPictureCapturePath = sFolderPath;

		DeleteFile(_T("PicturePath"));
		FILE *pFile = NULL;
		pFile = _tfopen(_T("PicturePath"), _T("ab+"));
		if(pFile)
		{
			int nLen = _tcslen(Buffer);
			fwrite(&nLen, sizeof(nLen), 1, pFile);
			fwrite(Buffer, nLen, 1, pFile);
			fclose(pFile);
		}

		UpdateData(FALSE);
	}
	LPMALLOC lpMalloc;
	if(FAILED(SHGetMalloc(&lpMalloc))) return;
	//释放内存
	lpMalloc->Free(pIDList);
	lpMalloc->Release();
}

void CSysManagerDialog::OnBnClickedEmailConfigBtn()
{
	// TODO: 在此添加控件通知处理程序代码
	AlarmDetectConfigDialog alarmConfigDlg;
	if(IDOK == alarmConfigDlg.DoModal())
	{
		//to do 
		theApp.m_csEmailAddrTo = alarmConfigDlg.m_csEmailAddrTo;
		theApp.m_csEmailCaption = alarmConfigDlg.m_csEmailCaption;
		theApp.m_csEmailContent = alarmConfigDlg.m_csEmailContent;
		theApp.m_csEmailFromAddr = alarmConfigDlg.m_csEmailFromAddr;
		theApp.m_csEmailUserName = alarmConfigDlg.m_csEmailUserName;
		theApp.m_csEmailUserPwd = alarmConfigDlg.m_csEmailUserPwd;
	}
}
