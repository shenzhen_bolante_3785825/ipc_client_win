#ifndef NVS_MODEL_PLAN_UI_H
#define NVS_MODEL_PLAN_UI_H
//#include "NvsNvrData.h"

//缺省为存储服务器的，重定义为客户端的
#if defined NVS_CS_MAX_SIZE
#undef NVS_CS_MAX_SIZE
#endif

#define NVS_CS_MAX_SIZE     32

//绘制计划表时用的
#define MINIDOWNROWLINE     7
#define MINIDOWNCOLLINE     48

#define DTNECOLREC          11 //录象计划小矩形纵向间距
#define DTNEROWREC          26 //录象计划小矩形横向间距
#define MAXNUMMODEL         256
#define NAME_LEN            32  //模板名长度

//模板
typedef struct {
	int			nIndex;
    int         recID;			//设备ID
	TCHAR		recUID[64];		//设备唯一ID
    BOOL        recPlan[MINIDOWNROWLINE][MINIDOWNCOLLINE];//记录那些天需要记录
}ModelInfo;

//模板列表
typedef struct {
    short       totalNum;
	bool		bStartRec;
    ModelInfo   recModelInfo[MAXNUMMODEL];
}ModelInfoList;

//绘制计划表时用的
void DrawModle(CDC *pDC, CRect grouprec, ModelInfo * pModeInfo);
void DrawTableRect(CDC *pDC, CRect grouprec);
void DrawTableField(CDC *pDC,  CPoint point, CRect grouprec, ModelInfo * pModelInfo, BOOL bFill=TRUE);

#endif