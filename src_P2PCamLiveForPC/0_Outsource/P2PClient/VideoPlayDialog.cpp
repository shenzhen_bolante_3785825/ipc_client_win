// VideoPlayDialog.cpp : 实现文件
//

#include "stdafx.h"
#include "P2PClient.h"
#include "VideoPlayDialog.h"
#include "NvsModelPlanUI.h"
#include "wave_out.h"
#include <time.h>
#include "SendData.h"
#include "ChineseCode.h"
#include "tinyxml.h"

#include "CSmtp.h"

//extern ModelInfoList	g_VideoRecordModeList;//模板列表
CVideoPlayDialog			*g_pVideoPlayDialog = NULL;
// CVideoPlayDialog 对话框

IMPLEMENT_DYNAMIC(CVideoPlayDialog, CSkinDialog)

CVideoPlayDialog::CVideoPlayDialog(CWnd* pParent /*=NULL*/)
	: CSkinDialog(CVideoPlayDialog::IDD, pParent)
	, m_bThreadFlag(FALSE)
	, m_ObjectName(_T("/interface/login_mb.ds"))
	, m_PostData(_T("user=001&&password=123"))
	, m_nDragWinIndex(-1)
	, m_bDeviceTreeDrag(TRUE)
	, m_pDragImage(NULL)
	, m_bInitNetDevices(FALSE)
{
	m_PostData.Format(_T("user=%s&&password=%s"), theApp.m_csUserName, theApp.m_csUserPwd);
}

CVideoPlayDialog::~CVideoPlayDialog()
{
	int i = 0;
}

void CVideoPlayDialog::DoDataExchange(CDataExchange* pDX)
{
	CSkinDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_DEVICE_TREE, m_deviceTree);
	DDX_Control(pDX, IDC_TREELIST_TAB, m_TreeListTab);
	DDX_Control(pDX, IDC_NETDEVICE_TREE, m_netDeviceTree);
	DDX_Control(pDX, IDC_LEFT_CONTROL_BTN, m_leftControlBtn);
	DDX_Control(pDX, IDC_RIGTH_CONTROL_BTN, m_rigthControlBtn);
	DDX_Control(pDX, IDC_TOP_CONTROL_BTN, m_topControlBtn);
	DDX_Control(pDX, IDC_BOTTOM_CONTROL__BTN, m_bottomControlBtn);
	DDX_Control(pDX, IDC_SCREEN1_BTN, m_screen1btn);
	DDX_Control(pDX, IDC_SCREEN4_BTN, m_screen4btn);
	DDX_Control(pDX, IDC_SCREEN9_BTN, m_screen9btn);
	DDX_Control(pDX, IDC_SCREEN6_BTN, m_screen6btn);
	DDX_Control(pDX, IDC_SCREEN8_BTN, m_screen8btn);
	DDX_Control(pDX, IDC_SCREEN16_BTN, m_screen16btn);
	DDX_Control(pDX, IDC_SCREEN25_BTN, m_screen25btn);
	DDX_Control(pDX, IDC_SCREEN36_BTN, m_screen36btn);
	DDX_Control(pDX, IDC_STATIC_SPLIT_SCREEN, m_splitscreen);
	DDX_Control(pDX, IDC_UPDATE_NET_BTN, m_updateNetBtn);
}


BEGIN_MESSAGE_MAP(CVideoPlayDialog, CSkinDialog)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_SCREEN6_BTN, &CVideoPlayDialog::OnBnClickedScreen6Btn)
	ON_BN_CLICKED(IDC_SCREEN9_BTN, &CVideoPlayDialog::OnBnClickedScreen9Btn)
	ON_BN_CLICKED(IDC_SCREEN4_BTN, &CVideoPlayDialog::OnBnClickedScreen4Btn)
	ON_BN_CLICKED(IDC_SCREEN1_BTN, &CVideoPlayDialog::OnBnClickedScreen1Btn)
	ON_BN_CLICKED(IDC_SCREEN8_BTN, &CVideoPlayDialog::OnBnClickedScreen8Btn)
	ON_BN_CLICKED(IDC_SCREEN16_BTN, &CVideoPlayDialog::OnBnClickedScreen16Btn)
	ON_BN_CLICKED(IDC_SCREEN25_BTN, &CVideoPlayDialog::OnBnClickedScreen25Btn)
	ON_BN_CLICKED(IDC_SCREEN36_BTN, &CVideoPlayDialog::OnBnClickedScreen36Btn)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TREELIST_TAB, &CVideoPlayDialog::OnTcnSelchangeTreelistTab)
	//ON_NOTIFY(NM_DBLCLK, IDC_DEVICE_TREE, &CVideoPlayDialog::OnNMDblclkDeviceTree)
	ON_WM_ACTIVATE()
	ON_MESSAGE(WM_USER_UPDATE_DATA, OnUpdateDataMessage)
	ON_MESSAGE(WM_USER_UPDATE_NET_DATA, OnUpdateNetDataMessage)
	//ON_NOTIFY(NM_DBLCLK, IDC_NETDEVICE_TREE, &CVideoPlayDialog::OnNMDblclkNetdeviceTree)
	ON_NOTIFY(TVN_BEGINDRAG, IDC_DEVICE_TREE, &CVideoPlayDialog::OnTvnBegindragDeviceTree)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_WM_TIMER()
	ON_NOTIFY(TVN_BEGINRDRAG, IDC_NETDEVICE_TREE, &CVideoPlayDialog::OnTvnBeginrdragNetdeviceTree)
	ON_NOTIFY(TVN_BEGINDRAG, IDC_NETDEVICE_TREE, &CVideoPlayDialog::OnTvnBegindragNetdeviceTree)
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CVideoPlayDialog 消息处理程序

BOOL CVideoPlayDialog::OnInitDialog()
{
	CSkinDialog::OnInitDialog();

	//AfxSetResourceHandle(theApp.m_hResourceHandle);

	// TODO:  在此添加额外的初始化
	this->UserDefSkin(false, false, false);

	m_image.Create(IDB_TREEIMAGE,16,1,RGB(255,255,255));
	//this->SetBKSkinImage("image\\bk_body.png", BF_PNG);
	/*this->SetSysBtnImage("image\\sys_btn_close (2).png", BF_PNG, 3);
	this->SetSysBtnImage("image\\sys_btn_max.png", BF_PNG, 2);
	this->SetSysBtnImage("image\\sys_btn_min.png", BF_PNG, 1);*/

	m_screenPannel.Create(
		NULL,
		NULL,
		WS_CHILD|WS_VISIBLE,  
		CRect(0,0,0,0), 
		this, 
		1981);
	RECT rect;
	this->GetClientRect(&rect);	
	m_bFullScreen=FALSE;
	m_nScreenNum = 4;
	m_screenPannel.SetShowPlayWin(m_nScreenNum, 0, 0);
	m_screenPannel.MoveWindow(&rect);
	m_screenPannel.ShowWindow(SW_SHOW);

	m_hWndParent = GetParent();

	DWORD dwStyle=GetWindowLong(m_deviceTree.m_hWnd ,GWL_STYLE);//获得树的信息
	dwStyle|=TVS_HASBUTTONS|TVS_HASLINES|TVS_LINESATROOT;//设置风格
	::SetWindowLong (m_deviceTree.m_hWnd ,GWL_STYLE,dwStyle);
	m_deviceTree.SetImageList(&m_image,TVSIL_NORMAL );
	CString strDeviceRoot;
	strDeviceRoot.LoadString(IDS_STR_REAL_DEVICE_ROOT);
	m_hParentItem = m_deviceTree.InsertItem(strDeviceRoot, IMAGE_INDEX_ROOT,IMAGE_INDEX_ROOT,TVI_ROOT);
	InitTreeView();
	//m_devicelist.SetItemImage(curNode, IMAGE_INDEX_PREVIEW, IMAGE_INDEX_PREVIEW);
	/*m_deviceTree.InsertItem(_T("测试2"));
	m_deviceTree.InsertItem(_T("测试3"));
	m_deviceTree.InsertItem(_T("测试4"));
	m_deviceTree.InsertItem(_T("测试5"));
	m_deviceTree.InsertItem(_T("测试6"));*/


	dwStyle=GetWindowLong(m_netDeviceTree.m_hWnd ,GWL_STYLE);//获得树的信息
	dwStyle|=TVS_HASBUTTONS|TVS_HASLINES|TVS_LINESATROOT;//设置风格
	::SetWindowLong (m_netDeviceTree.m_hWnd ,GWL_STYLE,dwStyle);
	m_netDeviceTree.SetImageList(&m_image,TVSIL_NORMAL );
	CString strNetDeviceRoot;
	strNetDeviceRoot.LoadString(IDS_STR_REAL_NET_DEVICE_LIST);
	m_hNetParentItem = m_netDeviceTree.InsertItem(strNetDeviceRoot, IMAGE_INDEX_ROOT,IMAGE_INDEX_ROOT,TVI_ROOT);
	m_netDeviceTree.ShowWindow(SW_HIDE);
	//InitNetTreeView();
	
	CString strDeviceList;
	strDeviceList.LoadString(IDS_STR_REAL_DEVICE_LIST_TAB);
	m_TreeListTab.InsertItem(0, strDeviceList);
	CString strNetDeviceList;
	strNetDeviceList.LoadString(IDS_STR_REAL_NET_DEVICE_LIST_TAB);
	m_TreeListTab.InsertItem(1, strNetDeviceList);
	CSize cs(200, 27);
	m_TreeListTab.SetItemSize(cs);
	m_TreeListTab.SetCurSel(0);

	Set_WIN_Params(INVALID_FILEDESC, 8000.0f, 16, 1);

	m_screen1btn.SetButtonImage(_T("image\\split1Btn.png"), BF_PNG, true);
	m_screen4btn.SetButtonImage(_T("image\\split4Btn.png"), BF_PNG, true);
	m_screen6btn.SetButtonImage(_T("image\\split6Btn.png"), BF_PNG, true);
	m_screen8btn.SetButtonImage(_T("image\\split8Btn.png"), BF_PNG, true);
	m_screen9btn.SetButtonImage(_T("image\\split9Btn.png"), BF_PNG, true);
	m_screen16btn.SetButtonImage(_T("image\\split16Btn.png"), BF_PNG, true);
	m_screen25btn.SetButtonImage(_T("image\\split25Btn.png"), BF_PNG, true);
	m_screen36btn.SetButtonImage(_T("image\\split36Btn.png"), BF_PNG, true);

	ConnectAllDevice(0);
	m_bThreadFlag = TRUE;
	CreateThread(NULL, 0, ThreadConnectDev, this, 0, NULL);

	g_pVideoPlayDialog = this;

	//SetTimer(10000, 1000, NULL);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CVideoPlayDialog::ExitThread()
{
	m_bThreadFlag = FALSE;
}

void CVideoPlayDialog::OnSize(UINT nType, int cx, int cy)
{
	CSkinDialog::OnSize(nType, cx, cy);

	// TODO: 在此处添加消息处理程序代码
	if (m_screenPannel.GetSafeHwnd())
	{
		RECT rect;
		rect.left = 250;
		rect.top = 0;
		rect.bottom = cy;
		rect.right = cx;	
		m_screenPannel.MoveWindow(&rect);
	}

	if ( m_TreeListTab.GetSafeHwnd())
	{
		RECT rect;
		rect.left = 3;
		rect.top = 225;
		rect.bottom = cy - 10;
		rect.right = 244;
		m_TreeListTab.MoveWindow(&rect);


		rect.top += 30;
		if(m_deviceTree.GetSafeHwnd()/* && m_deviceTree.IsWindowVisible()*/)
		{
			m_deviceTree.MoveWindow(&rect);
		}

		if(m_netDeviceTree/* && m_netDeviceTree.IsWindowVisible()*/)
		{
			rect.bottom -= 50;
			m_netDeviceTree.MoveWindow(&rect);
			RECT rc;
			rc.left = rect.right - 80;
			rc.top = rect.bottom + 10;
			rc.right = rect.right - 10;
			rc.bottom = rect.bottom + 60;
			m_updateNetBtn.MoveWindow(&rc);
		}
	}
}

BOOL CVideoPlayDialog::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 在此添加专用代码和/或调用基类
	if(pMsg->message==WM_KEYDOWN&&pMsg->wParam==VK_RETURN)    
        return TRUE; 
    if(pMsg->message==WM_KEYDOWN&&pMsg->wParam==VK_ESCAPE)    
	{
		m_screenPannel.SetFullScreen(false);
        return TRUE; 
	}
	return CSkinDialog::PreTranslateMessage(pMsg);
}

void CVideoPlayDialog::InitTreeView()
{
	theApp.m_SQLite3DB.open(_T("SystemDB"));
	TCHAR strQuerySQL[256] = {0};
	try
	{
		wsprintf(strQuerySQL, _T("select * from DeviceInfo order by uID asc")/*, theApp.m_nCurUserID*/);
		CppSQLite3Query SQLiteQuery = theApp.m_SQLite3DB.execQuery(strQuerySQL);

		int nListRow = 0;
		while( !SQLiteQuery.eof())
		{
			CString name = SQLiteQuery.fieldValue(_T("DeviceName"));
			HTREEITEM hCurItem = m_deviceTree.InsertItem(SQLiteQuery.fieldValue(_T("DeviceName")), IMAGE_INDEX_NOPIC, IMAGE_INDEX_NOPIC, m_hParentItem);

			__CAMERA_INFO *pCameraInfo = new __CAMERA_INFO();
#ifndef UNICODE
			strcpy(pCameraInfo->sDevUID, SQLiteQuery.fieldValue(_T("DeviceID")));
			strcpy(pCameraInfo->sUserName, SQLiteQuery.fieldValue(_T("DeviceUser")));
			strcpy(pCameraInfo->sUserPwd, SQLiteQuery.fieldValue(_T("DevicePwd")));
#else
			wcscpy(pCameraInfo->sDevUID, SQLiteQuery.fieldValue(_T("DeviceID")));
			wcscpy(pCameraInfo->sUserName, SQLiteQuery.fieldValue(_T("DeviceUser")));
			wcscpy(pCameraInfo->sUserPwd, SQLiteQuery.fieldValue(_T("DevicePwd")));
#endif
			pCameraInfo->hCurItem = hCurItem;

			m_deviceTree.SetItemData(hCurItem, (DWORD_PTR)pCameraInfo);

			m_curDeviceItemMap.insert(make_pair(SQLiteQuery.fieldValue(_T("DeviceID")), hCurItem));
			
			SQLiteQuery.nextRow();
		}
		SQLiteQuery.finalize();
	}
	catch(...)
	{
	}
	theApp.m_SQLite3DB.close();

	m_deviceTree.Expand(m_hParentItem, TVE_EXPAND);
}

void CVideoPlayDialog::InitNetTreeView()
{
	try
	{
		m_PostData.Format(_T("user=%s&&password=%s"), theApp.m_csUserName, theApp.m_csUserPwd);
		CSendData	MySend;

		TCHAR		*pRecvData = NULL;
		DWORD		dwRecvSize = 0;
		DWORD		nRetCode = 0;
		int			SendCode = 1;//1ASSII
		int			RecvCode = 1;//2 UNCODE

		if(theApp.m_ServerName.GetLength() <= 0 || theApp.m_nPort < 0 || m_ObjectName.GetLength() <= 0)
		{
			CString strText;
			strText.LoadString(IDS_STR_REAL_NET_PARAM_ERR);
			CString strCaption;
			strCaption.LoadString(IDS_STR_REAL_VIDEO_CAPTION);
			MessageBox(strText, strCaption, MB_ICONWARNING);
			//MessageBox("网络设备参数错误，请配置正确！");
			return ;
		}

		MySend.SetServerParam((LPTSTR)(LPCTSTR)theApp.m_ServerName,(LPTSTR)(LPCTSTR)m_ObjectName, theApp.m_nPort);


		nRetCode = MySend.PostDataMethod2(m_PostData,m_PostData.GetLength(),
			&pRecvData,dwRecvSize,SendCode,RecvCode);
		//返回错误代码列表：
		//100：正常成功
		//101：服务器无法连接
		//102：提交页面无法打开
		//103：数据发送失败
		//104：服务器处理失败
		//500：异常错误
		if (nRetCode == 100)
		{
			m_ObjectName = _T("/interface/deviceinfo_mb.ds");
			MySend.SetServerParam((LPTSTR)(LPCTSTR)theApp.m_ServerName,(LPTSTR)(LPCTSTR)m_ObjectName, theApp.m_nPort);

			nRetCode = MySend.PostDataMethod2(m_PostData,m_PostData.GetLength(),
				&pRecvData,dwRecvSize,SendCode,RecvCode);
		}
		else if (nRetCode == 101)
		{
			m_netDeviceTree.DeleteAllItems();
			CString strText;
			strText.LoadString(IDS_STR_NET_SERVER_DISCONNECT);
			m_hNetParentItem = m_netDeviceTree.InsertItem(strText);
			//m_hNetParentItem = m_netDeviceTree.InsertItem(_T("网络设备列表(服务器无法连接)"));
			//MessageBox(_T("服务器无法连接"), _T("网络设备列表"));
			return ;
		}
		else if (nRetCode == 102)
		{
			m_netDeviceTree.DeleteAllItems();
			CString strText;
			strText.LoadString(IDS_STR_NET_PAGE_NOT_OPEN);
			m_hNetParentItem = m_netDeviceTree.InsertItem(strText);
			//m_hNetParentItem = m_netDeviceTree.InsertItem(_T("网络设备列表(提交页面无法打开)"));
			//MessageBox(_T("提交页面无法打开"), _T("网络设备列表"));
			return ;
		}
		else if (nRetCode == 103)
		{
			m_netDeviceTree.DeleteAllItems();
			CString strText;
			strText.LoadString(IDS_STR_NET_DATA_SEND_ERR);
			m_hNetParentItem = m_netDeviceTree.InsertItem(strText);
			//MessageBox(_T("数据发送失败"), _T("网络设备列表"));
			return ;
		}
		else if (nRetCode == 104)
		{
			m_netDeviceTree.DeleteAllItems();
			CString strText;
			strText.LoadString(IDS_STR_NET_SERVER_HANDLE_ERR);
			m_hNetParentItem = m_netDeviceTree.InsertItem(strText);
			//m_hNetParentItem = m_netDeviceTree.InsertItem(_T("网络设备列表(服务器处理失败)"));
			//MessageBox(_T("服务器处理失败"), _T("网络设备列表"));
			return ;
		}
		else if (nRetCode == 500)
		{
			m_netDeviceTree.DeleteAllItems();
			CString strText;
			strText.LoadString(IDS_STR_NET_EXCEPTION_ERR);
			m_hNetParentItem = m_netDeviceTree.InsertItem(strText);
			//m_hNetParentItem = m_netDeviceTree.InsertItem(_T("网络设备列表(异常错误)"));
			//MessageBox(_T("异常错误"), _T("网络设备列表"));
			return ;
		}
		else
		{
			m_netDeviceTree.DeleteAllItems();
			CString strText;
			strText.LoadString(IDS_STR_NET_UNKNOW_ERR);
			m_hNetParentItem = m_netDeviceTree.InsertItem(strText);
			//m_hNetParentItem = m_netDeviceTree.InsertItem(_T("网络设备列表(未知错误)"));
			//MessageBox(_T("未知错误"), _T("网络设备列表"));
			return ;
		}

		if(dwRecvSize <= 0 || pRecvData == NULL)
		{
			m_netDeviceTree.DeleteAllItems();
			CString strText;
			strText.LoadString(IDS_STR_NET_DATA_SEND_ERR);
			m_hNetParentItem = m_netDeviceTree.InsertItem(strText);
			//m_hNetParentItem = m_netDeviceTree.InsertItem(_T("网络设备列表(接收数据出错)"));
			//MessageBox(_T("接收数据出错"), _T("网络设备列表"));
			return ;
		}
#ifdef UNICODE
		std::wstring strXML = pRecvData;
#else
		std::string strXML = pRecvData;
#endif
		int nPos = strXML.find_last_of('>');
		TCHAR sXML[1024*20] = {0};
		if(nPos > 0)
		{
			strXML = strXML.substr(0, nPos+1);
#ifndef UNICODE
			CChineseCode::GB2312ToUTF_8(sXML, (char*)strXML.c_str(), strXML.length());
#else
			wcscpy(sXML, strXML.c_str());
#endif
		}

		TiXmlDocument* myDocument = new TiXmlDocument();
		const char *pData = myDocument->Parse((const char*)sXML, NULL, TIXML_ENCODING_UTF8);
		//myDocument->LoadFile("test.xml");
		TiXmlElement* rootElement = myDocument->FirstChildElement();
		if(rootElement)
		{
			TiXmlElement *headElement = rootElement->FirstChildElement();
			TiXmlElement *dataElement = headElement->NextSiblingElement();
			for( TiXmlNode*  item = rootElement->FirstChild( "head");  
				item;  
				item = item->NextSibling("data"))
			{  
				for( TiXmlNode*  itemRaw = item->FirstChild( "row");  
					itemRaw;  
					itemRaw = itemRaw->NextSibling("row" ))
				{ 
					TiXmlNode*  itemNameColumn = itemRaw->FirstChild("column");
					if(itemNameColumn)
					{
						TiXmlNode*  itemIDColumn = itemNameColumn->NextSibling("column");
						TiXmlNode*  itemPwdColumn = itemIDColumn->NextSibling("column");
						if(itemIDColumn && itemPwdColumn)
						{
							HTREEITEM hCurItem = NULL;
							__CAMERA_INFO *pCameraInfo = new __CAMERA_INFO();
#ifdef UNICODE
							wcscpy(pCameraInfo->sUserName, _T("admin"));
#else
							strcpy(pCameraInfo->sUserName, _T("admin"));
#endif

							const char* name = itemNameColumn->ToElement()->GetText();  
							if (name) {  
#ifdef UNICODE
								TCHAR sName[1024] = {0};
								CChineseCode::Gb2312ToUnicode(sName, (char*)name);
								hCurItem = m_netDeviceTree.InsertItem(sName, IMAGE_INDEX_NO_PTZ, IMAGE_INDEX_BADPIC, m_hNetParentItem);
#else
								hCurItem = m_netDeviceTree.InsertItem(name, IMAGE_INDEX_NO_PTZ, IMAGE_INDEX_BADPIC, m_hNetParentItem);
#endif
							}
							else
							{
								continue;
							}

							const char* id = itemIDColumn->ToElement()->GetText(); 
							TCHAR sID[1024] = {0};
							if (id) {  
#ifdef UNICODE	
								CChineseCode::Gb2312ToUnicode(sID, (char*)id);
								wcscpy(pCameraInfo->sDevUID, sID);
#else
								strcpy(pCameraInfo->sDevUID, id);
								strcpy(sID, id);
#endif
							}
							else
							{
								continue;
							}

							const char* pwd = itemPwdColumn->ToElement()->GetText();  
							if (pwd) {  
#ifdef UNICODE	
								TCHAR sPwd[1024] = {0};
								CChineseCode::Gb2312ToUnicode(sPwd, (char*)pwd);
								wcscpy(pCameraInfo->sUserPwd, sPwd);
#else
								strcpy(pCameraInfo->sUserPwd, pwd);
#endif
							}
							else
							{
								continue;
							}

							if(hCurItem)
								m_netDeviceTree.SetItemData(hCurItem, (DWORD_PTR)pCameraInfo);


							//连接设备
							m_curNetDeviceMap.insert(make_pair(sID, pCameraInfo));
						}
					}
				}
			}
		}
	}
	catch(...)
	{

	}

	m_netDeviceTree.Expand(m_hNetParentItem, TVE_EXPAND);
}

void CVideoPlayDialog::ConnectAllDevice(int nInitFlag)
{
	theApp.m_SQLite3DB.open(_T("SystemDB"));
	TCHAR strQuerySQL[256] = {0};

	int iCount = 0;
	//网络设备
#ifdef UNICODE
	map<wstring, __CAMERA_INFO*>::iterator itNetDev = m_curNetDeviceMap.begin();
#else
	map<string, __CAMERA_INFO*>::iterator itNetDev = m_curNetDeviceMap.begin();
#endif
	while( itNetDev != m_curNetDeviceMap.end() && (nInitFlag == 1 || nInitFlag == 0))
	{
#ifndef UNICODE
		map<string, __PLAY_INFO>::iterator itPlayInfo = m_curDeviceMap.find(itNetDev->second->sDevUID);
#else
		map<wstring, __PLAY_INFO>::iterator itPlayInfo = m_curDeviceMap.find(itNetDev->second->sDevUID);
#endif
		if( itPlayInfo != m_curDeviceMap.end())
		{
			++itNetDev;
			continue;
		}

		wsprintf(strQuerySQL, _T("select * from DeviceInfo where DeviceID = '%s'"), itNetDev->second->sDevUID);
		CppSQLite3Query SQLiteQuery = theApp.m_SQLite3DB.execQuery(strQuerySQL);
		if(SQLiteQuery.eof())//如果该网络设备存在于本地列表中，即跳过
		{
			TCHAR *pUserName = _T("admin");
#ifdef UNICODE
			char sUserName[64] = {0};
			char sUserPwd[64] = {0};
			char sDevUID[64] = {0};

			wcstombs(sDevUID, (TCHAR*)itNetDev->second->sDevUID, 64);
			wcstombs(sUserName, (TCHAR*)pUserName, 64);
			wcstombs(sUserPwd, (TCHAR*)itNetDev->second->sUserPwd, 64);
			tkPVOID objPlayStream = apiCreateObj(iCount, (tkCHAR*)sDevUID, (tkCHAR*)sUserName, (tkCHAR*)sUserPwd);
#else
			tkPVOID objPlayStream = apiCreateObj(iCount, (tkCHAR*)itNetDev->second->sDevUID, (tkCHAR*)pUserName, (tkCHAR*)itNetDev->second->sUserPwd);
#endif
			__PLAY_INFO playInfo;
			::memset(&playInfo, 0, sizeof(playInfo));
			playInfo.nConnected = -1;
			playInfo.objPlayStream = objPlayStream;
			playInfo.bPlaying = FALSE;
			playInfo.bRecording = FALSE;
			RealStreamManager *pRealStreamMgr = new RealStreamManager();
			playInfo.fRealStreamMgr = pRealStreamMgr;
			playInfo.hCurItem = itNetDev->second->hCurItem;
			_tcscpy(playInfo.sDeviceID, itNetDev->second->sDevUID);

			m_curDeviceMap.insert(make_pair(itNetDev->second->sDevUID, playInfo));
			if(objPlayStream != NULL) {
				apiRegCB_OnStatus(objPlayStream, OnStatus, this);
				apiRegCB_OnRecvIOCtrl(objPlayStream, OnRecvIOCtrl, this);
			}
			m_objPlayStream = objPlayStream;
			apiConnect(objPlayStream);
			iCount++;
		}
		SQLiteQuery.finalize();
		
		++itNetDev;
	}
	if(iCount == 0)
	{
		iCount = m_curNetDeviceMap.size();
	}
	
	try
	{
		wsprintf(strQuerySQL, _T("select * from DeviceInfo order by uID desc")/*, theApp.m_nCurUserID*/);
		CppSQLite3Query SQLiteQuery = theApp.m_SQLite3DB.execQuery(strQuerySQL);

		int nListRow = 0;
		while( !SQLiteQuery.eof()  && (nInitFlag == 2 || nInitFlag == 0))
		{
#ifndef UNICODE
			map<string, __PLAY_INFO>::iterator itPlayInfo = m_curDeviceMap.find(SQLiteQuery.fieldValue(_T("DeviceID")));
#else
			map<wstring, __PLAY_INFO>::iterator itPlayInfo = m_curDeviceMap.find(SQLiteQuery.fieldValue(_T("DeviceID")));
#endif
			if( itPlayInfo != m_curDeviceMap.end())
			{
				SQLiteQuery.nextRow();
				continue;
			}
			
#ifdef UNICODE
			wstring strUID = SQLiteQuery.fieldValue(_T("DeviceID"));

			char sUserName[64] = {0};
			char sUserPwd[64] = {0};
			char sDevUID[64] = {0};

			wcstombs(sDevUID, (TCHAR*)SQLiteQuery.fieldValue(_T("DeviceID")), 64);
			wcstombs(sUserName, (TCHAR*)SQLiteQuery.fieldValue(_T("DeviceUser")), 64);
			wcstombs(sUserPwd, (TCHAR*)SQLiteQuery.fieldValue(_T("DevicePwd")), 64);
			tkPVOID objPlayStream = apiCreateObj(iCount, (tkCHAR*)sDevUID, (tkCHAR*)sUserName, (tkCHAR*)sUserPwd);
#else
			string strUID = SQLiteQuery.fieldValue(_T("DeviceID"));
			tkPVOID objPlayStream = apiCreateObj(iCount, (tkCHAR*)SQLiteQuery.fieldValue(_T("DeviceID")), (tkCHAR*)SQLiteQuery.fieldValue(_T("DeviceUser")), (tkCHAR*)SQLiteQuery.fieldValue(_T("DevicePwd")));
			JJTrace(0, _T("**************connect to device : %s---%s----%s=====%d"), strUID.c_str(), SQLiteQuery.fieldValue(_T("DeviceUser")), SQLiteQuery.fieldValue(_T("DevicePwd")), objPlayStream);
#endif
			__PLAY_INFO playInfo;
			playInfo.nConnected = -1;
			playInfo.objPlayStream = objPlayStream;
			playInfo.bPlaying = FALSE;
			playInfo.bRecording = FALSE;
			RealStreamManager *pRealStreamMgr = new RealStreamManager();
			playInfo.fRealStreamMgr = pRealStreamMgr;
			_tcscpy(playInfo.sDeviceID, strUID.c_str());

			m_curDeviceMap.insert(make_pair(strUID, playInfo));
			if(objPlayStream != NULL) {
				apiRegCB_OnStatus(objPlayStream, OnStatus, this);
#ifndef UNICODE
				JJTrace(0, _T("**********************apiRegCB_OnStatus(objPlayStream, OnStatus, this);-----%d"), objPlayStream);
#endif
			}
			m_objPlayStream = objPlayStream;
			apiConnect(objPlayStream);
			//m_objPlayStream = NULL;

			iCount++;
			SQLiteQuery.nextRow();
		}
		SQLiteQuery.finalize();
	}
	catch(...)
	{
	}
	theApp.m_SQLite3DB.close();
}

void CVideoPlayDialog::SetConnectStatus(tkPVOID objPlayStream, int nConnected, BOOL bRecord, const TCHAR *pErrorCode)
{
#ifdef UNICODE
	wstring sDevID;
	map<wstring, __PLAY_INFO>::iterator itPlayInfo = m_curDeviceMap.begin();
#else
	string	sDevID;
	map<string, __PLAY_INFO>::iterator itPlayInfo = m_curDeviceMap.begin();
#endif
	
	while(itPlayInfo != m_curDeviceMap.end())
	{
		if(itPlayInfo->second.objPlayStream == objPlayStream)
		{
			itPlayInfo->second.nConnected = nConnected;
			if(pErrorCode)
			{
#ifdef UNICODE
				wcscpy_s(itPlayInfo->second.sError, 255, pErrorCode);
#else
				strcpy_s(itPlayInfo->second.sError, 255, pErrorCode);
#endif
			}
			sDevID = itPlayInfo->second.sDeviceID;
			break;
		}
		++itPlayInfo;
	}

#ifndef UNICODE
	map<string, HTREEITEM>::iterator itTree = m_curDeviceItemMap.find(sDevID);
#else
	map<wstring, HTREEITEM>::iterator itTree = m_curDeviceItemMap.find(sDevID);
#endif
	if(itTree != m_curDeviceItemMap.end())
	{
		if(nConnected == SINFO_CONNECTED)
		{
			if( bRecord )
			{
				m_deviceTree.SetItemImage(itTree->second, IMAGE_REC_STATUS, IMAGE_REC_STATUS);
			}
			else
			{

				m_deviceTree.SetItemImage(itTree->second, IMAGE_INDEX_NO_PTZ, IMAGE_INDEX_BADPIC);
			}
		}
		else
		{
			m_deviceTree.SetItemImage(itTree->second, IMAGE_INDEX_NOPIC, IMAGE_INDEX_NOPIC);
		}
		
		
		
	}
}

LRESULT CVideoPlayDialog::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	// TODO: 在此添加专用代码和/或调用基类
	CVideoPlayDialog *pContainer = (CVideoPlayDialog*)m_hWndParent;
	CPlayWnd *pWnd = NULL;
	switch (message)
	{
	case VIDEO_MENU_CLOSESTREAM:
		{
			TCHAR *sDevUID = (TCHAR*)wParam;
#ifdef UNICODE
			map<wstring, __PLAY_INFO>::iterator itPlayInfo =  m_curDeviceMap.find(sDevUID);
#else
			map<string, __PLAY_INFO>::iterator itPlayInfo =  m_curDeviceMap.find(sDevUID);
#endif
			if(itPlayInfo != m_curDeviceMap.end())
			{
				itPlayInfo->second.bPlaying = FALSE;
				itPlayInfo->second.fRealStreamMgr->StopStream();
			}

			
			
			break;
		}
	case VIDEO_MENU_START_TALK:
		{
			m_curScreen=m_screenPannel.GetCurScreen();
			pWnd=m_screenPannel.GetPlayWnd(m_curScreen);
			
			break;
		}
	case VIDEO_MENU_STOP_TALK:
		{
			m_curScreen=m_screenPannel.GetCurScreen();
			pWnd=m_screenPannel.GetPlayWnd(m_curScreen);
			
			break;
		}
	case VIDEO_MENU_FULLSCREEN:
		{
			m_bFullScreen = !m_screenPannel.GetFullScreen(); 
			m_screenPannel.SetFullScreen(m_bFullScreen);;
			break;
		}
	case MSG_CHANGEPLAYWND:
		{
			SplitInfoNode siNode;
			int nMsg=0;
			m_curScreen=m_screenPannel.GetCurScreen();
			pWnd=m_screenPannel.GetPlayWnd(m_curScreen);
			pWnd->GetSplitInfo(&siNode);
			if(siNode.Type == SPLIT_TYPE_MONITOR)
			{
				nMsg=1;
			}
			m_objPlayStream = (tkPVOID)wParam;
			m_leftControlBtn.SetPlayStreamObject(m_objPlayStream, AVIOCTRL_PTZ_LEFT);
			m_rigthControlBtn.SetPlayStreamObject(m_objPlayStream, AVIOCTRL_PTZ_RIGHT);
			m_topControlBtn.SetPlayStreamObject(m_objPlayStream, AVIOCTRL_PTZ_UP);
			m_bottomControlBtn.SetPlayStreamObject(m_objPlayStream, AVIOCTRL_PTZ_DOWN);
			//pContainer->SendMsg(MSG_CHANGEPLAYWND,nMsg,pWnd->m_nPlayPort,m_curScreen);
			break;		
		}
	case MSG_MULTISCREEN:
		{
			m_curScreen=m_screenPannel.GetCurScreen();
			pWnd=m_screenPannel.GetPlayWnd(m_curScreen);
			BOOL bMulti = m_screenPannel.GetMultiScreen();
			m_screenPannel.SetMultiScreen(!bMulti);
			m_screenPannel.SetActivePage(pWnd);
			Invalidate();
			break;
		}
	}

	return CSkinDialog::DefWindowProc(message, wParam, lParam);
}

void CVideoPlayDialog::FullScreen()
{
	//::SetFocus(GetSafeHwnd());
	//if(!m_bFullScreen)
	//{	          
	//	//SetParent(NULL);
	//	ShowWindow(SW_HIDE);
	//	this->ShowWindow(SW_SHOWMAXIMIZED);  
	//	int cx = ::GetSystemMetrics(SM_CXSCREEN);   
	//	int cy = ::GetSystemMetrics(SM_CYSCREEN);     
	//	MoveWindow(0, 0, cx, cy, TRUE); 
	//	SetWindowPos(&wndTopMost,0,0,cx,cy,SWP_NOZORDER);  		
	//}
	//else
	//{ 
	//	//SetParent(m_hWndParent);     
	//	ShowWindow(SW_SHOW); 
	//} 
	// 


	//m_screenPannel.SetFullScreen(m_bFullScreen);
	//m_bFullScreen = !m_bFullScreen; 
}

void CVideoPlayDialog::OnBnClickedScreen6Btn()
{
	// TODO: 在此添加控件通知处理程序代码
	m_screenPannel.m_splitCol = 3;
	m_screenPannel.m_splitRow = 3;
	m_screenPannel.SetShowPlayWin(6, 0, true);
}

void CVideoPlayDialog::OnBnClickedScreen9Btn()
{
	// TODO: 在此添加控件通知处理程序代码
	m_screenPannel.m_splitCol = 3;
	m_screenPannel.m_splitRow = 3;
	m_screenPannel.SetShowPlayWin(9, 0, 0);
}

void CVideoPlayDialog::OnBnClickedScreen4Btn()
{
	// TODO: 在此添加控件通知处理程序代码
	m_screenPannel.m_splitCol = 2;
	m_screenPannel.m_splitRow = 2;
	m_screenPannel.SetShowPlayWin(4, 0, 0);
}

void CVideoPlayDialog::OnBnClickedScreen1Btn()
{
	// TODO: 在此添加控件通知处理程序代码
	m_screenPannel.m_splitCol = 1;
	m_screenPannel.m_splitRow = 1;
	m_screenPannel.SetShowPlayWin(1, 0, 0);
}

void CVideoPlayDialog::OnBnClickedScreen8Btn()
{
	// TODO: 在此添加控件通知处理程序代码
	m_screenPannel.m_splitCol = 4;
	m_screenPannel.m_splitRow = 4;
	m_screenPannel.SetShowPlayWin(8, 0, true);
}

void CVideoPlayDialog::OnBnClickedScreen16Btn()
{
	// TODO: 在此添加控件通知处理程序代码
	m_screenPannel.m_splitCol = 4;
	m_screenPannel.m_splitRow = 4;
	m_screenPannel.SetShowPlayWin(16, 0, 0);
}

void CVideoPlayDialog::OnBnClickedScreen25Btn()
{
	// TODO: 在此添加控件通知处理程序代码
	m_screenPannel.m_splitCol = 5;
	m_screenPannel.m_splitRow = 5;
	m_screenPannel.SetShowPlayWin(25, 0, 0);
}

void CVideoPlayDialog::OnBnClickedScreen36Btn()
{
	// TODO: 在此添加控件通知处理程序代码
	m_screenPannel.m_splitCol = 6;
	m_screenPannel.m_splitRow = 6;
	m_screenPannel.SetShowPlayWin(36, 0, 0);
}

void CVideoPlayDialog::OnTcnSelchangeTreelistTab(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 在此添加控件通知处理程序代码
	int nCurSel = m_TreeListTab.GetCurSel();
	switch(nCurSel)
	{
	case 0:
		m_deviceTree.ShowWindow(SW_SHOW);
		m_netDeviceTree.ShowWindow(SW_HIDE);
		m_updateNetBtn.ShowWindow(SW_HIDE);
		break;
	case 1:
		m_deviceTree.ShowWindow(SW_HIDE);
		m_netDeviceTree.ShowWindow(SW_SHOW);
		m_updateNetBtn.ShowWindow(SW_SHOW);
		break;
	}
	//m_deviceTree.Invalidate(false);
	*pResult = 0;
}

void CVideoPlayDialog::OnNMDblclkDeviceTree(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 在此添加控件通知处理程序代码
	HTREEITEM curNode = m_deviceTree.GetSelectedItem();
	if(!curNode)
	{
		return;
	}

	__CAMERA_INFO *pInfo = (__CAMERA_INFO *) m_deviceTree.GetItemData(curNode);
	if(pInfo)
	{
#ifdef UNICODE
		map<wstring, __PLAY_INFO>::iterator itPlayInfo =  m_curDeviceMap.find(pInfo->sDevUID);
#else
		map<string, __PLAY_INFO>::iterator itPlayInfo =  m_curDeviceMap.find(pInfo->sDevUID);
#endif
		if(itPlayInfo != m_curDeviceMap.end())
		{
			

			CPlayWnd *pWnd = NULL;
			if(m_nDragWinIndex >= 0)
			{
				pWnd = (CPlayWnd*)m_screenPannel.GetPage(m_nDragWinIndex);
				m_nDragWinIndex = -1;
			}
			else
			{
				pWnd = (CPlayWnd*)m_screenPannel.GetPage(m_screenPannel.GetCurScreen());
			}
			
			//出错时处理
			if( itPlayInfo->second.nConnected != SINFO_CONNECTED && pWnd)
			{
				pWnd->m_csErrorCode = itPlayInfo->second.sError;
				pWnd->Invalidate();
				return ;
			}
			if( pWnd && pWnd->IsPlaying())
			{
				pWnd->StopStream();
				itPlayInfo->second.fRealStreamMgr->StopStream();
				itPlayInfo->second.bPlaying = FALSE;
			}

			if(itPlayInfo->second.bPlaying == FALSE)
			{
				pWnd->PlayStream(itPlayInfo->second.objPlayStream, pInfo->sDevUID);
				itPlayInfo->second.fRealStreamMgr->PlayStream(itPlayInfo->second.objPlayStream, pWnd->OnAVFrame, pWnd);
				itPlayInfo->second.bPlaying = TRUE;

				m_leftControlBtn.SetPlayStreamObject(itPlayInfo->second.objPlayStream, AVIOCTRL_PTZ_LEFT);
				m_rigthControlBtn.SetPlayStreamObject(itPlayInfo->second.objPlayStream, AVIOCTRL_PTZ_RIGHT);
				m_topControlBtn.SetPlayStreamObject(itPlayInfo->second.objPlayStream, AVIOCTRL_PTZ_UP);
				m_bottomControlBtn.SetPlayStreamObject(itPlayInfo->second.objPlayStream, AVIOCTRL_PTZ_DOWN);
			}
			else if(pWnd)
			{
				pWnd->StopStream();
				itPlayInfo->second.fRealStreamMgr->StopStream();
				itPlayInfo->second.bPlaying = FALSE;
			}
		}
	}
	*pResult = 0;
}

tkVOID WINAPI CVideoPlayDialog::OnStatus(tkINT32 nType, tkINT32 nValue, tkPVOID pObj, tkUINT32 nTag, tkVOID *param)
{
	CVideoPlayDialog *pThis=(CVideoPlayDialog *)param;
#ifndef UNICODE
	JJTrace(0, _T("*************************OnStatus, cam%d, nType=%d, nValue=%d, param=0x%X\n"), 
			  nTag, nType, nValue, param);
#endif
	switch(nValue)
	{
		case SINFO_CONNECTING:
			{
				CString strTxt;
				strTxt.LoadString(IDS_STR_STATUS_DEV_CONNECTING);
				pThis->SetConnectStatus(pObj, nValue, FALSE, strTxt.GetBuffer());
			}
			break;

		case SINFO_CONNECTED:
			pThis->SetConnectStatus(pObj, nValue);
			break;

		case SINFO_DISCONNECTED:
			{
				CString strTxt;
				strTxt.LoadString(IDS_STR_STATUS_DEV_DISCONNECT);
				pThis->SetConnectStatus(pObj, nValue, FALSE, strTxt.GetBuffer());
			}
			break;
			//非法UID
		case IOTC_ER_UNLICENSE:
			{
				CString strTxt;
				strTxt.LoadString(IDS_STR_STATUS_DEV_UID_ERR);
				pThis->SetConnectStatus(pObj, nValue, FALSE, strTxt);
			}
			break;
			//设备密码错误
		case AV_ER_WRONG_VIEWACCorPWD:
			{
				CString strTxt;
				strTxt.LoadString(IDS_STR_STATUS_DEV_PWD_ERR);
				pThis->SetConnectStatus(pObj, nValue, FALSE, strTxt.GetBuffer());
			}
			break;
			//设备不在线
		case IOTC_ER_CAN_NOT_FIND_DEVICE:
			{
				CString strTxt;
				strTxt.LoadString(IDS_STR_STATUS_DEV_NOT_ON_LINE);
				pThis->SetConnectStatus(pObj, nValue, FALSE, strTxt.GetBuffer());
			}
			break;
		default:
			break;
	}
}

tkVOID WINAPI CVideoPlayDialog::OnRecvIOCtrl(tkINT32 nIOType, tkCHAR *pIOData, tkINT32 nIODataSize, tkPVOID pObj, tkUINT32 nTag, tkVOID *param)
{
	CVideoPlayDialog *pThis=(CVideoPlayDialog *)param;
	if(pThis == NULL)
		return ;
	switch(nIOType)
	{
	case IOTYPE_USER_IPCAM_EVENT_REPORT:
		{
			pThis->SendAlarmEmail();
		}
		break;
	}
	return ;
}

void CVideoPlayDialog::SendAlarmEmail()
{
	try
	{
		CSmtp mail;

		mail.SetSMTPServer(theApp.m_csEmailFromAddr.GetBuffer(), 25);
		mail.SetLogin(_T("***"));
		mail.SetPassword(_T("***"));
		mail.SetSenderName(theApp.m_csEmailUserName.GetBuffer());
  		mail.SetSenderMail(theApp.m_csEmailFromAddr.GetBuffer());
  		mail.SetReplyTo(theApp.m_csEmailFromAddr.GetBuffer());
		mail.SetSubject(theApp.m_csEmailCaption.GetBuffer());
		mail.AddRecipient(theApp.m_csEmailAddrTo.GetBuffer());
  		mail.SetXPriority(XPRIORITY_NORMAL);
  		mail.SetXMailer(_T("The Bat! (v3.02) Professional"));
		mail.AddMsgLine(theApp.m_csEmailContent.GetBuffer());
	/*	mail.AddMsgLine("");
		mail.AddMsgLine("...");
		mail.AddMsgLine("How are you today?");
		mail.AddMsgLine("");
		mail.AddMsgLine("Regards");
		mail.ModMsgLine(5,"regards");
		mail.DelMsgLine(2);
		mail.AddMsgLine("User");*/
		
  		//mail.AddAttachment("../test1.jpg");
  		//mail.AddAttachment("c:\\test2.exe");
		//mail.AddAttachment("c:\\test3.txt");
		mail.Send();
	}
	catch(ECSmtp e)
	{

	}

}

DWORD WINAPI CVideoPlayDialog::ThreadConnectDev(LPVOID lpPara)
{
	CVideoPlayDialog *pDlg = (CVideoPlayDialog*)lpPara;
	
	while(pDlg->m_bThreadFlag)
	{
		if(!pDlg->m_bInitNetDevices)
		{
			pDlg->InitNetTreeView();
			pDlg->ConnectAllDevice(1);
			pDlg->m_bInitNetDevices = TRUE;
		}

		Sleep(5000);
		//printf("ThreadConnectDev is running!\n");

		//connect and play video

		/*map<string, __PLAY_INFO>::iterator itPlayInfo =  pDlg->m_curDeviceMap.begin();
		while(itPlayInfo != pDlg->m_curDeviceMap.end())
		{
			if( itPlayInfo->second.bConnected == FALSE)
			{
				apiConnect(itPlayInfo->second.objPlayStream);
				++itPlayInfo;
				continue;
			}
			CPlayWnd *pWnd = (CPlayWnd*)pDlg->m_screenPannel.GetPage(pDlg->m_screenPannel.GetCurScreen());
			if( pWnd && itPlayInfo->second.bPlaying == FALSE)
			{
				pWnd->PlayStream(itPlayInfo->second.objPlayStream, (char*)itPlayInfo->first.c_str());
				itPlayInfo->second.fRealStreamMgr->PlayStream(itPlayInfo->second.objPlayStream, pWnd->OnAVFrame, pWnd);
			}
			itPlayInfo->second.bPlaying = TRUE;
			++itPlayInfo;
		}*/

		for (int i = 0; i < theApp.m_modeList.totalNum ; i++)
		{
			ModelInfo *pModelInfo = &theApp.m_modeList.recModelInfo[i];

			if(theApp.m_modeList.bStartRec)
			{
				time_t curTime = ::time(NULL);
				tm *tt = localtime(&curTime);
				int tHour = 0;
				if(tt->tm_min < 30)
					tHour = tt->tm_hour*2;
				else
					tHour = tt->tm_hour * 2 + 1;
				if(pModelInfo->recPlan[tt->tm_wday][tHour] == true)
				{
					//if need record
#ifndef UNICODE
					map<string, __PLAY_INFO>::iterator itPlayInfo =  pDlg->m_curDeviceMap.find(pModelInfo->recUID);
#else
					map<wstring, __PLAY_INFO>::iterator itPlayInfo =  pDlg->m_curDeviceMap.find(pModelInfo->recUID);
#endif
					if(itPlayInfo != pDlg->m_curDeviceMap.end())
					{
						pDlg->SetConnectStatus(itPlayInfo->second.objPlayStream, itPlayInfo->second.nConnected, TRUE);
						itPlayInfo->second.fRealStreamMgr->SetRecIndex(pModelInfo->nIndex, NULL, pModelInfo->recUID);
						//if device is connected and not start play, request to play stream
						if(itPlayInfo->second.bRecording == FALSE && itPlayInfo->second.nConnected == SINFO_CONNECTED)
						{
							//CPlayWnd *pWnd = (CPlayWnd*)pDlg->m_screenPannel.GetPage(pDlg->m_screenPannel.GetCurScreen());
							//if( pWnd && itPlayInfo->second.bPlaying == FALSE)
							{
								itPlayInfo->second.bRecording = TRUE;
								//pWnd->PlayStream(itPlayInfo->second.objPlayStream, (TCHAR*)itPlayInfo->first.c_str());
								itPlayInfo->second.fRealStreamMgr->PlayStream(itPlayInfo->second.objPlayStream, NULL, NULL);
							}

						}
						/*else if( itPlayInfo->second.bConnected == FALSE)
						{
						apiConnect(itPlayInfo->second.objPlayStream);
						}*/
					}
				}
				else
				{
#ifndef UNICODE
					map<string, __PLAY_INFO>::iterator itPlayInfo =  pDlg->m_curDeviceMap.find(pModelInfo->recUID);
#else
					map<wstring, __PLAY_INFO>::iterator itPlayInfo =  pDlg->m_curDeviceMap.find(pModelInfo->recUID);
#endif
					if(itPlayInfo != pDlg->m_curDeviceMap.end())
					{
						itPlayInfo->second.bRecording = FALSE;
					}
				}
			}
			else
			{
#ifndef UNICODE
				map<string, __PLAY_INFO>::iterator itPlayInfo =  pDlg->m_curDeviceMap.find(pModelInfo->recUID);
#else
				map<wstring, __PLAY_INFO>::iterator itPlayInfo =  pDlg->m_curDeviceMap.find(pModelInfo->recUID);
#endif
				if(itPlayInfo != pDlg->m_curDeviceMap.end())
				{
					itPlayInfo->second.bRecording = FALSE;
				}
			}
		}//end for

		//这里是重连设备
#ifndef UNICODE
		map<string, __PLAY_INFO>::iterator itPlayInfo =  pDlg->m_curDeviceMap.begin();
#else
		map<wstring, __PLAY_INFO>::iterator itPlayInfo =  pDlg->m_curDeviceMap.begin();
#endif
		while(itPlayInfo != pDlg->m_curDeviceMap.end())
		{
			if( (itPlayInfo->second.nConnected != SINFO_CONNECTED) && (itPlayInfo->second.nConnected != SINFO_CONNECTING))
			{
				apiConnect(itPlayInfo->second.objPlayStream);
			}
			itPlayInfo++;
		}
	}
	return 0;
}

void CVideoPlayDialog::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
	CSkinDialog::OnActivate(nState, pWndOther, bMinimized);

	// TODO: 在此处添加消息处理程序代码
}

LRESULT CVideoPlayDialog::OnUpdateDataMessage(WPARAM wParam, LPARAM lParam)
{
	CString strText;
	strText.LoadString(IDS_STR_REAL_UPDATE_DEV_LIST);
	CString strCaption;
	strCaption.LoadString(IDS_STR_REAL_VIDEO_CAPTION);
	//MessageBox(strText, strCaption, MB_ICONWARNING);
	if(IDYES == MessageBox(strText, strCaption,  MB_YESNO))
	{
		m_deviceTree.DeleteAllItems();
		CString strDeviceRoot;
		strDeviceRoot.LoadString(IDS_STR_REAL_DEVICE_ROOT);
		m_hParentItem = m_deviceTree.InsertItem(strDeviceRoot);
		TCHAR* pDeviceID = (TCHAR*)wParam;
		int nID = (int)lParam;
		if(nID == 3)
		{
#ifdef UNICODE
			map<wstring, __PLAY_INFO>::iterator itPlayInfo =  m_curDeviceMap.find(pDeviceID);
#else
			map<string, __PLAY_INFO>::iterator itPlayInfo =  m_curDeviceMap.find(pDeviceID);
#endif
			if(itPlayInfo != m_curDeviceMap.end())
			{
				apiStopVideo(itPlayInfo->second.objPlayStream);
				apiStopAudio(itPlayInfo->second.objPlayStream);
				apiStopSpeak(itPlayInfo->second.objPlayStream);
				apiDisconnect(itPlayInfo->second.objPlayStream, 1);
				//apiReleaseObj((tkHandle*)itPlayInfo->second.objPlayStream);//这里会出错，先去掉
				m_curDeviceMap.erase(itPlayInfo);
			}
		}
		InitTreeView();
		ConnectAllDevice(2);
		strText.LoadString(IDS_STR_REAL_UPDATE_OK);
		MessageBox(strText, strCaption);
	}
	return 0;
}

LRESULT CVideoPlayDialog::OnUpdateNetDataMessage(WPARAM wParam, LPARAM lParam)
{
	CString strText;
	strText.LoadString(IDS_STR_REAL_UPDATE_NET_DEV_LIST);
	CString strCaption;
	strCaption.LoadString(IDS_STR_REAL_VIDEO_CAPTION);
	if(IDYES == MessageBox(strText, strCaption, MB_YESNO))
	{
		m_netDeviceTree.DeleteAllItems();
		CString strNetDeviceRoot;
		strNetDeviceRoot.LoadString(IDS_STR_REAL_NET_DEVICE_LIST);
		m_hNetParentItem = m_netDeviceTree.InsertItem(strNetDeviceRoot);
		InitNetTreeView();
		ConnectAllDevice(1);
		//MessageBox("更新完成！", "更新设备列表");
		strText.LoadString(IDS_STR_REAL_UPDATE_OK);
		MessageBox(strText, strCaption);
	}
	return 0;
}
void CVideoPlayDialog::OnNMDblclkNetdeviceTree(NMHDR *pNMHDR, LRESULT *pResult)
{
	HTREEITEM curNode = m_netDeviceTree.GetSelectedItem();
	if(!curNode)
	{
		return;
	}

	__CAMERA_INFO *pInfo = (__CAMERA_INFO *) m_netDeviceTree.GetItemData(curNode);
	if(pInfo)
	{
#ifndef UNICODE
		map<string, __PLAY_INFO>::iterator itPlayInfo =  m_curDeviceMap.find(pInfo->sDevUID);
#else
		map<wstring, __PLAY_INFO>::iterator itPlayInfo =  m_curDeviceMap.find(pInfo->sDevUID);
#endif
		if(itPlayInfo != m_curDeviceMap.end())
		{
			CPlayWnd *pWnd = NULL;
			if(m_nDragWinIndex >= 0)
			{
				pWnd = (CPlayWnd*)m_screenPannel.GetPage(m_nDragWinIndex);
				m_nDragWinIndex = -1;
			}
			else
			{
				pWnd = (CPlayWnd*)m_screenPannel.GetPage(m_screenPannel.GetCurScreen());
			}
			
			//出错时处理
			if( itPlayInfo->second.nConnected != SINFO_CONNECTED && pWnd)
			{
				pWnd->m_csErrorCode = itPlayInfo->second.sError;
				pWnd->Invalidate();
				return ;
			}
			//CPlayWnd *pWnd = (CPlayWnd*)m_screenPannel.GetPage(m_screenPannel.GetCurScreen());
			/*if( pWnd && itPlayInfo->second.bPlaying == FALSE)
			{
				pWnd->PlayStream(itPlayInfo->second.objPlayStream, pInfo->sDevUID);
				itPlayInfo->second.fRealStreamMgr->PlayStream(itPlayInfo->second.objPlayStream, pWnd->OnAVFrame, pWnd);
				itPlayInfo->second.bPlaying = TRUE;

				m_leftControlBtn.SetPlayStreamObject(itPlayInfo->second.objPlayStream, AVIOCTRL_PTZ_LEFT);
				m_rigthControlBtn.SetPlayStreamObject(itPlayInfo->second.objPlayStream, AVIOCTRL_PTZ_RIGHT);
				m_topControlBtn.SetPlayStreamObject(itPlayInfo->second.objPlayStream, AVIOCTRL_PTZ_UP);
				m_bottomControlBtn.SetPlayStreamObject(itPlayInfo->second.objPlayStream, AVIOCTRL_PTZ_DOWN);
			}
			else if(pWnd)
			{
				pWnd->StopStream();
				itPlayInfo->second.bPlaying = FALSE;
			}*/
			if(itPlayInfo->second.bPlaying == FALSE)
			{
				pWnd->PlayStream(itPlayInfo->second.objPlayStream, pInfo->sDevUID);
				itPlayInfo->second.fRealStreamMgr->PlayStream(itPlayInfo->second.objPlayStream, pWnd->OnAVFrame, pWnd);
				itPlayInfo->second.bPlaying = TRUE;

				m_leftControlBtn.SetPlayStreamObject(itPlayInfo->second.objPlayStream, AVIOCTRL_PTZ_LEFT);
				m_rigthControlBtn.SetPlayStreamObject(itPlayInfo->second.objPlayStream, AVIOCTRL_PTZ_RIGHT);
				m_topControlBtn.SetPlayStreamObject(itPlayInfo->second.objPlayStream, AVIOCTRL_PTZ_UP);
				m_bottomControlBtn.SetPlayStreamObject(itPlayInfo->second.objPlayStream, AVIOCTRL_PTZ_DOWN);
			}
			else if(pWnd)
			{
				pWnd->StopStream();
				itPlayInfo->second.fRealStreamMgr->StopStream();
				itPlayInfo->second.bPlaying = FALSE;
			}
		}
	}
	*pResult = 0;
}

void CVideoPlayDialog::OnTvnBegindragDeviceTree(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	m_hItemDragS = pNMTreeView->itemNew.hItem;
	m_bDeviceTreeDrag = TRUE;
    m_deviceTree.SelectItem(m_hItemDragS);

    m_pDragImage = m_deviceTree.CreateDragImage( m_hItemDragS );
    if( !m_pDragImage )
        return;

    m_pDragImage->BeginDrag ( 0,CPoint(8,8) );
    CPoint pt = pNMTreeView->ptDrag;;
    m_pDragImage->DragEnter ( this, pt); //"this"将拖动操作限制在该窗口

	SetCursor(LoadCursor(NULL,IDC_HAND));
    SetCapture();
	*pResult = 0;
}

void CVideoPlayDialog::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	CPoint pt = point;
    CImageList::DragMove( pt ); 
    //鼠标经过时高亮显示
    CImageList::DragShowNolock( false ); //避免鼠标经过时留下难看的痕迹
    CImageList::DragShowNolock( true );

	CSkinDialog::OnMouseMove(nFlags, point);
}

void CVideoPlayDialog::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	SetCursor(LoadCursor(NULL,IDC_ICON));

	if(m_pDragImage != NULL)
	{
		CImageList::DragLeave(this);
		CImageList::EndDrag();

		delete m_pDragImage;
		m_pDragImage = NULL;

		int x = point.x;
		int y = point.y;

		CRect rt;
		m_screenPannel.GetWindowRect(&rt);

		if (x >0 && y >0 && point.x < rt.right && point.y < rt.bottom)
		{
			int nCount=m_screenPannel.GetCount();
			for(int nIndex = 0; nIndex < nCount; nIndex++)
			{
				CRect rect;
				m_screenPannel.GetPageRect(&rect,nIndex);

				if(x > rect.left && x < rect.right
					&& y > rect.top && y < rect.bottom)
				{      
					m_nDragWinIndex = nIndex;
					LONG	lResult = 0;
					if(m_bDeviceTreeDrag)
					{
						OnNMDblclkDeviceTree(NULL, &lResult);
					}
					else
					{
						OnNMDblclkNetdeviceTree(NULL, &lResult);
					}
						break;
				}
			}
		}            
		//Operator finished then release
		ReleaseCapture();
	}
	CSkinDialog::OnLButtonUp(nFlags, point);
}

void CVideoPlayDialog::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if(nIDEvent == 10000)
	{
		KillTimer(10000);
		InitNetTreeView();
		ConnectAllDevice(1);
	}
	CSkinDialog::OnTimer(nIDEvent);
}

#ifdef UNICODE
tkPVOID CVideoPlayDialog::FindDeviceObjFromUID(std::wstring sUID, int &nStatus)
{
	map<wstring, __PLAY_INFO>::iterator itDevice = m_curDeviceMap.find(sUID);
#else
tkPVOID CVideoPlayDialog::FindDeviceObjFromUID(std::string sUID,int &nStatus)
{
	map<string, __PLAY_INFO>::iterator itDevice = m_curDeviceMap.find(sUID);
#endif
	if(itDevice != m_curDeviceMap.end())
	{
		nStatus = itDevice->second.nConnected;
		return itDevice->second.objPlayStream;
	}

	return NULL;
}
void CVideoPlayDialog::OnTvnBeginrdragNetdeviceTree(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	m_hItemDragS = pNMTreeView->itemNew.hItem;
	m_bDeviceTreeDrag = FALSE;
    m_netDeviceTree.SelectItem(m_hItemDragS);

    m_pDragImage = m_netDeviceTree.CreateDragImage( m_hItemDragS );
    if( !m_pDragImage )
        return;

    m_pDragImage->BeginDrag ( 0,CPoint(8,8) );
    CPoint pt = pNMTreeView->ptDrag;;
    m_pDragImage->DragEnter ( this, pt); //"this"将拖动操作限制在该窗口

	SetCursor(LoadCursor(NULL,IDC_HAND));
    SetCapture();
	*pResult = 0;
}

void CVideoPlayDialog::OnTvnBegindragNetdeviceTree(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	m_hItemDragS = pNMTreeView->itemNew.hItem;
	m_bDeviceTreeDrag = FALSE;
    m_netDeviceTree.SelectItem(m_hItemDragS);

    m_pDragImage = m_netDeviceTree.CreateDragImage( m_hItemDragS );
    if( !m_pDragImage )
        return;

    m_pDragImage->BeginDrag ( 0,CPoint(8,8) );
    CPoint pt = pNMTreeView->ptDrag;;
    m_pDragImage->DragEnter ( this, pt); //"this"将拖动操作限制在该窗口

	SetCursor(LoadCursor(NULL,IDC_HAND));
    SetCapture();
	*pResult = 0;
}

void CVideoPlayDialog::OnDestroy()
{
	CSkinDialog::OnDestroy();

	// TODO: 在此处添加消息处理程序代码
#ifndef UNICODE
	map<string, __PLAY_INFO>::iterator itPlayInfo =  m_curDeviceMap.begin();
#else
	map<wstring, __PLAY_INFO>::iterator itPlayInfo =  m_curDeviceMap.begin();
#endif
	while(itPlayInfo != m_curDeviceMap.end())
	{
		itPlayInfo->second.fRealStreamMgr->StopRecVideo();
		itPlayInfo->second.fRealStreamMgr->StopStream();
		delete itPlayInfo->second.fRealStreamMgr;
		itPlayInfo->second.fRealStreamMgr = NULL;
		
		m_curDeviceMap.erase(itPlayInfo++);
	}


	#ifdef UNICODE
	map<wstring, __CAMERA_INFO*>::iterator itNetDev = m_curNetDeviceMap.begin();
#else
	map<string, __CAMERA_INFO*>::iterator itNetDev = m_curNetDeviceMap.begin();
#endif
	while( itNetDev != m_curNetDeviceMap.end())
	{
		__CAMERA_INFO *pInfo = itNetDev->second;
		delete pInfo;
		pInfo = NULL;
		m_curNetDeviceMap.erase(itNetDev++);
	}
}
