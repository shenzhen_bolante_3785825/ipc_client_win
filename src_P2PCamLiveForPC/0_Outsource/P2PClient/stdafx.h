// stdafx.h : 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 特定于项目的包含文件

#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// 从 Windows 头中排除极少使用的资料
#endif

// 如果您必须使用下列所指定的平台之前的平台，则修改下面的定义。
// 有关不同平台的相应值的最新信息，请参考 MSDN。
#ifndef WINVER				// 允许使用特定于 Windows XP 或更高版本的功能。
#define WINVER 0x0501		// 将此值更改为相应的值，以适用于 Windows 的其他版本。
#endif

#ifndef _WIN32_WINNT		// 允许使用特定于 Windows XP 或更高版本的功能。
#define _WIN32_WINNT 0x0501	// 将此值更改为相应的值，以适用于 Windows 的其他版本。
#endif						

#ifndef _WIN32_WINDOWS		// 允许使用特定于 Windows 98 或更高版本的功能。
#define _WIN32_WINDOWS 0x0410 // 将它更改为适合 Windows Me 或更高版本的相应值。
#endif

#ifndef _WIN32_IE			// 允许使用特定于 IE 6.0 或更高版本的功能。
#define _WIN32_IE 0x0600	// 将此值更改为相应的值，以适用于 IE 的其他版本。值。
#endif

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// 某些 CString 构造函数将是显式的

// 关闭 MFC 对某些常见但经常可放心忽略的警告消息的隐藏
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC 核心组件和标准组件
#include <afxext.h>         // MFC 扩展


#include <afxdisp.h>        // MFC 自动化类



#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// MFC 对 Internet Explorer 4 公共控件的支持
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC 对 Windows 公共控件的支持
#endif // _AFX_NO_AFXCMN_SUPPORT
#include <afxhtml.h>

#define OS_MAX_PORT 128

#define	BLACK			RGB( 000, 000, 000 )

//画面分割类型
enum{
	SPLIT1 = 1,
	SPLIT4 = 4,
	SPLIT9 = 9,
	SPLIT6 = 6,
	SPLIT8 = 8,
	SPLIT16 = 16,
	SPLIT25 = 25,
	SPLIT36 = 36,
	SPLIT49 = 49,
	SPLIT64 = 64,	
	SPLIT_TOTAL
};

typedef enum {
	MSG_NULL=0x2012,
	MSG_CLOSESTREAM,   //关闭视频流
	MSG_CHANGEPLAYWND,    //改变播放窗口
	MSG_CHANGETALK,    //改变对讲状态
	MSG_MULTISCREEN, //多屏或单屏
	MSG_ESCAPE	//esc按下消息
}MsgType_t;

typedef enum
{
	WIN_USE_NULL,
	WIN_USE_PREVIEW,
	WIN_USE_PLAYBACK,
	WIN_USE_CARDCONFIG,
	WIN_USE_ALARMPLAYBACK
};

//当前画面显示内容的类型
typedef enum _SplitType{
	SPLIT_TYPE_NULL = 0,   //空白
	SPLIT_TYPE_MONITOR,    //网络监视
	SPLIT_TYPE_EXCEPTION    //网络监视异常
}SplitType;

//监视信息参数
typedef struct _SplitMonitorParam
{
	//DeviceNode *pDevice;  //设备指针
	int iChannel;   //对应的设备中的通道序号
	int iStreamId;	//预览返回的ID
	BOOL  isTalkOpen;  //是否打开语音对讲
	BOOL  isOpenSound;
	DWORD dwTreeNode;

	//use for cycle   
	int cycleType;
	UINT iInterval;			//画面切换间隔时间（秒）  
}SplitMonitorParam;

//画面分割通道显示信息(可以定义成type/param，param自定义)
typedef struct _SplitInfoNode
{
	SplitType Type;     //显示类型 空白/监视/网络回放/本地回放等
	DWORD iHandle;  //用于记录通道id(监视通道ID/播放文件iD等)
	BOOL  isOpenSound;
	SplitMonitorParam Param;  //信息参数,对于不同的显示有不同的参数
}SplitInfoNode;

#define IMAGE_INDEX_DEV         2
#define IMAGE_INDEX_PREVIEW     4
#define IMAGE_INDEX_DISCON      5
#define IMAGE_INDEX_NOPIC       6
#define IMAGE_INDEX_BADPIC      7
#define IMAGE_INDEX_PTZ         8
#define IMAGE_INDEX_NO_PTZ      3
#define IMAGE_INDEX_ROOT        0
#define IMAGE_INDEX_ALARM       10
#define LOG_IMAGE_INDEX         9
#define IMAGE_REC_DISCON        11
#define IMAGE_REC_STATUS        12



#pragma comment(lib, "../SDK/Lib/Win32/sqlite3.lib")
#pragma   comment(linker, "/NODEFAULTLIB:libc.lib")

#include "../SDK/Include/SkinControls/SkinControls.h"

#include "JJLog.h"

#define JJTrace CJJLog::GetInstance()->Print

#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif


