// VideoRecordPlanDialog.cpp : 实现文件
//

#include "stdafx.h"
#include "P2PClient.h"
#include "VideoRecordPlanDialog.h"


// VideoRecordPlanDialog 对话框
IMPLEMENT_DYNAMIC(VideoRecordPlanDialog, CSkinDialog)

VideoRecordPlanDialog::VideoRecordPlanDialog(CWnd* pParent /*=NULL*/)
	: CSkinDialog(VideoRecordPlanDialog::IDD, pParent)
	, m_csVideoSavePath(_T("C:\\"))
	, m_uVideoFileCapacity(30)
	, m_nModelID(0)
{
	//memset(&m_modeList, 0, sizeof(m_modeList));
}

VideoRecordPlanDialog::~VideoRecordPlanDialog()
{
}

void VideoRecordPlanDialog::DoDataExchange(CDataExchange* pDX)
{
	CSkinDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_VIDEO_SAVE_PATH_EDIT, m_csVideoSavePath);
	DDX_Text(pDX, IDC_VIDEO_CAPACITY_EDIT, m_uVideoFileCapacity);
	DDX_Control(pDX, IDC_VIDEO_TREE, m_VideoTree);
	DDX_Control(pDX, IDOK, m_bntOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_SAVE_PATH_BTN, m_btnPathSave);
	DDX_Control(pDX, IDC_STATIC_RECORD_PATH, m_staticRecordPath);
	DDX_Control(pDX, IDC_STATIC_RECORD_TIME, m_staticRecordTime);
	DDX_Control(pDX, IDC_STATIC_RECORD_MIN, m_staticRecordMin);
}


BEGIN_MESSAGE_MAP(VideoRecordPlanDialog, CSkinDialog)
	ON_BN_CLICKED(IDOK, &VideoRecordPlanDialog::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &VideoRecordPlanDialog::OnBnClickedCancel)
	ON_WM_PAINT()
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_SAVE_PATH_BTN, &VideoRecordPlanDialog::OnBnClickedSavePathBtn)
	ON_NOTIFY(TVN_SELCHANGED, IDC_VIDEO_TREE, &VideoRecordPlanDialog::OnTvnSelchangedVideoTree)
END_MESSAGE_MAP()


// VideoRecordPlanDialog 消息处理程序

BOOL VideoRecordPlanDialog::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 在此添加专用代码和/或调用基类
	if(pMsg->message==WM_KEYDOWN&&pMsg->wParam==VK_RETURN)    
        return TRUE; 
    if(pMsg->message==WM_KEYDOWN&&pMsg->wParam==VK_ESCAPE)    
	{
        return TRUE; 
	}

	return CSkinDialog::PreTranslateMessage(pMsg);
}

void VideoRecordPlanDialog::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);
	theApp.m_uVideoFileTime = m_uVideoFileCapacity;
	DeleteFile(_T("RecordPlan"));
	FILE *pFile = NULL;
	pFile = _tfopen(_T("RecordPlan"), _T("ab+"));
	if(pFile)
	{
		theApp.m_modeList.bStartRec = TRUE;
		int nLen = theApp.m_csVideoRecordPath.GetLength();
		fwrite(&theApp.m_modeList, sizeof(ModelInfoList), 1, pFile);
		fwrite(&nLen, sizeof(nLen), 1, pFile);
		fwrite(theApp.m_csVideoRecordPath.GetBuffer(), theApp.m_csVideoRecordPath.GetLength(), 1, pFile);
		fwrite(&theApp.m_uVideoFileTime, sizeof(theApp.m_uVideoFileTime), 1, pFile);
		fclose(pFile);
	}
	
	OnOK();
}

void VideoRecordPlanDialog::OnBnClickedCancel()
{
	// TODO: 在此添加控件通知处理程序代码
	::memset(&theApp.m_modeList.recModelInfo[m_nModelID], 0, sizeof(ModelInfo));
	OnCancel();
}

BOOL VideoRecordPlanDialog::OnInitDialog()
{
	CSkinDialog::OnInitDialog();

	this->UserDefSkin(false, true, true);
	this->SetBKSkinImage(_T("image\\bk_body.png"), BF_PNG);
	this->SetSysBtnImage(_T("image\\sys_btn_close (2).png"), BF_PNG, 3);
	this->SetSysBtnImage(_T("image\\sys_btn_max.png"), BF_PNG, 2);
	this->SetSysBtnImage(_T("image\\sys_btn_min.png"), BF_PNG, 1);
	//this->SetCaptionImage(_T("image\\bk_body.png"), BF_PNG);

	m_image.Create(IDB_TREEIMAGE,16,1,RGB(255,255,255));
	// TODO:  在此添加额外的初始化
	DWORD dwStyle=GetWindowLong(m_VideoTree.m_hWnd ,GWL_STYLE);//获得树的信息
	dwStyle|=TVS_HASBUTTONS|TVS_HASLINES|TVS_LINESATROOT;//设置风格
	::SetWindowLong (m_VideoTree.m_hWnd ,GWL_STYLE,dwStyle);
	m_VideoTree.SetImageList(&m_image,TVSIL_NORMAL );
	HTREEITEM m_hParentItem = m_VideoTree.InsertItem(_T("设备列表"), IMAGE_INDEX_ROOT,IMAGE_INDEX_ROOT,TVI_ROOT);

	/*
	int nIndex = 0;
	theApp.m_SQLite3DB.open(_T("SystemDB"));
	TCHAR strQuerySQL[256] = {0};
	try
	{
		wsprintf(strQuerySQL, _T("select * from DeviceInfo order by uID asc"));	//, theApp.m_nCurUserID);
		CppSQLite3Query SQLiteQuery = theApp.m_SQLite3DB.execQuery(strQuerySQL);

		int nListRow = 0;
		while( !SQLiteQuery.eof())
		{
			
			HTREEITEM hCurItem = m_VideoTree.InsertItem(SQLiteQuery.fieldValue(_T("DeviceName")), IMAGE_INDEX_NO_PTZ, IMAGE_INDEX_BADPIC, m_hParentItem);
			int nID = ::_ttoi(SQLiteQuery.fieldValue(_T("uID")));
			nListRow++;
			theApp.m_modeList.recModelInfo[nIndex].nIndex			= nIndex;
			theApp.m_modeList.recModelInfo[nIndex].recID			= nID;
			_tcscpy(theApp.m_modeList.recModelInfo[nIndex].recUID, SQLiteQuery.fieldValue(_T("DeviceID")));
			m_VideoTree.SetItemData(hCurItem, (DWORD_PTR)nIndex);
			nIndex++;
			SQLiteQuery.nextRow();
		}
		SQLiteQuery.finalize();

		theApp.m_modeList.totalNum = nListRow;
		//memcpy(&g_VideoRecordModeList, &m_modeList, sizeof(m_modeList));;
	}catch(...){}
	theApp.m_SQLite3DB.close();
	*/

	m_VideoTree.Expand(m_hParentItem, TVE_EXPAND);

	
	//UpdateData();
	 //初始化模板显示区
    GetDlgItem(IDC_VIDEO_PLAN)->GetWindowRect(&m_grouprec);
    ScreenToClient(&m_grouprec);
    m_grouprec.DeflateRect(4,4);

	//FILE *pReadFile = NULL;
	//pReadFile = fopen(_T("RecordPlan"), _T("ab+"));
	//if(pReadFile)
	//{
	//	int nLen = 0;
	//	fread(&theApp.m_modeList, sizeof(ModelInfoList), 1, pReadFile);
	//	fread(&nLen, sizeof(nLen), 1, pReadFile);
	//	char sVideoRecordPath[256] = {0};
	//	fread(sVideoRecordPath, nLen, 1, pReadFile);
	//	fread(&theApp.m_uVideoFileTime, sizeof(theApp.m_uVideoFileTime), 1, pReadFile);
	//	fclose(pReadFile);
	//	theApp.m_csVideoRecordPath = sVideoRecordPath;
	//	theApp.m_csVideoRecordPath.TrimRight();
	//	m_csVideoSavePath = theApp.m_csVideoRecordPath;
	//	m_uVideoFileCapacity = theApp.m_uVideoFileTime;
	//	UpdateData(FALSE);
	//}
	//else
	if(theApp.m_csVideoRecordPath.GetLength() == 0)
	{
		theApp.m_csVideoRecordPath	= m_csVideoSavePath;
		theApp.m_uVideoFileTime		= m_uVideoFileCapacity;
		
	}
	else
	{
		m_csVideoSavePath = theApp.m_csVideoRecordPath;
		m_uVideoFileCapacity = theApp.m_uVideoFileTime;
	}
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void VideoRecordPlanDialog::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 在此处添加消息处理程序代码
	//重绘模板
    UpdateModelUI();
	// 不为绘图消息调用 CSkinDialog::OnPaint()
}


void VideoRecordPlanDialog::UpdateModelUI()
{
   /* HTREEITEM htree = m_VideoTree.GetSelectedItem();
    if(htree == NULL)
        return;
    
    int nModelID = m_VideoTree.GetItemData(htree);
    if(nModelID > MAXNUMMODEL)
        return;*/
    
    //重画模板计划
    CDC *pDC = GetDC();
    DrawModle(pDC, m_grouprec, &theApp.m_modeList.recModelInfo[m_nModelID]);
    //UpdateData(FALSE);
}
void VideoRecordPlanDialog::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	CRect rc(m_grouprec.right-DTNECOLREC*MINIDOWNCOLLINE, m_grouprec.bottom-DTNEROWREC*MINIDOWNROWLINE, m_grouprec.right, m_grouprec.bottom);
    if(rc.PtInRect(point))
    {      
        //重画模板计划
        ModelInfo *pModelInfo = &theApp.m_modeList.recModelInfo[m_nModelID];        
        
        //m_saveBtn.EnableWindow(TRUE);
        //m_abandonBtn.EnableWindow(TRUE);
        
        CDC * pDC = GetDC();
        DrawTableField(pDC, point, m_grouprec, pModelInfo);   			
        
        //UpdateBtnStatus(TRUE);
    }

	CSkinDialog::OnLButtonDown(nFlags, point);
}

void VideoRecordPlanDialog::OnRButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	CRect rc(m_grouprec.right-DTNECOLREC*MINIDOWNCOLLINE, m_grouprec.bottom-DTNEROWREC*MINIDOWNROWLINE, m_grouprec.right, m_grouprec.bottom);
    if(rc.PtInRect(point))
    {
        //重画模板计划
        ModelInfo *pModelInfo = &theApp.m_modeList.recModelInfo[m_nModelID];        
    
       // m_saveBtn.EnableWindow(TRUE);
        //m_abandonBtn.EnableWindow(TRUE);
    
        CDC * pDC = GetDC();
        DrawTableField(pDC, point, m_grouprec, pModelInfo, FALSE);   			
    
        //UpdateBtnStatus(TRUE);
    }
	CSkinDialog::OnRButtonDown(nFlags, point);
}

void VideoRecordPlanDialog::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	CRect rc(m_grouprec.right-DTNECOLREC*MINIDOWNCOLLINE, m_grouprec.bottom-DTNEROWREC*MINIDOWNROWLINE, m_grouprec.right, m_grouprec.bottom);
    if(rc.PtInRect(point))
	{
        ModelInfo *pModelInfo = &theApp.m_modeList.recModelInfo[m_nModelID];        
        
        CDC * pDC = GetDC();
        if(nFlags == MK_RBUTTON)
        {
            DrawTableField(pDC, point, m_grouprec,  pModelInfo, FALSE);   
            //UpdateBtnStatus(TRUE);
        }
        else if(nFlags == MK_LBUTTON)
        {
            DrawTableField(pDC, point, m_grouprec,  pModelInfo);   	
            //UpdateBtnStatus(TRUE);
        }
    }

	CSkinDialog::OnMouseMove(nFlags, point);
}

void VideoRecordPlanDialog::OnBnClickedSavePathBtn()
{
	CString sFolderPath;
	BROWSEINFO bi;
	TCHAR Buffer[MAX_PATH];
	//初始化入口参数bi开始
	bi.hwndOwner= NULL;
	bi.pidlRoot =NULL;//初始化制定的root目录很不容易，
	bi.pszDisplayName = Buffer;//此参数如为NULL则不能显示对话框
	if(theApp.m_nCurLanIndex == 0)
	{
		bi.lpszTitle = _T("修改录像存储路径");
	}
	else if(theApp.m_nCurLanIndex == 1)
	{
		bi.lpszTitle = _T("修改錄影儲存路徑");
	}
	else if(theApp.m_nCurLanIndex == 2)
	{
		bi.lpszTitle = _T("modify the video storage path");
	}
	//bi.ulFlags = BIF_BROWSEINCLUDEFILES;//包括文件
	bi.ulFlags = BIF_EDITBOX;//包括文件

	bi.lpfn = NULL;
	bi.iImage=IDR_MAINFRAME;
	//初始化入口参数bi结束
	LPITEMIDLIST pIDList = SHBrowseForFolder(&bi);//调用显示选择对话框
	if(pIDList)
	{
		SHGetPathFromIDList(pIDList, Buffer);
		//取得文件夹路径到Buffer里
		sFolderPath = Buffer;//将路径保存在一个CString对象里
		theApp.m_csVideoRecordPath = sFolderPath;

		m_csVideoSavePath = sFolderPath;
		UpdateData(FALSE);
		theApp.m_modeList.bStartRec = FALSE;
	}
	LPMALLOC lpMalloc;
	if(FAILED(SHGetMalloc(&lpMalloc))) return;
	//释放内存
	lpMalloc->Free(pIDList);
	lpMalloc->Release();
	
}

void VideoRecordPlanDialog::OnTvnSelchangedVideoTree(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	HTREEITEM curNode = m_VideoTree.GetSelectedItem();
	if(!curNode)
	{
		return;
	}

	if(m_hPreItem)
	{
		m_VideoTree.SetItemImage(m_hPreItem, IMAGE_INDEX_NO_PTZ, IMAGE_INDEX_BADPIC);
	}
	m_hPreItem = curNode;
	m_VideoTree.SetItemImage(curNode, IMAGE_REC_STATUS, IMAGE_REC_STATUS);
	m_nModelID = (int )m_VideoTree.GetItemData(curNode);
	if( m_nModelID >= MAXNUMMODEL) MessageBox(_T("Error"));
	UpdateModelUI();
	
	*pResult = 0;
}
