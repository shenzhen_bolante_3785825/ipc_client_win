#pragma once
#include "NvsModelPlanUI.h"
#include "afxcmn.h"
#include "afxwin.h"

// VideoRecordPlanDialog 对话框

class VideoRecordPlanDialog : public CSkinDialog
{
	DECLARE_DYNAMIC(VideoRecordPlanDialog)

public:
	VideoRecordPlanDialog(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~VideoRecordPlanDialog();

// 对话框数据
	enum { IDD = IDD_VIDEORECORDPLANDIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);

private:
	CRect		m_grouprec;
	int			m_nModelID;
	ModelInfoList m_modeList;       //模板列表
	CImageList m_image;
public:
	afx_msg void OnBnClickedOk();
public:
	afx_msg void OnBnClickedCancel();
public:
	virtual BOOL OnInitDialog();
public:
	afx_msg void OnPaint();
	void UpdateModelUI();
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
public:
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
public:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
public:
	afx_msg void OnBnClickedSavePathBtn();
public:
	CString m_csVideoSavePath;
public:
	UINT m_uVideoFileCapacity;
public:
	CTreeCtrl m_VideoTree;
public:
	afx_msg void OnTvnSelchangedVideoTree(NMHDR *pNMHDR, LRESULT *pResult);
public:
	CSkinButton m_bntOK;
public:
	CSkinButton m_btnCancel;
	CSkinButton m_btnPathSave;
	CSkinStatic m_staticRecordPath;
	CSkinStatic m_staticRecordTime;
	CSkinStatic m_staticRecordMin;

	HTREEITEM	m_hPreItem;
};
