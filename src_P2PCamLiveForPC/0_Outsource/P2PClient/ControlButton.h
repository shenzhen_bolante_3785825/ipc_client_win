#pragma once

#include "P2PAVCore.h"
// CControlButton

class CControlButton : public CSkinButton
{
	DECLARE_DYNAMIC(CControlButton)

public:
	CControlButton();
	virtual ~CControlButton();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
public:
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);

public:
	void SetPlayStreamObject(tkPVOID objStream, unsigned char control);
private:
	tkPVOID					m_objPlayStream;
	unsigned char			m_controlCMD;
};


