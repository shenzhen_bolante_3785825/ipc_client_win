#pragma once
#include "web_explorer.h"


// CRemoteWebDialog 对话框

class CRemoteWebDialog : public CSkinDialog
{
	DECLARE_DYNAMIC(CRemoteWebDialog)

public:
	CRemoteWebDialog(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CRemoteWebDialog();

// 对话框数据
	enum { IDD = IDD_REMOTEWEBDIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CWeb_explorer m_webExplorer;
public:
	virtual BOOL OnInitDialog();
public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
};
