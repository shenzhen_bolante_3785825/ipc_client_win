// EquipmentConfigDialog.cpp : 实现文件
//

#include "stdafx.h"
#include "P2PClient.h"
#include "EquipmentConfigDialog.h"
#include "VideoPlayDialog.h"
#include "ChineseCode.h"
extern CVideoPlayDialog			*g_pVideoPlayDialog;
// CEquipmentConfigDialog 对话框

IMPLEMENT_DYNAMIC(CEquipmentConfigDialog, CSkinDialog)

CEquipmentConfigDialog::CEquipmentConfigDialog(CWnd* pParent /*=NULL*/)
	: CSkinDialog(CEquipmentConfigDialog::IDD, pParent)
	, m_csIPCamModeEdit(_T(""))
	, m_csManufacturerEdit(_T(""))
	, m_csFirmwareEdit(_T(""))
	, m_csTotalStatusEdit(_T(""))
	, m_nSDFreeSpaceEdit(0)
	, m_csWifiSSIDEdit(_T(""))
	, m_csWifiStatusEdit(_T(""))
	, m_csNewDevPwdEdit(_T(""))
	, m_sDeviceUID(_T(""))
	, m_objItem(NULL)
{

}

CEquipmentConfigDialog::~CEquipmentConfigDialog()
{
}

void CEquipmentConfigDialog::DoDataExchange(CDataExchange* pDX)
{
	CSkinDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MODIFY_DEV_MODE_BTN, m_ModifyDevModeBtn);
	DDX_Control(pDX, IDC_MODIFY_DEV_PWD_BTN, m_ModifyDevPwdBtn);
	DDX_Control(pDX, IDC_MODIFY_WIFI_MSG_BTN, m_ModifyWifiMsgBtn);
	DDX_Control(pDX, IDOK, m_IDOKBtn);
	DDX_Control(pDX, IDCANCEL, m_IDCancelBtn);
	DDX_Text(pDX, IDC_IPCAM_MODE_EDIT, m_csIPCamModeEdit);
	DDX_Text(pDX, IDC_MANUFACTURER_EDIT, m_csManufacturerEdit);
	DDX_Text(pDX, IDC_FIRMWARE_EDIT, m_csFirmwareEdit);
	DDX_Text(pDX, IDC_TOTAL_STATUS_EDIT, m_csTotalStatusEdit);
	DDX_Text(pDX, IDC_SD_FREE_SPACE_EDIT, m_nSDFreeSpaceEdit);
	DDX_Text(pDX, IDC_WIFI_SSID_EDIT, m_csWifiSSIDEdit);
	DDX_Control(pDX, IDC_AP_MODE_COMBO, m_APModeCombo);
	DDX_Control(pDX, IDC_WIFI_ENCTYPE_COMBO, m_WifiEnctypeCombo);
	DDX_Control(pDX, IDC_WIFI_SIGNAL_PROGRESS, m_WifiSignalProgress);
	DDX_Text(pDX, IDC_WIFI_STATUS_EDIT, m_csWifiStatusEdit);
	DDX_Control(pDX, IDC_VIDEO_QU_COMBO, m_VideoQUCombo);
	DDX_Control(pDX, IDC_TURN_MODE_COMBO, m_TurnModeCombo);
	DDX_Control(pDX, IDC_EN_MODE_COMBO, m_EnModeCombo);
	DDX_Control(pDX, IDC_RECORD_MODE_COMBO, m_RecordModeCombo);
	DDX_Control(pDX, IDC_DETECT_MODE_COMBO, m_DetectModeCombo);
	DDX_Text(pDX, IDC_NEW_PWD_EDIT, m_csNewDevPwdEdit);
}


BEGIN_MESSAGE_MAP(CEquipmentConfigDialog, CSkinDialog)
	ON_BN_CLICKED(IDC_MODIFY_DEV_PWD_BTN, &CEquipmentConfigDialog::OnBnClickedModifyDevPwdBtn)
	ON_BN_CLICKED(IDC_MODIFY_DEV_MODE_BTN, &CEquipmentConfigDialog::OnBnClickedModifyDevModeBtn)
	ON_BN_CLICKED(IDC_MODIFY_WIFI_MSG_BTN, &CEquipmentConfigDialog::OnBnClickedModifyWifiMsgBtn)
	ON_MESSAGE(WM_USER_UDPATE_DATA, OnUpdateDataMessage)
END_MESSAGE_MAP()


// CEquipmentConfigDialog 消息处理程序

BOOL CEquipmentConfigDialog::OnInitDialog()
{
	CSkinDialog::OnInitDialog();

	// TODO:  在此添加额外的初始化
	this->UserDefSkin(false, true, true);
	this->SetBKSkinImage(_T("image\\bk_body.png"), BF_PNG);
	this->SetSysBtnImage(_T("image\\sys_btn_close (2).png"), BF_PNG, 3);
	this->SetSysBtnImage(_T("image\\sys_btn_max.png"), BF_PNG, 2);
	this->SetSysBtnImage(_T("image\\sys_btn_min.png"), BF_PNG, 1);
	this->SetCaptionImage(_T("image\\bk_body.png"), BF_PNG);

	m_APModeCombo.InsertString(0, _T("NULL"));
	m_APModeCombo.InsertString(1, _T("MANAGED"));
	m_APModeCombo.InsertString(2, _T("ADHOC"));
	m_APModeCombo.SetCurSel(0);

	m_WifiEnctypeCombo.InsertString(0, _T("INVALID"));
	m_WifiEnctypeCombo.InsertString(1, _T("NONE"));
	m_WifiEnctypeCombo.InsertString(2, _T("WEP"));
	m_WifiEnctypeCombo.InsertString(3, _T("WPA_TKIP"));
	m_WifiEnctypeCombo.InsertString(4, _T("WPA_AES"));
	m_WifiEnctypeCombo.InsertString(5, _T("WPA2_TKIP"));
	m_WifiEnctypeCombo.InsertString(6, _T("WPA2_AES"));
	m_WifiEnctypeCombo.InsertString(7, _T("WPA_PSK_TKIP"));
	m_WifiEnctypeCombo.InsertString(8, _T("WPA_PSK_AES"));
	m_WifiEnctypeCombo.InsertString(9, _T("WPA2_PSK_TKIP"));
	m_WifiEnctypeCombo.InsertString(10, _T("WPA2_PSK_AES"));
	m_WifiEnctypeCombo.SetCurSel(0);

	m_WifiSignalProgress.SetRange(0, 100);
	m_WifiSignalProgress.SetPos(51);

	m_VideoQUCombo.InsertString(0, _T("INVALID"));
	m_VideoQUCombo.InsertString(1, _T("640*480, 15fps, 320kbps/1280x720, 5fps, 320kbps"));
	m_VideoQUCombo.InsertString(2, _T("640*480, 10fps, 256kbps"));
	m_VideoQUCombo.InsertString(3, _T("320*240, 15fps, 256kbps"));
	m_VideoQUCombo.InsertString(4, _T("320*240, 10fps, 128kbps"));
	m_VideoQUCombo.InsertString(5, _T("160*120, 10fps, 64kbps"));
	m_VideoQUCombo.SetCurSel(0);

	m_TurnModeCombo.InsertString(0, _T("NORMAL"));
	m_TurnModeCombo.InsertString(1, _T("FLIP"));
	m_TurnModeCombo.InsertString(2, _T("MIRROR"));
	m_TurnModeCombo.InsertString(3, _T("FLIP_MIRROR"));
	m_TurnModeCombo.SetCurSel(0);

	m_EnModeCombo.InsertString(0, _T("INDOOR_50HZ"));
	m_EnModeCombo.InsertString(1, _T("INDOOR_60HZ"));
	m_EnModeCombo.InsertString(2, _T("OUTDOOR"));
	m_EnModeCombo.InsertString(3, _T("NIGHT"));
	m_EnModeCombo.SetCurSel(0);

	m_RecordModeCombo.InsertString(0, _T("OFF"));
	m_RecordModeCombo.InsertString(1, _T("FULLTIME"));
	m_RecordModeCombo.InsertString(2, _T("ALARM"));
	m_RecordModeCombo.InsertString(3, _T("MANUAL"));
	m_RecordModeCombo.SetCurSel(0);

	m_DetectModeCombo.InsertString(0, _T("0"));
	m_DetectModeCombo.InsertString(1, _T("1"));
	m_DetectModeCombo.InsertString(2, _T("2"));
	m_DetectModeCombo.InsertString(3, _T("3"));
	m_DetectModeCombo.InsertString(4, _T("4"));
	m_DetectModeCombo.SetCurSel(0);
	//获取各种状态信息
	if(g_pVideoPlayDialog)
	{
		int nStatus = -1;
		tkPVOID objItem = g_pVideoPlayDialog->FindDeviceObjFromUID(m_sDeviceUID, nStatus);
		if(nStatus != SINFO_CONNECTED || objItem == NULL)
		{
			CString strDevUnOnLine;
			strDevUnOnLine.LoadString(IDS_STR_DEV_UN_LINE);
			CString strDevMgr;
			strDevMgr.LoadString(IDS_STR_DEV_MGR);
			MessageBox(strDevUnOnLine, strDevMgr);
		}
		else if(objItem )
		{
			m_objItem = objItem;
			apiRegCB_OnRecvIOCtrl(objItem, OnRecvIOCtrl, this);
			//设备信息
			SMsgAVIoctrlDeviceInfoReq deviceInfoReq;
			memset(&deviceInfoReq, 0, sizeof(deviceInfoReq));
			int nRet=apiSendIOCtrl(objItem, IOTYPE_USER_IPCAM_DEVINFO_REQ, (tkUCHAR *)&deviceInfoReq, sizeof(deviceInfoReq));

			//获取视频质量
			SMsgAVIoctrlSetStreamCtrlReq streamCtrlReq;
			memset(&streamCtrlReq, 0, sizeof(streamCtrlReq));
			nRet=apiSendIOCtrl(objItem, IOTYPE_USER_IPCAM_GETSTREAMCTRL_REQ, (tkUCHAR *)&streamCtrlReq, sizeof(streamCtrlReq));

			//获取视频翻转模式
			SMsgAVIoctrlGetVideoModeReq videoModereq;
			memset(&videoModereq, 0, sizeof(videoModereq));
			nRet=apiSendIOCtrl(objItem, IOTYPE_USER_IPCAM_GET_VIDEOMODE_REQ, (tkUCHAR *)&videoModereq, sizeof(videoModereq));

			//获取环境模式
			SMsgAVIoctrlGetEnvironmentReq envreq;
			memset(&envreq, 0, sizeof(envreq));
			nRet=apiSendIOCtrl(objItem, IOTYPE_USER_IPCAM_GET_ENVIRONMENT_REQ, (tkUCHAR *)&envreq, sizeof(envreq));

			//获取移动侦测灵敏度
			SMsgAVIoctrlGetMotionDetectReq motionreq;
			memset(&motionreq, 0, sizeof(motionreq));
			nRet=apiSendIOCtrl(objItem, IOTYPE_USER_IPCAM_GETMOTIONDETECT_REQ	, (tkUCHAR *)&motionreq, sizeof(motionreq));

			//获取录像模式
			SMsgAVIoctrlGetRecordReq recordreq;
			memset(&recordreq, 0, sizeof(recordreq));
			nRet=apiSendIOCtrl(objItem, IOTYPE_USER_IPCAM_GETRECORD_REQ	, (tkUCHAR *)&recordreq, sizeof(recordreq));

			//WIFI
			SMsgAVIoctrlListWifiApReq wifireq;
			memset(&wifireq, 0, sizeof(wifireq));
			nRet=apiSendIOCtrl(objItem, IOTYPE_USER_IPCAM_LISTWIFIAP_REQ, (tkUCHAR *)&wifireq, sizeof(wifireq));
			Sleep(2000);
		}
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CEquipmentConfigDialog::OnBnClickedModifyDevPwdBtn()
{
	//修改设备密码
	SMsgAVIoctrlSetPasswdReq pwdReq;
	memset(&pwdReq, 0, sizeof(pwdReq));
#ifdef UNICODE
	char sNewPwd[32] = {0};
	char sOldPwd[32] = {0};
	//CChineseCode::UnicodeToGB2312(sNewPwd, m_csNewDevPwdEdit.GetBuffer());
	//wcscpy_s(pwdReq.newpasswd, 32, m_csNewDevPwdEdit.GetBuffer());
	//wcscpy_s(pwdReq.oldpasswd, 32, m_csOldDevPwd.GetBuffer());
	strcpy_s(pwdReq.newpasswd, 32, sNewPwd);
	strcpy_s(pwdReq.oldpasswd, 32, sOldPwd);
#else
	strcpy_s(pwdReq.newpasswd, 32, m_csNewDevPwdEdit.GetBuffer());
	strcpy_s(pwdReq.oldpasswd, 32, m_csOldDevPwd.GetBuffer());
#endif
	int nRet=apiSendIOCtrl(m_objItem, IOTYPE_USER_IPCAM_SETPASSWORD_REQ, (tkUCHAR *)&pwdReq, sizeof(pwdReq));
	// TODO: 在此添加控件通知处理程序代码IOTYPE_USER_IPCAM_SETPASSWORD_REQ
}

void CEquipmentConfigDialog::OnBnClickedModifyDevModeBtn()
{
	// TODO: 在此添加控件通知处理程序代码
	//获取视频质量
	SMsgAVIoctrlSetStreamCtrlReq streamCtrlReq;
	memset(&streamCtrlReq, 0, sizeof(streamCtrlReq));
	int nRet=apiSendIOCtrl(m_objItem, IOTYPE_USER_IPCAM_SETSTREAMCTRL_REQ, (tkUCHAR *)&streamCtrlReq, sizeof(streamCtrlReq));

	//获取视频翻转模式
	SMsgAVIoctrlSetVideoModeReq videoModereq;
	memset(&videoModereq, 0, sizeof(videoModereq));
	nRet=apiSendIOCtrl(m_objItem, IOTYPE_USER_IPCAM_SET_VIDEOMODE_REQ, (tkUCHAR *)&videoModereq, sizeof(videoModereq));

	//获取环境模式
	SMsgAVIoctrlSetEnvironmentReq envreq;
	memset(&envreq, 0, sizeof(envreq));
	nRet=apiSendIOCtrl(m_objItem, IOTYPE_USER_IPCAM_SET_ENVIRONMENT_REQ, (tkUCHAR *)&envreq, sizeof(envreq));

	//获取移动侦测灵敏度
	SMsgAVIoctrlSetMotionDetectReq motionreq;
	memset(&motionreq, 0, sizeof(motionreq));
	nRet=apiSendIOCtrl(m_objItem, IOTYPE_USER_IPCAM_SETMOTIONDETECT_REQ	, (tkUCHAR *)&motionreq, sizeof(motionreq));

	//获取录像模式
	SMsgAVIoctrlSetRecordReq recordreq;
	memset(&recordreq, 0, sizeof(recordreq));
	nRet=apiSendIOCtrl(m_objItem, IOTYPE_USER_IPCAM_SETRECORD_REQ	, (tkUCHAR *)&recordreq, sizeof(recordreq));
}

void CEquipmentConfigDialog::OnBnClickedModifyWifiMsgBtn()
{
	// TODO: 在此添加控件通知处理程序代码
	//WIFI
	SMsgAVIoctrlSetWifiReq wifireq;
	memset(&wifireq, 0, sizeof(wifireq));
	int nRet=apiSendIOCtrl(m_objItem, IOTYPE_USER_IPCAM_SETWIFI_REQ, (tkUCHAR *)&wifireq, sizeof(wifireq));
}

tkVOID WINAPI CEquipmentConfigDialog::OnRecvIOCtrl(tkINT32 nIOType, tkCHAR *pIOData, tkINT32 nIODataSize, tkPVOID pObj, tkUINT32 nTag, tkVOID *param)
{
	TRACE(_T("OnRecvIOCtrl, nIOType=0x%X, pObj=0x%X, cam%d, param=0x%X\n"), nIOType, pObj, nTag, param);
	CEquipmentConfigDialog *pThis=(CEquipmentConfigDialog *)param;
	pThis->AppendIOCtrl(nIOType, pIOData, nIODataSize, pObj, nTag);
	
}

tkVOID CEquipmentConfigDialog::AppendIOCtrl(tkINT32 nIOType, tkCHAR *pIOData, tkINT32 nIODataSize, tkPVOID pObj, tkUINT32 nTag)
{
	switch(nIOType)
	{
	case IOTYPE_USER_IPCAM_DEVINFO_RESP:
		{
			SMsgAVIoctrlDeviceInfoResp deviceInfoResp;
			::memcpy(&deviceInfoResp, pIOData, nIODataSize);
			m_csIPCamModeEdit = deviceInfoResp.model;
			m_csManufacturerEdit = deviceInfoResp.vendor;
			m_csManufacturerEdit.Format(_T("%d"), deviceInfoResp.version);
			m_csTotalStatusEdit.Format(_T("%d"), deviceInfoResp.total);
			m_nSDFreeSpaceEdit = deviceInfoResp.free;
			
		}
		break;
		//获取视频质量
	case IOTYPE_USER_IPCAM_GETSTREAMCTRL_RESP:
		{
			SMsgAVIoctrlGetStreamCtrlResq streamCtrlResp;
			::memcpy(&streamCtrlResp, pIOData, nIODataSize);
			m_VideoQUCombo.SetCurSel(streamCtrlResp.quality);
		}
		break;
		//获取视频翻转模式
	case IOTYPE_USER_IPCAM_GET_VIDEOMODE_RESP:
		{
			SMsgAVIoctrlGetVideoModeResp resp;
			::memcpy(&resp, pIOData, nIODataSize);
			m_TurnModeCombo.SetCurSel(resp.mode);
		}
		break;
		//获取环境模式
	case IOTYPE_USER_IPCAM_GET_ENVIRONMENT_RESP:
		{
			SMsgAVIoctrlGetEnvironmentResp resp;
			::memcpy(&resp, pIOData, nIODataSize);
			m_EnModeCombo.SetCurSel(resp.mode);
		}
		break;
		//获取移动侦测灵敏度
	case IOTYPE_USER_IPCAM_GETMOTIONDETECT_RESP:
		{
			SMsgAVIoctrlGetMotionDetectResp resp;
			::memcpy(&resp, pIOData, nIODataSize);
			m_DetectModeCombo.SetCurSel(resp.sensitivity/25);
		}
		break;
		//获取录像模式
	case IOTYPE_USER_IPCAM_GETRECORD_RESP:
		{
			SMsgAVIoctrlGetRecordResq resp;
			::memcpy(&resp, pIOData, nIODataSize);
			m_RecordModeCombo.SetCurSel(resp.recordType);
			m_csWifiSSIDEdit = "asdf"/*resp.stWifiAp[0].ssid*/;
		}
		break;
		//WIFI
	case IOTYPE_USER_IPCAM_LISTWIFIAP_RESP:
		{
			SMsgAVIoctrlListWifiApResp *resp = (SMsgAVIoctrlListWifiApResp*)pIOData;
			m_csWifiSSIDEdit = resp->stWifiAp[0].ssid;
			m_APModeCombo.SetCurSel(resp->stWifiAp[0].mode);
			m_WifiEnctypeCombo.SetCurSel(resp->stWifiAp[0].enctype);
			m_WifiSignalProgress.SetPos(resp->stWifiAp[0].signal);
			switch(resp->stWifiAp[0].status)
			{
			case 0:
				m_csWifiStatusEdit.Format(_T("invalid ssid or disconnected"));
				break;
			case 1:
				m_csWifiStatusEdit.Format(_T("connected with default gateway"));
				break;
			case 2:
				m_csWifiStatusEdit.Format(_T("unmatched password"));
				break;
			case 3:
				m_csWifiStatusEdit.Format(_T("weak signal and connected"));
				break;
			case 4:
				m_csWifiStatusEdit.Format(_T("password matched and disconnected or connected but not default gateway"));
				break;
			}
		}
		break;
	}

	PostMessage(WM_USER_UDPATE_DATA);
}

LRESULT CEquipmentConfigDialog::OnUpdateDataMessage(WPARAM, LPARAM)
{
	UpdateData(false);
	return 0;
}