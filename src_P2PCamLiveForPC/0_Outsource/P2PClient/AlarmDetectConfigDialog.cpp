// AlarmDetectConfigDialog.cpp : 实现文件
//

#include "stdafx.h"
#include "P2PClient.h"
#include "AlarmDetectConfigDialog.h"


// AlarmDetectConfigDialog 对话框

IMPLEMENT_DYNAMIC(AlarmDetectConfigDialog, CSkinDialog)

AlarmDetectConfigDialog::AlarmDetectConfigDialog(CWnd* pParent /*=NULL*/)
	: CSkinDialog(AlarmDetectConfigDialog::IDD, pParent)
	, m_csEmailCaption(_T(""))
	, m_csEmailContent(_T(""))
	, m_csEmailFromAddr(_T(""))
	, m_csEmailAddrTo(_T(""))
	, m_csEmailUserName(_T(""))
	, m_csEmailUserPwd(_T(""))
{

}

AlarmDetectConfigDialog::~AlarmDetectConfigDialog()
{
}

void AlarmDetectConfigDialog::DoDataExchange(CDataExchange* pDX)
{
	CSkinDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_csEmailCaption);
	DDX_Text(pDX, IDC_EDIT2, m_csEmailContent);
	DDX_Text(pDX, IDC_EDIT3, m_csEmailFromAddr);
	DDX_Text(pDX, IDC_EDIT4, m_csEmailAddrTo);
	DDX_Text(pDX, IDC_EDIT5, m_csEmailUserName);
	DDX_Text(pDX, IDC_EDIT6, m_csEmailUserPwd);
}


BEGIN_MESSAGE_MAP(AlarmDetectConfigDialog, CSkinDialog)
	ON_BN_CLICKED(IDOK, &AlarmDetectConfigDialog::OnBnClickedOk)
END_MESSAGE_MAP()


// AlarmDetectConfigDialog 消息处理程序

BOOL AlarmDetectConfigDialog::OnInitDialog()
{
	CSkinDialog::OnInitDialog();

	// TODO:  在此添加额外的初始化
	this->UserDefSkin(false, true, true);
	this->SetBKSkinImage(_T("image\\bk_body.png"), BF_PNG);
	this->SetSysBtnImage(_T("image\\sys_btn_close (2).png"), BF_PNG, 3);
	this->SetSysBtnImage(_T("image\\sys_btn_max.png"), BF_PNG, 2);
	this->SetSysBtnImage(_T("image\\sys_btn_min.png"), BF_PNG, 1);
	this->SetCaptionImage(_T("image\\bk_body.png"), BF_PNG);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void AlarmDetectConfigDialog::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData();
	OnOK();
}
