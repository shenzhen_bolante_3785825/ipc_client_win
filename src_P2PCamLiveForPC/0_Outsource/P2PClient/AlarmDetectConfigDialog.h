#pragma once


// AlarmDetectConfigDialog 对话框

class AlarmDetectConfigDialog : public CSkinDialog
{
	DECLARE_DYNAMIC(AlarmDetectConfigDialog)

public:
	AlarmDetectConfigDialog(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~AlarmDetectConfigDialog();

// 对话框数据
	enum { IDD = IDD_ALARMDETECTCONFIGDIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
public:
	CString m_csEmailCaption;
public:
	CString m_csEmailContent;
public:
	CString m_csEmailFromAddr;
public:
	CString m_csEmailAddrTo;
public:
	CString m_csEmailUserName;
public:
	CString m_csEmailUserPwd;
public:
	afx_msg void OnBnClickedOk();
};
