// P2PClient.h : PROJECT_NAME 应用程序的主头文件
//

#pragma once

#ifndef __AFXWIN_H__
	#error "在包含此文件之前包含“stdafx.h”以生成 PCH 文件"
#endif

#include "resource.h"		// 主符号
#include "CppSQLite3.h"
#include "NvsModelPlanUI.h"
#include "VideoRecordPlanDialog.h"
// CP2PClientApp:
// 有关此类的实现，请参阅 P2PClient.cpp
//

#define WM_USER_UPDATE_DATA			WM_USER+10
#define WM_USER_UPDATE_NET_DATA		WM_USER+11

class CP2PClientApp : public CWinApp
{
public:
	CP2PClientApp();

// 重写
	public:
	virtual BOOL InitInstance();

	CppSQLite3DB		m_SQLite3DB;
	int					m_nCurUserID;
	bool				m_bFirstTime;
	CString				m_csUserName;
	CString				m_csUserPwd;

	ModelInfoList		m_modeList;//模板列表
	CString				m_csVideoRecordPath;//录像存储路径
	CString				m_csPictureCapturePath;
	UINT				m_uVideoFileTime;//录像大小

	//VideoRecordPlanDialog m_VideoRecordPlanDlg;

	UINT	m_nPort;
	CString	m_ServerName;

	HINSTANCE m_hResourceHandle;
	HINSTANCE m_hOldResourceHandle;

	int			m_nCurLanIndex;
	BOOL		m_bLanChange;

	CString m_csEmailCaption;
	CString m_csEmailContent;
	CString m_csEmailFromAddr;
	CString m_csEmailAddrTo;
	CString m_csEmailUserName;
	CString m_csEmailUserPwd;
// 实现

	DECLARE_MESSAGE_MAP()
public:
	virtual int ExitInstance();
};

extern CP2PClientApp theApp;