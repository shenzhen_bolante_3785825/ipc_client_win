// EquipmentSearchDialog.cpp : 实现文件
//

#include "stdafx.h"
#include "P2PClient.h"
#include "EquipmentSearchDialog.h"
#include "p2pavcore.h"
#include "DeviceInfoDialog.h"
#include "ChineseCode.h"

// CEquipmentSearchDialog 对话框

IMPLEMENT_DYNAMIC(CEquipmentSearchDialog, CSkinDialog)

CEquipmentSearchDialog::CEquipmentSearchDialog(CWnd* pParent /*=NULL*/)
	: CSkinDialog(CEquipmentSearchDialog::IDD, pParent)
	, m_bAddDevice(FALSE)
{

}

CEquipmentSearchDialog::~CEquipmentSearchDialog()
{
}

void CEquipmentSearchDialog::DoDataExchange(CDataExchange* pDX)
{
	CSkinDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SEARCH_DEV_LIST, m_SearchDevListCtrl);
	DDX_Control(pDX, ID_DEV_SEARCH_BTN, m_DevSearchBtn);
	DDX_Control(pDX, IDCANCEL, m_IDCancel);
	DDX_Control(pDX, ID_DEV_ADD_BTN, m_DevAddBtn);
}


BEGIN_MESSAGE_MAP(CEquipmentSearchDialog, CSkinDialog)
	ON_BN_CLICKED(ID_DEV_ADD_BTN, &CEquipmentSearchDialog::OnBnClickedDevAddBtn)
	ON_BN_CLICKED(ID_DEV_SEARCH_BTN, &CEquipmentSearchDialog::OnBnClickedDevSearchBtn)
END_MESSAGE_MAP()


// CEquipmentSearchDialog 消息处理程序

BOOL CEquipmentSearchDialog::OnInitDialog()
{
	CSkinDialog::OnInitDialog();

	// TODO:  在此添加额外的初始化
	this->UserDefSkin(false, true, true);
	this->SetBKSkinImage(_T("image\\bk_body.png"), BF_PNG);
	this->SetSysBtnImage(_T("image\\sys_btn_close (2).png"), BF_PNG, 3);
	this->SetSysBtnImage(_T("image\\sys_btn_max.png"), BF_PNG, 2);
	this->SetSysBtnImage(_T("image\\sys_btn_min.png"), BF_PNG, 1);
	this->SetCaptionImage(_T("image\\bk_body.png"), BF_PNG);

	DWORD dwStyle = m_SearchDevListCtrl.GetExtendedStyle();
	dwStyle |= LVS_EX_FULLROWSELECT;//选中某行使整行高亮（只适用与report风格的listctrl）
	dwStyle |= LVS_EX_GRIDLINES;//网格线（只适用与report风格的listctrl）
	//dwStyle |= LVS_EX_CHECKBOXES;//item前生成checkbox控件
	m_SearchDevListCtrl.SetExtendedStyle(dwStyle); //设置扩展风格

	CString strDeviceIP;
	strDeviceIP.LoadString(IDS_STR_SEARCH_DEVICE_IP);
	m_SearchDevListCtrl.InsertColumn(0, strDeviceIP, LVCFMT_CENTER, 180 );
	CString strDeviceID;
	strDeviceID.LoadString(IDS_STR_SEARCH_DEVICE_ID);
	m_SearchDevListCtrl.InsertColumn(1, strDeviceID, LVCFMT_CENTER, 300 );

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CEquipmentSearchDialog::OnBnClickedDevAddBtn()
{
	// TODO: 在此添加控件通知处理程序代码
	int nRow = m_SearchDevListCtrl.GetSelectionMark();
	if(nRow < 0)
	{
		CString strText;
		strText.LoadString(IDS_STR_DEV_SEARCH);
		CString strCaption;
		strCaption.LoadString(IDS_STR_DEV_CAPTION);
		MessageBox(strText, strCaption);
		//MessageBox("请选择搜索到的设备！", "搜索");
		return ;
	}
	CString strUID = m_SearchDevListCtrl.GetItemText(nRow, 1);
	CDeviceInfoDialog deviceDlg;
	deviceDlg.m_csDeivceUID		= strUID;
	if( IDOK == deviceDlg.DoModal())
	{
		try
		{
			theApp.m_SQLite3DB.open(_T("SystemDB"));

			CString csUpdateSQL;
			csUpdateSQL.Format(_T("select * from DeviceInfo where DeviceID = '%s'"), strUID);
			CppSQLite3Query SQLiteQuery = theApp.m_SQLite3DB.execQuery(csUpdateSQL.GetBuffer());
			if(!SQLiteQuery.eof())
			{
				//MessageBox("对不起，该设备已经存在！", "搜索");
				CString strText;
				strText.LoadString(IDS_STR_SEARCH_DEVICE_IN_USE);
				CString strCaption;
				strCaption.LoadString(IDS_STR_DEV_CAPTION);
				MessageBox(strText, strCaption, MB_ICONWARNING);
				SQLiteQuery.finalize();
				theApp.m_SQLite3DB.close();
				return ;
			}

			SQLiteQuery.finalize();

			int nID = 0;
			csUpdateSQL.Format(_T("select * from DeviceInfo order by uID desc"));
			SQLiteQuery = theApp.m_SQLite3DB.execQuery(csUpdateSQL.GetBuffer());
			if(!SQLiteQuery.eof())
			{
				nID = ::_ttoi(SQLiteQuery.fieldValue(_T("uID")));
			}
			SQLiteQuery.finalize();

			nID++;
			CString csDeviceName		= deviceDlg.m_csDeviceName;
			CString csDeviceUID			= deviceDlg.m_csDeivceUID;
			CString csDeviceUser		= deviceDlg.m_csDeviceUser;
			CString csDeivcePwd			= deviceDlg.m_csDevicePwd;

			csUpdateSQL.Format(_T("insert into DeviceInfo(uID, DeviceName, DeviceID, DeviceUser, DevicePwd, UserID) values(%d, '%s', '%s', '%s', '%s', %d)"), 
				nID, csDeviceName, csDeviceUID, csDeviceUser, csDeivcePwd, theApp.m_nCurUserID);

			theApp.m_SQLite3DB.execDML(csUpdateSQL.GetBuffer());
			theApp.m_SQLite3DB.close();
		}catch(...)
		{
			theApp.m_SQLite3DB.close();
		}

		m_bAddDevice = TRUE;
	}
	else
	{
		m_bAddDevice = FALSE;
	}
}

void CEquipmentSearchDialog::OnBnClickedDevSearchBtn()
{
	// TODO: 在此添加控件通知处理程序代码
	m_SearchDevListCtrl.DeleteAllItems();
	CORE_STLanSearchInfo lanSearchInfo[256];
	int nRet = apiGetLanObj(lanSearchInfo, 256, 2000);
	if(nRet < 0)
	{
		//MessageBox("搜索失败！", "搜索");
		CString strText;
		strText.LoadString(IDS_STR_SEARCH_ERROR);
		CString strCaption;
		strCaption.LoadString(IDS_STR_DEV_CAPTION);
		MessageBox(strText, strCaption, MB_ICONWARNING);
		return ;
	}

	int nRow = 0;
	for(int i = 0; i < nRet && nRet < 256; i++)
	{
		if(strlen(lanSearchInfo[i].UID) > 0)
		{
			
#ifdef UNICODE
			TCHAR sUID[64] = {0};
			TCHAR sIP[64] = {0};
			CChineseCode::Gb2312ToUnicode(sIP, lanSearchInfo[i].IP);
			CChineseCode::Gb2312ToUnicode(sUID, lanSearchInfo[i].UID);
			
			nRow = m_SearchDevListCtrl.InsertItem(i, sIP);//插入行
			m_SearchDevListCtrl.SetItemText(nRow, 1, sUID);
#else
			nRow = m_SearchDevListCtrl.InsertItem(i, lanSearchInfo[i].IP);//插入行
			m_SearchDevListCtrl.SetItemText(nRow, 1, lanSearchInfo[i].UID);
#endif
		}
	}

	//MessageBox("搜索完毕！", "搜索");
	CString strText;
	strText.LoadString(IDS_STR_SEARCH_END);
	CString strCaption;
	strCaption.LoadString(IDS_STR_DEV_CAPTION);
	MessageBox(strText, strCaption, MB_ICONWARNING);
}

