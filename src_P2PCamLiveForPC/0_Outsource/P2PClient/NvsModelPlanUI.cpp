#include "Stdafx.h"
#include "P2pClient.h"
#include "NvsModelPlanUI.h"

////////////////////////////////////////////////////////////////
//用于模板自画
#define FIELD_COLOR     RGB(0,255,0)
#define TEXT_COLOR      RGB(0,0,0)

void DrawTableRect(CDC *pDC, CRect grouprec)
{
    //初始化计划表空
    for (int u=0;u<MINIDOWNROWLINE ;u++)
    {
        for (int o=0;o<MINIDOWNCOLLINE;o++)						  
        {	            
            RECT nrect;
            CBrush brush(pDC->GetBkColor());
            nrect.bottom=grouprec.bottom-DTNEROWREC*u;
            nrect.top=grouprec.bottom-DTNEROWREC*(u+1);
            nrect.left=grouprec.right-DTNECOLREC*(o+1);
            nrect.right=grouprec.right-DTNECOLREC*o;
            CRect smallrec(&nrect);
            pDC->FillRect(&smallrec,&brush) ;
            //	DispalyTable(u,o,&brush);
        }	
    }
    
    //画行线
    for (int a=0;a<MINIDOWNROWLINE+2;a++)
    {			
        pDC->MoveTo(grouprec.left,grouprec.bottom-a*DTNEROWREC);
        pDC->LineTo(grouprec.right,grouprec.bottom-a*DTNEROWREC);			
    }		
    
    //画列线
    for (int c=0;c<MINIDOWNCOLLINE+1;c++)
    {
        //每两列改变色
        if (c%2==0)				
        {
            CPen pen2(PS_SOLID,1,RGB(0,0,255));
            pDC->SelectObject(&pen2);
            pDC->MoveTo(grouprec.right-DTNECOLREC*c,grouprec.bottom-MINIDOWNROWLINE*DTNEROWREC);
            pDC->LineTo(grouprec.right-DTNECOLREC*c,grouprec.bottom);
        }			
        else
        {
            CPen pen3(PS_SOLID,1,RGB(100,100,100));
            pDC->SelectObject(&pen3);
            pDC->MoveTo(grouprec.right-DTNECOLREC*c,grouprec.bottom-MINIDOWNROWLINE*DTNEROWREC);
            pDC->LineTo(grouprec.right-DTNECOLREC*c,grouprec.bottom);
        }				
	}  


    CPen pen1(PS_SOLID,2,RGB(100,100,100));
    pDC->SelectObject(&pen1);
    
    pDC->MoveTo(grouprec.right-DTNECOLREC*MINIDOWNCOLLINE, grouprec.bottom-DTNECOLREC*DTNEROWREC + 103);
    pDC->LineTo(grouprec.right-DTNECOLREC*MINIDOWNCOLLINE,grouprec.bottom);

    pDC->MoveTo(grouprec.left,grouprec.bottom-MINIDOWNROWLINE*DTNEROWREC);
    pDC->LineTo(grouprec.right,grouprec.bottom-MINIDOWNROWLINE*DTNEROWREC);
    
    int nLeft = grouprec.left + 21;
    int nTop = grouprec.bottom - 18;

    pDC->SetTextColor(TEXT_COLOR);

    //填星期表头
	if(theApp.m_nCurLanIndex == 0)
	{
		pDC->TextOut(nLeft,nTop,_T("星期六"));
		pDC->TextOut(nLeft,nTop-DTNEROWREC,_T("星期五"));
		pDC->TextOut(nLeft,nTop-DTNEROWREC*2,_T("星期四"));
		pDC->TextOut(nLeft,nTop-DTNEROWREC*3,_T("星期三"));
		pDC->TextOut(nLeft,nTop-DTNEROWREC*4,_T("星期二"));
		pDC->TextOut(nLeft,nTop-DTNEROWREC*5,_T("星期一"));
		pDC->TextOut(nLeft,nTop-DTNEROWREC*6,_T("星期日"));
	}
	else if(theApp.m_nCurLanIndex == 1)
	{
		pDC->TextOut(nLeft,nTop,_T("星期六"));
		pDC->TextOut(nLeft,nTop-DTNEROWREC,_T("星期五"));
		pDC->TextOut(nLeft,nTop-DTNEROWREC*2,_T("星期四"));
		pDC->TextOut(nLeft,nTop-DTNEROWREC*3,_T("星期三"));
		pDC->TextOut(nLeft,nTop-DTNEROWREC*4,_T("星期二"));
		pDC->TextOut(nLeft,nTop-DTNEROWREC*5,_T("星期一"));
		pDC->TextOut(nLeft,nTop-DTNEROWREC*6,_T("星期日"));
	}
	else if(theApp.m_nCurLanIndex == 2)
	{
		pDC->TextOut(nLeft,nTop,_T("Saturday"));
		pDC->TextOut(nLeft,nTop-DTNEROWREC,_T("Friday"));
		pDC->TextOut(nLeft,nTop-DTNEROWREC*2,_T("Thursday"));
		pDC->TextOut(nLeft,nTop-DTNEROWREC*3,_T("Wednesday"));
		pDC->TextOut(nLeft,nTop-DTNEROWREC*4,_T("Tuesday"));
		pDC->TextOut(nLeft,nTop-DTNEROWREC*5,_T("Monday"));
		pDC->TextOut(nLeft,nTop-DTNEROWREC*6,_T("Sunday"));
	}
    
    //填时间表头
    for (int d = 1;d < 24; d++)
    {   
        CString strl;
        strl.Format(_T("%d"),d);
        if (d>9)
        {
            pDC->TextOut(grouprec.left + 85 +DTNECOLREC*2*d,nTop-DTNEROWREC*MINIDOWNROWLINE,strl);
        } 
        else
        {
            pDC->TextOut(grouprec.left + 90 +DTNECOLREC*2*d,nTop-DTNEROWREC*MINIDOWNROWLINE,strl);
        }        
   }
}

void DrawTableData(CDC *pDC, CRect grouprec, ModelInfo * pModeInfo)
{
    for (int m=0;m<MINIDOWNROWLINE;m++)
    {
        for(int n=0;n<MINIDOWNCOLLINE;n++)
        {
            if(pModeInfo->recPlan[MINIDOWNROWLINE-m-1][MINIDOWNCOLLINE-n-1])
            {
                //显示指定模板的表格框	
                RECT nrect;
                CBrush brush(FIELD_COLOR);
                nrect.bottom=grouprec.bottom-DTNEROWREC*m;
                nrect.top=grouprec.bottom-DTNEROWREC*(m+1)+1;
                nrect.left=grouprec.right-DTNECOLREC*(n+1)+1;
                nrect.right=grouprec.right-DTNECOLREC*n;
                CRect smallrec(&nrect);
                pDC->FillRect(&smallrec,&brush) ; 
            }
        }
    }
}

void DrawModle(CDC *pDC, CRect grouprec, ModelInfo * pModeInfo)
{
    DrawTableRect(pDC, grouprec);
    DrawTableData(pDC, grouprec, pModeInfo);
}

void DrawTableField(CDC *pDC,  CPoint point, CRect grouprec, ModelInfo * pModelInfo, BOOL bFill)
{
    if ((grouprec.left<point.x&&point.x<grouprec.right)&&(grouprec.top<point.y&&point.y<grouprec.bottom))
    {               
        for (int i=0;i<MINIDOWNROWLINE ;i++)
        {
            for (int j=0;j<MINIDOWNCOLLINE ;j++)
            {
                if ((grouprec.right-DTNECOLREC*(j+1)<point.x&&point.x<grouprec.right-DTNECOLREC*j)
                    && (grouprec.bottom-DTNEROWREC*(i+1)<point.y&&point.y<grouprec.bottom-DTNEROWREC*i))
                { //点击在小矩形填充 
                    pModelInfo->recPlan[MINIDOWNROWLINE-i-1][MINIDOWNCOLLINE-j-1] = bFill;
                    if (pModelInfo->recPlan[MINIDOWNROWLINE-i-1][MINIDOWNCOLLINE-j-1])
                    {
                        RECT nrect;
                        CBrush brush(FIELD_COLOR);
                        nrect.bottom=grouprec.bottom-DTNEROWREC*i;
                        nrect.top=grouprec.bottom-DTNEROWREC*(i+1)+1;
                        nrect.left=grouprec.right-DTNECOLREC*(j+1)+1;
                        nrect.right=grouprec.right-DTNECOLREC*j;
                        CRect smallrec(&nrect);
                        pDC->FillRect(&smallrec,&brush) ; 									
                        //pModelInfo->recPlan[i][j]=TRUE;                                                        
                    } 
                    else									                            
                    {   

                        RECT nrect;
                        CBrush brush(pDC->GetBkColor());
                        CBrush *pBrush = CBrush::FromHandle((HBRUSH)GetStockObject(NULL_BRUSH));
                        nrect.bottom=grouprec.bottom-DTNEROWREC*i;
                        nrect.top=grouprec.bottom-DTNEROWREC*(i+1)+1;
                        nrect.left=grouprec.right-DTNECOLREC*(j+1)+1;
                        nrect.right=grouprec.right-DTNECOLREC*j;
                        CRect smallrec(&nrect);
                        pDC->FillRect(&smallrec,&brush) ; 									
                        //pModelInfo->recPlan[i][j]=FALSE;
                            
                    }
                    break;
                }
            }
        }
    }                
}