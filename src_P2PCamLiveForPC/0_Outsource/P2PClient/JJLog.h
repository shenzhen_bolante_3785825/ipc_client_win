#ifndef _JJ_LOG_H_
#define _JJ_LOG_H_

#include <windows.h>
#include <stdarg.h>
#include <stdio.h>

#define DEBUG_LOG 1
#define FILE_LOG 2
#define CONSOLE_LOG 4

#define JJLOG_DEBUG(s)      ("[��Ϣ]" s)
#define JJLOG_WARN(s)       ("[����]" s)
#define JJLOG_ALARM(s)      ("[����]" s)
#define JJLOG_ERROR(s)      ("[����]" s)


const int g_JJLog_ToDebug   = 1;
const int g_JJLog_ToFile    = 2;
const int g_JJLog_ToConsole = 4;

class CJJLog  
{
public:
	CJJLog();
    virtual ~CJJLog();

    static CJJLog* GetInstance()
    {
        if (NULL == m_pJJLog)
        {
            m_pJJLog = new CJJLog;
        }

        return m_pJJLog;
    }

    static void Free()
    {
        if (NULL != m_pJJLog)
        {
            delete m_pJJLog;
            m_pJJLog = NULL;
        }
    }

    void Print(int level, const char* format, ...);
    
    void SetLevel(int level);
	int  GetLevel() {return m_level;};

    // ToDebug/ToFile/ToConsole
    void SetMode(int mode);
	int  GetMode() {return m_mode;};

    void SetFile(const char* filename, bool append = false);

private:
    static CJJLog* m_pJJLog;    

    void ReallyPrint(const char* format, va_list ap);
	void OpenFile();
    void CloseFile();
    bool m_tofile, m_todebug, m_toconsole;
	int m_mode;
    int m_level;
    HANDLE m_hlogfile;
	LPSTR m_filename;
	bool m_append;
};


#define JJTrace CJJLog::GetInstance()->Print
#endif // _JJ_LOG_H_
