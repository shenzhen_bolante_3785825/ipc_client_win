#pragma once
#include "afxcmn.h"
#include "afxwin.h"


// CEquipmentSearchDialog 对话框

class CEquipmentSearchDialog : public CSkinDialog
{
	DECLARE_DYNAMIC(CEquipmentSearchDialog)

public:
	CEquipmentSearchDialog(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CEquipmentSearchDialog();

// 对话框数据
	enum { IDD = IDD_EQUIPMENTSEARCHDIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
public:
	CListCtrl m_SearchDevListCtrl;
public:
	CSkinButton m_DevSearchBtn;
public:
	CSkinButton m_IDCancel;
public:
	CSkinButton m_DevAddBtn;
public:
	afx_msg void OnBnClickedDevAddBtn();
public:
	afx_msg void OnBnClickedDevSearchBtn();
public:
	BOOL			m_bAddDevice;
};
