// RemoteConfigView.cpp : 实现文件
//

#include "stdafx.h"
#include "P2PClient.h"
#include "RemoteConfigView.h"


// CRemoteConfigView

IMPLEMENT_DYNCREATE(CRemoteConfigView, CHtmlView)

CRemoteConfigView::CRemoteConfigView()
{

}

CRemoteConfigView::~CRemoteConfigView()
{
}

void CRemoteConfigView::DoDataExchange(CDataExchange* pDX)
{
	CHtmlView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CRemoteConfigView, CHtmlView)
END_MESSAGE_MAP()


// CRemoteConfigView 诊断

#ifdef _DEBUG
void CRemoteConfigView::AssertValid() const
{
	CHtmlView::AssertValid();
}

void CRemoteConfigView::Dump(CDumpContext& dc) const
{
	CHtmlView::Dump(dc);
}
#endif //_DEBUG


// CRemoteConfigView 消息处理程序
