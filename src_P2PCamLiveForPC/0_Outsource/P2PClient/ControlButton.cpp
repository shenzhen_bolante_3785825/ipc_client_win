// ControlButton.cpp : 实现文件
//

#include "stdafx.h"
#include "P2PClient.h"
#include "ControlButton.h"
#include "AVIOCTRLDEFs.h"

// CControlButton

IMPLEMENT_DYNAMIC(CControlButton, CSkinButton)

CControlButton::CControlButton()
{
	m_objPlayStream = NULL;
}

CControlButton::~CControlButton()
{
}


BEGIN_MESSAGE_MAP(CControlButton, CSkinButton)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
END_MESSAGE_MAP()



// CControlButton 消息处理程序

void CControlButton::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if(m_objPlayStream)
	{
		SMsgAVIoctrlPtzCmd req;
		::memset(&req, 0, sizeof(req));
		req.control=m_controlCMD;
		int nRet=apiSendIOCtrl(m_objPlayStream, IOTYPE_USER_IPCAM_PTZ_COMMAND, (tkUCHAR *)&req, sizeof(req));
	}
	CSkinButton::OnLButtonDown(nFlags, point);
}

void CControlButton::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if(m_objPlayStream)
	{
		SMsgAVIoctrlPtzCmd req;
		memset(&req, 0, sizeof(req));
		req.control=AVIOCTRL_PTZ_STOP;
		int nRet=apiSendIOCtrl(m_objPlayStream, IOTYPE_USER_IPCAM_PTZ_COMMAND, (tkUCHAR *)&req, sizeof(req));
	}
	CSkinButton::OnLButtonUp(nFlags, point);
}

void CControlButton::SetPlayStreamObject(tkPVOID objStream, unsigned char control)
{
	m_objPlayStream = objStream;
	m_controlCMD = control;
}
