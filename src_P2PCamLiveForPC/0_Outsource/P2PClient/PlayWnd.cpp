// PlayWnd.cpp : implementation file
//

#include "stdafx.h"
#include "P2PClient.h"
#include "PlayWnd.h"
#include "ScreenPannel.h"
#include "adpcm.h"
#include "wave_out.h"
#include "AVDecoderLib.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern tkPVOID			m_objPlayStream2;

#define GET_CONFIG_TIMER    100
#define GET_CONFIG_INTERVAL 5000

void CALLBACK DrawFun(long nPort,HDC hDc,long nUser)
{
    RECT *rc = (RECT *)nUser;
    if(rc == NULL || nPort >= OS_MAX_PORT)
        return ;

   
}

/////////////////////////////////////////////////////////////////////////////
// CPlayWnd dialog
CPlayWnd::CPlayWnd()
	:m_nWndID(0)
{
	memset(&m_splitInfo, 0, sizeof(m_splitInfo));
	m_nPreSplit = 0;
	m_hwndTT = 0;
	
	m_nDspIndex = 0;
	m_nOutput = -1;
	//{{AFX_DATA_INIT(CPlayWnd)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
    
    InitializeCriticalSection(&m_operCS);
    m_nCodeType = 0;
    m_nPicQuality = 3;
    m_nSize = 0;
    m_iChannel = 1;
    m_bRecord = FALSE;
    m_nMatrixHandle = -1;
    m_bGetConfig = FALSE;

    m_bOperPtz = FALSE;
    m_bNeedStart = FALSE;

    m_moveCur = 0;//LoadCursor(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDC_CURSOR_MHAND));
    m_nPlayPort = -1;
    m_bAlarmRec = FALSE;

	m_objPlayStream = NULL;

	m_pH264DecObj = NULL;

	//m_pBufBmp24 = new BYTE[2765800];
	m_pBufBmp24 = NULL;

	m_objShow = NULL;

	m_bPlaying = FALSE;
	m_bVoice	= FALSE;
	m_bListen	= FALSE;
	memset(m_sDevID, 0, 64);

	m_csErrorCode = "欢迎使用";

	m_pBufAudio=new BYTE[MAXSIZE_PCM_DATA];

	m_bmiHead.biSize  = sizeof(BITMAPINFOHEADER);
	m_bmiHead.biWidth = 0;
	m_bmiHead.biHeight= 0;
	m_bmiHead.biPlanes= 1;
	m_bmiHead.biBitCount = 24;
	m_bmiHead.biCompression = BI_RGB;
	m_bmiHead.biSizeImage   = 0;
	m_bmiHead.biXPelsPerMeter = 0;
	m_bmiHead.biYPelsPerMeter = 0;
	m_bmiHead.biClrUsed       = 0;
	m_bmiHead.biClrImportant  = 0;

	m_lStreamPlayer = 0;
}

CPlayWnd::~CPlayWnd()
{
	m_bPlaying = FALSE;
	StopStream();
}

BEGIN_MESSAGE_MAP(CPlayWnd, CWnd)
	//{{AFX_MSG_MAP(CPlayWnd)
	ON_WM_ERASEBKGND()
//	ON_WM_CONTEXTMENU()
    ON_WM_TIMER()
	ON_WM_CREATE()
	ON_WM_INITMENU()
	ON_WM_PAINT()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
	ON_COMMAND_RANGE(VIDEO_MENU_BASE, VIDEO_MENU_END, OnVideoMenu)
	ON_MESSAGE(VIDEO_REPAINT, OnRepaintWnd)
	//}}AFX_MSG_MAP
	ON_WM_CONTEXTMENU()
	ON_WM_DESTROY()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPlayWnd message handlers
BOOL CPlayWnd::OnEraseBkgnd(CDC* pDC) 
{
	// TODO: Add your message handler code here and/or call default
	CRect rt;

	GetClientRect(&rt);

	CBrush br;
	br.CreateSolidBrush(VIDEO_BACK_COLOR);
	pDC->FillRect(&rt,&br);
	return CWnd::OnEraseBkgnd(pDC);
}

void CPlayWnd::ShowToolTips(TCHAR * strInfo)
{
	INITCOMMONCONTROLSEX iccex; 
	// struct specifying info about tool in ToolTip control
	TOOLINFO ti;
	unsigned int uid = 0;       // for ti initialization
	TCHAR strTT[256];
	LPTSTR lptstr = strTT;
	RECT rect;                  // for client area coordinates
	
	_tcscpy(strTT, strInfo);

	/* INITIALIZE COMMON CONTROLS */
	iccex.dwICC = ICC_WIN95_CLASSES;
	iccex.dwSize = sizeof(INITCOMMONCONTROLSEX);
	InitCommonControlsEx(&iccex);
	
	/* CREATE A TOOLTIP WINDOW */
	if(m_hwndTT != 0)
	{	
		::DestroyWindow(m_hwndTT);
		m_hwndTT = NULL;
	}

	m_hwndTT = CreateWindowEx(WS_EX_TOPMOST,
		TOOLTIPS_CLASS,
		NULL,
		WS_POPUP | TTS_NOPREFIX | TTS_ALWAYSTIP,		
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		this->m_hWnd,
		NULL,
		AfxGetApp()->m_hInstance,
		NULL
		);
	
	::SetWindowPos(m_hwndTT,
		HWND_TOPMOST,
		0,
		0,
		0,
		0,
		SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);
			
	GetClientRect (&rect);
					
	ti.cbSize = sizeof(TOOLINFO);
	ti.uFlags = TTF_SUBCLASS;
	ti.hwnd = this->m_hWnd;
	ti.hinst = AfxGetApp()->m_hInstance;
	ti.uId = uid;
	ti.lpszText = lptstr;
	// ToolTip control will cover the whole window
	ti.rect.left = rect.left;    
	ti.rect.top = rect.top;
	ti.rect.right = rect.right;
	ti.rect.bottom = rect.bottom;
	
	/* SEND AN ADDTOOL MESSAGE TO THE TOOLTIP CONTROL WINDOW */
	::SendMessage(m_hwndTT, TTM_ADDTOOL, 0, (LPARAM) (LPTOOLINFO) &ti);	
}

LRESULT CPlayWnd::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	//Handle key lock
	CScreenPannel *pContainer = (CScreenPannel *)GetParent();
	if(pContainer)
	{
		switch(message)
		{
		case WM_LBUTTONDOWN:
		case WM_RBUTTONDOWN:
            {
				 pContainer->SetActivePage(this);
				::PostMessage(pContainer->GetParent()->GetSafeHwnd(),MSG_CHANGEPLAYWND,(WPARAM)m_objPlayStream,0);
            }
			break;
        case WM_LBUTTONUP:
        case WM_MOUSELEAVE:
            if(m_bOperPtz)
            {
                
            }
            m_bNeedStart = FALSE;
            break;
		case WM_LBUTTONDBLCLK:
			{
				::PostMessage(pContainer->GetParent()->GetSafeHwnd(),MSG_MULTISCREEN,0,0);
			}
			break;
        case WM_MOUSEWHEEL:
            {

            }
            break;
        case WM_MOUSEMOVE:
            {
                
            }
        case WM_SIZE:

            GetClientRect(&m_wndRect);
            m_wndRect.top = m_wndRect.bottom*2 /3;
            break;
		case WM_KEYDOWN:
			{

			}
			break;
		default:
			break;
		}
	}
	return CWnd::DefWindowProc(message, wParam, lParam);
}

#define NAME_MENU_CYCLE_DVR					_T("轮询DVR")
#define NAME_MENU_CYCLE_GROUP				_T("轮询组")
#define NAME_MENU_CYCLE_STOP				_T("停止轮询")

#define NAME_MENU_FULLSCREEN				_T("全屏显示")
#define NAME_MENU_MULTISCREEN				_T("多屏显示")
#define NAME_MENU_AUTOADJUST				_T("自动调整")

#define NAME_MENU_OPENSOUND				    _T("打开监听")
#define NAME_MENU_CLOSESOUND				_T("关闭监听")
#define NAME_MENU_CLOSESTREAM				_T("关闭视频")

#define NAME_MENU_STARTTALK				    _T("启动对讲")
#define NAME_MENU_STOPTALK					_T("停止对讲")

#define NAME_MENU_CODETYPE					_T("码流类型")
#define NAME_MENU_PICQUILTY					_T("图像质量")
#define NAME_MENU_LOCK				        _T("PTZ锁定")
#define NAME_MENU_UNLOCK				    _T("PTZ解锁")
#define NAME_MENU_SIZE                      _T("图像大小")

#define NAME_MENU_TAKE_PIC					_T("拍照")

void CPlayWnd::OnContextMenu(CWnd* /*pWnd*/, CPoint point)
{
	CScreenPannel *pContainer = (CScreenPannel *)GetParent();
	// TODO: 在此处添加消息处理程序代码
	CMenu playMenu;
	playMenu.CreatePopupMenu();
	if (m_splitInfo.Type == SPLIT_TYPE_MONITOR)
	{
		playMenu.AppendMenu(MF_STRING , VIDEO_MENU_CLOSESTREAM, NAME_MENU_CLOSESTREAM);

		if (m_bVoice == TRUE)
		{
			playMenu.AppendMenu(MF_STRING , VIDEO_MENU_STOP_TALK, NAME_MENU_STOPTALK);
		}
		else
		{
			playMenu.AppendMenu(MF_STRING, VIDEO_MENU_START_TALK, NAME_MENU_STARTTALK);
		}

		if (m_bListen == TRUE)
		{
			playMenu.AppendMenu(MF_STRING , VIDEO_MENU_STOP_LISTEN, NAME_MENU_CLOSESOUND);
		}
		else
		{
			playMenu.AppendMenu(MF_STRING, VIDEO_MENU_START_LISTEN, NAME_MENU_OPENSOUND);
		}

		playMenu.AppendMenu(MF_STRING, VIDEO_MENU_TAKE_PIC, NAME_MENU_TAKE_PIC);
	}
	else
	{
		playMenu.AppendMenu(MF_STRING|MF_DISABLED | MF_GRAYED , VIDEO_MENU_CLOSESTREAM, NAME_MENU_CLOSESTREAM);
		playMenu.AppendMenu(MF_STRING |MF_DISABLED | MF_GRAYED, VIDEO_MENU_START_TALK, NAME_MENU_STARTTALK);
		playMenu.AppendMenu(MF_STRING |MF_DISABLED | MF_GRAYED, VIDEO_MENU_START_LISTEN, NAME_MENU_OPENSOUND);
		playMenu.AppendMenu(MF_STRING |MF_DISABLED | MF_GRAYED, VIDEO_MENU_TAKE_PIC, NAME_MENU_TAKE_PIC);
		
	}
	playMenu.AppendMenu(MF_STRING | pContainer->GetFullScreen()    ? MF_CHECKED : MF_UNCHECKED, VIDEO_MENU_FULLSCREEN, NAME_MENU_FULLSCREEN);
	playMenu.TrackPopupMenu( TPM_LEFTALIGN,point.x,point.y,this);

}

void CPlayWnd::OnVideoMenu(UINT nID)
{
	CScreenPannel *pContainer = (CScreenPannel *)GetParent();
	switch(nID)
	{
	case VIDEO_MENU_CLOSESTREAM:
		{
			StopStream();
			::PostMessage(pContainer->GetParent()->GetSafeHwnd(),VIDEO_MENU_CLOSESTREAM, (WPARAM)m_sDevID,0);
		}
		break;
	case VIDEO_MENU_START_TALK:
		if( m_objPlayStream )
		{
			apiStartAudio(m_objPlayStream);
			apiStartSpeak(m_objPlayStream, MEDIA_CODEC_AUDIO_ADPCM);
			m_bVoice = TRUE;
		}
		//::PostMessage(pContainer->GetParent()->GetSafeHwnd(),VIDEO_MENU_START_TALK,(WPARAM)this,0);
		break;
	case VIDEO_MENU_STOP_TALK:
		if(m_objPlayStream)
		{
			apiStopAudio(m_objPlayStream);
			apiStopSpeak(m_objPlayStream);
			WIN_Audio_reset();
			m_bVoice = FALSE;
		}
		//::PostMessage(pContainer->GetParent()->GetSafeHwnd(),VIDEO_MENU_STOP_TALK,(WPARAM)this,0);
		break;
	case VIDEO_MENU_FULLSCREEN:
		//::PostMessage(pContainer->GetParent()->GetSafeHwnd(),VIDEO_MENU_FULLSCREEN,(WPARAM)this,0);
		pContainer->SetFullScreen(!pContainer->GetFullScreen());;
		break;
	case VIDEO_MENU_START_LISTEN:
		{
			if(m_objPlayStream)
			{
				apiStartAudio(m_objPlayStream);
				m_bListen = TRUE;
			}
		}
		break;
	case VIDEO_MENU_STOP_LISTEN:
		{
			if(m_objPlayStream)
			{
				apiStopAudio(m_objPlayStream);
				m_bListen = FALSE;
			}
		}
		break;
	case VIDEO_MENU_TAKE_PIC:
		{
			if(m_lStreamPlayer > 0)
			{
				SYSTEMTIME sysTime;
				GetSystemTime(&sysTime);
				TCHAR sFilePath[256] = {0};
				TCHAR sFilePathDirectory[256] = {0};
				TCHAR sCurName[256] = {0};
				TCHAR szFullPath[MAX_PATH] = {0};
				TCHAR szDir[MAX_PATH] = {0};
				TCHAR szDrive[MAX_PATH] = {0};
				::GetModuleFileName(NULL,szFullPath,MAX_PATH); 
#ifdef UNICODE
				_tsplitpath_s(szFullPath,szDrive,sizeof(szDrive),szDir,sizeof(szDir),NULL,NULL,NULL,NULL);
				_stprintf_s(sFilePath, _T("%s%s\\CaptureImage\\%04d_%04d_%04d_%02d_%02d_%02d.jpg"),
					szDrive, szDir, sysTime.wYear, sysTime.wMonth, sysTime.wDay, sysTime.wHour, sysTime.wMinute, sysTime.wSecond);
				_stprintf_s(sFilePathDirectory, _T("%s%s\\CaptureImage"), szDrive, szDir);
#else
				_splitpath_s(szFullPath,szDrive,sizeof(szDrive),szDir,sizeof(szDir),NULL,NULL,NULL,NULL);
				if(theApp.m_csPictureCapturePath.GetLength() == 0)
				{
					sprintf_s(sFilePath,sizeof(char)*256,"%s%s\\CaptureImage\\%04d_%04d_%04d_%02d_%02d_%02d.jpg",
						szDrive, szDir, sysTime.wYear, sysTime.wMonth, sysTime.wDay, sysTime.wHour, sysTime.wMinute, sysTime.wSecond);
					sprintf_s(sFilePathDirectory, sizeof(char)*256,"%s%s\\CaptureImage", szDrive, szDir);
				}
				else
				{
					sprintf_s(sFilePath,sizeof(char)*256,"%s\\%04d_%02d_%02d_%02d_%02d_%02d.jpg",
						theApp.m_csPictureCapturePath.GetBuffer(), sysTime.wYear, sysTime.wMonth, sysTime.wDay, sysTime.wHour, sysTime.wMinute, sysTime.wSecond);
					sprintf_s(sFilePathDirectory, sizeof(char)*256,"%s", theApp.m_csPictureCapturePath.GetBuffer());
				}
#endif
				//
				WIN32_FIND_DATA wfd;
				HANDLE hFind= FindFirstFile(sFilePathDirectory, &wfd);
				if((hFind!=INVALID_HANDLE_VALUE)&&
					(wfd.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY))
				{
					
				}
				else
				{
					::CreateDirectory(sFilePathDirectory, NULL);
				}
				FindClose(hFind);

				SaveBitmap(m_lStreamPlayer, sFilePath);
			}
		}
		break;
	}
}

BOOL CPlayWnd::GetSplitInfo(SplitInfoNode* info)
{
	if (!info)
	{
		return FALSE;
	}
	
	memcpy(info, &m_splitInfo, sizeof(SplitInfoNode));	

	return TRUE;
}

BOOL CPlayWnd::SetSplitInfo(SplitInfoNode* info)
{
	if (!info)
	{
		return FALSE;
	}
	
	memcpy(&m_splitInfo, info, sizeof(SplitInfoNode));
	
	return TRUE;
}

LRESULT CPlayWnd::OnRepaintWnd(WPARAM wParam, LPARAM lParam)
{
	Invalidate();
	UpdateWindow();

	return 0;
}

BOOL CPlayWnd::DestroyWindow() 
{	
    DeleteCriticalSection(&m_operCS);

	return CWnd::DestroyWindow();
}


void CPlayWnd::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	if( !IsPlaying() && m_csErrorCode.GetLength())
	{
		dc.SetBkMode(TRANSPARENT);
		CRect rcClient;
		GetClientRect(&rcClient);
		dc.SetTextAlign(dc.GetTextAlign() | TA_CENTER | VTA_CENTER);
		dc.TextOut(rcClient.Width()/2, rcClient.Height()/2, m_csErrorCode, m_csErrorCode.GetLength());
	}
	// Do not call CWnd::OnPaint() for painting messages
}


//2010-07-01 窗口分屏切换
void CPlayWnd::OnSize(UINT nType, int cx, int cy) 
{
	CWnd::OnSize(nType, cx, cy);

	CWnd * pWnd = GetWindow (GW_CHILD);
    if(pWnd != NULL)
    {
        CRect rc;
        GetClientRect(&rc);

        pWnd->MoveWindow(rc);
        pWnd->Invalidate();
    }	
}

int CPlayWnd::PlayStream(tkPVOID objPlayStream, TCHAR *sDevID)
{
	_tcscpy(m_sDevID, sDevID);
	m_objPlayStream = objPlayStream;
	m_splitInfo.Type = SPLIT_TYPE_MONITOR;
	m_bPlaying = TRUE;
	m_lStreamPlayer = InitialPlayer(GetSafeHwnd());
	m_csErrorCode = _T("欢迎使用");
	return 0;
}

int CPlayWnd::StopStream()
{
	m_csPlayInfo = "";
	m_bPlaying = FALSE;

	UninitalPlayer(m_lStreamPlayer);
	m_lStreamPlayer = 0;

	/*if(m_pH264DecObj != NULL)
	{
		apiH264ReleaseObj((tkPVOID*)m_pH264DecObj);
		m_pH264DecObj = NULL;
	}*/

	if(m_objPlayStream != NULL)
	{
		apiStopVideo(m_objPlayStream);
		m_objPlayStream = NULL;
		m_splitInfo.Type = SPLIT_TYPE_NULL;
	}
	Invalidate();
	return 0;
}

int CPlayWnd::PlayRecord()
{
	m_lStreamPlayer = InitialPlayer(GetSafeHwnd());
	return 0;
}

int CPlayWnd::StopRecord()
{
	UninitalPlayer(m_lStreamPlayer);
	m_lStreamPlayer = 0;
	return 0;
}

int CPlayWnd::OnRecordAVFrame(TCHAR *pData, int nSize)
{
	if(m_lStreamPlayer <= 0)
		return -1;
	FRAMEINFO_t stFrmInfo;
	stFrmInfo.codec_id = MEDIA_CODEC_VIDEO_H264;
	AddFrameData(m_lStreamPlayer, (BYTE*)pData, nSize, &stFrmInfo);
	//if(m_pH264DecObj == NULL)
	//{
	//	m_pH264DecObj = apiH264CreateObj(1280, 720, ROTATION_NONE);
	//}
	//int nRet = apiH264Decode(m_pH264DecObj, (tkUCHAR *)pData, nSize, &m_outFrame);
	//if(nRet<0) {
	//	
	//	return -1;
	//}
	//if(m_pBufBmp24 == NULL)
	//	m_pBufBmp24 = new BYTE[m_outFrame.nWidth*m_outFrame.nHeight*3];

	//conv.YV12_to_RGB24((PBYTE)m_outFrame.pY,(PBYTE)m_outFrame.pU,
	//				(PBYTE)m_outFrame.pV,(PBYTE)m_pBufBmp24, m_outFrame.nWidth, m_outFrame.nHeight);
	///*bool bGet=YUV420toRGB24(m_outFrame.pY,m_outFrame.pU,m_outFrame.pV, m_pBufBmp24, m_outFrame.nWidth, m_outFrame.nHeight);

	//if(bGet) */{

	//	m_bmiHead.biWidth		= m_outFrame.nWidth;
	//	m_bmiHead.biHeight		= m_outFrame.nHeight;
	//	m_bmiHead.biSizeImage	= m_outFrame.nWidth*m_outFrame.nHeight*3;

	//	VShow_H264(m_pBufBmp24, &m_bmiHead);
	//	if(m_pBufBmp24)
	//	{
	//		delete []m_pBufBmp24;
	//		m_pBufBmp24 = NULL;
	//	}
	//}
	return 0;
}

tkVOID WINAPI CPlayWnd::OnAVFrame(tkCHAR *pData, tkINT32 nSize, tkVOID *pFrmInfo, tkINT32 nFrmInfoSize, tkUINT32 nTag, tkVOID *param)
{
#ifdef _DEBUG
	TRACE(_T("CPlayWnd::OnAVFrame\n"));
#endif
	CPlayWnd *pThis = (CPlayWnd *)param;

	if( pData == NULL || pFrmInfo == NULL || nSize <= 0) return;

	FRAMEINFO_t *pstFrmInfo =( FRAMEINFO_t *)pFrmInfo;
	switch(pstFrmInfo->codec_id)
	{
		case MEDIA_CODEC_VIDEO_H264:
			{
				pThis->UpdatePlayInfo();
				AddFrameData(pThis->m_lStreamPlayer, (BYTE*)pData, nSize, pFrmInfo);

				//OUT_FRAME outFrame;
				//memset(&outFrame, 0, sizeof(OUT_FRAME));
				/*if(pThis->m_pH264DecObj == NULL)
				{
					pThis->m_pH264DecObj = apiH264CreateObj(1280, 720, ROTATION_NONE);
				}
				int nRet = apiH264Decode(pThis->m_pH264DecObj, (tkUCHAR *)pData, nSize, &pThis->m_outFrame);
				if(nRet<0) {
					TRACE(_T("AVPlay, apiH264Decode=%d,cam%d flags=%d, nSize=%d, %dX%d\n"), 
						nRet, nTag, pstFrmInfo->flags, nSize, 
						pThis->m_outFrame.nWidth, pThis->m_outFrame.nHeight);
					return;
				}

				if(pThis->m_pBufBmp24 == NULL)
					pThis->m_pBufBmp24 = new BYTE[pThis->m_outFrame.nWidth*pThis->m_outFrame.nHeight*3];

				pThis->conv.YV12_to_RGB24((PBYTE)pThis->m_outFrame.pY,(PBYTE)pThis->m_outFrame.pU,
					(PBYTE)pThis->m_outFrame.pV,(PBYTE)pThis->m_pBufBmp24, pThis->m_outFrame.nWidth, pThis->m_outFrame.nHeight);

				pThis->m_bmiHead.biWidth		= pThis->m_outFrame.nWidth;
				pThis->m_bmiHead.biHeight		= pThis->m_outFrame.nHeight;
				pThis->m_bmiHead.biSizeImage	= pThis->m_outFrame.nWidth*pThis->m_outFrame.nHeight*3;

				pThis->VShow_H264(pThis->m_pBufBmp24, &pThis->m_bmiHead);
				if(pThis->m_pBufBmp24)
				{
					delete []pThis->m_pBufBmp24;
					pThis->m_pBufBmp24 = NULL;
				}*/
			}
			break;

		case MEDIA_CODEC_VIDEO_MJPEG:
			pThis->VShow_JPEG((BYTE *)pData, nSize);
			break;

		case MEDIA_CODEC_AUDIO_PCM:
			{
				AddFrameData(pThis->m_lStreamPlayer, (BYTE*)pData, nSize, pFrmInfo);
				//WIN_Play_Samples(pData, nSize);
			}
			break;

		case MEDIA_CODEC_AUDIO_ADPCM:
			{
				AddFrameData(pThis->m_lStreamPlayer, (BYTE*)pData, nSize, pFrmInfo);
				/*int nPCMSize=nSize*4;
				Decode((char *)pData, nSize, (char *)pThis->m_pBufAudio);
				TRACE(_T("AVPlay, cam%d MEDIA_CODEC_AUDIO_ADPCM, nSize=%d, nPCMSize=%d\n"), nTag, nSize, nPCMSize);
				int nRet = WIN_Play_Samples(pThis->m_pBufAudio, nPCMSize);
				if(nRet < 0)
				{
				int i = 0;
				}*/
			}
			break;

		case MEDIA_CODEC_AUDIO_G726:
			{
				TRACE(_T("AVPlay, cam%d MEDIA_CODEC_AUDIO_G726, nSize=%d\n"), nTag, nSize);
			}break;

		default:;
	}
}

bool CPlayWnd::IsPlaying()
{
	return m_bPlaying;
}

void CPlayWnd::UpdatePlayInfo()
{
	static CString CONN_MODE[]={_T("P2P"), _T("RLY"), _T("LAN")};
	tkUCHAR nOnlineNum=0, nConnMode=0;
	tkFLOAT fFrameRate=0.0f, fBytesRate=0.0f;
	apiGetObjInfo(m_objPlayStream, GOI_OnlineNum, &nOnlineNum);
	apiGetObjInfo(m_objPlayStream, GOI_ConnectMode, &nConnMode);
	apiGetObjInfo(m_objPlayStream, GOI_FrameRate, &fFrameRate);
	apiGetObjInfo(m_objPlayStream, GOI_BytesRate, &fBytesRate);
	m_csPlayInfo.Format(_T("%s  Online=%d  %0.2f FPS  %0.3f kbps"), 
		CONN_MODE[nConnMode], nOnlineNum, fFrameRate,fBytesRate*8);
	UpdateOSDData(m_lStreamPlayer, m_csPlayInfo.GetBuffer(), m_csPlayInfo.GetLength());
}

//废弃
void CPlayWnd::VShow_JPEG(BYTE *pData, int nDataSize)
{
	if(m_objShow == NULL) m_objShow = new CPicture();

	CRect rcClient;
    GetClientRect(&rcClient);
	CDC *pDC = this->GetDC();
	if(m_objShow->PushData(pData, nDataSize)) {
		if(m_objShow->Show(pDC, &rcClient)){
			m_outFrame.nWidth =m_objShow->GetVWidth();
			m_outFrame.nHeight=m_objShow->GetVHeight();
		}
	}
}

//废弃
void CPlayWnd::VShow_H264(BYTE *pData, BITMAPINFOHEADER *pBmiHead)
{
	if(m_bPlaying == FALSE) return;
	if(m_objShow == NULL) m_objShow = new CPicture();
	
	if(this->GetSafeHwnd() == FALSE)
		return ;
	CRect rcClient;
    GetClientRect(&rcClient);
	CDC *pDC = this->GetDC();
	if(m_objShow->PushData(pData, pBmiHead))
	{
		BOOL bShow = m_objShow->Show(pDC, &rcClient);

		static CString CONN_MODE[]={_T("P2P"), _T("RLY"), _T("LAN")};
		tkUCHAR nOnlineNum=0, nConnMode=0;
		tkFLOAT fFrameRate=0.0f, fBytesRate=0.0f;
		apiGetObjInfo(m_objPlayStream, GOI_OnlineNum, &nOnlineNum);
		apiGetObjInfo(m_objPlayStream, GOI_ConnectMode, &nConnMode);
		apiGetObjInfo(m_objPlayStream, GOI_FrameRate, &fFrameRate);
		apiGetObjInfo(m_objPlayStream, GOI_BytesRate, &fBytesRate);
		m_csPlayInfo.Format(_T("%s  %dX%d  Online=%d  %0.2f FPS  %0.3f kbps\n"), 
			CONN_MODE[nConnMode],
			m_outFrame.nWidth, m_outFrame.nHeight, nOnlineNum, fFrameRate,fBytesRate*8);
	
		HDC hDc = ::GetDC(GetSafeHwnd());
		if(m_csPlayInfo.GetLength())
		{
			SetBkMode(hDc, TRANSPARENT);
			TextOut(hDc, 0, 0, m_csPlayInfo, m_csPlayInfo.GetLength());
		}
	}
}

//废弃
tkVOID WINAPI CPlayWnd::OnStatus(tkINT32 nType, tkINT32 nValue, tkPVOID pObj, tkUINT32 nTag, tkVOID *param)
{
	CPlayWnd *pThis=(CPlayWnd *)param;

	TRACE(_T("OnStatus, cam%d, nType=%d, nValue=%d, param=0x%X\n"), 
			  nTag, nType, nValue, param);
	switch(nValue)
	{
		case SINFO_CONNECTING:
			
			break;

		case SINFO_CONNECTED:
			apiRegCB_OnAVFrame(pThis->m_objPlayStream, OnAVFrame, pThis);
 			apiStartVideo(pThis->m_objPlayStream);
			pThis->m_bPlaying = TRUE;
			//InitiaPlayer(432, 240);
			//StartPlayer(pThis->GetSafeHwnd());
			break;

		case SINFO_DISCONNECTED:
			
			break;
		default:
			break;
	}
}

//废弃
tkVOID WINAPI CPlayWnd::OnRecvIOCtrl(tkINT32 nIOType, tkCHAR *pIOData, tkINT32 nIODataSize, tkPVOID pObj, tkUINT32 nTag, tkVOID *param)
{
	TRACE(_T("OnRecvIOCtrl, nIOType=0x%X, pObj=0x%X, cam%d, param=0x%X\n"), nIOType, pObj, nTag, param);
	CPlayWnd *pThis=(CPlayWnd *)param;
}

void CPlayWnd::OnDestroy()
{
	CWnd::OnDestroy();

	// TODO: 在此处添加消息处理程序代码
	StopStream();
}
