#pragma once
#include "P2PAVCore.h"

typedef tkVOID(CALLBACK *fOnAVFrame)(tkCHAR *pData, tkINT32 nSize, tkVOID *pFrmInfo, tkINT32 nFrmInfoSize, tkUINT32 nTag, tkVOID *param);

class RealStreamManager
{
public:
	RealStreamManager(void);
public:
	virtual ~RealStreamManager(void);

private:
	static tkVOID WINAPI OnAVFrame(tkCHAR *pData, tkINT32 nSize, tkVOID *pFrmInfo, tkINT32 nFrmInfoSize, tkUINT32 nTag, tkVOID *param);
public:
	int		PlayStream(tkPVOID objPlayStream, fOnAVFrame subOnAVFrame, void *param);

	int		StopStream();

	int		StopRecVideo();

	void	SetRecIndex(int nIndex, TCHAR *sDeviceID, TCHAR *sDeviceName);

	void	DealRecVideo(tkCHAR *pData, tkINT32 nSize, tkVOID *pFrmInfo, tkINT32 nFrmInfoSize, tkUINT32 nTag, tkVOID *param);
private:
	fOnAVFrame			m_fOnAVFrame;

	void				*m_pParam;

	int					m_nRecIndex;

	DWORD				m_dwLeastSpace;

	FILE				*m_pRecordFile;

	DWORD				m_dwStartRecordTime;

	CString				m_csStartRecordTime;
	CString				m_csEndRecordTime;

	TCHAR				m_sDeviceID[256];
	TCHAR				m_sDeviceName[256];

	TCHAR				m_sVideoRecordPath[256];

	BOOL				m_bPlaying;

	BOOL				m_bRecording;

	tkPVOID				m_objPlayStream;
};
