#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include <string>
#include "P2PAVCore.h"
#include "AVIOCTRLDEFs.h"
using namespace std;
// CEquipmentConfigDialog 对话框


#define WM_USER_UDPATE_DATA	WM_USER+11111

class CEquipmentConfigDialog : public CSkinDialog
{
	DECLARE_DYNAMIC(CEquipmentConfigDialog)

public:
	CEquipmentConfigDialog(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CEquipmentConfigDialog();

// 对话框数据
	enum { IDD = IDD_EQUIPMENTCONFIGDIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
public:
	CSkinButton m_ModifyDevModeBtn;
public:
	CSkinButton m_ModifyDevPwdBtn;
public:
	CSkinButton m_ModifyWifiMsgBtn;
public:
	afx_msg void OnBnClickedModifyDevPwdBtn();
public:
	afx_msg void OnBnClickedModifyDevModeBtn();
public:
	afx_msg void OnBnClickedModifyWifiMsgBtn();
public:
	CSkinButton m_IDOKBtn;
public:
	CSkinButton m_IDCancelBtn;
public:
	CString m_csIPCamModeEdit;
public:
	CString m_csManufacturerEdit;
public:
	CString m_csFirmwareEdit;
public:
	CString m_csTotalStatusEdit;
public:
	int m_nSDFreeSpaceEdit;
public:
	CString m_csWifiSSIDEdit;
public:
	CSkinProgressCtrl m_WifiSignalProgress;
public:
	CString m_csWifiStatusEdit;
#ifdef UNICODE
public:
	CComboBox m_APModeCombo;
public:
	CComboBox m_WifiEnctypeCombo;
public:
	CComboBox m_VideoQUCombo;
public:
	CComboBox m_TurnModeCombo;
public:
	CComboBox m_EnModeCombo;
public:
	CComboBox m_RecordModeCombo;
public:
	CComboBox m_DetectModeCombo;
#else
	public:
	CSkinComboBox m_APModeCombo;
public:
	CSkinComboBox m_WifiEnctypeCombo;
public:
	CSkinComboBox m_VideoQUCombo;
public:
	CSkinComboBox m_TurnModeCombo;
public:
	CSkinComboBox m_EnModeCombo;
public:
	CSkinComboBox m_RecordModeCombo;
public:
	CSkinComboBox m_DetectModeCombo;
#endif
public:
	CString m_csNewDevPwdEdit;

	CString	m_csOldDevPwd;
public:
#ifdef UNICODE
	wstring			m_sDeviceUID;
#else
	string			m_sDeviceUID;
#endif

private:
	static tkVOID WINAPI OnRecvIOCtrl(tkINT32 nIOType, tkCHAR *pIOData, tkINT32 nIODataSize, tkPVOID pObj, tkUINT32 nTag, tkVOID *param);
	tkVOID AppendIOCtrl(tkINT32 nIOType, tkCHAR *pIOData, tkINT32 nIODataSize, tkPVOID pObj, tkUINT32 nTag);

	afx_msg LRESULT OnUpdateDataMessage(WPARAM, LPARAM);

	tkPVOID			m_objItem ;
};
