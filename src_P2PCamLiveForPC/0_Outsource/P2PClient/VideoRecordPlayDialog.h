#pragma once
#include "afxcmn.h"
#include "ScreenPannel.h"
#include "afxdtctl.h"
#include "afxwin.h"

// VideoRecordPlayDialog 对话框

class VideoRecordPlayDialog : public CSkinDialog
{
	DECLARE_DYNAMIC(VideoRecordPlayDialog)

public:
	VideoRecordPlayDialog(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~VideoRecordPlayDialog();

// 对话框数据
	enum { IDD = IDD_VIDEORECORDPLAYDIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	HTREEITEM		m_hParentItem;
	//CTreeCtrl m_DeviceTree;
	CImageList m_image;
public:
	afx_msg void OnPaint();
public:
	CListCtrl m_VideoRecordList;

private:
	CScreenPannel		m_screenPannel;		//播放屏幕底板－子窗口
	int		m_nScreenNum;

	FILE		*m_pRecordFile;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedSearchBtn();

	void InitTreeView();
	CDateTimeCtrl m_StartDataTimeCtrl;
	CDateTimeCtrl m_EndDataTimeCtrl;
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnHdnItemdblclickVideoRecordList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkVideoRecordList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg LRESULT OnUpdateDataMessage(WPARAM wParam, LPARAM lParam);
	CSkinButton m_btnSearchRecord;
	CSkinStatic m_startTimestaitc;
	CSkinStatic m_endtimestatic;
	afx_msg void OnTvnSelchangedDeviceTree(NMHDR *pNMHDR, LRESULT *pResult);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	CTreeCtrl m_RecordTree;
};
