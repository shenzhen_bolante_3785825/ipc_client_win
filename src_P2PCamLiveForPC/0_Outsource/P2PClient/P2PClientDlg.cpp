// P2PClientDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "P2PClient.h"
#include "P2PClientDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
#define TAB_HEIGTH 23
#define TAB_WIDTH 120

// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CSkinDialog
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CSkinDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CSkinDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CSkinDialog)
END_MESSAGE_MAP()


// CP2PClientDlg 对话框




CP2PClientDlg::CP2PClientDlg(CWnd* pParent /*=NULL*/)
	: CSkinDialog(CP2PClientDlg::IDD, pParent)
	, m_pVideoPlayDialog(NULL)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_pVideoPlayDialog = new CVideoPlayDialog();
	ASSERT(m_pVideoPlayDialog != NULL);
	m_pSystemMgrDialog = new CSysManagerDialog();
	ASSERT(m_pSystemMgrDialog != NULL);
	m_pRemoteWebDialog = new CRemoteWebDialog();
	ASSERT(m_pRemoteWebDialog != NULL);

	m_pVideoRecordPlayDialog = new VideoRecordPlayDialog();
	ASSERT(m_pVideoRecordPlayDialog != NULL);
}

void CP2PClientDlg::DoDataExchange(CDataExchange* pDX)
{
	CSkinDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CAPTION_TAB, m_ctrlCaptionTab);
}

BEGIN_MESSAGE_MAP(CP2PClientDlg, CSkinDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_NOTIFY(TCN_SELCHANGE, IDC_CAPTION_TAB, &CP2PClientDlg::OnTcnSelchangeCaptionTab)
END_MESSAGE_MAP()


// CP2PClientDlg 消息处理程序

BOOL CP2PClientDlg::OnInitDialog()
{
	CSkinDialog::OnInitDialog();

	//AfxSetResourceHandle(theApp.m_hResourceHandle);

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	this->UserDefSkin(false, true, true);
	this->SetBKSkinImage(_T("image\\bk_body.png"), BF_PNG);
	this->SetSysBtnImage(_T("image\\sys_btn_close (2).png"), BF_PNG, 3);
	this->SetSysBtnImage(_T("image\\sys_btn_max.png"), BF_PNG, 2);
	this->SetSysBtnImage(_T("image\\sys_btn_min.png"), BF_PNG, 1);
	this->SetCaptionImage(_T("image\\bk_body.png"), BF_PNG);

	CRect m_winRect;
	m_winRect.left = 0;
    m_winRect.right = GetSystemMetrics(SM_CXSCREEN);
    m_winRect.top = 0;
    m_winRect.bottom = GetSystemMetrics(SM_CYSCREEN);
	m_winRect.bottom -= 20;
	MoveWindow(&m_winRect, TRUE);

	CString strRealVideo;
	strRealVideo.LoadString(IDS_STR_MAIN_REAL_VIDEO);
	m_ctrlCaptionTab.InsertItem(0, strRealVideo);
	CString strRecordVideo;
	strRecordVideo.LoadString(IDS_STR_MAIN_RECORD_VIDEO);
	m_ctrlCaptionTab.InsertItem(1, strRecordVideo);
	CString strLocalConfig;
	strLocalConfig.LoadString(IDS_STR_MAIN_LOCAL_CONFIG);
	m_ctrlCaptionTab.InsertItem(2, strLocalConfig);
	CString strNetDevConfig;
	strNetDevConfig.LoadString(IDS_STR_MAIN_NET_DEV_MGR);
	m_ctrlCaptionTab.InsertItem(3, strNetDevConfig);
	
	//设置TAB item高度、宽度
	//CSize cs(TAB_WIDTH, TAB_HEIGTH);
	//m_ctrlCaptionTab.SetItemSize(cs);

	m_pVideoPlayDialog->Create(IDD_VIDEOPLAYDIALOG, this);
	m_pSystemMgrDialog->Create(IDD_SYSMANAGERDIALOG, this);
	m_pRemoteWebDialog->Create(IDD_REMOTEWEBDIALOG, this);
	m_pVideoRecordPlayDialog->Create(IDD_VIDEORECORDPLAYDIALOG, this);
	/*if(theApp.m_bFirstTime)
	{
		m_ctrlCaptionTab.SetCurSel(1);
		m_pVideoPlayDialog->ShowWindow(SW_HIDE);
		m_pSystemMgrDialog->ShowWindow(SW_SHOW);
		
	}
	else*/
	{
		m_pVideoPlayDialog->ShowWindow(SW_SHOW);
		m_pSystemMgrDialog->ShowWindow(SW_HIDE);
		m_pVideoRecordPlayDialog->ShowWindow(SW_HIDE);
	}
	m_pRemoteWebDialog->ShowWindow(SW_HIDE);
	m_pVideoRecordPlayDialog->ShowWindow(SW_HIDE);

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CP2PClientDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CSkinDialog::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CP2PClientDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		if(m_ctrlCaptionTab.GetSafeHwnd())
		{
			CRect rcClient;
			GetClientRect(&rcClient);
			m_ctrlCaptionTab.MoveWindow(rcClient);

			CRect rcTab(rcClient);
			//m_ctrlCaptionTab.GetClientRect(rcTab);
			rcTab.top += (TAB_HEIGTH);
			//rcTab.right -= 200;
			if(m_pVideoPlayDialog->GetSafeHwnd() && m_pVideoPlayDialog->IsWindowVisible())
			{
				m_pVideoPlayDialog->MoveWindow(rcTab);
			}

			if(m_pSystemMgrDialog->GetSafeHwnd() && m_pSystemMgrDialog->IsWindowVisible())
			{
				m_pSystemMgrDialog->MoveWindow(rcTab);
			}

			if(m_pRemoteWebDialog->GetSafeHwnd())
			{
				m_pRemoteWebDialog->MoveWindow(rcTab);
			}

			if(m_pVideoRecordPlayDialog->GetSafeHwnd())
			{
				m_pVideoRecordPlayDialog->MoveWindow(rcTab);
			}
		}

		CSkinDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标显示。
//
HCURSOR CP2PClientDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}
BOOL CP2PClientDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 在此添加专用代码和/或调用基类
	if(pMsg->message==WM_KEYDOWN&&pMsg->wParam==VK_RETURN)    
        return TRUE; 
    if(pMsg->message==WM_KEYDOWN&&pMsg->wParam==VK_ESCAPE)    
        return TRUE; 
	return CSkinDialog::PreTranslateMessage(pMsg);
}

void CP2PClientDlg::OnTcnSelchangeCaptionTab(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 在此添加控件通知处理程序代码
	*pResult = 0;
	int nCurSel = m_ctrlCaptionTab.GetCurSel();
	/*TCHAR sMsg[256] = {0};
	_stprintf(sMsg, _T("%d"), nCurSel);
	MessageBox(sMsg);*/
	switch(nCurSel)
	{
	case 0:
		m_pVideoPlayDialog->ShowWindow(SW_SHOW);
		m_pSystemMgrDialog->ShowWindow(SW_HIDE);
		m_pRemoteWebDialog->ShowWindow(SW_HIDE);
		m_pVideoRecordPlayDialog->ShowWindow(SW_HIDE);
		break;
	case 1:
		m_pVideoRecordPlayDialog->ShowWindow(SW_SHOW);
		m_pSystemMgrDialog->ShowWindow(SW_HIDE);
		m_pVideoPlayDialog->ShowWindow(SW_HIDE);
		m_pRemoteWebDialog->ShowWindow(SW_HIDE);
		break;
	case 3:
		m_pRemoteWebDialog->ShowWindow(SW_SHOW);
		m_pSystemMgrDialog->ShowWindow(SW_HIDE);
		m_pVideoPlayDialog->ShowWindow(SW_HIDE);
		break;
	case 2:
		m_pVideoRecordPlayDialog->ShowWindow(SW_HIDE);
		m_pSystemMgrDialog->ShowWindow(SW_SHOW);
		m_pVideoPlayDialog->ShowWindow(SW_HIDE);
		m_pRemoteWebDialog->ShowWindow(SW_HIDE);
		break;
	}
}
