// SendData.h: interface for the CSendData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SENDDATA_H__F98D6FB7_D797_4947_A17E_13E88D20F35F__INCLUDED_)
#define AFX_SENDDATA_H__F98D6FB7_D797_4947_A17E_13E88D20F35F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxinet.h>
#include <string>
using std::string;

class CSendData  
{
public:
	CSendData();
	virtual ~CSendData();
	void SetServerParam(TCHAR ServerName[100],TCHAR ObjectName[100],WORD Port);
	DWORD PostDataMethod2(LPCTSTR strDataSend, int DataSize, 
		LPTSTR *pRecvData, DWORD &RecvSize,
		int	SendCode,int RecvCode);
private:
	void MultiToWide(LPCSTR strDataSend, int DataSize);
	void MultiToMulti(LPCSTR strDataSend, int DataSize);
	void WideToMulti(LPCWSTR wpWideData, int WideLen);
	//����������
	INTERNET_PORT m_nPort;	
	TCHAR m_tzObjectName[100];
	TCHAR m_tzServerName[100];
	/////����ת��/////////////////////////////////////////////////////////////
	char	* m_lpUTF8;			//UTF8���뻺��ָ��
	char	* m_lpMultiByte;	//Multil���ֽڱ���ָ��
	wchar_t	* m_wpWideChar;		//Widechar���뻺��ָ��
	int		m_nUTF8Len;	
	int		m_nMultiByteLen;
	int		m_nWideCharLen;
	
	void WideCharToUTF8(LPCWSTR wpWideData, int WideLen);
	void MultiByteToUTF8(LPCSTR lpMultiData, int MultiLen);
	void UTF8ToWideChar(LPCSTR pUTF8,int UTF8Len);
	void UTF8ToMultiByte(LPCSTR pUTF8, int UTF8Len);

};

#endif // !defined(AFX_SENDDATA_H__F98D6FB7_D797_4947_A17E_13E88D20F35F__INCLUDED_)
