// DeviceInfoDialog.cpp : 实现文件
//

#include "stdafx.h"
#include "P2PClient.h"
#include "DeviceInfoDialog.h"


// CDeviceInfoDialog 对话框

IMPLEMENT_DYNAMIC(CDeviceInfoDialog, CSkinDialog)

CDeviceInfoDialog::CDeviceInfoDialog(CWnd* pParent /*=NULL*/)
	: CSkinDialog(CDeviceInfoDialog::IDD, pParent)
	, m_csDeviceName(_T(""))
	, m_csDeivceUID(_T(""))
	, m_csDevicePwd(_T(""))
	, m_csDeviceUser(_T(""))
{

}

CDeviceInfoDialog::~CDeviceInfoDialog()
{
}

void CDeviceInfoDialog::DoDataExchange(CDataExchange* pDX)
{
	CSkinDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_DEVICE_NAME_EDIT, m_csDeviceName);
	DDX_Text(pDX, IDC_DEVICE_UID_EDIT, m_csDeivceUID);
	DDX_Text(pDX, IDC_DEVICE_PWD_EDIT, m_csDevicePwd);
	DDX_Text(pDX, IDC_DEVICE_USER_EDIT, m_csDeviceUser);
	DDX_Control(pDX, IDC_DEVICE_NAME_EDIT, m_DeviceNameEdit);
	DDX_Control(pDX, IDC_DEVICE_UID_EDIT, m_DeviceUIDEdit);
	DDX_Control(pDX, IDC_DEVICE_USER_EDIT, m_DeviceUserEdit);
	DDX_Control(pDX, IDC_DEVICE_PWD_EDIT, m_DevicePwdEdit);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
}


BEGIN_MESSAGE_MAP(CDeviceInfoDialog, CSkinDialog)
	ON_BN_CLICKED(IDOK, &CDeviceInfoDialog::OnBnClickedOk)
END_MESSAGE_MAP()


// CDeviceInfoDialog 消息处理程序

BOOL CDeviceInfoDialog::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 在此添加专用代码和/或调用基类
	if(pMsg->message==WM_KEYDOWN&&pMsg->wParam==VK_RETURN)    
        return TRUE; 
    if(pMsg->message==WM_KEYDOWN&&pMsg->wParam==VK_ESCAPE)    
        return TRUE; 
	return CSkinDialog::PreTranslateMessage(pMsg);
}

void CDeviceInfoDialog::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData();
	OnOK();
}

BOOL CDeviceInfoDialog::OnInitDialog()
{
	CSkinDialog::OnInitDialog();

	// TODO:  在此添加额外的初始化
	this->UserDefSkin(false, true, true);
	this->SetBKSkinImage(_T("image\\bk_body.png"), BF_PNG);
	this->SetSysBtnImage(_T("image\\sys_btn_close (2).png"), BF_PNG, 3);
	this->SetSysBtnImage(_T("image\\sys_btn_max.png"), BF_PNG, 2);
	this->SetSysBtnImage(_T("image\\sys_btn_min.png"), BF_PNG, 1);
	this->SetCaptionImage(_T("image\\bk_body.png"), BF_PNG);

	UpdateData(false);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

