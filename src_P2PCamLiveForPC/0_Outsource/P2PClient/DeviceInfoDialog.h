#pragma once
#include "afxwin.h"


// CDeviceInfoDialog 对话框

class CDeviceInfoDialog : public CSkinDialog
{
	DECLARE_DYNAMIC(CDeviceInfoDialog)

public:
	CDeviceInfoDialog(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CDeviceInfoDialog();

// 对话框数据
	enum { IDD = IDD_DEVICEINFODIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
public:
	CString m_csDeviceName;
public:
	CString m_csDeivceUID;
public:
	CString m_csDevicePwd;
public:
	afx_msg void OnBnClickedOk();
public:
	virtual BOOL OnInitDialog();
public:
	CString m_csDeviceUser;
public:
	CSkinEdit m_DeviceNameEdit;
public:
	CSkinEdit m_DeviceUIDEdit;
public:
	CSkinEdit m_DeviceUserEdit;
public:
	CSkinEdit m_DevicePwdEdit;
public:
	CSkinButton m_btnOK;
public:
	CSkinButton m_btnCancel;
};
