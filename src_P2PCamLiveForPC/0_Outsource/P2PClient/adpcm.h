
#ifndef _ADPCM_H_
#define _ADPCM_H_

#ifdef __cplusplus
extern "C" {
#endif

void ResetEncode();
void Encode(unsigned char *pRaw, int nLenRaw, unsigned char *pBufEncoded);
void ResetDecode();
void Decode(char *pDataCompressed, int nLenData, char *pDecoded);

#ifdef __cplusplus
}
#endif

#endif