#pragma once
#include "afxwin.h"


// CUserLoginDialog 对话框

class CUserLoginDialog : public CSkinDialog
{
	DECLARE_DYNAMIC(CUserLoginDialog)

public:
	CUserLoginDialog(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CUserLoginDialog();

// 对话框数据
	enum { IDD = IDD_USERLOGINDIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
public:
	afx_msg void OnBnClickedCancel();
public:
	CString m_csUserName;
public:
	CString m_csUserPwd;
public:
	virtual BOOL OnInitDialog();
public:
	CSkinButton m_btnUserLogin;
public:
	CSkinButton m_btnLoginCancel;
public:
	CStatic m_UserNameStatic;
public:
	CStatic m_passwordStatic;

	CString	m_ObjectName;
	CString	m_PostData;
public:
	BOOL LoginFromNetwork();
public:
#ifndef UNICODE
	CSkinComboBox m_SysLanCombo;
#else
	CComboBox m_SysLanCombo;
#endif
public:
	afx_msg void OnCbnSelchangeComboLan();
};
