// P2PClient.cpp : 定义应用程序的类行为。
//

#include "stdafx.h"
#include "P2PClient.h"
#include "P2PClientDlg.h"
#include "UserLoginDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CP2PClientApp

BEGIN_MESSAGE_MAP(CP2PClientApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CP2PClientApp 构造

CP2PClientApp::CP2PClientApp()
{
	// TODO: 在此处添加构造代码，
	// 将所有重要的初始化放置在 InitInstance 中

	m_nCurUserID		= 0;
	m_bFirstTime		= true;
	m_uVideoFileTime	= 30;
	m_hResourceHandle	= NULL;
	m_nCurLanIndex		= 1;
	m_bLanChange		= FALSE;
}


// 唯一的一个 CP2PClientApp 对象

CP2PClientApp theApp;


// CP2PClientApp 初始化

BOOL CP2PClientApp::InitInstance()
{
	// 如果一个运行在 Windows XP 上的应用程序清单指定要
	// 使用 ComCtl32.dll 版本 6 或更高版本来启用可视化方式，
	//则需要 InitCommonControlsEx()。否则，将无法创建窗口。MultiLanChineseS.dll
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 将它设置为包括所有要在应用程序中使用的
	// 公共控件类。
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	// 标准初始化
	// 如果未使用这些功能并希望减小
	// 最终可执行文件的大小，则应移除下列
	// 不需要的特定初始化例程
	// 更改用于存储设置的注册表项
	// TODO: 应适当修改该字符串，
	// 例如修改为公司或组织名
	SetRegistryKey(_T("应用程序向导生成的本地应用程序"));

	AfxEnableControlContainer();

/*
	FILE *pReadFile = NULL;
	pReadFile = _tfopen(_T("RecordPlan"), _T("ab+"));
	if(pReadFile)
	{
		int nLen = 0;
		fread(&theApp.m_modeList, sizeof(ModelInfoList), 1, pReadFile);
		fread(&nLen, sizeof(nLen), 1, pReadFile);
		TCHAR sVideoRecordPath[256] = {0};
		fread(sVideoRecordPath, nLen, 1, pReadFile);
		fread(&theApp.m_uVideoFileTime, sizeof(theApp.m_uVideoFileTime), 1, pReadFile);
		fclose(pReadFile);
		theApp.m_csVideoRecordPath = sVideoRecordPath;
		theApp.m_csVideoRecordPath.TrimRight();
	}
*/

	FILE *pFile = NULL;
	pFile = _tfopen(_T("PicturePath"), _T("ab+"));
	if(pFile)
	{
		TCHAR Buffer[256] = {0};
		int nLen = 0;
		fread(&nLen, sizeof(nLen), 1, pFile);
		fread(Buffer, nLen, 1, pFile);
		m_csPictureCapturePath = Buffer;
		fclose(pFile);
	}


	m_nCurLanIndex = GetProfileInt(_T("Language"), _T("LanIndex"), 0);
RESTART:
	if (m_hResourceHandle)
	{
		AfxFreeLibrary(m_hResourceHandle);
		m_hResourceHandle = NULL;
	}
	if(m_hOldResourceHandle)
	{
		AfxSetResourceHandle(m_hOldResourceHandle);
	}
	switch(m_nCurLanIndex)
	{
	case 0:
		//简体中文
		m_hResourceHandle = AfxLoadLibrary(_T("MultiLanChineseS.dll"));
		break;
	case 1:
		//繁体中文
		m_hResourceHandle = AfxLoadLibrary(_T("MultiLanChineseT.dll"));
		break;
	case 2:
		//英文
		m_hResourceHandle = AfxLoadLibrary(_T("MultiLanEnglish.dll"));
		break;
	}
	if (m_hResourceHandle)
	{
		m_hOldResourceHandle = AfxGetResourceHandle();
		AfxSetResourceHandle(m_hResourceHandle);
	}
	else
	{
		return FALSE;
	}

	m_ServerName	= "sztutk.3322.org";
	m_nPort			= 81;

	{
		CUserLoginDialog loginDlg;

		int nRet = loginDlg.DoModal();
		if(nRet == IDCANCEL)
		{
			return FALSE;
		}
		if(m_bLanChange)
		{
			m_bLanChange = FALSE;
			WriteProfileInt(_T("Language"), _T("LanIndex"), m_nCurLanIndex);
			goto RESTART;
		}
	}

	CP2PClientDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: 在此处放置处理何时用“确定”来关闭
		//  对话框的代码
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: 在此放置处理何时用“取消”来关闭
		//  对话框的代码
	}

	// 由于对话框已关闭，所以将返回 FALSE 以便退出应用程序，
	//  而不是启动应用程序的消息泵。
	return FALSE;
}

int CP2PClientApp::ExitInstance()
{
	// TODO: 在此添加专用代码和/或调用基类
	if (m_hResourceHandle)
	{
		AfxFreeLibrary(m_hResourceHandle);
	}
	return CWinApp::ExitInstance();
}
