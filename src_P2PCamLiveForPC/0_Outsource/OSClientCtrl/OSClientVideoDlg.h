#pragma once
#include "ScreenPannel.h"
#include <string>
#include <map>
#include "P2PAVCore.h"
using namespace std;
typedef struct P2P_REAL_PLAY_INFO
{
	TCHAR		sDeviceUID[64];
	tkHandle	objHandle;
	DWORD		dwPlayIndex;
	BOOL		bPlaying;
}*PP2P_REAL_PLAY_INFO;

#define	WM_USER_STATUS_MSG	WM_USER + 10000

// OSClientVideoDlg 对话框

class OSClientVideoDlg : public CDialog
{
	DECLARE_DYNAMIC(OSClientVideoDlg)

public:
	OSClientVideoDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~OSClientVideoDlg();

// 对话框数据
	enum { IDD = IDD_OSCLIENTVIDEODLG };

public:
	int									m_iCurWndNum;	//screen split mode 1, 4, 9, 16, 25, 36
	int									m_iCurWndIndex; //current selected split window index, start from 0
private:
	CScreenPannel						m_screenPannel;		//播放屏幕底板－子窗口
	int									m_curScreen;
	BOOL								m_bFullScreen;
	CWnd *								m_hWndParent;

	int									m_nScreenNum;

	DWORD								m_dwPlayIndex;

	CRITICAL_SECTION					m_RealPlayMapLock;
	map<int, PP2P_REAL_PLAY_INFO>	m_RealPlayMap;
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
protected:
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
public:
	void FullScreen();
public:
	void OS_SetVideoScreen(LONG inColNum, LONG inRowNum);

	LONG OS_PlayRealStream(TCHAR *inDeviceUID, TCHAR *inDeviceUser, TCHAR *inDevicePwd);
	LONG OS_StopRealStream(TCHAR *inDeviceUID);
	LONG OS_ControlPTZ(LPCTSTR inDeviceUID, LONG inControlCMD);
	afx_msg LRESULT OnStatusMessage(WPARAM wParam, LPARAM lParam);
	static tkVOID WINAPI OnStatus(tkINT32 nType, tkINT32 nValue, tkPVOID pObj, tkUINT32 nTag, tkVOID *param);
};
