#if !defined(AFX_PLAYWND_H__E6AAF690_8D19_43AC_AE4E_E64F2412D706__INCLUDED_)
#define AFX_PLAYWND_H__E6AAF690_8D19_43AC_AE4E_E64F2412D706__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PlayWnd.h : header file
//analyse

#include "P2PAVCore.h"
#include "AVFRAMEINFO.h"
#include "H264Decoder.h"
/////////////////////////
//	POPUP MENU ID DEFINE

#define		VIDEO_MENU_BASE				WM_USER + 1970

#define		VIDEO_MENU_CYCLE_DVR		WM_USER + 1976
#define		VIDEO_MENU_CYCLE_GROUP		WM_USER + 1977
#define		VIDEO_MENU_CYCLE_STOP		WM_USER + 1978
#define		VIDEO_MENU_FULLSCREEN		WM_USER + 1979
#define		VIDEO_MENU_MULTISCREEN		WM_USER + 1980
#define		VIDEO_MENU_AUTOADJUST		WM_USER + 1981
#define		VIDEO_MENU_RECORDVIDEO		WM_USER + 1982
#define		VIDEO_MENU_PRINTSCREEN		WM_USER + 1983
#define		VIDEO_MENU_EXITDECODE		WM_USER + 1984
#define		VIDEO_MENU_EXITCYCLE		WM_USER + 1985
#define		VIDEO_MENU_OPENSOUND		WM_USER + 1986
#define		VIDEO_MENU_CLOSESOUND		WM_USER + 1987
#define		VIDEO_MENU_CLOSESTREAM		WM_USER + 1988
#define		VIDEO_MENU_SEPARATOR		WM_USER + 1989

#define		VIDEO_MENU_SOUND_ONE		WM_USER + 1990
#define		VIDEO_MENU_SOUND_TWO		WM_USER + 1991
#define		VIDEO_MENU_SOUND_THREE		WM_USER + 1992
#define		VIDEO_MENU_SOUND_FOUR		WM_USER + 1993

#define		VIDEO_MENU_START_TALK		WM_USER + 1994
#define		VIDEO_MENU_STOP_TALK		WM_USER + 1995
#define     LIST_MENU_LOCK			    WM_USER + 1996
#define     LIST_MENU_UNLOCK			WM_USER + 1997
#define		VIDEO_MENU_END				65535//WM_USER + 1998

#define		VIDEO_REPAINT				WM_USER + 1999

#define		VIDEO_MENU_START_LISTEN		WM_USER+2011
#define		VIDEO_MENU_STOP_LISTEN		WM_USER+2012

#define		VIDEO_MENU_TAKE_PIC			WM_USER+2013

//	KeyColor
//#define		VIDEO_BACK_COLOR	RGB(100,100,160)
//#define		VIDEO_BACK_COLOR	RGB(125,140,145)//frank change
#define		VIDEO_BACK_COLOR	RGB(52, 52, 52)//same change

/////////////////////////////////////////////////////////////////////////////
// CPlayWnd dialog

class CPlayWnd : public CWnd
{
// Construction
public:
	CPlayWnd();   // standard constructor
    ~CPlayWnd();

    int     m_nPlayPort;
    BOOL    m_bOperPtz;         //是否启动
    BOOL    m_bNeedStart;       //是否需要启动

    POINT   m_stPoint;
    int     m_nPtzType;

    HCURSOR m_moveCur;

    RECT    m_wndRect;
    int m_nMatrixHandle;
    BOOL m_bGetConfig;

#if defined MATRIX_ANALOG
    NvsMatrixInChanList * m_pMatrixChanList;
#endif

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPlayWnd)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL
	afx_msg void OnVideoMenu(UINT nID);
	afx_msg LRESULT OnRepaintWnd(WPARAM wParam, LPARAM lParam);

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CPlayWnd)
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
    //afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
    BOOL  m_bAlarmRec;
	int m_nDspIndex;
	int m_nOutput;
	int m_outputIndex;
	int m_regionIndex;
	void SetWinID(int ID) {m_nWndID = ID;}
	int  GetWinID(void){return m_nWndID;}

	void UpdateCardConfig();

	//get split info 
	BOOL			GetSplitInfo(SplitInfoNode* info);
	//set split info -- copy whole structure
	BOOL			SetSplitInfo(SplitInfoNode* info);

	void ShowToolTips(TCHAR * strInfo);


	int m_winUseType;
	int m_cardSplitType;
	BOOL m_isVideoOpen;
	CRITICAL_SECTION m_operCS; 
    int m_iChannel;
    BOOL m_bRecord;


    BOOL m_bFullScreen;
    int m_nWndID;
    HWND m_hwndTT;	//Use for tool tips
    
    SplitInfoNode m_splitInfo;
    int m_nPreSplit;
    BOOL m_isOpenSound;
    
    int m_nCodeType;
    int m_nPicQuality;
    int m_nSize;

	int		m_nCameraID;

	CString			m_csErrorCode;
	CString			m_csPlayInfo;
	tkHandle		m_objHandle;
	LONG			m_lStreamPlayer;

	BOOL			m_bIsPlaying;

	BOOL			m_bVoice;

	BOOL			m_bListen;

	TCHAR			m_sDeviceUID[64];
public:
	BOOL SetErrorCode(TCHAR *inErrString);

	BOOL PlayRealStream(tkHandle objHandle);

	BOOL StopRealStream();

	void UpdatePlayInfo();

	void SetDeviceUID(TCHAR *inDeviceUID);

	static tkVOID WINAPI OnAVFrame(tkCHAR *pData, tkINT32 nSize, tkVOID *pFrmInfo, tkINT32 nFrmInfoSize, tkUINT32 nTag, tkVOID *param);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PLAYWND_H__E6AAF690_8D19_43AC_AE4E_E64F2412D706__INCLUDED_)
