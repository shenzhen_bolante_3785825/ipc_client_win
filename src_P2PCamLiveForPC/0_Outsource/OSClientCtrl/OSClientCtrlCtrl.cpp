// OSClientCtrlCtrl.cpp : COSClientCtrlCtrl ActiveX 控件类的实现。

#include "stdafx.h"
#include "OSClientCtrl.h"
#include "OSClientCtrlCtrl.h"
#include "OSClientCtrlPropPage.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNCREATE(COSClientCtrlCtrl, COleControl)



// 消息映射

BEGIN_MESSAGE_MAP(COSClientCtrlCtrl, COleControl)
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()



// 调度映射

BEGIN_DISPATCH_MAP(COSClientCtrlCtrl, COleControl)
	DISP_FUNCTION_ID(COSClientCtrlCtrl, "P2P_SetVideoScreen", dispidP2P_SetVideoScreen, P2P_SetVideoScreen, VT_EMPTY, VTS_I4 VTS_I4)
	DISP_FUNCTION_ID(COSClientCtrlCtrl, "P2P_PlayRealStream", dispidP2P_PlayRealStream, P2P_PlayRealStream, VT_I4, VTS_BSTR VTS_BSTR VTS_BSTR)
	DISP_FUNCTION_ID(COSClientCtrlCtrl, "P2P_StopRealStream", dispidP2P_StopRealStream, P2P_StopRealStream, VT_I4, VTS_BSTR)
	DISP_FUNCTION_ID(COSClientCtrlCtrl, "P2P_ControlPTZ", dispidP2P_ControlPTZ, P2P_ControlPTZ, VT_I4, VTS_BSTR VTS_I4)
END_DISPATCH_MAP()



// 事件映射

BEGIN_EVENT_MAP(COSClientCtrlCtrl, COleControl)
END_EVENT_MAP()



// 属性页

// TODO: 按需要添加更多属性页。请记住增加计数!
BEGIN_PROPPAGEIDS(COSClientCtrlCtrl, 1)
	PROPPAGEID(COSClientCtrlPropPage::guid)
END_PROPPAGEIDS(COSClientCtrlCtrl)



// 初始化类工厂和 guid

IMPLEMENT_OLECREATE_EX(COSClientCtrlCtrl, "OSCLIENTCTRL.OSClientCtrlCtrl.1",
	0x3493fdce, 0x95fe, 0x43f3, 0xab, 0xad, 0x2b, 0x88, 0xac, 0x37, 0xf2, 0)



// 键入库 ID 和版本

IMPLEMENT_OLETYPELIB(COSClientCtrlCtrl, _tlid, _wVerMajor, _wVerMinor)



// 接口 ID

const IID BASED_CODE IID_DOSClientCtrl =
		{ 0xA8D053A1, 0x6B0A, 0x4513, { 0xB2, 0x7D, 0x14, 0x43, 0xDA, 0xBC, 0xD6, 0x6D } };
const IID BASED_CODE IID_DOSClientCtrlEvents =
		{ 0x5C774740, 0x557F, 0x4BE3, { 0x99, 0xA0, 0x3E, 0xD1, 0x98, 0xCB, 0xEB, 0x65 } };



// 控件类型信息

static const DWORD BASED_CODE _dwOSClientCtrlOleMisc =
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(COSClientCtrlCtrl, IDS_OSCLIENTCTRL, _dwOSClientCtrlOleMisc)



// COSClientCtrlCtrl::COSClientCtrlCtrlFactory::UpdateRegistry -
// 添加或移除 COSClientCtrlCtrl 的系统注册表项

BOOL COSClientCtrlCtrl::COSClientCtrlCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: 验证您的控件是否符合单元模型线程处理规则。
	// 有关更多信息，请参考 MFC 技术说明 64。
	// 如果您的控件不符合单元模型规则，则
	// 必须修改如下代码，将第六个参数从
	// afxRegApartmentThreading 改为 0。

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_OSCLIENTCTRL,
			IDB_OSCLIENTCTRL,
			afxRegApartmentThreading,
			_dwOSClientCtrlOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}



// 授权字符串

static const TCHAR BASED_CODE _szLicFileName[] = _T("OSClientCtrl.lic");

static const WCHAR BASED_CODE _szLicString[] =
	L"Copyright (c) 2012 ";



// COSClientCtrlCtrl::COSClientCtrlCtrlFactory::VerifyUserLicense -
// 检查是否存在用户许可证

BOOL COSClientCtrlCtrl::COSClientCtrlCtrlFactory::VerifyUserLicense()
{
	return AfxVerifyLicFile(AfxGetInstanceHandle(), _szLicFileName,
		_szLicString);
}



// COSClientCtrlCtrl::COSClientCtrlCtrlFactory::GetLicenseKey -
// 返回运行时授权密钥

BOOL COSClientCtrlCtrl::COSClientCtrlCtrlFactory::GetLicenseKey(DWORD dwReserved,
	BSTR FAR* pbstrKey)
{
	if (pbstrKey == NULL)
		return FALSE;

	*pbstrKey = SysAllocString(_szLicString);
	return (*pbstrKey != NULL);
}



// COSClientCtrlCtrl::COSClientCtrlCtrl - 构造函数

COSClientCtrlCtrl::COSClientCtrlCtrl()
{
	time_t curTime = ::time(NULL);
	tm *tt = localtime(&curTime);
	if((tt->tm_year+1900) >= 2013 && (tt->tm_mon+1) >= 8)
	{
		return ;
	}

	InitializeIIDs(&IID_DOSClientCtrl, &IID_DOSClientCtrlEvents);
	// TODO: 在此初始化控件的实例数据。
}



// COSClientCtrlCtrl::~COSClientCtrlCtrl - 析构函数

COSClientCtrlCtrl::~COSClientCtrlCtrl()
{
	// TODO: 在此清理控件的实例数据。
}



// COSClientCtrlCtrl::OnDraw - 绘图函数

void COSClientCtrlCtrl::OnDraw(
			CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
	if (!pdc)
		return;

	// TODO: 用您自己的绘图代码替换下面的代码。
	//pdc->FillRect(rcBounds, CBrush::FromHandle((HBRUSH)GetStockObject(WHITE_BRUSH)));
	//pdc->Ellipse(rcBounds);
}



// COSClientCtrlCtrl::DoPropExchange - 持久性支持

void COSClientCtrlCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO: 为每个持久的自定义属性调用 PX_ 函数。
}



// COSClientCtrlCtrl::OnResetState - 将控件重置为默认状态

void COSClientCtrlCtrl::OnResetState()
{
	COleControl::OnResetState();  // 重置 DoPropExchange 中找到的默认值

	// TODO: 在此重置任意其他控件状态。
}



// COSClientCtrlCtrl 消息处理程序

int COSClientCtrlCtrl::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	time_t curTime = ::time(NULL);
	tm *tt = localtime(&curTime);
	if((tt->tm_year+1900) >= 2013 && (tt->tm_mon+1) >= 8)
	{
		return FALSE;
	}

	if (COleControl::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  在此添加您专用的创建代码
	m_ClientVideoDlg.Create(IDD_OSCLIENTVIDEODLG, this);
	m_ClientVideoDlg.ShowWindow(SW_SHOW);

	//OnActivateInPlace   (TRUE,   NULL); 
	return 0;
}

void COSClientCtrlCtrl::OnSize(UINT nType, int cx, int cy)
{
	COleControl::OnSize(nType, cx, cy);

	// TODO: 在此处添加消息处理程序代码
	if (m_ClientVideoDlg.GetSafeHwnd())
	{
		RECT rect;
		rect.left = 0;
		rect.top = 0;
		rect.bottom = cy;
		rect.right = cx;
		m_ClientVideoDlg.MoveWindow(&rect);
	}
}

BOOL COSClientCtrlCtrl::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 在此添加专用代码和/或调用基类
	if   (pMsg-> message==WM_KEYDOWN) 
	{ 
		UINT   nkeyc=(UINT)(pMsg-> wParam); 
		if(nkeyc==VK_ESCAPE) 
		{
			if (m_ClientVideoDlg.GetSafeHwnd())
			{
				::PostMessage(m_ClientVideoDlg.GetSafeHwnd(),MSG_ESCAPE,0,0);
			}
			pMsg-> wParam=0; 
		} 
	}
	return COleControl::PreTranslateMessage(pMsg);
}

void COSClientCtrlCtrl::P2P_SetVideoScreen(LONG inColNum, LONG inRowNum)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO: 在此添加调度处理程序代码
	m_ClientVideoDlg.OS_SetVideoScreen(inColNum, inRowNum);
}

LONG COSClientCtrlCtrl::P2P_PlayRealStream(LPCTSTR inDeviceUID, LPCTSTR inDeviceUser, LPCTSTR inDevicePwd)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO: 在此添加调度处理程序代码
	return m_ClientVideoDlg.OS_PlayRealStream((TCHAR*)inDeviceUID, (TCHAR*)inDeviceUser, (TCHAR*)inDevicePwd);
}

LONG COSClientCtrlCtrl::P2P_StopRealStream(LPCTSTR inDeviceUID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO: 在此添加调度处理程序代码
	return m_ClientVideoDlg.OS_StopRealStream((TCHAR*)inDeviceUID);
}

LONG COSClientCtrlCtrl::P2P_ControlPTZ(LPCTSTR inDeviceUID, LONG inControlCMD)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO: 在此添加调度处理程序代码

	return m_ClientVideoDlg.OS_ControlPTZ(inDeviceUID, inControlCMD);
}
