// OSClientCtrl.cpp : COSClientCtrlApp 和 DLL 注册的实现。

#include "stdafx.h"
#include "OSClientCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


COSClientCtrlApp theApp;

const GUID CDECL BASED_CODE _tlid =
		{ 0xE4AD9CF7, 0x4E06, 0x44CC, { 0x88, 0x34, 0xB4, 0x5F, 0x7C, 0x24, 0xA8, 0xD7 } };
const WORD _wVerMajor = 1;
const WORD _wVerMinor = 0;



// COSClientCtrlApp::InitInstance - DLL 初始化

BOOL COSClientCtrlApp::InitInstance()
{
	BOOL bInit = COleControlModule::InitInstance();

	if (bInit)
	{
		// TODO: 在此添加您自己的模块初始化代码。
	}

	return bInit;
}



// COSClientCtrlApp::ExitInstance - DLL 终止

int COSClientCtrlApp::ExitInstance()
{
	// TODO: 在此添加您自己的模块终止代码。

	return COleControlModule::ExitInstance();
}



// DllRegisterServer - 将项添加到系统注册表

STDAPI DllRegisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleRegisterTypeLib(AfxGetInstanceHandle(), _tlid))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(TRUE))
		return ResultFromScode(SELFREG_E_CLASS);

	return NOERROR;
}



// DllUnregisterServer - 将项从系统注册表中移除

STDAPI DllUnregisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleUnregisterTypeLib(_tlid, _wVerMajor, _wVerMinor))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(FALSE))
		return ResultFromScode(SELFREG_E_CLASS);

	return NOERROR;
}
