// OSClientVideoDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "OSClientCtrl.h"
#include "OSClientVideoDlg.h"
#include "OSClientCtrlCtrl.h"
#include "JJLog.h"
#include "PlayWnd.h"
#include "err_iotc.h"
#include "AVIOCTRLDEFs.h"

// OSClientVideoDlg 对话框

IMPLEMENT_DYNAMIC(OSClientVideoDlg, CDialog)

OSClientVideoDlg::OSClientVideoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(OSClientVideoDlg::IDD, pParent)
	, m_nScreenNum(0)
	, m_dwPlayIndex(0)
{
	InitializeCriticalSection(&m_RealPlayMapLock);
}

OSClientVideoDlg::~OSClientVideoDlg()
{
	//退出控件时，关闭所有视频及断开与服务器的连接

	EnterCriticalSection(&m_RealPlayMapLock);
	map<int, PP2P_REAL_PLAY_INFO>::iterator itPlayInfo = m_RealPlayMap.begin();
	while(itPlayInfo != m_RealPlayMap.end())
	{
		CPlayWnd *pWnd = (CPlayWnd*)m_screenPannel.GetPage(itPlayInfo->second->dwPlayIndex);
		if(pWnd && pWnd->GetSafeHwnd())
		{
			pWnd->StopRealStream();
		}

		apiDisconnect(itPlayInfo->second->objHandle, 1);
		apiReleaseObj(&itPlayInfo->second->objHandle);

		P2P_REAL_PLAY_INFO *realPlayInfo = itPlayInfo->second;
		delete realPlayInfo;
		realPlayInfo = NULL;

		m_RealPlayMap.erase(itPlayInfo++);
	}
	LeaveCriticalSection(&m_RealPlayMapLock);

	DeleteCriticalSection(&m_RealPlayMapLock);
}

void OSClientVideoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(OSClientVideoDlg, CDialog)
	ON_WM_SIZE()
	ON_MESSAGE(WM_USER_STATUS_MSG, OnStatusMessage)
END_MESSAGE_MAP()


// OSClientVideoDlg 消息处理程序

BOOL OSClientVideoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  在此添加额外的初始化
	m_screenPannel.Create(
		NULL,
		NULL,
		WS_CHILD|WS_VISIBLE,  
		CRect(0,0,0,0), 
		this, 
		1981);
	RECT rect;
	this->GetClientRect(&rect);	
	m_bFullScreen=FALSE;
	m_nScreenNum = 4;
	m_screenPannel.SetShowPlayWin(m_nScreenNum, 0, 0);
	m_screenPannel.MoveWindow(&rect);
	m_screenPannel.ShowWindow(SW_SHOW);
	m_hWndParent=GetParent();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void OSClientVideoDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	// TODO: 在此处添加消息处理程序代码
	if (m_screenPannel.GetSafeHwnd())
	{
		RECT rect;
		rect.left = 0;
		rect.top = 0;
		rect.bottom = cy;
		rect.right = cx;	
		m_screenPannel.MoveWindow(&rect);
	}
}

LRESULT OSClientVideoDlg::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	// TODO: 在此添加专用代码和/或调用基类
	COSClientCtrlCtrl *pContainer = (COSClientCtrlCtrl*)m_hWndParent;
	CPlayWnd *pWnd = NULL;
	switch (message)
	{
	case VIDEO_MENU_CLOSESTREAM:
		{
			/*m_curScreen=m_screenPannel.GetCurScreen();
			pWnd=m_screenPannel.GetPlayWnd(m_curScreen);*/
			TCHAR *pDeviceUID = (TCHAR*)wParam;
#ifdef UNICODE
			if(wcslen(pDeviceUID) > 0)
#else
			if(strlen(pDeviceUID) > 0)
#endif
			{
				OS_StopRealStream(pDeviceUID);
			}
			break;
		}
	case VIDEO_MENU_START_TALK:
		{
			m_curScreen=m_screenPannel.GetCurScreen();
			pWnd=m_screenPannel.GetPlayWnd(m_curScreen);
			
			break;
		}
	case VIDEO_MENU_STOP_TALK:
		{
			m_curScreen=m_screenPannel.GetCurScreen();
			pWnd=m_screenPannel.GetPlayWnd(m_curScreen);
			
			break;
		}
	case VIDEO_MENU_FULLSCREEN:
		{
			FullScreen();
			m_screenPannel.SetFullScreen(m_bFullScreen);
			break;
		}
	case MSG_CHANGEPLAYWND:
		{
			SplitInfoNode siNode;
			int nMsg=0;
			m_curScreen=m_screenPannel.GetCurScreen();
			pWnd=m_screenPannel.GetPlayWnd(m_curScreen);
			pWnd->GetSplitInfo(&siNode);
			if(siNode.Type == SPLIT_TYPE_MONITOR)
			{
				nMsg=1;
			}
			//pContainer->SendMsg(MSG_CHANGEPLAYWND,nMsg,pWnd->m_nPlayPort,m_curScreen);
			break;		
		}
	case MSG_MULTISCREEN:
		{
			m_curScreen=m_screenPannel.GetCurScreen();
			pWnd=m_screenPannel.GetPlayWnd(m_curScreen);
			BOOL bMulti = m_screenPannel.GetMultiScreen();
			m_screenPannel.SetMultiScreen(!bMulti);
			m_screenPannel.SetActivePage(pWnd);
			Invalidate();
			break;
		}
	case MSG_ESCAPE:
		{
			if (m_bFullScreen)
			{
				FullScreen();
				m_screenPannel.SetFullScreen(m_bFullScreen);
			}
			break;
		}
	}
	return CDialog::DefWindowProc(message, wParam, lParam);
}

void OSClientVideoDlg::FullScreen()
{
	::SetFocus(m_hWndParent->GetSafeHwnd());
	if(!m_bFullScreen)
	{	          
		SetParent(NULL);
		ShowWindow(SW_HIDE);
		this->ShowWindow(SW_SHOWMAXIMIZED);  
		int cx = ::GetSystemMetrics(SM_CXSCREEN);    
		int cy = ::GetSystemMetrics(SM_CYSCREEN);     
		MoveWindow(0, 0, cx, cy, TRUE); 
		SetWindowPos(&wndTopMost,0,0,cx,cy,SWP_NOZORDER);  		
	}
	else
	{ 
		SetParent(m_hWndParent);     
		ShowWindow(SW_SHOW); 
	} 
	m_bFullScreen = !m_bFullScreen;  
}

void OSClientVideoDlg::OS_SetVideoScreen(LONG inColNum, LONG inRowNum)
{
	m_screenPannel.m_splitCol = inColNum;
	m_screenPannel.m_splitRow = inRowNum;
	m_nScreenNum = inColNum * inRowNum;
	m_screenPannel.SetShowPlayWin(m_nScreenNum, 0, 0);
}

LONG OSClientVideoDlg::OS_PlayRealStream(TCHAR *inDeviceUID, TCHAR *inDeviceUser, TCHAR *inDevicePwd)
{
	time_t curTime = ::time(NULL);
	tm *tt = localtime(&curTime);
	if((tt->tm_year+1900) >= 2014 && (tt->tm_mon+1) >= 3)
	{
		return -1;
	}

	if(inDeviceUID == NULL || inDeviceUser == NULL || inDevicePwd == NULL)
		return -1;//错误参数

	m_dwPlayIndex = m_screenPannel.GetCurScreen();
	map<int, PP2P_REAL_PLAY_INFO>::iterator itPlayInfo = m_RealPlayMap.find(m_dwPlayIndex);
	if(itPlayInfo != m_RealPlayMap.end())
	{
		apiSetConnInfo(itPlayInfo->second->objHandle, m_dwPlayIndex, (tkCHAR*)inDeviceUID, (tkCHAR*)inDeviceUser, (tkCHAR*)inDevicePwd);
		apiRegCB_OnStatus(itPlayInfo->second->objHandle, OnStatus, this);
		apiConnect(itPlayInfo->second->objHandle);
		LeaveCriticalSection(&m_RealPlayMapLock);
		return 0;//
	}
	
	tkHandle objHandleItem = apiCreateObj(m_dwPlayIndex, (tkCHAR*)inDeviceUID, (tkCHAR*)inDeviceUser, (tkCHAR*)inDevicePwd);
	if(objHandleItem == NULL)
	{
		LeaveCriticalSection(&m_RealPlayMapLock);
		return -2;//
	}

	CPlayWnd *pWnd = (CPlayWnd*)m_screenPannel.GetPage(m_dwPlayIndex);
	if(pWnd)
	{
		pWnd->SetDeviceUID(inDeviceUID);
	}

	if(objHandleItem != NULL) {
		apiRegCB_OnStatus(objHandleItem, OnStatus, this);
	}
	apiConnect(objHandleItem);

	P2P_REAL_PLAY_INFO *realPlayInfo = new P2P_REAL_PLAY_INFO();
	realPlayInfo->objHandle		= objHandleItem;
	realPlayInfo->dwPlayIndex	= m_dwPlayIndex;
	realPlayInfo->bPlaying		= TRUE;
#ifdef UNICODE
	wcscpy(realPlayInfo->sDeviceUID, inDeviceUID);
#else
	strcpy(realPlayInfo->sDeviceUID, inDeviceUID);
#endif
	m_RealPlayMap.insert(make_pair(m_dwPlayIndex, realPlayInfo));
	LeaveCriticalSection(&m_RealPlayMapLock);
	return 0;
}
LONG OSClientVideoDlg::OS_StopRealStream(TCHAR *inDeviceUID)
{
	EnterCriticalSection(&m_RealPlayMapLock);
	map<int, PP2P_REAL_PLAY_INFO>::iterator itPlayInfo = m_RealPlayMap.begin();
	while(itPlayInfo != m_RealPlayMap.end())
	{
#ifdef UNICODE
		if(wcscmp(inDeviceUID, itPlayInfo->second->sDeviceUID) == 0)
#else
		if(strcmp(inDeviceUID, itPlayInfo->second->sDeviceUID) == 0)
#endif
		{
			CPlayWnd *pWnd = (CPlayWnd*)m_screenPannel.GetPage(itPlayInfo->second->dwPlayIndex);
			if(pWnd && pWnd->GetSafeHwnd())
			{
				pWnd->StopRealStream();
			}
			//apiRegCB_OnStatus(itPlayInfo->second->objHandle, NULL, NULL);
			apiDisconnect(itPlayInfo->second->objHandle, 1);
			itPlayInfo->second->bPlaying = FALSE;
			break;
		}
		itPlayInfo++;
	}
	LeaveCriticalSection(&m_RealPlayMapLock);
	return 0;
}

LONG OSClientVideoDlg::OS_ControlPTZ(LPCTSTR inDeviceUID, LONG inControlCMD)
{
	EnterCriticalSection(&m_RealPlayMapLock);
	map<int, PP2P_REAL_PLAY_INFO>::iterator itPlayInfo = m_RealPlayMap.begin();
	while(itPlayInfo != m_RealPlayMap.end())
	{
#ifdef UNICODE
		if(wcscmp(inDeviceUID, itPlayInfo->second->sDeviceUID) == 0)
#else
		if(strcmp(inDeviceUID, itPlayInfo->second->sDeviceUID) == 0)
#endif
		{
			SMsgAVIoctrlPtzCmd req;
			memset(&req, 0, sizeof(req));
			req.control = inControlCMD;
			int nRet=apiSendIOCtrl(itPlayInfo->second->objHandle, IOTYPE_USER_IPCAM_PTZ_COMMAND, (tkUCHAR *)&req, sizeof(req));
			break;
		}
		itPlayInfo++;
	}
	LeaveCriticalSection(&m_RealPlayMapLock);
	return 0;
}

tkVOID WINAPI OSClientVideoDlg::OnStatus(tkINT32 nType, tkINT32 nValue, tkPVOID pObj, tkUINT32 nTag, tkVOID *param)
{
	OSClientVideoDlg *pThis=(OSClientVideoDlg *)param;
	if(pThis->GetSafeHwnd())
	{
		CPlayWnd *pWnd = (CPlayWnd*)pThis->m_screenPannel.GetPage(nTag);
		if(pWnd == NULL || !pWnd->GetSafeHwnd())
		{
			return ;
		}

		switch(nValue)
		{
		case SINFO_CONNECTING:
			pWnd->SetErrorCode(_T("设备正在连接......"));
			break;

		case SINFO_CONNECTED:
			pWnd->PlayRealStream(pObj);
			break;

		case SINFO_DISCONNECTED:
			//pThis->SetConnectStatus(pObj, nValue, FALSE, "设备连接断开");
			pWnd->SetErrorCode(_T("设备连接断开"));
			pThis->PostMessage(WM_USER_STATUS_MSG, (WPARAM)pObj, (LPARAM)nValue);
			break;
			//非法UID
		case IOTC_ER_UNLICENSE:
			{
				//pThis->SetConnectStatus(pObj, nValue, FALSE, "非法UID");
				pThis->PostMessage(WM_USER_STATUS_MSG, (WPARAM)pObj, (LPARAM)nValue);
				pWnd->SetErrorCode(_T("非法UID"));
			}
			break;
			//设备密码错误
		case AV_ER_WRONG_VIEWACCorPWD:
			{
				//pThis->SetConnectStatus(pObj, nValue, FALSE, "设备密码错误");
				pThis->PostMessage(WM_USER_STATUS_MSG, (WPARAM)pObj, (LPARAM)nValue);
				pWnd->SetErrorCode(_T("设备密码错误"));
			}
			break;
			//设备不在线
		case IOTC_ER_CAN_NOT_FIND_DEVICE:
			{
				//pThis->SetConnectStatus(pObj, nValue, FALSE, "设备不在线");
				pThis->PostMessage(WM_USER_STATUS_MSG, (WPARAM)pObj, (LPARAM)nValue);
				pWnd->SetErrorCode(_T("设备不在线"));
			}
			break;
		default:
			break;
		}
	}
}

LRESULT OSClientVideoDlg::OnStatusMessage(WPARAM wParam, LPARAM lParam)
{
	int nValue = (int)lParam;
	//如果中途断开连接或者还没有连接成功，即重新连接设备
	tkPVOID pObj = (tkPVOID)wParam;
	EnterCriticalSection(&m_RealPlayMapLock);
	map<int, PP2P_REAL_PLAY_INFO>::iterator itPlayInfo = m_RealPlayMap.begin();
	while(itPlayInfo != m_RealPlayMap.end())
	{
		if(itPlayInfo->second->objHandle == pObj)
		{
			//这里为断开即不管了，设置为关闭状态
			/*CPlayWnd *pWnd = (CPlayWnd*)m_screenPannel.GetPage(itPlayInfo->second->dwPlayIndex);
			if(pWnd)
			{
				pWnd->StopRealStream();
			}
			apiDisconnect(itPlayInfo->second->objHandle, 1);
			itPlayInfo->second->bPlaying = FALSE;*/
			//这里为重连设备，直到调用关闭视频接口
			apiConnect(itPlayInfo->second->objHandle);
			break;
		}
		itPlayInfo++;
	}
	LeaveCriticalSection(&m_RealPlayMapLock);
	return 0;
}