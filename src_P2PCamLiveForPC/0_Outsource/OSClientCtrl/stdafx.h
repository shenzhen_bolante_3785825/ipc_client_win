#pragma once

// stdafx.h : 标准系统包含文件的包含文件，
// 或是经常使用但不常更改的
// 特定于项目的包含文件

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// 从 Windows 头中排除极少使用的资料
#endif

// 如果必须将位于下面指定平台之前的平台作为目标，请修改下列定义。
// 有关不同平台对应值的最新信息，请参考 MSDN。
#ifndef WINVER				// 允许使用特定于 Windows XP 或更高版本的功能。
#define WINVER 0x0501		// 将此值更改为相应的值，以适用于 Windows 的其他版本。
#endif

#ifndef _WIN32_WINNT		// 允许使用特定于 Windows XP 或更高版本的功能。
#define _WIN32_WINNT 0x0501	// 将此值更改为相应的值，以适用于 Windows 的其他版本。
#endif						

#ifndef _WIN32_WINDOWS		// 允许使用特定于 Windows 98 或更高版本的功能。
#define _WIN32_WINDOWS 0x0410 // 将此值更改为适当的值，以指定将 Windows Me 或更高版本作为目标。
#endif

#ifndef _WIN32_IE			// 允许使用特定于 IE 6.0 或更高版本的功能。
#define _WIN32_IE 0x0600	// 将此值更改为相应的值，以适用于 IE 的其他版本。
#endif

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// 某些 CString 构造函数将是显式的

#include <afxctl.h>         // ActiveX 控件的 MFC 支持
#include <afxext.h>         // MFC 扩展
#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// MFC 对 Internet Explorer 4 公共控件的支持
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC 对 Windows 公共控件的支持
#endif // _AFX_NO_AFXCMN_SUPPORT

// 如果不希望使用 MFC 数据库类，
//  请删除下面的两个包含文件
#ifndef _WIN64

#ifndef _AFX_NO_DB_SUPPORT
#include <afxdb.h>			// MFC ODBC 数据库类
#endif // _AFX_NO_DB_SUPPORT

#ifndef _AFX_NO_DAO_SUPPORT
#include <afxdao.h>			// MFC DAO 数据库类
#endif // _AFX_NO_DAO_SUPPORT

#define OS_MAX_PORT 128

#define	BLACK			RGB( 000, 000, 000 )

//画面分割类型
enum{
	SPLIT1 = 1,
	SPLIT4 = 4,
	SPLIT9 = 9,
	SPLIT6 = 6,
	SPLIT8 = 8,
	SPLIT16 = 16,
	SPLIT25 = 25,
	SPLIT36 = 36,
	SPLIT49 = 49,
	SPLIT64 = 64,	
	SPLIT_TOTAL
};

typedef enum {
	MSG_NULL=0x2012,
	MSG_CLOSESTREAM,   //关闭视频流
	MSG_CHANGEPLAYWND,    //改变播放窗口
	MSG_CHANGETALK,    //改变对讲状态
	MSG_MULTISCREEN, //多屏或单屏
	MSG_ESCAPE	//esc按下消息
}MsgType_t;

typedef enum
{
	WIN_USE_NULL,
	WIN_USE_PREVIEW,
	WIN_USE_PLAYBACK,
	WIN_USE_CARDCONFIG,
	WIN_USE_ALARMPLAYBACK
};

//当前画面显示内容的类型
typedef enum _SplitType{
	SPLIT_TYPE_NULL = 0,   //空白
	SPLIT_TYPE_MONITOR,    //网络监视
	SPLIT_TYPE_EXCEPTION    //网络监视异常
}SplitType;

//监视信息参数
typedef struct _SplitMonitorParam
{
	//DeviceNode *pDevice;  //设备指针
	int iChannel;   //对应的设备中的通道序号
	int iStreamId;	//预览返回的ID
	BOOL  isTalkOpen;  //是否打开语音对讲
	BOOL  isOpenSound;
	DWORD dwTreeNode;

	//use for cycle   
	int cycleType;
	UINT iInterval;			//画面切换间隔时间（秒）  
}SplitMonitorParam;

//画面分割通道显示信息(可以定义成type/param，param自定义)
typedef struct _SplitInfoNode
{
	SplitType Type;     //显示类型 空白/监视/网络回放/本地回放等
	DWORD iHandle;  //用于记录通道id(监视通道ID/播放文件iD等)
	BOOL  isOpenSound;
	SplitMonitorParam Param;  //信息参数,对于不同的显示有不同的参数
}SplitInfoNode;

#endif // _WIN64
#include <afxwin.h>

