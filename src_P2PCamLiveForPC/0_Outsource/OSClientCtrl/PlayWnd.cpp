// PlayWnd.cpp : implementation file
//

#include "stdafx.h"
#include "PlayWnd.h"
#include "ScreenPannel.h"
#include "AVDecoderLib.h"
#include "wave_out.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



#define GET_CONFIG_TIMER    100
#define GET_CONFIG_INTERVAL 5000

void CALLBACK DrawFun(long nPort,HDC hDc,long nUser)
{
    RECT *rc = (RECT *)nUser;
    if(rc == NULL || nPort >= OS_MAX_PORT)
        return ;

   
}

/////////////////////////////////////////////////////////////////////////////
// CPlayWnd dialog
CPlayWnd::CPlayWnd()
	:m_nWndID(0)
{
	memset(&m_splitInfo, 0, sizeof(m_splitInfo));
	m_nPreSplit = 0;
	m_hwndTT = 0;
	
	m_nDspIndex = 0;
	m_nOutput = -1;
	//{{AFX_DATA_INIT(CPlayWnd)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
    
    InitializeCriticalSection(&m_operCS);
    m_nCodeType = 0;
    m_nPicQuality = 3;
    m_nSize = 0;
    m_iChannel = 1;
    m_bRecord = FALSE;
    m_nMatrixHandle = -1;
    m_bGetConfig = FALSE;

    m_bOperPtz = FALSE;
    m_bNeedStart = FALSE;

    m_moveCur = 0;//LoadCursor(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDC_CURSOR_MHAND));
    m_nPlayPort = -1;
    m_bAlarmRec = FALSE;

	m_csErrorCode = "欢迎使用";
	m_csPlayInfo = "";

	m_lStreamPlayer		= -1;
	m_objHandle			= NULL;
	m_bIsPlaying		= FALSE;

	m_bVoice			= FALSE;
	m_bListen			= FALSE;

	::memset(m_sDeviceUID, 0, 64);
}

CPlayWnd::~CPlayWnd()
{
 

}

BEGIN_MESSAGE_MAP(CPlayWnd, CWnd)
	//{{AFX_MSG_MAP(CPlayWnd)
	ON_WM_ERASEBKGND()
//	ON_WM_CONTEXTMENU()
    ON_WM_TIMER()
	ON_WM_CREATE()
	ON_WM_INITMENU()
	ON_WM_PAINT()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
	ON_COMMAND_RANGE(VIDEO_MENU_BASE, VIDEO_MENU_END, OnVideoMenu)
	ON_MESSAGE(VIDEO_REPAINT, OnRepaintWnd)
	//}}AFX_MSG_MAP
	ON_WM_CONTEXTMENU()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPlayWnd message handlers
BOOL CPlayWnd::OnEraseBkgnd(CDC* pDC) 
{
	// TODO: Add your message handler code here and/or call default
	CRect rt;

	GetClientRect(&rt);

	CBrush br;
	br.CreateSolidBrush(VIDEO_BACK_COLOR);
	pDC->FillRect(&rt,&br);
	return CWnd::OnEraseBkgnd(pDC);
}

void CPlayWnd::ShowToolTips(TCHAR * strInfo)
{
	INITCOMMONCONTROLSEX iccex; 
	// struct specifying info about tool in ToolTip control
	TOOLINFO ti;
	unsigned int uid = 0;       // for ti initialization
	TCHAR strTT[256];
	LPTSTR lptstr = strTT;
	RECT rect;                  // for client area coordinates
#ifdef UNICODE
	wcscpy(strTT, strInfo);
#else
	strcpy(strTT, strInfo);
#endif

	/* INITIALIZE COMMON CONTROLS */
	iccex.dwICC = ICC_WIN95_CLASSES;
	iccex.dwSize = sizeof(INITCOMMONCONTROLSEX);
	InitCommonControlsEx(&iccex);
	
	/* CREATE A TOOLTIP WINDOW */
	if(m_hwndTT != 0)
	{	
		::DestroyWindow(m_hwndTT);
		m_hwndTT = NULL;
	}

	m_hwndTT = CreateWindowEx(WS_EX_TOPMOST,
		TOOLTIPS_CLASS,
		NULL,
		WS_POPUP | TTS_NOPREFIX | TTS_ALWAYSTIP,		
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		this->m_hWnd,
		NULL,
		AfxGetApp()->m_hInstance,
		NULL
		);
	
	::SetWindowPos(m_hwndTT,
		HWND_TOPMOST,
		0,
		0,
		0,
		0,
		SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);
			
	GetClientRect (&rect);
					
	ti.cbSize = sizeof(TOOLINFO);
	ti.uFlags = TTF_SUBCLASS;
	ti.hwnd = this->m_hWnd;
	ti.hinst = AfxGetApp()->m_hInstance;
	ti.uId = uid;
	ti.lpszText = lptstr;
	// ToolTip control will cover the whole window
	ti.rect.left = rect.left;    
	ti.rect.top = rect.top;
	ti.rect.right = rect.right;
	ti.rect.bottom = rect.bottom;
	
	/* SEND AN ADDTOOL MESSAGE TO THE TOOLTIP CONTROL WINDOW */
	::SendMessage(m_hwndTT, TTM_ADDTOOL, 0, (LPARAM) (LPTOOLINFO) &ti);	
}

LRESULT CPlayWnd::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	//Handle key lock
	CScreenPannel *pContainer = (CScreenPannel *)GetParent();
	if(pContainer)
	{
		switch(message)
		{
		case WM_LBUTTONDOWN:
		case WM_RBUTTONDOWN:
            {
				 pContainer->SetActivePage(this);
				::PostMessage(pContainer->GetParent()->GetSafeHwnd(),MSG_CHANGEPLAYWND,0,0);
            }
			break;
        case WM_LBUTTONUP:
        case WM_MOUSELEAVE:
            if(m_bOperPtz)
            {
                
            }
            m_bNeedStart = FALSE;
            break;
		case WM_LBUTTONDBLCLK:
			{
				::PostMessage(pContainer->GetParent()->GetSafeHwnd(),MSG_MULTISCREEN,0,0);
			}
			break;
        case WM_MOUSEWHEEL:
            {

            }
            break;
        case WM_MOUSEMOVE:
            {
                
            }
        case WM_SIZE:

            GetClientRect(&m_wndRect);
            m_wndRect.top = m_wndRect.bottom*2 /3;
            break;
		case WM_KEYDOWN:
			{

			}
			break;
		default:
			break;
		}
	}
	return CWnd::DefWindowProc(message, wParam, lParam);
}

#define NAME_MENU_CYCLE_DVR					_T("轮询DVR")
#define NAME_MENU_CYCLE_GROUP				_T"轮询组")
#define NAME_MENU_CYCLE_STOP				_T("停止轮询")

#define NAME_MENU_FULLSCREEN				_T("全屏显示")
#define NAME_MENU_MULTISCREEN				_T("多屏显示")
#define NAME_MENU_AUTOADJUST				_T("自动调整")

#define NAME_MENU_OPENSOUND				    _T("打开声音")
#define NAME_MENU_CLOSESOUND				_T("关闭声音")
#define NAME_MENU_CLOSESTREAM				_T("关闭视频")

#define NAME_MENU_STARTTALK				    _T("启动对讲")
#define NAME_MENU_STOPTALK					_T("停止对讲")

#define NAME_MENU_CODETYPE					_T("码流类型")
#define NAME_MENU_PICQUILTY					_T("图像质量")
#define NAME_MENU_LOCK				        _T("PTZ锁定")
#define NAME_MENU_UNLOCK				    _T("PTZ解锁")
#define NAME_MENU_SIZE                      _T("图像大小")

#define NAME_MENU_OPENSOUND				    _T("打开监听")
#define NAME_MENU_CLOSESOUND				_T("关闭监听")
#define NAME_MENU_TAKE_PIC					_T("拍照")

void CPlayWnd::OnContextMenu(CWnd* /*pWnd*/, CPoint point)
{
	CScreenPannel *pContainer = (CScreenPannel *)GetParent();
	// TODO: 在此处添加消息处理程序代码
	CMenu playMenu;
	playMenu.CreatePopupMenu();
	if (m_splitInfo.Type== SPLIT_TYPE_MONITOR)
	{
		playMenu.AppendMenu(MF_STRING , VIDEO_MENU_CLOSESTREAM, NAME_MENU_CLOSESTREAM);

		if (m_bVoice == TRUE)
		{
			playMenu.AppendMenu(MF_STRING , VIDEO_MENU_STOP_TALK, NAME_MENU_STOPTALK);
		}
		else
		{
			playMenu.AppendMenu(MF_STRING, VIDEO_MENU_START_TALK, NAME_MENU_STARTTALK);
		}

		if (m_bListen == TRUE)
		{
			playMenu.AppendMenu(MF_STRING , VIDEO_MENU_STOP_LISTEN, NAME_MENU_CLOSESOUND);
		}
		else
		{
			playMenu.AppendMenu(MF_STRING, VIDEO_MENU_START_LISTEN, NAME_MENU_OPENSOUND);
		}

		playMenu.AppendMenu(MF_STRING, VIDEO_MENU_TAKE_PIC, NAME_MENU_TAKE_PIC);
	}
	else
	{
		playMenu.AppendMenu(MF_STRING|MF_DISABLED | MF_GRAYED , VIDEO_MENU_CLOSESTREAM, NAME_MENU_CLOSESTREAM);
		playMenu.AppendMenu(MF_STRING |MF_DISABLED | MF_GRAYED, VIDEO_MENU_START_TALK, NAME_MENU_STARTTALK);
		playMenu.AppendMenu(MF_STRING |MF_DISABLED | MF_GRAYED, VIDEO_MENU_START_LISTEN, NAME_MENU_OPENSOUND);
		playMenu.AppendMenu(MF_STRING |MF_DISABLED | MF_GRAYED, VIDEO_MENU_TAKE_PIC, NAME_MENU_TAKE_PIC);
		
	}
	playMenu.AppendMenu(MF_STRING | pContainer->GetFullScreen()    ? MF_CHECKED : MF_UNCHECKED, VIDEO_MENU_FULLSCREEN, NAME_MENU_FULLSCREEN);
	playMenu.TrackPopupMenu( TPM_LEFTALIGN,point.x,point.y,this);

}

void CPlayWnd::OnVideoMenu(UINT nID)
{
	CScreenPannel *pContainer = (CScreenPannel *)GetParent();
	switch(nID)
	{
	case VIDEO_MENU_CLOSESTREAM:
		::PostMessage(pContainer->GetParent()->GetSafeHwnd(),VIDEO_MENU_CLOSESTREAM,(WPARAM)m_sDeviceUID,0);
		//StopRealStream();
		break;
	case VIDEO_MENU_START_TALK:
		//::PostMessage(pContainer->GetParent()->GetSafeHwnd(),VIDEO_MENU_START_TALK,(WPARAM)this,0);
		if( m_objHandle )
		{
			apiStartAudio(m_objHandle);
			apiStartSpeak(m_objHandle, MEDIA_CODEC_AUDIO_ADPCM);
			m_bVoice = TRUE;
		}
		break;
	case VIDEO_MENU_STOP_TALK:
		//::PostMessage(pContainer->GetParent()->GetSafeHwnd(),VIDEO_MENU_STOP_TALK,(WPARAM)this,0);
		if(m_objHandle)
		{
			apiStopAudio(m_objHandle);
			apiStopSpeak(m_objHandle);
			WIN_Audio_reset();
			m_bVoice = FALSE;
		}
		break;
	case VIDEO_MENU_FULLSCREEN:
		::PostMessage(pContainer->GetParent()->GetSafeHwnd(),VIDEO_MENU_FULLSCREEN,(WPARAM)this,0);
		break;
	case VIDEO_MENU_START_LISTEN:
		{
			if(m_objHandle)
			{
				apiStartAudio(m_objHandle);
				m_bListen = TRUE;
			}
		}
		break;
	case VIDEO_MENU_STOP_LISTEN:
		{
			if(m_objHandle)
			{
				apiStopAudio(m_objHandle);
				m_bListen = FALSE;
			}
		}
		break;
	case VIDEO_MENU_TAKE_PIC:
		{
			if(m_lStreamPlayer > 0)
			{
				SYSTEMTIME sysTime;
				GetSystemTime(&sysTime);
				TCHAR sFilePath[256] = {0};
				TCHAR sFilePathDirectory[256] = {0};
				TCHAR sCurName[256] = {0};
				TCHAR szFullPath[MAX_PATH] = {0};
				TCHAR szDir[MAX_PATH] = {0};
				TCHAR szDrive[MAX_PATH] = {0};
				::GetModuleFileName(NULL,szFullPath,MAX_PATH); 
#ifdef UNICODE
				_tsplitpath_s(szFullPath,szDrive,sizeof(szDrive),szDir,sizeof(szDir),NULL,NULL,NULL,NULL);
				_stprintf_s(sFilePath, _T("%s%s\\CaptureImage\\%04d_%04d_%04d_%02d_%02d_%02d.jpg"),
					szDrive, szDir, sysTime.wYear, sysTime.wMonth, sysTime.wDay, sysTime.wHour, sysTime.wMinute, sysTime.wSecond);
				_stprintf_s(sFilePathDirectory, _T("%s%s\\CaptureImage"), szDrive, szDir);
#else
				_splitpath_s(szFullPath,szDrive,sizeof(szDrive),szDir,sizeof(szDir),NULL,NULL,NULL,NULL);
				sprintf_s(sFilePath,sizeof(char)*256,"%s%s\\CaptureImage\\%04d_%04d_%04d_%02d_%02d_%02d.jpg",
					szDrive, szDir, sysTime.wYear, sysTime.wMonth, sysTime.wDay, sysTime.wHour, sysTime.wMinute, sysTime.wSecond);
				sprintf_s(sFilePathDirectory, sizeof(char)*256,"%s%s\\CaptureImage", szDrive, szDir);
#endif
				//
				WIN32_FIND_DATA wfd;
				HANDLE hFind= FindFirstFile(sFilePathDirectory, &wfd);
				if((hFind!=INVALID_HANDLE_VALUE)&&
					(wfd.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY))
				{
					
				}
				else
				{
					::CreateDirectory(sFilePathDirectory, NULL);
				}
				FindClose(hFind);

				SaveBitmap(m_lStreamPlayer, sFilePath);
			}
		}
		break;
	}
}

BOOL CPlayWnd::GetSplitInfo(SplitInfoNode* info)
{
	if (!info)
	{
		return FALSE;
	}
	
	memcpy(info, &m_splitInfo, sizeof(SplitInfoNode));	

	return TRUE;
}

BOOL CPlayWnd::SetSplitInfo(SplitInfoNode* info)
{
	if (!info)
	{
		return FALSE;
	}
	
	memcpy(&m_splitInfo, info, sizeof(SplitInfoNode));
	
	return TRUE;
}

LRESULT CPlayWnd::OnRepaintWnd(WPARAM wParam, LPARAM lParam)
{
	Invalidate();
	UpdateWindow();

	return 0;
}

BOOL CPlayWnd::DestroyWindow() 
{	
    DeleteCriticalSection(&m_operCS);

	return CWnd::DestroyWindow();
}


void CPlayWnd::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	if(!m_bIsPlaying && m_csErrorCode.GetLength())
	{
		dc.SetBkMode(TRANSPARENT);
		CRect rcClient;
		GetClientRect(&rcClient);
		dc.SetTextAlign(dc.GetTextAlign() | TA_CENTER | VTA_CENTER);
		dc.TextOut(rcClient.Width()/2, rcClient.Height()/2, m_csErrorCode, m_csErrorCode.GetLength());
	}
	// Do not call CWnd::OnPaint() for painting messages
}


//2010-07-01 窗口分屏切换
void CPlayWnd::OnSize(UINT nType, int cx, int cy) 
{
	CWnd::OnSize(nType, cx, cy);

	CWnd * pWnd = GetWindow (GW_CHILD);
    if(pWnd != NULL)
    {
        CRect rc;
        GetClientRect(&rc);

        pWnd->MoveWindow(rc);
        pWnd->Invalidate();
    }	
}

BOOL CPlayWnd::SetErrorCode(TCHAR *inErrString)
{
	m_csErrorCode = inErrString;
	return TRUE;
}

BOOL CPlayWnd::PlayRealStream(tkHandle objHandle)
{
	m_lStreamPlayer = InitialPlayer(GetSafeHwnd());
	m_objHandle = objHandle;
	apiRegCB_OnAVFrame(objHandle, OnAVFrame, this);
	apiStartVideo(objHandle);
	m_bIsPlaying = TRUE;
	m_splitInfo.Type = SPLIT_TYPE_MONITOR;
	return TRUE;
}

BOOL CPlayWnd::StopRealStream()
{
	m_bIsPlaying = FALSE;
	apiRegCB_OnAVFrame(m_objHandle, NULL, NULL);
	if(m_lStreamPlayer > 0)
		UninitalPlayer(m_lStreamPlayer);
	apiStopVideo(m_objHandle);
	m_csErrorCode = "欢迎使用";
	m_splitInfo.Type = SPLIT_TYPE_NULL;
	Invalidate();
	return TRUE;
}

tkVOID WINAPI CPlayWnd::OnAVFrame(tkCHAR *pData, tkINT32 nSize, tkVOID *pFrmInfo, tkINT32 nFrmInfoSize, tkUINT32 nTag, tkVOID *param)
{
	CPlayWnd *pThis = (CPlayWnd *)param;

	if(pThis->m_bIsPlaying == FALSE)
		return ;

	if( pData == NULL || pFrmInfo == NULL || nSize <= 0 || pThis->m_lStreamPlayer <= 0) return;

	FRAMEINFO_t *pstFrmInfo =( FRAMEINFO_t *)pFrmInfo;
	switch(pstFrmInfo->codec_id)
	{
		case MEDIA_CODEC_VIDEO_H264:
			{
				pThis->UpdatePlayInfo();
				AddFrameData(pThis->m_lStreamPlayer, (BYTE*)pData, nSize, pFrmInfo);
			}
			break;

		case MEDIA_CODEC_VIDEO_MJPEG:
			AddFrameData(pThis->m_lStreamPlayer, (BYTE*)pData, nSize, pFrmInfo);
			break;

		case MEDIA_CODEC_AUDIO_PCM:
			{
				AddFrameData(pThis->m_lStreamPlayer, (BYTE*)pData, nSize, pFrmInfo);
			}
			break;

		case MEDIA_CODEC_AUDIO_ADPCM:
			{
				AddFrameData(pThis->m_lStreamPlayer, (BYTE*)pData, nSize, pFrmInfo);
			}
			break;

		case MEDIA_CODEC_AUDIO_G726:
			{
				TRACE(_T("AVPlay, cam%d MEDIA_CODEC_AUDIO_G726, nSize=%d\n"), nTag, nSize);
			}break;

		default:;
	}
}

void CPlayWnd::UpdatePlayInfo()
{
	static CString CONN_MODE[]={_T("P2P"), _T("RLY"), _T("LAN")};
	tkUCHAR nOnlineNum=0, nConnMode=0;
	tkFLOAT fFrameRate=0.0f, fBytesRate=0.0f;
	apiGetObjInfo(m_objHandle, GOI_OnlineNum, &nOnlineNum);
	apiGetObjInfo(m_objHandle, GOI_ConnectMode, &nConnMode);
	apiGetObjInfo(m_objHandle, GOI_FrameRate, &fFrameRate);
	apiGetObjInfo(m_objHandle, GOI_BytesRate, &fBytesRate);
	m_csPlayInfo.Format(_T("%s  Online=%d  %0.2f FPS  %0.3f kbps"), 
		CONN_MODE[nConnMode], nOnlineNum, fFrameRate,fBytesRate*8);
	UpdateOSDData(m_lStreamPlayer, m_csPlayInfo.GetBuffer(), m_csPlayInfo.GetLength());
}

void CPlayWnd::SetDeviceUID(TCHAR *inDeviceUID)
{
#ifdef UNICODE
	wcscpy_s(m_sDeviceUID, 64, inDeviceUID);
#else
	strcpy_s(m_sDeviceUID, 64, inDeviceUID);
#endif
}