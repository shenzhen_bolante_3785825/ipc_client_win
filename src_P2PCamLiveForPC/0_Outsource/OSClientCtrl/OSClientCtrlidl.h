

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 6.00.0366 */
/* at Wed Jul 17 15:29:27 2013
 */
/* Compiler settings for .\OSClientCtrl.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __OSClientCtrlidl_h__
#define __OSClientCtrlidl_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef ___DOSClientCtrl_FWD_DEFINED__
#define ___DOSClientCtrl_FWD_DEFINED__
typedef interface _DOSClientCtrl _DOSClientCtrl;
#endif 	/* ___DOSClientCtrl_FWD_DEFINED__ */


#ifndef ___DOSClientCtrlEvents_FWD_DEFINED__
#define ___DOSClientCtrlEvents_FWD_DEFINED__
typedef interface _DOSClientCtrlEvents _DOSClientCtrlEvents;
#endif 	/* ___DOSClientCtrlEvents_FWD_DEFINED__ */


#ifndef __OSClientCtrl_FWD_DEFINED__
#define __OSClientCtrl_FWD_DEFINED__

#ifdef __cplusplus
typedef class OSClientCtrl OSClientCtrl;
#else
typedef struct OSClientCtrl OSClientCtrl;
#endif /* __cplusplus */

#endif 	/* __OSClientCtrl_FWD_DEFINED__ */


#ifdef __cplusplus
extern "C"{
#endif 

void * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void * ); 


#ifndef __OSClientCtrlLib_LIBRARY_DEFINED__
#define __OSClientCtrlLib_LIBRARY_DEFINED__

/* library OSClientCtrlLib */
/* [control][helpstring][helpfile][version][uuid] */ 


EXTERN_C const IID LIBID_OSClientCtrlLib;

#ifndef ___DOSClientCtrl_DISPINTERFACE_DEFINED__
#define ___DOSClientCtrl_DISPINTERFACE_DEFINED__

/* dispinterface _DOSClientCtrl */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__DOSClientCtrl;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("A8D053A1-6B0A-4513-B27D-1443DABCD66D")
    _DOSClientCtrl : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _DOSClientCtrlVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _DOSClientCtrl * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _DOSClientCtrl * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _DOSClientCtrl * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _DOSClientCtrl * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _DOSClientCtrl * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _DOSClientCtrl * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _DOSClientCtrl * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _DOSClientCtrlVtbl;

    interface _DOSClientCtrl
    {
        CONST_VTBL struct _DOSClientCtrlVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _DOSClientCtrl_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _DOSClientCtrl_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _DOSClientCtrl_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _DOSClientCtrl_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _DOSClientCtrl_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _DOSClientCtrl_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _DOSClientCtrl_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___DOSClientCtrl_DISPINTERFACE_DEFINED__ */


#ifndef ___DOSClientCtrlEvents_DISPINTERFACE_DEFINED__
#define ___DOSClientCtrlEvents_DISPINTERFACE_DEFINED__

/* dispinterface _DOSClientCtrlEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__DOSClientCtrlEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("5C774740-557F-4BE3-99A0-3ED198CBEB65")
    _DOSClientCtrlEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _DOSClientCtrlEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _DOSClientCtrlEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _DOSClientCtrlEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _DOSClientCtrlEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _DOSClientCtrlEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _DOSClientCtrlEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _DOSClientCtrlEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _DOSClientCtrlEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _DOSClientCtrlEventsVtbl;

    interface _DOSClientCtrlEvents
    {
        CONST_VTBL struct _DOSClientCtrlEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _DOSClientCtrlEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _DOSClientCtrlEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _DOSClientCtrlEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _DOSClientCtrlEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _DOSClientCtrlEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _DOSClientCtrlEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _DOSClientCtrlEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___DOSClientCtrlEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_OSClientCtrl;

#ifdef __cplusplus

class DECLSPEC_UUID("3493FDCE-95FE-43F3-ABAD-2B88AC37F200")
OSClientCtrl;
#endif
#endif /* __OSClientCtrlLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


