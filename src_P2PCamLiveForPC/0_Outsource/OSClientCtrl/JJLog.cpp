#include "stdafx.h"
#include <io.h>
#include "JJLog.h"


CJJLog* CJJLog::m_pJJLog = NULL;

CJJLog::CJJLog()
{
	m_filename = NULL;
	m_append = false;
    m_hlogfile = NULL;
    m_todebug = true;
    m_toconsole = false;
    m_tofile = false;
    m_level = 3;
	SetMode(g_JJLog_ToDebug);
}

CJJLog::~CJJLog()
{
#ifndef UNICODE
    if (m_filename != NULL)
        free(m_filename);
    CloseFile();
#endif
}

void CJJLog::SetMode(int mode)
{
#ifndef UNICODE
	m_mode = mode;
    if (mode & g_JJLog_ToDebug)
        m_todebug = true;
    else
        m_todebug = false;

    if (mode & g_JJLog_ToFile)  
    {
		if (!m_tofile)
			OpenFile();
	} 
    else 
    {
		CloseFile();
        m_tofile = false;
    }
    
    if (mode & g_JJLog_ToConsole) 
    {
        if (!m_toconsole) 
        {
            AllocConsole();
            fclose(stdout);
            fclose(stderr);
#ifdef _MSC_VER
            int fh = _open_osfhandle((long)GetStdHandle(STD_OUTPUT_HANDLE), 0);
            _dup2(fh, 1);
            _dup2(fh, 2);
            _fdopen(1, "wt");
            _fdopen(2, "wt");
            printf("fh is %d\n",fh);
            fflush(stdout);
#endif
        }

        m_toconsole = true;
    } 
    else 
    {
        m_toconsole = false;
    }
#endif
}


void CJJLog::SetLevel(int level) 
{
    m_level = level;
}

void CJJLog::SetFile(const char* filename, bool append) 
{
#ifndef UNICODE
	if (m_filename != NULL)
		free(m_filename);
	m_filename = strdup(filename);
	m_append = append;
	if (m_tofile)
		OpenFile();
#endif
}

void CJJLog::OpenFile()
{
#ifndef UNICODE
	if (m_filename == NULL)
	{
        m_todebug = true;
        m_tofile = false;
        Print(0, "Error opening log file\n");
		return;
	}

    m_tofile  = true;
    
	if (!m_append)
	{
		char *backupfilename = new char[strlen(m_filename)+5];
		if (backupfilename)
		{
			strcpy(backupfilename, m_filename);
			strcat(backupfilename, ".bak");
			MoveFileEx(m_filename, backupfilename, MOVEFILE_REPLACE_EXISTING);
			delete [] backupfilename;
		}
	}

	CloseFile();

    m_hlogfile = CreateFile(
        m_filename,  GENERIC_WRITE, FILE_SHARE_READ, NULL,
        OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL  );
    
    if (m_hlogfile == INVALID_HANDLE_VALUE) 
    {
        m_todebug = true;
        m_tofile = false;
        Print(0, "Error opening log file %s\n", m_filename);
    }

    if (m_append) 
    {
        SetFilePointer( m_hlogfile, 0, NULL, FILE_END );
    } 
    else 
    {
        SetEndOfFile( m_hlogfile );
    }
#endif
}

void CJJLog::CloseFile() 
{
#ifndef UNICODE
    if (m_hlogfile != NULL) 
    {
        CloseHandle(m_hlogfile);
        m_hlogfile = NULL;
    }
#endif
}

void CJJLog::ReallyPrint(const char* format, va_list ap) 
{
#ifndef UNICODE
    char line[1024] = {0};

    SYSTEMTIME st;
    GetLocalTime(&st);

    if (m_todebug) 
    {
        vsprintf(line, format, ap);
        OutputDebugString(line);
    }

    sprintf(line, "[%04d-%02d-%02d %02d:%02d:%02d]", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond);
    vsprintf(line+21, format, ap);
    
    if (m_toconsole) 
    {
        DWORD byteswritten;
        WriteConsole(GetStdHandle(STD_OUTPUT_HANDLE), line, strlen(line), &byteswritten, NULL); 
    }
    
    if (m_tofile && (m_hlogfile != NULL)) 
    {
        DWORD byteswritten;
        WriteFile(m_hlogfile, line, strlen(line), &byteswritten, NULL); 
    }
#endif
}


void CJJLog::Print(int level, const char* format, ...)
{
#ifndef UNICODE
    if (level > m_level) 
        return;
    va_list ap;
    va_start(ap, format);
    ReallyPrint(format, ap);
    va_end(ap);
#endif
}
