// OSClientCtrlPropPage.cpp : COSClientCtrlPropPage 属性页类的实现。

#include "stdafx.h"
#include "OSClientCtrl.h"
#include "OSClientCtrlPropPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNCREATE(COSClientCtrlPropPage, COlePropertyPage)



// 消息映射

BEGIN_MESSAGE_MAP(COSClientCtrlPropPage, COlePropertyPage)
END_MESSAGE_MAP()



// 初始化类工厂和 guid

IMPLEMENT_OLECREATE_EX(COSClientCtrlPropPage, "OSCLIENTCTRL.OSClientCtrlPropPage.1",
	0x7b841ee6, 0xf2cd, 0x4626, 0x82, 0x59, 0xa2, 0x81, 0xb1, 0xf, 0x3b, 0x5c)



// COSClientCtrlPropPage::COSClientCtrlPropPageFactory::UpdateRegistry -
// 添加或移除 COSClientCtrlPropPage 的系统注册表项

BOOL COSClientCtrlPropPage::COSClientCtrlPropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_OSCLIENTCTRL_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}



// COSClientCtrlPropPage::COSClientCtrlPropPage - 构造函数

COSClientCtrlPropPage::COSClientCtrlPropPage() :
	COlePropertyPage(IDD, IDS_OSCLIENTCTRL_PPG_CAPTION)
{
}



// COSClientCtrlPropPage::DoDataExchange - 在页和属性间移动数据

void COSClientCtrlPropPage::DoDataExchange(CDataExchange* pDX)
{
	DDP_PostProcessing(pDX);
}



// COSClientCtrlPropPage 消息处理程序
