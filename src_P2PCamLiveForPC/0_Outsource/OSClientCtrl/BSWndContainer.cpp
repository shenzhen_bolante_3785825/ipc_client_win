// BSWndContainer.cpp : implementation file
//

#include "stdafx.h"
#include "BSWndContainer.h"
#include "PlayWnd.h"
#include <math.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CBSWndContainer
CBSWndContainer::CBSWndContainer()
{
	// init active page pointer
	m_pActivePage	= NULL;
	m_winUseType = WIN_USE_NULL;

	// init window state
	m_bFullScreen	= FALSE;	// 全屏标记
	m_bMultiScreen	= TRUE;	// 多屏标记
	m_bAutoAdjustPos= FALSE;//ClientParam.m_bAutoAdjustPos ;	// 自动调节标记

	SetDrawActivePage(TRUE);	//	启动边框

	m_nShowPortion=100;			//	显示比例
	// 	m_splitCol = 2;
	// 	m_splitRow = 1;
	// 	m_splitType = 2;
	m_splitRow=2;
	m_splitCol=2;
	m_splitType=4;
	m_bSpecial = FALSE;
	m_bVideoFlag = FALSE;
}

CBSWndContainer::~CBSWndContainer()
{
	// remove all pages
	while(!m_PageList.IsEmpty())
		m_PageList.RemoveHead();

}


BEGIN_MESSAGE_MAP(CBSWndContainer, CWnd)
	//{{AFX_MSG_MAP(CBSWndContainer)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CBSWndContainer member functions

///////////////////////////////////////////////////
// call this function to create container object.
// it is override from cwnd class
BOOL CBSWndContainer::Create( LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext )
{
	dwStyle|=WS_EX_TOOLWINDOW;
	return CWnd::Create(lpszClassName,lpszWindowName,dwStyle,rect,pParentWnd,nID,pContext );
}


///////////////////////////////////////////////////
// call this function to add a page wnd to 
// container. if success retrun TRUE,else return 
// FALSE.
BOOL CBSWndContainer::AddPage(CWnd *pWnd, BOOL bRepaint)
{
	// check parameter
	if(	!pWnd || !IsWindow(pWnd->m_hWnd) )	return FALSE;

	// check list 
	POSITION pos=m_PageList.Find(pWnd);
	if(pos!=NULL) 
	{
		TRACE("This Window has been added to container, the operation will terminate.\n");
		return TRUE;
	}

	// added page
	m_PageList.AddTail(pWnd);

	if( m_bDrawActive ) DrawActivePage(FALSE);

	// reset active page, frank remove for repeat show
	//SetActivePage(pWnd, bRepaint);

	return TRUE;
}

///////////////////////////////////////////////////
// call this function to remove a page wnd from
// container. 
CWnd *CBSWndContainer::DelPage(CWnd *pWnd)
{
	// check list
	POSITION pos=m_PageList.Find(pWnd);
	if(pos==NULL)
	{
		TRACE("This Window is not a member of container, the operation will terminate.\n");
		return NULL;
	}
	if(pWnd==m_pActivePage)
		if(m_pActivePage==GetPrevPage(pWnd))//m_PageList.IsEmpty()?NULL:m_PageList.GetHead();
			m_pActivePage=NULL;
		else m_pActivePage=GetPrevPage(pWnd);

		m_PageList.RemoveAt(pos);

		if (pWnd)
		{
			pWnd->ShowWindow(SW_HIDE);
		}

		//	Invalidate();

		return pWnd;
}

///////////////////////////////////////////////////
// call this function to remove active page from
// container.
CWnd *CBSWndContainer::DelPage()
{
	return DelPage(m_pActivePage);
}

///////////////////////////////////////////////////
// call this function to set a page to be active
// page.
void CBSWndContainer::SetActivePage(CWnd *pWnd, BOOL bRepaint, BOOL bVideo)
{
	if(bVideo)
	{
		POSITION pos=m_VideoList.Find(pWnd);
		if(pos == NULL)
		{
			m_VideoList.AddTail(pWnd);
			DrawActivePage(FALSE);
			return ;
		}
	}
	// check parameter
	//2009-10-20 接收键盘消息
	SetFocus();

	if(	!pWnd || !IsWindow(pWnd->m_hWnd) )	return;

	// if pWnd is the Active Page, return 
	if( m_pActivePage==pWnd ) return;

	// check list
	POSITION pos=m_PageList.Find(pWnd);
	if(pos==NULL)
	{
		TRACE("__This Window is not a member of container, the operation will terminate.\n");
		return;
	}

	if(bRepaint) UpdateWnd();

	if( m_bDrawActive ) DrawActivePage(FALSE);

	m_pActivePage=pWnd;

	//if( m_bDrawActive ) DrawActivePage(TRUE);
}

///////////////////////////////////////////////////
// call this function to get the active page's
// pointer. if no active page,return NULL;
CWnd *CBSWndContainer::GetActivePage()
{
	return m_pActivePage;
}

CWnd *CBSWndContainer::GetTailPage()
{
	return m_PageList.GetTail();
}

///////////////////////////////////////////////////
// call this function to get the next page by
// the page that user defined. if the defined 
// page is not find in container, return NULL.
CWnd *CBSWndContainer::GetNextPage(CWnd *pWnd)
{
	// check parameter
	if(	!pWnd || !IsWindow(pWnd->m_hWnd) ) return NULL;

	// check list
	POSITION pos=m_PageList.Find(pWnd);
	if(pos==NULL)
	{
		TRACE("This Window is not a member of container, the operation will terminate.\n");
		return NULL;
	}

	//
	m_PageList.GetNext(pos);
	if(pos==NULL)
		return m_PageList.GetHead();
	else 
		return m_PageList.GetNext(pos);
}

///////////////////////////////////////////////////
// call this function to get the prev page by
// the page that user defined. if the defined
// page is not find in container,return NULL.
CWnd *CBSWndContainer::GetPrevPage(CWnd *pWnd)
{
	// check parameter
	if(	!pWnd || !IsWindow(pWnd->m_hWnd) ) return NULL;

	// check list
	POSITION pos=m_PageList.Find(pWnd);
	if(pos==NULL)
	{
		TRACE("This Window is not a member of container, the operation will terminate.\n");
		return NULL;
	}

	//
	m_PageList.GetPrev(pos);
	if(pos==NULL)
		return m_PageList.GetTail();
	else 
		return m_PageList.GetPrev(pos);
}

CWnd *CBSWndContainer::GetPage(int nIndex)
{
	CWnd *pRet = NULL;
	POSITION pos = m_PageList.FindIndex(nIndex);
	if(pos == NULL) return pRet;

	return m_PageList.GetAt(pos);
}

int CBSWndContainer::GetCount() const
{
	return m_PageList.GetCount();
}

///////////////////////////////////////////////////
// call this function to  page wnds,when
// the window is resized.
void CBSWndContainer::UpdateWnd()
{
	if(!IsWindowVisible()) return;
	if(IsIconic()) return;
	/////////////////////
	//计算显示总区域

	//得到窗口的设备坐标
	CRect rtContainer;
	GetClientRect(&rtContainer);
	GetShowRect(&rtContainer);
	rtContainer.DeflateRect(1,1);

	//调整Container位置
	if(m_bAutoAdjustPos /*&& m_splitCol == m_splitRow*/)		
		AdjustRect(&rtContainer);

	/////////////////////
	//
	if(m_bMultiScreen)
	{ //多屏状态
		CRect rt;
		CRect offsetRt;
		int offsetWidth;
		int offsetHeight;
		int nCount=m_PageList.GetCount();
		int i=0;
		int indexOutput = 0;
		int nSplit = 1;
		CWnd *p;

		if(m_winUseType != WIN_USE_CARDCONFIG)
		{
			POSITION pos=m_PageList.GetHeadPosition();
			while(pos != NULL)
			{		
				rt=rtContainer;
				if(m_bVideoFlag)
					rt.top += 30;
				CalcPageRect(&rt,indexOutput,nCount);

#if defined TV_WALL_ON
				if(ClientParam.m_bUseCard && m_winUseType == WIN_USE_PREVIEW)
				{
					nSplit = ClientParam.m_tvSrvInfo.tvOutput[indexOutput].iSplitType;										
				}
#endif
				if(nSplit != 1 && nSplit != 0)
				{
					int nSqrt = (int)sqrt((float)nSplit);

					offsetWidth = (rt.right-rt.left)/nSqrt;
					offsetHeight = (rt.bottom-rt.top)/nSqrt;						

					for(int j=0; j< nSplit && p != NULL && pos != NULL; j++)
					{
						p=m_PageList.GetNext(pos);
						//int nSpace = (WINDOW_SPACE + (((CPlayWnd *)p)->GetInGroup()?2:0));
						offsetRt = rt;

						offsetRt.left += offsetWidth*(j%nSqrt) ;
						offsetRt.top += offsetHeight*(j/nSqrt);
						offsetRt.right = offsetRt.left+offsetWidth ;
						offsetRt.bottom = offsetRt.top+offsetHeight;

						offsetRt.DeflateRect(WINDOW_SPACE,WINDOW_SPACE,WINDOW_SPACE,WINDOW_SPACE); //窗口之间的间隔
						p->MoveWindow(&offsetRt);
						p->ShowWindow(SW_SHOW);
#if defined TV_WALL_ON
						((CPlayWnd *)p)->m_outputIndex = indexOutput;
						((CPlayWnd *)p)->m_regionIndex = j;
#endif
						i++;
					}					
				}
				else/*if(nSplit != 1 && nSplit != 0)*/
				{
					p=m_PageList.GetNext(pos);
					if(p != NULL)
					{
						rt.DeflateRect(WINDOW_SPACE,WINDOW_SPACE,WINDOW_SPACE,WINDOW_SPACE); //窗口之间的间隔
						p->MoveWindow(&rt);
						p->ShowWindow(SW_SHOW);
						i++;
					}
#if defined TV_WALL_ON
					((CPlayWnd *)p)->m_outputIndex = indexOutput;
					((CPlayWnd *)p)->m_regionIndex = 0;
#endif
				}
				indexOutput++;

#if defined TV_WALL_ON
				if(ClientParam.m_bUseCard && m_winUseType == WIN_USE_PREVIEW)
				{
					if(g_pTvCardParam != NULL)
					{
						if(indexOutput >= g_pTvCardParam->iDisplayChannel)
							break;
					}
				}
#endif				
			}
		}
		else/*if(m_winUseType != WIN_USE_CARDCONFIG)*/
		{
			for(POSITION pos=m_PageList.GetHeadPosition();pos!=NULL;)
			{
				CWnd *p=m_PageList.GetNext(pos);

				rt=rtContainer;
				CalcPageRect(&rt,i,nCount);
				rt.DeflateRect(WINDOW_SPACE,WINDOW_SPACE,WINDOW_SPACE,WINDOW_SPACE); //窗口之间的间隔
				p->MoveWindow(&rt);
				p->ShowWindow(SW_SHOW);
				i++;

			}
		}

		if( m_bDrawActive && m_PageList.GetCount()>1 ) DrawActivePage(TRUE);
	}
	else/*if(m_bMultiScreen)*/
	{ //单屏状态
		for(POSITION pos=m_PageList.GetHeadPosition();pos!=NULL;)
		{
			CWnd *p=m_PageList.GetNext(pos);
			if(p==m_pActivePage)
				p->MoveWindow(&rtContainer);
			else 
			{
				if(m_bFullScreen)
					p->MoveWindow(0,0,1,1);
				else
					p->MoveWindow(rtContainer.right+1,rtContainer.bottom+1,1,1);
			}
		}
	}
}

///////////////////////////////////////////////////
// full screen
void CBSWndContainer::SetFullScreen(BOOL bFlag)
{
//  	if(bFlag==m_bFullScreen) return;
//  
//  	int cx=GetSystemMetrics(SM_CXSCREEN);
//  	int cy=GetSystemMetrics(SM_CYSCREEN);
//  
//  	if( bFlag )
//  	{//全屏
//  		//得到显示器分辨率
//  
//  		//保存位置信息
//  		GetWindowPlacement(&_temppl);
//  
//  		//2010-07-07 不能使用，会影响窗口的正常风格
//  		//ModifyStyle(WS_CHILD,WS_POPUP);
//  
//  		//修改父窗口
//  		_tempparent=SetParent(NULL);
//  		_tempparent->ShowWindow(SW_HIDE);
//  		//移动窗口
//  		MoveWindow(0,0,cx,cy);
//  		SetWindowPos(&wndTopMost,0,0,cx,cy,NULL);
//  		//NvsCliTvCardPreview(((CPlayWnd *)m_pActivePage)->m_nWndID, m_pActivePage->m_hWnd, GetMultiScreen());
//  
//  	}
//  	else
//  	{//还原
//  		//还原父窗口
//  		_tempparent->ShowWindow(SW_SHOW);
//  		SetParent(_tempparent);
//  		//还原风格
//  		ModifyStyle(WS_POPUP,WS_CHILD);
//  		//还原位置
//  		SetWindowPlacement(&_temppl);
//  		SetWindowPos(&wndTopMost,0,0,cx,cy,NULL);
//  	}

	m_bFullScreen=bFlag;
	//Invalidate();

	//ClientParam.m_bFullScreen = m_bFullScreen;
}
BOOL CBSWndContainer::GetFullScreen()
{
	return m_bFullScreen;
}

///////////////////////////////////////////////////
// multiscreen
void CBSWndContainer::SetMultiScreen(BOOL bFlag)
{
	//if(m_bMultiScreen==bFlag) return;
	m_bMultiScreen=bFlag;
	//Invalidate();
	//ClientParam.m_bMultiScreen = m_bMultiScreen;
}
BOOL CBSWndContainer::GetMultiScreen()
{
	return m_bMultiScreen;
}

//////////////////////////////////////////////////
// autoadjustpos
void CBSWndContainer::SetAutoAdjustPos(BOOL bFlag)
{
	if(m_bAutoAdjustPos==bFlag) return;
	m_bAutoAdjustPos=bFlag;
	Invalidate();

	//ClientParam.m_bAutoAdjustPos = m_bAutoAdjustPos;
}
BOOL CBSWndContainer::GetAutoAdjustPos()
{
	return m_bAutoAdjustPos;
}

//////////////////////////////////////////////////
// draw active page
void CBSWndContainer::SetDrawActivePage(BOOL bFlag,COLORREF clrTopLeft,COLORREF clrBottomRight)
{
	if(m_bDrawActive==bFlag) return;
	if(bFlag)
	{
		m_clrTopLeft=clrTopLeft;
		m_clrBottomRight=clrBottomRight;
	}
	m_bDrawActive=bFlag;
	DrawActivePage(bFlag);
}
BOOL CBSWndContainer::GetDrawActivePage()
{
	return m_bDrawActive;
}

//////////////////////////////////////////////////
//	显示百分比
//	40 <= nPortion <=100
void CBSWndContainer::SetShowPortion(int nPortion)
{
	if(m_nShowPortion==nPortion) return;
	if(m_nShowPortion<40) m_nShowPortion=40;
	if(m_nShowPortion>100) m_nShowPortion=100;
	m_nShowPortion=nPortion;
	Invalidate();
}
int  CBSWndContainer::GetShowPortion()
{
	return m_nShowPortion;
}

///////////////////////////////////////////////////
// clean the no useful page in the container,
// return the page count.
int CBSWndContainer::UpdateList()
{
	POSITION posPrev;
	for(POSITION pos=m_PageList.GetHeadPosition();pos!=NULL;)
	{
		posPrev=pos;
		CWnd *p=m_PageList.GetNext(pos);
		if(!IsWindow(p->m_hWnd))
			m_PageList.RemoveAt(posPrev);
	}
	return m_PageList.GetCount();
}

///////////////////////////////////////////////////
// get a rect by the index of a child
void CBSWndContainer::CalcPageRect(LPRECT lpRect,int nIndex,int nPageCount)
{
	if((nPageCount<=0)||(nIndex>=nPageCount))
	{
		lpRect->left=lpRect->right=lpRect->top=lpRect->bottom=0;
		return;
	}

	if(m_splitCol == 0)
	{
		m_splitCol = 1;
	}

	if(m_splitRow == 0)
	{
		m_splitRow = 1;
	}
	//get row count
	//int nRow=0;
	//while((nRow)*(nRow)<nPageCount) nRow++;

	//get singledlg width and height
	int nWidth;
	int nHeight;
	int nRcWidth;
	int nRcHeight;
	int offsetX;
	int offsetY;
	CPoint pt;

	nRcWidth = lpRect->right-lpRect->left;
	nRcHeight = lpRect->bottom-lpRect->top;

	nWidth = nRcWidth/m_splitCol;
	nHeight = nRcHeight/m_splitRow;
	offsetX = 0;
	offsetY = 0;

	if(m_splitRow == 3 && m_bSpecial)
	{
		if(nIndex == 0)
		{
			pt.x=lpRect->left + offsetX;
			pt.y=lpRect->top + offsetY;

			lpRect->left=pt.x;
			lpRect->top=pt.y;
			lpRect->right=lpRect->left+nWidth*2;
			lpRect->bottom=lpRect->top+nHeight*2;
		}
		else if(nIndex == 1 || nIndex == 2)
		{
			pt.x=lpRect->left+nWidth*2 + offsetX;
			pt.y=lpRect->top+nHeight*(nIndex-1) + offsetY;

			lpRect->left=pt.x;
			lpRect->top=pt.y;
			lpRect->right=lpRect->left+nWidth;
			lpRect->bottom=lpRect->top+nHeight;
		}
		else if(nIndex == 3 || nIndex == 4 || nIndex == 5)
		{
			pt.x=lpRect->left+nWidth*(nIndex-3) + offsetX;
			pt.y=lpRect->top+nHeight*2 + offsetY;

			lpRect->left=pt.x;
			lpRect->top=pt.y;
			lpRect->right=lpRect->left+nWidth;
			lpRect->bottom=lpRect->top+nHeight;
		}		
	}
	else if(m_splitRow == 4 && m_bSpecial)
	{
		if(nIndex == 0)
		{
			pt.x=lpRect->left + offsetX;
			pt.y=lpRect->top + offsetY;

			lpRect->left=pt.x;
			lpRect->top=pt.y;
			lpRect->right=lpRect->left+nWidth*3;
			lpRect->bottom=lpRect->top+nHeight*3;
		}
		else if(nIndex == 1 || nIndex == 2 || nIndex == 3)
		{
			pt.x=lpRect->left+nWidth*3 + offsetX;
			pt.y=lpRect->top+nHeight*(nIndex-1) + offsetY;

			lpRect->left=pt.x;
			lpRect->top=pt.y;
			lpRect->right=lpRect->left+nWidth;
			lpRect->bottom=lpRect->top+nHeight;
		}
		else if(nIndex == 4 || nIndex == 5 || nIndex == 6 || nIndex == 7)
		{
			pt.x=lpRect->left+nWidth*(nIndex-4) + offsetX;
			pt.y=lpRect->top+nHeight*3 + offsetY;

			lpRect->left=pt.x;
			lpRect->top=pt.y;
			lpRect->right=lpRect->left+nWidth;
			lpRect->bottom=lpRect->top+nHeight;
		}	
	}
	else
	{
		pt.x=lpRect->left+nWidth*(nIndex%m_splitCol) + offsetX;
		pt.y=lpRect->top+ nHeight*(nIndex/m_splitCol) + offsetY;

		//set rect return back
		lpRect->left=pt.x;
		lpRect->top=pt.y;
		lpRect->right=lpRect->left+nWidth;
		lpRect->bottom=lpRect->top+nHeight;

		/*if(nIndex == 0 && !m_bVideoFlag)
		lpRect->right=lpRect->left+nWidth*2;
		else
		lpRect->right=lpRect->left+nWidth;
		lpRect->bottom=lpRect->top+nHeight;*/
	}
}

void CBSWndContainer::GetPageRect(LPRECT lpRect, int nIndex)
{
	CPlayWnd *plWnd = (CPlayWnd *)GetPage(nIndex);
	if(plWnd != NULL)
	{
		plWnd->GetWindowRect(lpRect);
	}
}
///////////////////////////////////////////////////
// adjust a rect by defined proportion 
void CBSWndContainer::AdjustRect(LPRECT lpRect)
{
	int nWidth=lpRect->right-lpRect->left;
	int nHeight=lpRect->bottom-lpRect->top;
	CPoint pt((lpRect->left+lpRect->right)/2,(lpRect->top+lpRect->bottom)/2);

	int nTemp=nWidth*9/11;
	if(nTemp>nHeight)
	{
		nWidth=nHeight*11/9;
	}
	else if(nTemp<nHeight)
	{
		nHeight=nTemp;
	}
	lpRect->left=pt.x-nWidth/2;
	lpRect->right=pt.x+nWidth/2;
	lpRect->top=pt.y-nHeight/2;
	lpRect->bottom=pt.y+nHeight/2;
}

///////////////////////////////////////////////////
//	按比例得到显示区域
void CBSWndContainer::GetShowRect(LPRECT lpRect)
{
	if(m_nShowPortion<40) m_nShowPortion=40;
	if(m_nShowPortion>100) m_nShowPortion=100;

	int nWidth	= lpRect->right-lpRect->left;
	int nHeight	= lpRect->bottom-lpRect->top;

	int nNewWidth	= (int)(nWidth*m_nShowPortion/100);
	int nNewHeight	= (int)(nHeight*m_nShowPortion/100);

	int ndx	= ( nWidth-nNewWidth )/2;
	int ndy = ( nHeight-nNewHeight )/2;

	lpRect->left	= lpRect->left	+ ndx;
	lpRect->top		= lpRect->top	+ ndy;
	lpRect->right	= lpRect->left	+ nNewWidth;
	lpRect->bottom	= lpRect->top	+ nNewHeight;	
}

///////////////////////////////////////////////////
// draw the frame of active page
void CBSWndContainer::DrawActivePage(BOOL bFlag)
{
	if( !m_bMultiScreen || 
		!m_pActivePage	|| 
		m_PageList.GetCount()<2 
		) return;

	POSITION pos=m_VideoList.GetHeadPosition();
	CWnd *pWnd = NULL;
	while(pos != NULL && !m_bVideoFlag)
	{	
		pWnd = m_VideoList.GetNext(pos);
		CRect rt;
		pWnd->GetWindowRect(&rt);
		ScreenToClient(&rt);
		rt.InflateRect(1,1);

		if(bFlag)
		{
			CDC *pDC=GetDC();
			if(!pDC) return;

			pDC->Draw3dRect(&rt,WND_ACTIVE_COLOR, WND_ACTIVE_COLOR);
			ReleaseDC(pDC);
		}
		else
		{
			InvalidateRect(&rt);
		}
	}


	CRect rt;
	m_pActivePage->GetWindowRect(&rt);
	ScreenToClient(&rt);
	rt.InflateRect(1,1);
	if(bFlag)
	{
		CDC *pDC=GetDC();
		if(!pDC) return;

		pDC->Draw3dRect(&rt,WND_ACTIVE_COLOR, WND_ACTIVE_COLOR);
		if(m_bVideoFlag)
			pDC->DrawText(_T("测试****************************"), -1,CRect( 0, 10, rt.Width(), 80), DT_CENTER | DT_LEFT);


		ReleaseDC(pDC);
	}
	else
	{
		CRect cRtTop,cRtBottom,cRtRight,cRtLeft;
		cRtTop=cRtBottom=cRtRight=cRtLeft=rt;
		cRtTop.bottom=rt.top+1;
		cRtBottom.top=rt.bottom-1;
		cRtRight.left=rt.right-1;
		cRtLeft.right=rt.left+1;
		InvalidateRect(&cRtTop);
		InvalidateRect(&cRtBottom);
		InvalidateRect(&cRtRight);
		InvalidateRect(&cRtLeft);
	}
}
