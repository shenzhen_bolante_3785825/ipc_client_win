

/* this ALWAYS GENERATED file contains the IIDs and CLSIDs */

/* link this file in with the server and any clients */


 /* File created by MIDL compiler version 6.00.0366 */
/* at Wed Jul 17 15:29:27 2013
 */
/* Compiler settings for .\OSClientCtrl.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


#ifdef __cplusplus
extern "C"{
#endif 


#include <rpc.h>
#include <rpcndr.h>

#ifdef _MIDL_USE_GUIDDEF_

#ifndef INITGUID
#define INITGUID
#include <guiddef.h>
#undef INITGUID
#else
#include <guiddef.h>
#endif

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        DEFINE_GUID(name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8)

#else // !_MIDL_USE_GUIDDEF_

#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        const type name = {l,w1,w2,{b1,b2,b3,b4,b5,b6,b7,b8}}

#endif !_MIDL_USE_GUIDDEF_

MIDL_DEFINE_GUID(IID, LIBID_OSClientCtrlLib,0xE4AD9CF7,0x4E06,0x44CC,0x88,0x34,0xB4,0x5F,0x7C,0x24,0xA8,0xD7);


MIDL_DEFINE_GUID(IID, DIID__DOSClientCtrl,0xA8D053A1,0x6B0A,0x4513,0xB2,0x7D,0x14,0x43,0xDA,0xBC,0xD6,0x6D);


MIDL_DEFINE_GUID(IID, DIID__DOSClientCtrlEvents,0x5C774740,0x557F,0x4BE3,0x99,0xA0,0x3E,0xD1,0x98,0xCB,0xEB,0x65);


MIDL_DEFINE_GUID(CLSID, CLSID_OSClientCtrl,0x3493FDCE,0x95FE,0x43F3,0xAB,0xAD,0x2B,0x88,0xAC,0x37,0xF2,0x00);

#undef MIDL_DEFINE_GUID

#ifdef __cplusplus
}
#endif



