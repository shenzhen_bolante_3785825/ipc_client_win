#pragma once

#include "OSClientVideoDlg.h"
// OSClientCtrlCtrl.h : COSClientCtrlCtrl ActiveX 控件类的声明。


// COSClientCtrlCtrl : 有关实现的信息，请参阅 OSClientCtrlCtrl.cpp。

class COSClientCtrlCtrl : public COleControl
{
	DECLARE_DYNCREATE(COSClientCtrlCtrl)

// 构造函数
public:
	COSClientCtrlCtrl();

// 重写
public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();
private:
	OSClientVideoDlg m_ClientVideoDlg;
// 实现
protected:
	~COSClientCtrlCtrl();

	BEGIN_OLEFACTORY(COSClientCtrlCtrl)        // 类工厂和 guid
		virtual BOOL VerifyUserLicense();
		virtual BOOL GetLicenseKey(DWORD, BSTR FAR*);
	END_OLEFACTORY(COSClientCtrlCtrl)

	DECLARE_OLETYPELIB(COSClientCtrlCtrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(COSClientCtrlCtrl)     // 属性页 ID
	DECLARE_OLECTLTYPE(COSClientCtrlCtrl)		// 类型名称和杂项状态

// 消息映射
	DECLARE_MESSAGE_MAP()

// 调度映射
	DECLARE_DISPATCH_MAP()

// 事件映射
	DECLARE_EVENT_MAP()

// 调度和事件 ID
public:
	enum {
		dispidP2P_ControlPTZ = 4L,
		dispidP2P_StopRealStream = 3L,
		dispidP2P_PlayRealStream = 2L,
		dispidP2P_SetVideoScreen = 1L,
	};
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
protected:
	void P2P_SetVideoScreen(LONG inColNum, LONG inRowNum);
	LONG P2P_PlayRealStream(LPCTSTR inDeviceUID, LPCTSTR inDeviceUser, LPCTSTR inDevicePwd);
	LONG P2P_StopRealStream(LPCTSTR inDeviceUID);
	LONG P2P_ControlPTZ(LPCTSTR inDeviceUID, LONG inControlCMD);
};

