// ScreenPannel.cpp : implementation file
//

#include "stdafx.h"
#include "ScreenPannel.h"
#include "Resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CScreenPannel class

CScreenPannel::CScreenPannel()
{
	m_bVideoFlag = FALSE;
}

CScreenPannel::~CScreenPannel()
{
	// remove all pages
	while(!m_PageList.IsEmpty())
		m_PageList.RemoveHead();
}


BEGIN_MESSAGE_MAP(CScreenPannel, CWnd)
	//{{AFX_MSG_MAP(CScreenPannel)
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
	ON_WM_CREATE()
	ON_WM_MOUSEWHEEL()
//	ON_WM_LBUTTONDOWN()
//	ON_WM_LBUTTONUP()
	//}}AFX_MSG_MAP
	ON_WM_LBUTTONDBLCLK()
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CScreenPannel member functions
void CScreenPannel::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	// TODO: Add your message handler code here
	UpdateWnd();
	// Do not call CWnd::OnPaint() for painting messages
}

BOOL CScreenPannel::OnEraseBkgnd(CDC* pDC) 
{
	// TODO: Add your message handler code here and/or call default
	CRect rt;
	GetClientRect(&rt);
	CBrush br;
//	br.CreateSolidBrush(WND_BACK_COLOR);//frank
	CBitmap bmp;
	if ( bmp.LoadBitmap(IDB_SCRENN_BG) )
	{
		br.CreatePatternBrush(&bmp);
	}
	else
	{
		br.CreateSolidBrush(BLACK);
	}
	pDC->FillRect(&rt, &br);

	return CWnd::OnEraseBkgnd(pDC);
}

int CScreenPannel::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	for(int i = 0; i < OS_MAX_PORT; i++)
	{
		m_wndVideo[i].Create(
			NULL, 
			NULL, 
			WS_VISIBLE | WS_CHILD |WS_CLIPCHILDREN |WS_CLIPSIBLINGS,
			CRect(0, 0, 0, 0),
			this,
			1979,
			NULL);

		m_wndVideo[i].SetWinID(i);
		
		AddPage(&m_wndVideo[i]);
	}

	SetActivePage(&m_wndVideo[0], TRUE);
	SetDrawActivePage(TRUE);

	return 0;
}

int CScreenPannel::SetShowPlayWin(int nMain, int nSub, BOOL bSpecial)
{
	if (nSub < 0 || nSub > OS_MAX_PORT)
	{
		nSub = 0;
	}

	int nNum = 16;
	int nBegin = 0;
	
	m_splitType= nMain;
	m_bSpecial = bSpecial;
	nBegin = 0;	// Can use for page mode
	nNum = nMain;

	//Clear first, use for clear the select frame
	DrawActivePage(FALSE);

	POSITION pos = m_PageList.GetHeadPosition();
	while(pos != NULL)
	{
		CWnd* pWnd = m_PageList.GetNext(pos);
		if (pWnd)
		{
			pWnd->ShowWindow(SW_HIDE);
		}
	}

	m_PageList.RemoveAll();
	
	for(int i=nBegin; i < (nBegin+nNum); i++)
	{
        m_wndVideo[i].SetWinID(i);
		AddPage(&m_wndVideo[i], TRUE);
	}

	if(nMain != SPLIT1 )
	{
		SetActivePage(&m_wndVideo[nSub], TRUE);	
	}
	else
	{
		m_wndVideo[nSub].ShowWindow(SW_SHOW);
	}

	return m_PageList.GetCount();
}

void CScreenPannel::OnDestroy() 
{
	CWnd::OnDestroy();
	
	// TODO: Add your message handler code here
	for(int i=0; i < OS_MAX_PORT; i++)
	{
		m_wndVideo[i].DestroyWindow();
	}
}

CWnd * CScreenPannel::GetPage(int nIndex)
{
	if (nIndex >= 0 && nIndex < OS_MAX_PORT)
	{
		return &m_wndVideo[nIndex];
	}

	return NULL;
}

void CScreenPannel::UpdateOtherWnd(DWORD devNode, BOOL isOriClose)
{
	BOOL ret;
	SplitMonitorParam *mparam;
	SplitInfoNode siNode;

	POSITION pos = m_PageList.GetHeadPosition();
	while(pos != NULL)
	{
		CPlayWnd* pWnd = (CPlayWnd*)m_PageList.GetNext(pos);
		if (pWnd)
		{			
			memset(&siNode, 0, sizeof(siNode));
			ret = pWnd->GetSplitInfo(&siNode);
			if(siNode.Type == SPLIT_TYPE_MONITOR)
			{
				mparam = &siNode.Param;
				if(mparam->dwTreeNode != devNode)
				{
					ret = pWnd->SetSplitInfo(&siNode);
				}
			}
		}
	}
}

//使用鼠标控制 PTZ的远近
BOOL CScreenPannel::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt) 
{

 	return CWnd::OnMouseWheel(nFlags, zDelta, pt);
}

CPlayWnd * CScreenPannel::GetPlayWnd( int nIndex )
{
	CPlayWnd *plWnd = NULL;
	if (nIndex < 0 || nIndex > OS_MAX_PORT)
	{
		return NULL;
	}
	plWnd = (CPlayWnd*)GetPage(nIndex);
	return plWnd;
}

int CScreenPannel::GetCurScreen()
{
	int nCurScreen = ((CPlayWnd*)GetActivePage())->GetWinID();
	return nCurScreen;
}
