#pragma once

// OSClientCtrlPropPage.h : COSClientCtrlPropPage 属性页类的声明。


// COSClientCtrlPropPage : 有关实现的信息，请参阅 OSClientCtrlPropPage.cpp。

class COSClientCtrlPropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(COSClientCtrlPropPage)
	DECLARE_OLECREATE_EX(COSClientCtrlPropPage)

// 构造函数
public:
	COSClientCtrlPropPage();

// 对话框数据
	enum { IDD = IDD_PROPPAGE_OSCLIENTCTRL };

// 实现
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 消息映射
protected:
	DECLARE_MESSAGE_MAP()
};

