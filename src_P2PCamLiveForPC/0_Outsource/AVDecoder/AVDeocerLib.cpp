#include "AVDecoderLib.h"
#include "AVInterface.h"

AVDECODE_API LONG InitialPlayer(HWND hWnd)
{
	AVInterface *fAVInterface = new AVInterface();
	if(fAVInterface)
		fAVInterface->InitialPlayer(hWnd);
	return (LONG)fAVInterface;
}

AVDECODE_API BOOL UninitalPlayer(LONG lHandler)
{
	BOOL bResult = FALSE;
	AVInterface *fAVInterface = (AVInterface*)(lHandler);
	if(fAVInterface)
	{
		bResult = fAVInterface->UninitalPlayer();
	}
	return bResult;
}

AVDECODE_API BOOL AddFrameData(LONG lHandler, BYTE* buff, int nSize, void *pFrmInfo)
{
	BOOL bResult = FALSE;
	AVInterface *fAVInterface = (AVInterface*)(lHandler);
	if(fAVInterface)
	{
		bResult = fAVInterface->addData(buff, nSize, pFrmInfo);
	}
	return bResult;
}

AVDECODE_API BOOL UpdateOSDData(LONG lHandler, const TCHAR* buff, int nSize)
{
	BOOL bResult = FALSE;
	AVInterface *fAVInterface = (AVInterface*)(lHandler);
	if(fAVInterface)
	{
		bResult = fAVInterface->UpdateOSDData(buff, nSize);
	}
	return bResult;
}

AVDECODE_API BOOL SaveBitmap(LONG lHandler, const TCHAR *inFilePath)
{
	BOOL bResult = FALSE;
	AVInterface *fAVInterface = (AVInterface*)(lHandler);
	if(fAVInterface)
	{
		bResult = fAVInterface->SaveBitmap(inFilePath);
	}
	return bResult;
}