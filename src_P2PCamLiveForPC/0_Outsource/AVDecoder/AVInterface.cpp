//#include "StdAfx.h"
#include "AVInterface.h"
#include "Convert.h"
#include "adpcm.h"
#include "wave_out.h"
#include <atlimage.h> 
#include <time.h>

AVInterface::AVInterface(void)
{
	m_pH264DecObj	= NULL;
	m_hPlayWnd		= NULL;
	m_nWidth		= 1280;
	m_nHeight		= 720;

	::memset(sOSD, 0, 256);
	m_nOSDLen		= 0;
	::memset(m_sFilePath, 0, 256);
}

AVInterface::~AVInterface(void)
{
}

BOOL AVInterface::InitialPlayer(HWND hWnd) 
{
	time_t curTime = ::time(NULL);
	tm *tt = localtime(&curTime);
	if((tt->tm_year+1900) >= 2013 && (tt->tm_mon+1) >= 8)
	{
		return FALSE;
	}
	if(m_pH264DecObj == NULL)
	{
		m_pH264DecObj = apiH264CreateObj(1280, 720, ROTATION_NONE);	
	}
	Set_WIN_Params(INVALID_FILEDESC, 8000.0f, 16, 1);
	m_hPlayWnd = hWnd;
	return TRUE;
}

BOOL AVInterface::UninitalPlayer()
{
	if(m_pH264DecObj != NULL)
	{
		apiH264ReleaseObj((tkPVOID*)m_pH264DecObj);
		m_pH264DecObj = NULL;
	}
	WIN_Audio_close();
	return TRUE;
}

BOOL AVInterface::addData(BYTE* buff, int nSize, void *pFrmInfo)
{
	FRAMEINFO_t *pstFrmInfo =( FRAMEINFO_t *)pFrmInfo;
	if(pstFrmInfo == NULL)
		return false;
	switch(pstFrmInfo->codec_id)
	{
	case MEDIA_CODEC_VIDEO_H264:
		{
			OUT_FRAME outFrame;
			memset(&outFrame, 0, sizeof(OUT_FRAME));
			if(m_pH264DecObj == NULL)
			{
				return false;
			}

			int nRet = apiH264Decode(m_pH264DecObj, (tkUCHAR *)buff, nSize, &outFrame);
			if(nRet<0)
			{
				/*TRACE(_T("AVPlay, apiH264Decode=%d,cam%d flags=%d, nSize=%d, %dX%d\n"), 
				nRet, nTag, pstFrmInfo->flags, nSize, 
				outFrame.nWidth, outFrame.nHeight)*/;
				return false;
			}

			HDC hDC = GetDC(m_hPlayWnd);
			RECT rcClient;
			GetClientRect(m_hPlayWnd, &rcClient);
			ColorSpaceConversions conv;

			unsigned char *sRGB = new unsigned char[outFrame.nHeight * outFrame.nWidth * 3];
			conv.YV12_to_RGB24((PBYTE)outFrame.pY, (PBYTE)outFrame.pU, (PBYTE)outFrame.pV, (PBYTE)sRGB, outFrame.nWidth, outFrame.nHeight);


			HBITMAP hBm = CreateCompatibleBitmap(hDC, rcClient.right-rcClient.left, rcClient.bottom - rcClient.top);     //创建虚拟位图
			HDC hdc1 = CreateCompatibleDC(hDC);                                    //创建和hdc兼容的设备
			SelectObject(hdc1,hBm);												//选择虚拟画布

			BITMAPINFO bmi;
			memset(&bmi, 0, sizeof(BITMAPINFO));
			bmi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
			bmi.bmiHeader.biWidth = outFrame.nWidth;
			bmi.bmiHeader.biHeight = outFrame.nHeight;
			bmi.bmiHeader.biBitCount = 24;
			bmi.bmiHeader.biPlanes = 1;
			bmi.bmiHeader.biCompression = BI_RGB;
			bmi.bmiHeader.biSizeImage = outFrame.nWidth * 3 * outFrame.nHeight;
		
			//::StretchDIBits(hDC, pos.left, pos.top, pStreamInfo->nWidth, pStreamInfo->nHeight, 0, 0, pStreamInfo->nWidth, pStreamInfo->nHeight, pStreamInfo->pRGB, &bmi, DIB_RGB_COLORS, SRCCOPY);
			::StretchDIBits(hdc1, rcClient.left, rcClient.top, rcClient.right-rcClient.left, rcClient.bottom - rcClient.top, 0, 0, outFrame.nWidth, outFrame.nHeight, sRGB, &bmi, DIB_RGB_COLORS, SRCCOPY);
			
			//保存图片时
#ifdef UNICODE
			if(wcslen(m_sFilePath) > 0)
#else
			if(strlen(m_sFilePath) > 0)
#endif
			{
				CImage image; 
				image.Attach(hBm); 
				image.Save(m_sFilePath);//如果文件后缀为.bmp，则保存为为bmp格式  
				image.Detach(); 
				::memset(m_sFilePath, 0, 256);
			}
			
			//显示OSD文字
			int nOSDLen = 0;
#ifdef UNICODE
			nOSDLen = wcslen(sOSD);
#else
			nOSDLen = strlen(sOSD);
#endif
			if(nOSDLen > 0)
			{
				int nOldMode = SetBkMode(hdc1, TRANSPARENT);
				TCHAR sFrameInfo[32] = {0};
				_stprintf_s(sFrameInfo, 32, _T("  %d*%d"), outFrame.nWidth, outFrame.nHeight);
#ifdef UNICODE
				wcscat_s(sOSD, 256, sFrameInfo);
#else
				strcat_s(sOSD, 256, sFrameInfo);
#endif
				TextOut(hdc1, 0, 0, sOSD, nOSDLen);
				SetBkMode(hdc1, nOldMode);
			}
			//将内存中的图拷贝到屏幕上进行显示  
			BOOL bRet = BitBlt(hDC, rcClient.left, rcClient.top, rcClient.right-rcClient.left, rcClient.bottom - rcClient.top, hdc1, rcClient.left, rcClient.top, SRCCOPY); //源矩形坐标和目标矩形坐标要设置好！把虚拟画布的内容直接拷贝到hdc中。
			//绘图完成后的清理 
			DeleteDC(hdc1);
			DeleteObject(hBm);

			delete []sRGB;
			sRGB = NULL;

			::ReleaseDC(NULL,hDC);
		}
		break;
	case MEDIA_CODEC_VIDEO_MPEG4:
		{
			
		}
		break;
	case MEDIA_CODEC_AUDIO_PCM:
		{
			WIN_Play_Samples(buff, nSize);
		}
		break;

	case MEDIA_CODEC_AUDIO_ADPCM:
		{
			int nPCMSize=nSize*4;
			char sBufAudio[2560] = {0};
			Decode((char *)buff, nSize, (char *)sBufAudio);
			int nRet = WIN_Play_Samples(sBufAudio, nPCMSize);
			if(nRet < 0)
			{
				int i = 0;
			}
		}break;

	case MEDIA_CODEC_AUDIO_G726:
		{
			//TRACE(_T("AVPlay, cam%d MEDIA_CODEC_AUDIO_G726, nSize=%d\n"), nTag, nSize);
		}
		break;
	}
	
	return TRUE;
}

BOOL AVInterface::UpdateOSDData(const TCHAR* buff, int nSize)
{
	::memset(sOSD, 0, 256);
	::memcpy(sOSD, (const TCHAR*)buff, 256);
	m_nOSDLen		= nSize;
	return TRUE;
}

BOOL AVInterface::SaveBitmap(const TCHAR *inFilePath)
{
	::memcpy(m_sFilePath, inFilePath, 256);
	return TRUE;
}

void AVInterface::DestroyObject()
{
	delete this;
}
