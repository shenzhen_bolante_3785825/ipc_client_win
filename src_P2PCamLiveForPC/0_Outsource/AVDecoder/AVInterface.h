#pragma once
#include <windows.h>
#include <map>
using namespace std;

#include "AVFRAMEINFO.h"
#include "H264Decoder.h"

#ifdef HD_DECODE
#define DECODE_WIDTH		1280
#define DECODE_HEIGHT		720
#else
#define DECODE_WIDTH       432
#define DECODE_HEIGHT      240
#endif



class AVInterface
{
public:
	AVInterface(void);
public:
	~AVInterface(void);

private:
	//CD3DRender		*m_pReader;
	void			*m_pH264DecObj;
	HWND			m_hPlayWnd;
	int				m_nWidth;
	int				m_nHeight;
public:
	BOOL InitialPlayer(HWND hWnd) ;
	BOOL UninitalPlayer();
	BOOL addData(BYTE* buff, int nSize, void *pFrmInfo);
	BOOL UpdateOSDData(const TCHAR* buff, int nSize);
	BOOL SaveBitmap(const TCHAR *inFilePath);
	void DestroyObject();
private:
	TCHAR sOSD[256];
	int m_nOSDLen;
	TCHAR m_sFilePath[256];
};
