::{{win32------------------------------
del *.exp /s /q
del *.ilk /s /q
del *.pdb /s /q
del *.clw /s /q

del *.ncb  /s /q
del *.user /s /q
del *.suo  /A /s /q

del *.aps /s /q
del *.suo /s /q
del *.plg /s /q
del *.opt /s /q
del *.positions /s /q

del	Bin_P2PClient\\*.lib /s /q

del	AVDecoder\\Debug /s /q
rmdir	AVDecoder\\Debug /s /q

del	AVDecoder\\Debug_Unicode /s /q
rmdir	AVDecoder\\Debug_Unicode /s /q

del	AVDecoder\\Release /s /q
rmdir	AVDecoder\\Release /s /q


del	H264Decoder\\Debug /s /q
rmdir	H264Decoder\\Debug /s /q

del	H264Decoder\\Debug_Unicode /s /q
rmdir	H264Decoder\\Debug_Unicode /s /q

del	H264Decoder\\Release /s /q
rmdir	H264Decoder\\Release /s /q


del	MultiLanChineseS\\Debug /s /q
rmdir	MultiLanChineseS\\Debug /s /q

del	MultiLanChineseS\\Debug_Unicode /s /q
rmdir	MultiLanChineseS\\Debug_Unicode /s /q

del	MultiLanChineseS\\Release /s /q
rmdir	MultiLanChineseS\\Release /s /q


del	MultiLanChineseT\\Debug /s /q
rmdir	MultiLanChineseT\\Debug /s /q

del	MultiLanChineseT\\Debug_Unicode /s /q
rmdir	MultiLanChineseT\\Debug_Unicode /s /q

del	MultiLanChineseT\\Release /s /q
rmdir	MultiLanChineseT\\Release /s /q


del	MultiLanEnglish\\Debug /s /q
rmdir	MultiLanEnglish\\Debug /s /q

del	MultiLanEnglish\\Debug_Unicode /s /q
rmdir	MultiLanEnglish\\Debug_Unicode /s /q

del	MultiLanEnglish\\Release /s /q
rmdir	MultiLanEnglish\\Release /s /q


del	OSClientCtrl\\Debug /s /q
rmdir	OSClientCtrl\\Debug /s /q

del	OSClientCtrl\\Debug_Unicode /s /q
rmdir	OSClientCtrl\\Debug_Unicode /s /q

del	OSClientCtrl\\Release /s /q
rmdir	OSClientCtrl\\Release /s /q


del	P2PAVCore\\Debug /s /q
rmdir	P2PAVCore\\Debug /s /q

del	P2PAVCore\\Debug_Unicode /s /q
rmdir	P2PAVCore\\Debug_Unicode /s /q

del	P2PAVCore\\Release /s /q
rmdir	P2PAVCore\\Release /s /q


del	P2PClient\\Debug /s /q
rmdir	P2PClient\\Debug /s /q

del	P2PClient\\Debug_Unicode /s /q
rmdir	P2PClient\\Debug_Unicode /s /q

del	P2PClient\\Release /s /q
rmdir	P2PClient\\Release /s /q
::}}win32------------------------------
