
#include "core_block_fifo.h"

// copy buf to b(core_block_t)
int core_block_Alloc(core_block_t *b, const void *buf, unsigned int size)
{
	if(b==NULL) return 0; //return-----------------
	b->p_next   = NULL;

	//if size==0 ???? joshua
	if(size > 0){
		b->p_buffer = (char *)malloc(size);
		if(b->p_buffer==NULL) return 0; //return---
		memcpy(b->p_buffer, buf, size);
	}else b->p_buffer = NULL;

	b->i_buffer		= size;
	return 1;	//return---------------------------
}

void core_block_Release(core_block_t **p_block)
{
	if(p_block!=NULL || *p_block!=NULL){
		if((*p_block)->p_buffer!=NULL){
			free((*p_block)->p_buffer);
			(*p_block)->p_buffer=NULL;
			(*p_block)->i_buffer=0;			
		}
		(*p_block)->p_next=NULL;
		free(*p_block);
		*p_block=NULL;
	}
}


//==========================================================
//create and init a new fifo
core_block_fifo_t *core_block_FifoNew( void)
{
	core_block_fifo_t *p_fifo = (core_block_fifo_t *)malloc( sizeof( core_block_fifo_t));
	if( !p_fifo ) return NULL;
	memset(p_fifo, 0, sizeof(core_block_fifo_t));
	
 	core_mutex_init( p_fifo->lock );

	p_fifo->p_first = NULL;
	p_fifo->pp_last = &p_fifo->p_first;
	p_fifo->i_depth = p_fifo->i_size = 0;
	return p_fifo;
}

//destroy a fifo and free all blocks in it.
void core_block_FifoRelease(core_block_fifo_t **p_fifo)
{
	if(p_fifo==NULL || *p_fifo==NULL) return;

	core_block_FifoEmpty( *p_fifo );
 	core_mutex_destroy( (*p_fifo)->lock);
	free(*p_fifo);
	*p_fifo=NULL;
}

//free all blocks in a fifo
void core_block_FifoEmpty( core_block_fifo_t *p_fifo)
{
	core_block_t *block;
	if(p_fifo==NULL) return;

	core_mutex_lock(p_fifo->lock);

	block = p_fifo->p_first;
	if (block != NULL)
	{
		p_fifo->i_depth = p_fifo->i_size = 0;
		p_fifo->p_first = NULL;
		p_fifo->pp_last = &p_fifo->p_first;
	}
 	core_mutex_unlock(p_fifo->lock );
	while(block != NULL)
	{
		core_block_t *buf;

		buf = block->p_next;
		core_block_Release(&block); //fixfix
		block = buf;
	}	
}

/**
* Immediately queue one block at the end of a FIFO.
* @param fifo queue
* @param block head of a block list to queue (may be NULL)
* @return total number of bytes appended to the queue
*/
unsigned int core_block_FifoPut( core_block_fifo_t *p_fifo, core_block_t *p_block)
{
	unsigned int i_size = 0, i_depth = 0;
	core_block_t *p_last;
	if(p_fifo==NULL || p_block == NULL) return 0;

	core_mutex_lock (p_fifo->lock);
		for(p_last = p_block; ; p_last = p_last->p_next)
		{
			i_size += p_last->i_buffer;
			i_depth++;
			if(!p_last->p_next) break;
		}

		*p_fifo->pp_last = p_block;
		p_fifo->pp_last  = &p_last->p_next;
		p_fifo->i_depth  += i_depth;
		p_fifo->i_size += i_size;
	core_mutex_unlock( p_fifo->lock );

	return i_size;
}

/**
* Dequeue the first block from the FIFO. If necessary, wait until there is
* one block in the queue. This function is (always) cancellation point.
*
* @return a valid block, or NULL if block_FifoWake() was called.
*/
core_block_t *core_block_FifoGet( core_block_fifo_t *p_fifo)
{
	core_block_t *b;
	if(p_fifo==NULL) return NULL;

 	core_mutex_lock( p_fifo->lock );
		b = p_fifo->p_first;

		if( b == NULL ){
			core_mutex_unlock( p_fifo->lock );
			return NULL;
		}

		p_fifo->p_first = b->p_next;
		p_fifo->i_depth--;
		p_fifo->i_size -= b->i_buffer;

		if( p_fifo->p_first == NULL ) p_fifo->pp_last = &p_fifo->p_first;
 	core_mutex_unlock( p_fifo->lock );
	b->p_next = NULL;

	return b;
}

//FIXME: not thread-safe
unsigned int core_block_FifoSize( const core_block_fifo_t *p_fifo )
{
	if(p_fifo==NULL) return 0;
	else return p_fifo->i_size;
}

//FIXME: not thread-safe
unsigned int core_block_FifoCount( const core_block_fifo_t *p_fifo)
{
	if(p_fifo==NULL) return 0;
	else return p_fifo->i_depth;
}


