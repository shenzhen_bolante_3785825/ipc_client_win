
#include "stdafx.h"
#include "P2PAVCoreType.h"
#include "IOTCCore.h"

BOOL APIENTRY DllMain( HMODULE hModule,
                       tkULONG  ul_reason_for_call,
                       tkPVOID lpReserved
					 )
{
	switch(ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
			InitializeCriticalSection(&gLockCommon);
			InitializeCriticalSection(&gLockLog);
			break;

		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
			break;

		case DLL_PROCESS_DETACH:
			DeleteCriticalSection(&gLockCommon);
			DeleteCriticalSection(&gLockLog);
			break;
	}
	return TRUE;
}

