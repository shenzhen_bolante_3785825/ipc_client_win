#pragma once

#include "P2PAVCore.h"
#include "core_block_fifo.h"
#include "IOTCAPIs.h"
#include <mmsystem.h>

extern tkUINT32	gInit;
extern CRITICAL_SECTION gLockCommon;
extern CRITICAL_SECTION gLockLog;

typedef struct{
	tkCHAR mszUID[24];
	tkCHAR mszUser[24];
	tkCHAR mszPwd[24];
	
	cbOnAVFrame mCBAVFrame;
	tkPVOID mCBAVFrameParam;
	cbOnStatus mCBStatus;
	tkPVOID mCBStatusParam;
	cbOnRecvIOCtrl mCBRecvIOCtrl;
	tkPVOID mCBRecvIOCtrlParam;

	tkUINT32 mnTag;
	tkINT32  mnSID;
	tkINT32  mnAVChannel;
	tkULONG  mnServType;
	
	tkINT32 mnAVChannelSpeak;
	tkINT32 mnIOTCChIdSpeak;
	tkINT32 nCodeID_audioSpeak;
	tkUCHAR mOnlineNum;
	tkUCHAR mConnMode; //0xFF: unknow mode
	tkUCHAR reserve[2];

	struct st_SInfo *mpSInfo;
	tkFLOAT mfFrameRate;
	tkFLOAT mfByteRate;
	tkINT32 mFrameTotal;
	tkINT32 mFrameLoseIncomplete;
}tkConnInfo;

typedef struct{
	tkINT32 nDataSize;
	tkVOID  *pData;
}tkSpeakDataInfo;

typedef struct {
	tkINT32 nChannel;
	tkINT32 nIOType;
	tkINT32 nIODataSize;
	tkCHAR  ioData[1024];
}ST_QUEUE_IOCTRL;

class CIOTCCore
{
public:
	CIOTCCore(tkVOID);
	virtual ~CIOTCCore(tkVOID);

	static tkULONG WINAPI ThreadConnectDev(tkPVOID lpPara);
			tkINT32 DoConnectDev();
	static tkULONG WINAPI ThreadRecvIOCtrl(tkPVOID lpPara);
			tkINT32 DoRecvIOCtrl();
	static tkULONG WINAPI ThreadRecvVideo(tkPVOID lpPara);
			tkINT32 DoRecvVideo();
	static tkULONG WINAPI ThreadRecvAudio(tkPVOID lpPara);
			tkINT32 DoRecvAudio();
	static tkULONG WINAPI ThreadSampleAudio(tkPVOID lpPara);
			tkINT32 DoSampleAudio();
	static tkULONG WINAPI ThreadSendAudio(tkPVOID lpPara);
			tkINT32 DoSendAudio();
	static tkULONG WINAPI ThreadIdle(tkPVOID lpPara);
			tkINT32 DoIdle();
	
	static tkVOID WINAPI GetIOTCVer(tkUINT32 *pArrayVer, tkUINT32 nArrMaxSize);
	static tkINT32	Init();
	static tkVOID	Deinit();
	tkVOID  SetConnInfo(tkUINT32 nTag, tkCHAR *szUID, tkCHAR *szUser, tkCHAR *szPwd);
	tkVOID	Connect();
	tkINT32	Disconnect(tkCHAR bForceStop);

	tkVOID RegCB_OnAVFrame(cbOnAVFrame cbFun,tkPVOID lParam);
	tkVOID RegCB_OnStatus(cbOnStatus cbFun,	tkPVOID lParam);
	tkVOID RegCB_OnRecvIOCtrl(cbOnRecvIOCtrl cbFun,tkPVOID lParam);

	tkINT32	StartVideo();
	tkVOID	StopVideo();
	tkINT32	StartAudio();
	tkVOID	StopAudio();
	tkINT32	StartSpeak(tkINT32 nCodeID_audio);
	tkVOID	StopSpeak();
	tkVOID  SendSpeakData(tkCHAR *pAudioData, tkINT32 nDataSize);
	tkINT32 InitWIEnv();

	tkINT32	SendIOCtrl(tkINT32 nIOType, tkUCHAR *pIOCtrlData, tkUINT32 nDataSize);
	tkVOID  AppendIOCtrl(tkINT32 nIOType, tkCHAR *pIOData, tkINT32 nIODataSize);
	tkVOID  AppendIOCtrl(ST_QUEUE_IOCTRL *pQueueSendIO);

	tkCHAR* GetUID(){
		if(m_pstConnInfo) return m_pstConnInfo->mszUID;
		else return NULL;
	}
	tkVOID	GetObjInfo(tkUINT32 nGOIType, tkVOID *out_pObj){
		switch(nGOIType){
			case GOI_OnlineNum:		//out_pObj is tkUCHAR *
				if(out_pObj && m_pstConnInfo){
					tkUCHAR *pOut=(tkUCHAR *)out_pObj;
					*pOut=m_pstConnInfo->mOnlineNum;
				}
				break;

			case GOI_ConnectMode:	//out_pObj is tkUCHAR *
				if(out_pObj && m_pstConnInfo){
					tkUCHAR *pOut=(tkUCHAR *)out_pObj;
					*pOut=m_pstConnInfo->mConnMode;
				}
				break;

			case GOI_FrameRate:		//out_pObj is tkFLOAT *
				if(out_pObj && m_pstConnInfo){
					tkFLOAT *pOut=(tkFLOAT *)out_pObj;
					*pOut=m_pstConnInfo->mfFrameRate;
				}
				break;

			case GOI_BytesRate:		//out_pObj is tkFLOAT *
				if(out_pObj && m_pstConnInfo){
					tkFLOAT *pOut=(tkFLOAT *)out_pObj;
					*pOut=m_pstConnInfo->mfByteRate;
				}
				break;

			case GOI_SessionInfo:
				if(out_pObj && m_pstConnInfo){
					CORE_STSInfo *pOut=(CORE_STSInfo *)out_pObj;
					int nSize=sizeof(struct st_SInfo);
					nSize=(nSize>sizeof(CORE_STSInfo)) ? sizeof(CORE_STSInfo) : nSize;
					memcpy(pOut,m_pstConnInfo->mpSInfo ,nSize);
				}
				break;

			case GOI_FrameTotal: //*out_pObj is tkINT32 *
				if(out_pObj && m_pstConnInfo){
					tkINT32 *pOut=(tkINT32 *)out_pObj;
					*pOut=m_pstConnInfo->mFrameTotal;
				}
				break;

			case GIO_FrameLoseIncomplete: //*out_pObj is tkINT32 *
				if(out_pObj && m_pstConnInfo){
					tkINT32 *pOut=(tkINT32 *)out_pObj;
					*pOut=m_pstConnInfo->mFrameLoseIncomplete;
				}
				break;

			default:;
		}
	}

private:
	tkVOID OnStatus(tkINT32 nType,tkINT32 nValue);

private:
	tkConnInfo *m_pstConnInfo;
	tkPVOID  m_hConnectDev, m_hRecvIOCtrl, m_hRecvVideo, m_hRecvAudio, m_hSampleAudio, m_hSendAudio;
	volatile tkCHAR m_bRecvingIOCtrl, m_bRecvingVideo, m_bRecvingAudio, m_bSamplingAudio, m_bSendingAudio;
	tkPVOID  m_hIdle;
	volatile tkCHAR m_bIdleRunning;
	core_block_fifo_t *m_fifoHandleIOCtrl;
	tkINT32  m_nCollect_interval;

	tkUINT32 m_nBytesCountAudio;
	
	HWAVEIN	 m_hWI;
	volatile tkCHAR m_bAudioInOpen;
	DWORD    m_threadIDSampleAudio;
	tkUCHAR  *m_pBufAudioEncoded;
	core_block_fifo_t *m_fifoSpeakData;

};
