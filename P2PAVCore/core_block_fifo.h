
#ifndef _CORE_BLOCK_FIFO_H_
#define _CORE_BLOCK_FIFO_H_

#ifdef WIN32
	#include <windows.h>

	typedef CRITICAL_SECTION core_mutex_t;
	#define core_mutex_init(theMutex)		InitializeCriticalSection(&theMutex)
	#define core_mutex_lock(theMutex)		EnterCriticalSection(&theMutex)
	#define core_mutex_unlock(theMutex)		LeaveCriticalSection(&theMutex)
	#define core_mutex_destroy(theMutex)	DeleteCriticalSection(&theMutex)

#endif

#ifdef LINUX
	#include <sys/types.h>
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <arpa/inet.h>
	#include <stdio.h>
	#include <unistd.h>

	typedef OS_MUTEX_T					  core_mutex_t;
	#define core_mutex_init(theMutex)	  theMutex.mutex_id=0;os_create_mutex(&theMutex, NULL, OS_FIFO)
	#define core_mutex_lock(theMutex)    os_obtain_mutex(&theMutex)
	#define core_mutex_unlock(theMutex)  os_release_mutex(&theMutex)
	#define core_mutex_destroy(theMutex) os_delete_mutex(&theMutex)
#endif

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

typedef struct _core_block
{
	struct _core_block *p_next;

	char		 reserve[4];
	unsigned int i_buffer;	
	char		 *p_buffer;
}core_block_t;

//@desc: copy buf to b(core_block_t *), b be malloced first, cann't be NULL
//@para  0: fail
//		>0: success
int  core_block_Alloc(core_block_t *b, const void *buf, unsigned int size);
void core_block_Release(core_block_t **p_block);


struct _core_block_fifo
{
	core_mutex_t  lock; // fifo data lock
	core_block_t  *p_first;
	core_block_t  **pp_last;

	unsigned int   i_depth;
	unsigned int   i_size;
};
typedef struct _core_block_fifo	core_block_fifo_t;

core_block_fifo_t  *core_block_FifoNew( void);
void          core_block_FifoRelease(core_block_fifo_t **p_fifo);
void          core_block_FifoEmpty(core_block_fifo_t *p_fifo);
unsigned int  core_block_FifoPut(core_block_fifo_t *p_fifo, core_block_t *p_block);
core_block_t  *core_block_FifoGet( core_block_fifo_t *p_fifo);
unsigned int  core_block_FifoSize( const core_block_fifo_t *p_fifo );
unsigned int  core_block_FifoCount( const core_block_fifo_t *p_fifo);

#ifdef __cplusplus
}
#endif //__cplusplus

#endif
