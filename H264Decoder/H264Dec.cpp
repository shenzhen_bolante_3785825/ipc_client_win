#include "StdAfx.h"
#include "H264Dec.h"

#pragma comment(lib, "hi_h264dec_w.lib")
#pragma comment(lib, "DllDeinterlace.lib")


extern LPCRITICAL_SECTION     g_LockH264;

CH264Dec::CH264Dec(tkUINT32 nMaxWidth, tkUINT32 nMaxHeight, ROTATIONTYPE eRotType)
{
	m_h264DecHandle = m_hDeinterlace = NULL;

	memset(&m_outframe, 0, sizeof(m_outframe));
	m_pYUV = NULL;
	m_iRotation = eRotType;
	m_sizePrev.cx = m_sizePrev.cy = 0;

	memset(&m_h264DecAttr, 0, sizeof(m_h264DecAttr));
	m_h264DecAttr.uPictureFormat = 0;	  //YUV420
	m_h264DecAttr.uBufNum        = 8;     // reference frames number: 16
	m_h264DecAttr.uPicWidthInMB  = nMaxWidth>>4;	//1280
	m_h264DecAttr.uPicHeightInMB = nMaxHeight>>4;	//720
	m_h264DecAttr.uStreamInType  = 0x00;	// bitstream begin with "00 00 01" or "00 00 00 01"
											//bit0 = 1: H.264 normal output mode; bit0 = 0: direct output mode
	//bit4 = 1:  enable deinteralce;bit4 = 0: disable deinterlace; 
	m_h264DecAttr.uWorkMode |= 0x10;

	EnterCriticalSection(g_LockH264);
	m_h264DecHandle = Hi264DecCreate(&m_h264DecAttr);
	LeaveCriticalSection(g_LockH264);
}

CH264Dec::~CH264Dec(void)
{
	if(m_h264DecHandle){
		EnterCriticalSection(g_LockH264);
		Hi264DecDestroy(m_h264DecHandle);
		LeaveCriticalSection(g_LockH264);
	}
	if(m_pYUV) FreeAlign16(m_pYUV);
	if(m_hDeinterlace) HI_ReleaseDeinterlace(m_hDeinterlace);
}

tkINT32 CH264Dec::Decode(BYTE *pInData, UINT nInDataSize, OUT_FRAME *pOutDec)
{
	if(m_h264DecHandle==NULL) return -2;
	int	nRet=0, nDec=-1;
	nDec = Hi264DecFrame(m_h264DecHandle, pInData,  nInDataSize, 0, &m_outframe,  nInDataSize>0?0:1);
	nRet=nDec;
	while(nDec != HI_H264DEC_NEED_MORE_BITS)
	{
		if(HI_H264DEC_NO_PICTURE == nDec) break;

		//a frame can be displayed
		if ( nDec != HI_H264DEC_OK ) return -3;

		if( (m_outframe.pU && m_outframe.pV))
		{
			if(m_outframe.uPicFlag == PIC_PROGRESSIVE) //progress
			{
				if(m_sizePrev.cx != m_outframe.uWidth || m_sizePrev.cy != m_outframe.uHeight)
				{
					if(m_pYUV){
						FreeAlign16(m_pYUV);
						m_pYUV = NULL;
					}
				}
				if(!m_pYUV)
				{
					m_pYUV = (unsigned char *)MallocAlign16(m_outframe.uHeight * m_outframe.uWidth * 1.5);
					m_struDeinFrame.pszY = m_pYUV;
					m_struDeinFrame.pszU = m_pYUV + m_outframe.uWidth * m_outframe.uHeight;
					m_struDeinFrame.pszV = m_struDeinFrame.pszU + m_outframe.uWidth * m_outframe.uHeight / 4;
				}

				CopyYUV(m_pYUV, &m_outframe);
				if(pOutDec)
				{
					pOutDec->pY = m_struDeinFrame.pszY;
					pOutDec->pU = m_struDeinFrame.pszU;
					pOutDec->pV = m_struDeinFrame.pszV;
					pOutDec->nWidth		= m_outframe.uWidth;
					pOutDec->nHeight	= m_outframe.uHeight;
					pOutDec->nYStride	= m_outframe.uWidth;
					pOutDec->nUVStride	= m_outframe.uWidth/2;

					priTransform(pOutDec);
				}
			}else{
				if(m_sizePrev.cx != m_outframe.uWidth || m_sizePrev.cy != m_outframe.uHeight)
				{
					if(m_pYUV){
						FreeAlign16(m_pYUV);
						m_pYUV = NULL;
					}
					if(m_hDeinterlace){
						HI_ReleaseDeinterlace(m_hDeinterlace);
						m_hDeinterlace = NULL;
					}
				}

				if(!m_pYUV)
				{
					m_pYUV = (unsigned char *)MallocAlign16(m_outframe.uHeight * m_outframe.uWidth * 3);
					m_struDeinFrame.pszY = m_pYUV;
					m_struDeinFrame.pszU = m_pYUV + m_outframe.uWidth * m_outframe.uHeight * 2;
					m_struDeinFrame.pszV = m_struDeinFrame.pszU + m_outframe.uWidth * m_outframe.uHeight / 2;
				}
				if(!m_hDeinterlace)
				{
					m_struDeinPara.iFieldWidth = m_outframe.uWidth;
					m_struDeinPara.iFieldHeight = m_outframe.uHeight;
					m_struDeinPara.iSrcYPitch = m_outframe.uYStride;
					m_struDeinPara.iSrcUVPitch = m_outframe.uUVStride;
					m_struDeinPara.iDstYPitch = m_outframe.uWidth;
					m_struDeinPara.iDstUVPitch = m_outframe.uWidth/2;
					HI_InitDeinterlace(&m_hDeinterlace, m_struDeinPara);
				}

				HI_Deinterlace(m_hDeinterlace, m_struDeinFrame, m_outframe.pY, m_outframe.pU, m_outframe.pV, (PIC_TYPE_E)m_outframe.uPicFlag);
				if(m_outframe.uPicFlag == PIC_INTERLACED_EVEN && pOutDec) //even field
				{
					pOutDec->pY = m_struDeinFrame.pszY;
					pOutDec->pU = m_struDeinFrame.pszU;
					pOutDec->pV = m_struDeinFrame.pszV;
					pOutDec->nWidth		= m_outframe.uWidth;
					pOutDec->nHeight	= 2*m_outframe.uHeight;
					pOutDec->nYStride	= m_outframe.uWidth;
					pOutDec->nUVStride	= m_outframe.uWidth/2;

					priTransform(pOutDec);
				}
			}

			m_sizePrev.cx = m_outframe.uWidth;
			m_sizePrev.cy = m_outframe.uHeight;

			nDec = Hi264DecFrame(m_h264DecHandle, NULL, 0, 0, &m_outframe,  nInDataSize>0?0:1);
		}//End of display
	}

	return nRet;
}

void CH264Dec::priTransform(const OUT_FRAME *pVDecOut)
{
	switch(m_iRotation)
	{
		case ROTATION_VFLIP:
			VFlip(pVDecOut->pY, pVDecOut->nYStride, pVDecOut->nHeight);
			VFlip(pVDecOut->pU, pVDecOut->nUVStride,pVDecOut->nHeight/2);
			VFlip(pVDecOut->pV, pVDecOut->nUVStride,pVDecOut->nHeight/2);
			break;

		case ROTATION_HFLIP:
			HFlip(pVDecOut->pY, pVDecOut->nWidth, pVDecOut->nYStride, pVDecOut->nHeight);
			HFlip(pVDecOut->pU, pVDecOut->nWidth/2, pVDecOut->nUVStride, pVDecOut->nHeight/2);
			HFlip(pVDecOut->pV, pVDecOut->nWidth/2, pVDecOut->nUVStride, pVDecOut->nHeight/2);
			break;

		case ROTATION_TURNOVER:
			TurnOver(pVDecOut->pY, pVDecOut->nWidth, pVDecOut->nYStride, pVDecOut->nHeight);
			TurnOver(pVDecOut->pU, pVDecOut->nWidth/2, pVDecOut->nUVStride, pVDecOut->nHeight/2);
			TurnOver(pVDecOut->pV, pVDecOut->nWidth/2, pVDecOut->nUVStride, pVDecOut->nHeight/2);
			break;

		default:;
	}
}

void* CH264Dec::MallocAlign16(size_t size)
{
	unsigned char *rawblock = (unsigned char *)calloc(size + 0x10,1);
	if (rawblock == NULL) return NULL;
	else if ((int)rawblock & 0xF){
		unsigned char *p = (unsigned char *)(((int)rawblock + 0xF) & (~0xF));
		p[-1] = p - rawblock;
		return p;

	}else{
		unsigned char *p = rawblock + 0x10;
		p[-1] = p - rawblock;
		return p;
	}
}

void CH264Dec::FreeAlign16(void *memblock)
{
	if(memblock != NULL){
		free((unsigned char *)memblock - *((unsigned char *)memblock - 1));
	}
}

void CH264Dec::CopyYUV(unsigned char *pDst, hiH264_DEC_FRAME_S *pframe)
{
	int i;
	unsigned char* p;
	p = pframe->pY;
	for(i = 0; i < pframe->uHeight; i++){
		memcpy(pDst, p, pframe->uWidth);
		pDst += pframe->uWidth;
		p += pframe->uYStride;
	}

	p = pframe->pU;
	for(i = 0; i < pframe->uHeight / 2; i++){
		memcpy(pDst, p, (pframe->uWidth >> 1));
		pDst += (pframe->uWidth >> 1);
		p += pframe->uUVStride;
	}

	p = pframe->pV;
	for(i = 0; i < pframe->uHeight / 2; i++){
		memcpy(pDst, p, (pframe->uWidth >> 1));
		pDst += (pframe->uWidth >> 1);
		p += pframe->uUVStride;
	}
}

void CH264Dec::VFlip(BYTE *p, int iStride, int height)
{
	DWORD oldSI, oldDI;

	_asm {
		mov oldSI, esi
			mov oldDI, edi

			mov esi, p			//ESI -> first line

			mov eax, iStride
			mov ebx, height
			mul ebx
			add eax, esi
			sub eax, iStride
			mov edi, eax		//EDI -> last line

			mov ecx, height
			shr ecx, 1
do_vflip:
		mov edx, iStride
vflip_line:
		mov eax, dword ptr [esi]
		mov ebx, dword ptr[edi]
		mov dword ptr [esi], ebx
			mov dword ptr [edi], eax
			add esi, 4
			add edi, 4
			sub edx, 4
			jne vflip_line
			sub edi, iStride
			sub edi, iStride
			loop do_vflip

			mov esi, oldSI
			mov edi, oldDI
	}
}

void CH264Dec::HFlip(BYTE* p, int width, int iStride, int height)
{
	_asm {
		push esi
			push edi

			mov esi, p
			push esi
			mov ecx, height
flip:
		mov edi, esi
			add edi, width
			sub edi, 4
flip_line:
		mov eax, [esi]
		bswap eax
			mov ebx, [edi]
		bswap ebx
			mov [edi], eax
			mov [esi], ebx
			add esi, 4
			sub edi, 4
			cmp esi, edi
			jc flip_line

			pop esi
			add esi, iStride
			push esi
			dec ecx
			jnz flip

			pop esi		//garbage
			pop edi
			pop esi
	}
}

void CH264Dec::TurnOver(BYTE *p, int width, int iStride, int height)
{
	int inner_cnt;
	unsigned int src, tgt;

	_asm {
		push esi
			push edi

			mov esi, p		//ESI
			mov src, esi

			mov eax, iStride
			mov ebx, height
			dec ebx
			mul ebx
			add eax, width
			sub eax, 4
			mov edi, eax		
			add edi, esi	//EDI --> last dword of last line
			mov tgt, edi

			mov eax, height
			shr eax, 1		//outter loop count

			mov ecx, width
			shr ecx, 2		//inner loop count
			mov inner_cnt, ecx

outter_loop:
		mov ecx, inner_cnt
			push eax
inner_loop:
		mov eax, dword ptr [esi]	
		bswap eax

			mov ebx, dword ptr[edi]
		bswap ebx

			mov dword ptr [esi], ebx
			mov dword ptr [edi], eax
			add esi, 4
			sub edi, 4

			dec ecx
			jnz inner_loop

			pop eax
			dec eax
			cmp eax, 0
			jz clean

			mov esi, src
			add esi, iStride
			mov src, esi
			mov edi, tgt
			sub edi, iStride
			mov tgt, edi
			jmp outter_loop

clean:
		pop edi
			pop esi
	}
}
