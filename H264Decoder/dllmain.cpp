// dllmain.cpp : 定义 DLL 应用程序的入口点。
#include "stdafx.h"

LPCRITICAL_SECTION  g_LockH264=NULL;

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:{
			g_LockH264=new CRITICAL_SECTION;
			InitializeCriticalSection(g_LockH264);
			}break;

		case DLL_THREAD_ATTACH:
			break;

		case DLL_THREAD_DETACH:
			break;

		case DLL_PROCESS_DETACH:{
			DeleteCriticalSection(g_LockH264);
			delete g_LockH264;
			g_LockH264=NULL;
			}break;
		default:;
	}
	return TRUE;
}

