// H264Decoder.cpp
//

#include "stdafx.h"
#include "H264Decoder.h"
#include "H264Dec.h"


#define API_VER_H264_DECODER	0x01000000

H264DECODER_API tkUINT32 apiH264Version()
{
	return API_VER_H264_DECODER;
}

H264DECODER_API tkPVOID apiH264CreateObj(tkUINT32 nMaxWidth, tkUINT32 nMaxHeight, ROTATIONTYPE eRotType)
{
	CH264Dec *pObj=new CH264Dec(nMaxWidth, nMaxHeight, eRotType);
	if(pObj->GetH264Dec()==NULL) {
		delete pObj;
		pObj=NULL;
		return NULL;
	}else return pObj;
}

H264DECODER_API tkVOID	apiH264ReleaseObj(tkPVOID *ppObj)
{
	CH264Dec *pObj=(CH264Dec *)(*ppObj);
	if(pObj){
		delete pObj;
		pObj=NULL;
	}
}

H264DECODER_API tkINT32	apiH264Decode(tkPVOID pObj, tkUCHAR *pInData, tkUINT32 nInDataSize, OUT_FRAME *pOutDec)
{
	CH264Dec *pH264=(CH264Dec *)(pObj);
	if(pH264) return pH264->Decode(pInData, nInDataSize,pOutDec);
	else return -1;
}