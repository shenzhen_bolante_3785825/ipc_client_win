#pragma once

#include "P2PAVCoreType.h"
#include "H264Decoder.h"
#include "hi_h264api.h"
#include "DllDeinterlace.h"

class CH264Dec
{
public:
	CH264Dec(tkUINT32 nMaxWidth, tkUINT32 nMaxHeight, ROTATIONTYPE eRotType);
	~CH264Dec(void);
	HANDLE  GetH264Dec(){ return m_h264DecHandle; }
	tkINT32 Decode(BYTE *pInData, UINT nInDataSize, OUT_FRAME *pOutDec);

protected:
	static void *MallocAlign16(size_t size);
	static void FreeAlign16(void *memblock);
	static void VFlip(BYTE *p, int iStride, int height);
	static void HFlip(BYTE* p, int width, int iStride, int height);
	static void TurnOver(BYTE *p, int width, int iStride, int height);
	static void CopyYUV(unsigned char *pDst, hiH264_DEC_FRAME_S *pframe);
	void priTransform(const OUT_FRAME *pVDecOut);

private:
	ROTATIONTYPE		m_iRotation;

	UINT				m_uStreamInType;
	HANDLE				m_h264DecHandle;
	H264_DEC_ATTR_S		m_h264DecAttr;	//H264 decoder 's attribute
	H264_DEC_FRAME_S	m_outframe;		//output frame

	HANDLE				m_hDeinterlace;
	DEINTERLACE_PARA_S	m_struDeinPara;
	DEINTERLACE_FRAME_S	m_struDeinFrame;

	SIZE				m_sizePrev;
	unsigned char		*m_pYUV;		//frame 's YUV data
};
