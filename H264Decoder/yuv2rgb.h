
#ifndef _YUVRGB_H_
#define _YUVRGB_H_

#ifdef __cplusplus
extern "C" {
#endif

	bool YUV420toRGB24(unsigned char* yData, unsigned char* vData,unsigned char* uData,unsigned char* pRGB24,int nW,int nH);

#ifdef __cplusplus
}
#endif

#endif // _YUVRGB_H_
