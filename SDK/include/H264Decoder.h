
/*! \file H264Decoder.h


\copyright Copyright (c) 2010 by Throughtek Co., Ltd. All Rights Reserved.

Revision Table

Version     | Name             |Date           |Description
------------|------------------|---------------|-------------------
1.0.0.0     |Joshua            |2013-02-18     |Trial Version


*/

#ifndef _H264DECODER_H_
#define _H264DECODER_H_

#include "P2PAVCoreType.h"

#ifdef H264DECODER_EXPORTS
#define H264DECODER_API __declspec(dllexport)
#else
#define H264DECODER_API __declspec(dllimport)
#endif

typedef enum { ROTATION_NONE, ROTATION_HFLIP, ROTATION_VFLIP, ROTATION_TURNOVER } ROTATIONTYPE;

typedef struct _tagOUT_FRAME {
	tkUCHAR *pY;
	tkUCHAR *pU;
	tkUCHAR *pV;
	tkUINT32 nWidth;
	tkUINT32 nHeight;
	tkUINT32 nYStride;
	tkUINT32 nUVStride;
}OUT_FRAME;

#ifdef __cplusplus
extern "C" {
#endif

	H264DECODER_API tkUINT32 apiH264Version();
	H264DECODER_API tkPVOID  apiH264CreateObj(tkUINT32 nMaxWidth, UINT nMaxHeight, ROTATIONTYPE eRotType);
	H264DECODER_API tkVOID	 apiH264ReleaseObj(tkPVOID *ppObj);
	H264DECODER_API tkINT32	 apiH264Decode(tkPVOID pObj, tkUCHAR *pInData, tkUINT32 nInDataSize, OUT_FRAME *pOutDec);
	H264DECODER_API tkCHAR   YUV420toRGB24(unsigned char* yData, unsigned char* vData,unsigned char* uData,unsigned char* pRGB24,int nW,int nH);
#ifdef __cplusplus
}
#endif

#endif